package com.imentors.wealthdragons.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.Badges;

import java.util.List;

public class BadgesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Badges.Badge> mPaymentOrderList;
    private Context mContext;
    private OnListItemClicked mOnItemClicked;

    //  calling constructor
    public BadgesListAdapter(Context context, OnListItemClicked onListItemClicked) {
        mContext = context;
        mOnItemClicked = onListItemClicked;
    }


    // set new items
    public void setNewItems(List<Badges.Badge> paymentOrderList) {
        if (mPaymentOrderList != null) {
            mPaymentOrderList.clear();
        }
        mPaymentOrderList = paymentOrderList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.badges_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        MyViewHolder viewHolder = (MyViewHolder) holder;

        viewHolder.bind(mPaymentOrderList.get(position), mContext);

        viewHolder.getmFlViewBadgeButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClicked.onItemClicked(mPaymentOrderList.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return mPaymentOrderList.size();
    }


    /**
     * different click handled by onItemClick
     */
    //  interface
    public interface OnListItemClicked {
        void onItemClicked(Badges.Badge item);
    }

    //   * View holder class for items fo recycler view

    private static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mTitle;
        private ImageView mIvBadge;
        private FrameLayout mFlViewBadgeButton;


        //  initialize data
        public MyViewHolder(View view) {
            super(view);
            mTitle = view.findViewById(R.id.tv_badge_title);
            mIvBadge = view.findViewById(R.id.iv_badge);
            mFlViewBadgeButton = view.findViewById(R.id.fl_view_badge);
        }

        public FrameLayout getmFlViewBadgeButton() {
            return mFlViewBadgeButton;
        }


        // Set values to the list items
        void bind(final Badges.Badge item, Context context) {
            mTitle.setText(item.getCourse_title());
            Glide.with(context).load(item.getBadge()).into(mIvBadge);
        }

    }
}
