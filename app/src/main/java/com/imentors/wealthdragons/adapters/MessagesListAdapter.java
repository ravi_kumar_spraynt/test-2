package com.imentors.wealthdragons.adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.Messages;
import com.imentors.wealthdragons.utils.DateTimeUtils;
import com.imentors.wealthdragons.utils.Utils;


import java.util.List;

public class MessagesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    private List<Messages.Notification> messagesList;
    private onMessagesItemClicked mOnItemClickListener;
    private Context mContext;
    private static final int TYPE_NEW_MESSAGE = 1;
    private static final int TYPE_INNER_MESSAGE = 0;
    private static int mSelectedItemCount = -1;

    public MessagesListAdapter(List<Messages.Notification> messagesList, onMessagesItemClicked onItemClickListener , Context context) {
        this.messagesList = messagesList;
        mOnItemClickListener = onItemClickListener;
        mContext = context;
        mSelectedItemCount = -1;
    }


    /**
     * different click handled by onItemClick
     */
    public interface onMessagesItemClicked {
        void onMessagetemClick(Messages.Notification item , int position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {
            case TYPE_INNER_MESSAGE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.inner_messages_list_item, parent, false);
                return new MessagesListAdapter.InnerMessageViewHolder(view,mOnItemClickListener);


            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.messages_list_item, parent, false);
                return new MessagesListAdapter.MessageViewHolder(view,mOnItemClickListener);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        if (holder instanceof MessagesListAdapter.MessageViewHolder) {
            //cast holder to ItemViewHolder and set data for header.
            MessagesListAdapter.MessageViewHolder viewHolder = (MessagesListAdapter.MessageViewHolder) holder;
            viewHolder.bind(messagesList.get(position), position,mContext);

        } else {
            //cast holder to FooterViewHolder and set data for header.
            InnerMessageViewHolder viewHolder = (InnerMessageViewHolder) holder;
            viewHolder.bind(messagesList.get(position),position,mContext);
        }

    }


    @Override
    public int getItemViewType(int position) {

        if(position>0){
        if (messagesList.get(position).getCreated().equals(messagesList.get(position-1).getCreated())) {
            return TYPE_INNER_MESSAGE;
        } else {
            return TYPE_NEW_MESSAGE;
        }
        }else{
            return TYPE_NEW_MESSAGE;
        }
    }


    @Override
    public int getItemCount() {
        return messagesList.size();
    }

    //   * View holder class for items fo recycler view
    private static class MessageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView mTvMessageTitle;
        public TextView mTvMessage;
        public TextView mTvMessageTime;
        public TextView mTvMessageDate;
        public LinearLayout mMessageHolder;
        private onMessagesItemClicked onItemClickListener;
        private Messages.Notification mItem;
        private int mPosition;


        //  initialize view
        public MessageViewHolder(View view , onMessagesItemClicked listener) {
            super(view);
            mMessageHolder = view.findViewById(R.id.message_container);
            mTvMessageTitle = view.findViewById(R.id.tv_message_title);
            mTvMessage = view.findViewById(R.id.tv_message);
            mTvMessageDate = view.findViewById(R.id.tv_message_date);
            mTvMessageTime = view.findViewById(R.id.tv_message_time);
            mMessageHolder.setOnClickListener(this);

            onItemClickListener = listener;
        }



        //  set data on view
        void bind(final Messages.Notification item, final int position , Context context){
            mItem= item;
            mPosition = position;
            if(DateTimeUtils.compareWithTodayDate(item.getDate())==0){
                mTvMessageTime.setText(DateTimeUtils.getTimeDifference(item.getDate()));
                mTvMessageDate.setText(context.getString(R.string.today));
            }else if(DateTimeUtils.getDaysDifference(item.getDate())==1){
                String test = Utils.getMessageTime(item.getDate());
                mTvMessageTime.setText(Utils.getMessageTime(item.getDate()));
                mTvMessageDate.setText(context.getString(R.string.yesterday));
            }else{
                mTvMessageTime.setText(Utils.getMessageTime(item.getDate()));
                mTvMessageDate.setText(Utils.getMessageDate(item.getDate()));

            }


            if(mPosition%2==0 && mSelectedItemCount!=mPosition){
                mMessageHolder.setBackgroundColor(ContextCompat.getColor(context,R.color.backgroundGray));
            }else{
                mMessageHolder.setBackgroundColor(ContextCompat.getColor(context,R.color.colorWhite));
            }

            mTvMessage.setText(item.getMessage());
            mTvMessageTitle.setText(item.getTitle());

            if(item.getSeen().equals(context.getString(R.string.unread))){
                mTvMessage.setTypeface(mTvMessage.getTypeface(), Typeface.BOLD);
                mTvMessageTitle.setTypeface(mTvMessageTitle.getTypeface(), Typeface.BOLD);
            }

        }


        @Override
        public void onClick(View v) {
            if(onItemClickListener != null){
                mSelectedItemCount = mPosition;
                onItemClickListener.onMessagetemClick(mItem,mPosition);
            }
        }
    }


    private static class InnerMessageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView mTvMessageTitle;
        public TextView mTvMessage;
        private onMessagesItemClicked onItemClickListener;
        private Messages.Notification mItem;
        public TextView mTvMessageTime;
        private int mPosition;
        public LinearLayout mMessageHolder;



        //  initialize view
        public InnerMessageViewHolder(View view , onMessagesItemClicked listener) {
            super(view);

            mMessageHolder = view.findViewById(R.id.message_container);
            mTvMessageTitle = view.findViewById(R.id.tv_inner_message_title);
            mTvMessage = view.findViewById(R.id.tv_inner_message);
            mTvMessageTime = view.findViewById(R.id.tv_message_time);
            view.setOnClickListener(this);
            onItemClickListener = listener;
        }


         //* Bind the the values of the view holder
        void bind(final Messages.Notification item, final int position , Context context){
            mItem= item;
            mPosition = position;
            if(DateTimeUtils.compareWithTodayDate(item.getDate())==0) {
                mTvMessageTime.setText(DateTimeUtils.getTimeDifference(item.getDate()));
            }else{
                mTvMessageTime.setText(Utils.getMessageTime(item.getDate()));
            }
            mTvMessage.setText(item.getMessage());
            mTvMessageTitle.setText(item.getTitle());

            if(mPosition%2==0 && mSelectedItemCount!=mPosition){
                mMessageHolder.setBackgroundColor(ContextCompat.getColor(context,R.color.backgroundGray));
            }else{
                mMessageHolder.setBackgroundColor(ContextCompat.getColor(context,R.color.colorWhite));
            }

            if(item.getSeen().equals(context.getString(R.string.unread))){
                mTvMessage.setTypeface(mTvMessage.getTypeface(), Typeface.BOLD);
                mTvMessageTitle.setTypeface(mTvMessageTitle.getTypeface(), Typeface.BOLD);
            }


        }



        @Override
        public void onClick(View v) {
            if(onItemClickListener != null){
                mSelectedItemCount = mPosition;
                onItemClickListener.onMessagetemClick(mItem , mPosition);

            }
        }
    }
}
