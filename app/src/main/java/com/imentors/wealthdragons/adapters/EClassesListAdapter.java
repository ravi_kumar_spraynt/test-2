package com.imentors.wealthdragons.adapters;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.DownloadFile;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.tonyodev.fetch2.AbstractFetchListener;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2.Status;
import com.tonyodev.fetch2core.Extras;
import com.tonyodev.fetch2core.Func;
import com.tonyodev.fetch2core.MutableExtras;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.facebook.FacebookSdk.getApplicationContext;

public class EClassesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Chapters.EClasses> mEClassesList;
    private OnEClassesClick mOnItemClickListener;
    private Context mContext;
    private int selectedRowIndex = -1;
    private int selectedAudio = -1;
    private static String selectedAudioId = null;
    private static boolean isNotifiedOnce = false;
    // created to stop mixing of video and audio player in multiple eclasses.
    private static boolean isAuidoActivated = false;
    // for identifying eclasses visibility
    private boolean  mIsProgramme;
    private boolean  mIsDownloaded;

    private String mCoursePrice;
    private String  mCourseId;
    private String  mItemType;
    private String  mCourseTitle;
    private String mBannerImageUrl;
    private String mBannerImagePath;
    private String mPunchLine;
    private String mMentorName;
    private DownloadFile mDownLoadTask;
    private Fetch mFetch;
    private List<Download> mDownLoadItemList;
    private RecyclerView mRecycleview;
    private ShowDownlaodIcon mShowDownloadIcon;
    private Chapters mChapter;
    private DashBoardCourseDetails mDashBaordCourseDetails;






    private HashMap<Integer,Boolean> expandingListData = new HashMap<>();


    public EClassesListAdapter(OnEClassesClick onItemClickListener, Context context, int rowSelectPos,  String coursePrice, String courseId, String itemType, String courseTitle, boolean isProgramme, String banner,
                               String punchline, String mentorName, RecyclerView recyclerView, ShowDownlaodIcon showDownlaodIcon, DashBoardCourseDetails dashBoardCourseDetails, boolean isDownloaded) {
        mOnItemClickListener = onItemClickListener;
        mContext = context;
        selectedRowIndex = rowSelectPos;
        selectedAudio = -1;
        mCoursePrice = coursePrice;
        mCourseId = courseId;
        mItemType = itemType;
        mCourseTitle = courseTitle;
        mIsProgramme = isProgramme;
        mBannerImageUrl=banner;
        mPunchLine=punchline;
        mMentorName=mentorName;
        mRecycleview=recyclerView;
        mFetch = Utils.getFetchInstance();
        mFetch.addListener(fetchListener);
        mShowDownloadIcon = showDownlaodIcon;
        mDashBaordCourseDetails = dashBoardCourseDetails;
        mIsDownloaded = isDownloaded;


    }

    private void showDonwloadIcon(final Chapters.EClasses items , final ProgressBar mProgressBar , final ProgressBar progrssBar, final ImageView downloadIcon,final ImageView downloadOrangeIcon) {
        if(!TextUtils.isEmpty(items.getPlay_url())
                && !items.getPlay_url().equals("NoVideoExist")
                && !items.getFree_paid().equals("Paid")) {

            mFetch.getDownloads(new Func<List<Download>>() {
                @Override
                public void call(@NotNull List<Download> result) {
                    mDownLoadItemList = result;
                    checkForCourse(items, progrssBar, downloadIcon,downloadOrangeIcon,  mProgressBar);
                    mShowDownloadIcon.onDownloaded(mChapter.isAllChaptersDownloaded());

                }
            });
        }else{
            progrssBar.setVisibility(GONE);
            downloadOrangeIcon.setVisibility(GONE);
        }
    }

    private void checkForCourse(Chapters.EClasses mItem,ProgressBar mDownloadProgressBar,ImageView mIvDownload,ImageView mIvDownloadOrange,ProgressBar progressBar) {

        progressBar.setVisibility(GONE);
            if(mDownLoadItemList!=null && mDownLoadItemList.size()>0){
                for(Download downloadTask:mDownLoadItemList){
                    if(downloadTask.getExtras().getString(Constants.ID,"1").equals(mItem.getId())){
                        if(downloadTask.getStatus() == Status.COMPLETED ){
                            mItem.setDownloadedVideoAddress(downloadTask.getFile());
                            mItem.setDownloadedStatus(Constants.FINISH);
                            mDownloadProgressBar.setVisibility(GONE);
                            mIvDownload.setVisibility(VISIBLE);
                            mIvDownloadOrange.setVisibility(GONE);

                        }else if(downloadTask.getStatus() == Status.FAILED
                                || downloadTask.getStatus() == Status.CANCELLED
                                || downloadTask.getStatus()== Status.DELETED
                                || downloadTask.getStatus()== Status.REMOVED
                        ){
                            mItem.setDownloadedStatus(Constants.STARTED);
                            mItem.setDownloadedVideoAddress(null);

                            mDownloadProgressBar.setVisibility(GONE);
                            mIvDownloadOrange.setVisibility(VISIBLE);
                            mIvDownload.setVisibility(GONE);
                        }else{
                            mItem.setDownloadedStatus(Constants.PENDING);
                            mItem.setDownloadedVideoAddress(downloadTask.getFile());

                            mDownloadProgressBar.setVisibility(VISIBLE);
                            mIvDownloadOrange.setVisibility(GONE);
                            mIvDownload.setVisibility(GONE);
                        }
                        break;
                    }else{
                        mItem.setDownloadedStatus(Constants.STARTED);
                        mItem.setDownloadedVideoAddress(null);
                        mDownloadProgressBar.setVisibility(GONE);
                        mIvDownload.setVisibility(GONE);
                        mIvDownloadOrange.setVisibility(VISIBLE);
                    }
                }
            }else{
                mItem.setDownloadedVideoAddress(null);
                mItem.setDownloadedStatus(Constants.STARTED);

                mDownloadProgressBar.setVisibility(GONE);
                mIvDownload.setVisibility(GONE);
                mIvDownloadOrange.setVisibility(VISIBLE);
            }


    }

    //  set items
    public void setItems(Chapters chapters) {

        mChapter = chapters;
        this.mEClassesList = mChapter.getE_classes();
        for(int i=0;i<mChapter.getE_classes().size();i++){
            expandingListData.put(i,false);
        }
        notifyDataSetChanged();
    }

    public void deselectRow() {
        if (!isNotifiedOnce) {
            selectedRowIndex = -1;
            notifyDataSetChanged();
        }
    }

    public void disableAudio() {
        selectedAudio = -1;
        isAuidoActivated = false;
        notifyDataSetChanged();
        Utils.releaseAudioPlayer();
    }


    public void startDownload(){
        for(int i=0; i<mEClassesList.size();i++){

            Chapters.EClasses eClasses = mEClassesList.get(i);

            if(eClasses.getType().equals(Constants.TYPE_DETAIL_VIDEO)){
                RecyclerView.ViewHolder viewHolder = mRecycleview.findViewHolderForLayoutPosition(i);
                MyViewHolder holder = (MyViewHolder) viewHolder;
                if (holder !=null && holder.mProgressBar!=null && holder.ivDownloadOrangeIcon.getVisibility()== VISIBLE) {
                    mEClassesList.get(i).setDownloadedStopped(false);
                    holder.ivDownloadOrangeIcon.performClick();
                }
            }
        }
    }



    public void cancelVideo(){

        for(int i=0; i<mEClassesList.size();i++){

            Chapters.EClasses eClasses = mEClassesList.get(i);

            if(eClasses.getType().equals(Constants.TYPE_DETAIL_VIDEO)){

                if (eClasses.getDownloadedStatus().equals(Constants.PENDING) || eClasses.getDownloadedStatus().equals(Constants.STARTED)) {
                    mFetch.delete(eClasses.getmReuest().getId());
                    mEClassesList.get(i).setDownloadedStopped(true);
                    mEClassesList.get(i).setDownloadedStatus(Constants.STARTED);

                    mEClassesList.get(i).setDownloadedVideoAddress(null);
                    mShowDownloadIcon.onDownloaded(mChapter.isAllChaptersDownloaded());
                    RecyclerView.ViewHolder viewHolder = mRecycleview.findViewHolderForLayoutPosition(i);
                    MyViewHolder holder = (MyViewHolder) viewHolder;
                    if (holder !=null && holder.mProgressBar!=null) {
                        holder.ivDownloadOrangeIcon.setVisibility(VISIBLE);
                        holder.mProgressBar.setVisibility(GONE);
                        holder.mDownloadprogresBar.setVisibility(GONE);
                        holder.ivDownloadGreyIcon.setVisibility(GONE);

                    }


                }
            }
        }
    }


    public interface startVideoDownload {
        void onStartDownload();

    }




    // isVideoPlayIconTapped is require to solve the case of disabling audio player when we ahve played video from big play icon from course detail header class
    public void selectRow(int position, boolean isVideoPlayIconTapped) {

        // change notified style in case of video icon played by user
        if (isVideoPlayIconTapped) {
            isNotifiedOnce = false;
            isAuidoActivated = false;
            Utils.releaseAudioPlayer();
        }


        // is Notified Once because notify is called two times in case to handle this
        if (!isNotifiedOnce) {
            if (!isVideoPlayIconTapped) {
                // if audio is selected nothing would be selected
                if (selectedAudio > -1) {
                    selectedRowIndex = -1;
                } else {
                    selectedRowIndex = position;

                }
            } else {
                selectedAudio = -1;
                selectedRowIndex = position;
            }

            notifyDataSetChanged();
        }
    }



    /**
     * different click handled by onItemClick
     */
    public interface OnEClassesClick {
        void onEClassesClick(Chapters.EClasses item, int position, String coursePrice, String courseID, String itemType, String courseTitle, boolean isProgramme, boolean isDownloaded);

    }

    /**
     * different click handled by onItemClick
     */
    public interface ShowDownlaodIcon {
        void onDownloaded(String show);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.e_classes_list_item, parent, false);

        return new MyViewHolder(itemView, mContext);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final MyViewHolder viewHolder = (MyViewHolder) holder;
        final Chapters.EClasses eClass = mEClassesList.get(position);


        // if audio is activated disable all videos
        if (isAuidoActivated) {
            selectedRowIndex = -1;
        }

        // if disable all audio if selected audio is greater than 1 . one audio will be played at 1 time
        if (position != selectedAudio) {
            eClass.setIsPlayActive(false);
        } else {
            if (isAuidoActivated && !TextUtils.isEmpty(selectedAudioId)) {
                if (eClass.getId().equals(selectedAudioId)) {

                    eClass.setIsPlayActive(true);
                } else {
                    eClass.setIsPlayActive(false);
                }
            } else {
                eClass.setIsPlayActive(false);

            }
        }


        viewHolder.bind(eClass, position, selectedRowIndex);


        viewHolder.mLLEclassContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!TextUtils.isEmpty(eClass.getDescription())){
                    if(expandingListData.get(position)){
                        viewHolder.mLlEclassDescription.setVisibility(GONE);
                    }else{
                        viewHolder.mLlEclassDescription.setVisibility(VISIBLE);
                        viewHolder.mEclassDescription.setText(eClass.getDescription());

                    }

                    expandingListData.put(position,!expandingListData.get(position));
                }


            }
        });

        viewHolder.ivDownloadOrangeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Utils.isNetworkAvailable(mContext)){

                    eClass.setDownloadedStopped(false);

                    viewHolder.mProgressBar.setProgress(0);
                    viewHolder.mProgressBar.setVisibility(GONE);
                    viewHolder.mDownloadprogresBar.setVisibility(VISIBLE);
                    viewHolder.ivDownloadGreyIcon.setVisibility(GONE);
                    viewHolder.ivDownloadOrangeIcon.setVisibility(GONE);
                    eClass.setDownloadedStatus(Constants.PENDING);

                    Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.download_added), Toast.LENGTH_SHORT).show();
                    Utils.callEventLogApi("downloading eclass started  <b>" +mEClassesList.get(position).getTitle()+" </b>" + "from Course Detail eclass" );

                    new EClassesListAdapter.startImageDownload().execute(eClass);
                }else{
                    Toast.makeText(mContext, R.string.error_api_no_internet_connection, Toast.LENGTH_SHORT).show();
                }



            }
        });

        viewHolder.mDownloadprogresBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        Toast.makeText(mContext, R.string.download_canceled, Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi("downloading eclass cancelled  <b>" +mEClassesList.get(position).getTitle()+" </b>" + "from Course Detail eclass" );

                        mFetch.delete(eClass.getmReuest().getId());
                        eClass.setDownloadedStopped(true);
                        eClass.setDownloadedVideoAddress(null);
                        mShowDownloadIcon.onDownloaded(mChapter.isAllChaptersDownloaded());
                        eClass.setDownloadedStatus(Constants.STARTED);

                        dialog.dismiss();
                        viewHolder.mProgressBar.setVisibility(GONE);
                        viewHolder.mDownloadprogresBar.setVisibility(GONE);
                        viewHolder.ivDownloadGreyIcon.setVisibility(GONE);
                        viewHolder.ivDownloadOrangeIcon.setVisibility(VISIBLE);

                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();
                    }
                }, mContext, mContext.getString(R.string.cancel), mContext.getString(R.string.cancel_message), mContext.getString(R.string.yes), mContext.getString(R.string.no));
            }
        });

        viewHolder.mProgressBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        Toast.makeText(mContext, R.string.download_canceled, Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi("downloading eclass cancelled  <b>" +mEClassesList.get(position).getTitle()+" </b>" + "from Course Detail eclass" );


                        mFetch.delete(eClass.getmReuest().getId());
                        eClass.setDownloadedStopped(true);
                        eClass.setDownloadedVideoAddress(null);
                        mShowDownloadIcon.onDownloaded(mChapter.isAllChaptersDownloaded());
                        eClass.setDownloadedStatus(Constants.STARTED);

                        dialog.dismiss();
                        viewHolder.mProgressBar.setVisibility(GONE);
                        viewHolder.ivDownloadGreyIcon.setVisibility(GONE);
                        viewHolder.ivDownloadOrangeIcon.setVisibility(VISIBLE);
                        viewHolder.mDownloadprogresBar.setVisibility(GONE);


                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();
                    }
                }, mContext, mContext.getString(R.string.cancel), mContext.getString(R.string.cancel_message), mContext.getString(R.string.yes), mContext.getString(R.string.no));

            }
        });

        viewHolder.ivDownloadGreyIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        Toast.makeText(getApplicationContext(), R.string.download_removed, Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi("downloaded eclass removed  <b>" +mEClassesList.get(position).getTitle()+" </b>" + "from Course Detail eclass" );

                        mFetch.delete(eClass.getmReuest().getId());
                        eClass.setDownloadedStopped(true);
                        eClass.setDownloadedVideoAddress(null);
                        eClass.setDownloadedStatus(Constants.STARTED);
                        dialog.dismiss();
                        viewHolder.mProgressBar.setVisibility(GONE);
                        viewHolder.ivDownloadGreyIcon.setVisibility(GONE);
                        viewHolder.mDownloadprogresBar.setVisibility(GONE);
                        viewHolder.ivDownloadOrangeIcon.setVisibility(VISIBLE);

                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();
                    }
                }, mContext,mContext.getString(R.string.remove),mContext.getString(R.string.remove_message), mContext.getString(R.string.yes),mContext.getString(R.string.no));
            }
        });


        if(!expandingListData.get(position)){
            viewHolder.mLlEclassDescription.setVisibility(GONE);
        }else{
            viewHolder.mLlEclassDescription.setVisibility(VISIBLE);
            viewHolder.mEclassDescription.setText(eClass.getDescription());

        }


        viewHolder.mInactiveLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (eClass.getType().equals(Constants.TYPE_DETAIL_AUDIO) && eClass.getFree_paid().equals("Free")) {

                    // it is notified from exo player listener too if not used this boolean value
                    isNotifiedOnce = true;

                    selectedRowIndex = -1;
                    selectedAudio = position;
                    isAuidoActivated = true;

                    selectedAudioId = eClass.getId();

                    Utils.callEventLogApi("started<b> " + eClass.getTitle() + "</b> audio player");


                } else {
                    isNotifiedOnce = false;
                    selectedAudio = -1;
                    selectedRowIndex = position;
                    Utils.releaseAudioPlayer();
                    isAuidoActivated = false;
                    selectedAudioId = null;


                }


                mOnItemClickListener.onEClassesClick(eClass, position, mCoursePrice, mCourseId, mItemType, mCourseTitle, mIsProgramme,mIsDownloaded);


                // to refresh all list present special case for programmes having multiple list and notify them all
                LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcast(new Intent(Constants.BROADCAST_REFRESH_LIST));

            }
        });


        viewHolder.ivChapterPlayIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (eClass.getType().equals(Constants.TYPE_DETAIL_AUDIO) && eClass.getFree_paid().equals("Free")) {

                    // it is notified from exo player listener too if not used this boolean value
                    isNotifiedOnce = true;

                    selectedRowIndex = -1;
                    selectedAudio = position;
                    isAuidoActivated = true;

                    selectedAudioId = eClass.getId();

                    Utils.callEventLogApi("started<b> " + eClass.getTitle() + "</b> audio player");


                } else {
                    isNotifiedOnce = false;
                    selectedAudio = -1;
                    selectedRowIndex = position;
                    Utils.releaseAudioPlayer();
                    isAuidoActivated = false;
                    selectedAudioId = null;


                }


                mOnItemClickListener.onEClassesClick(eClass, position, mCoursePrice, mCourseId, mItemType, mCourseTitle, mIsProgramme,mIsDownloaded);


                // to refresh all list present special case for programmes having multiple list and notify them all
                LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcast(new Intent(Constants.BROADCAST_REFRESH_LIST));

            }
        });

        viewHolder.cancelAudioPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedRowIndex = -1;
                selectedAudio = -1;
                isNotifiedOnce = false;
                isAuidoActivated = false;
                selectedAudioId = null;
                mOnItemClickListener.onEClassesClick(eClass, position, mCoursePrice, mCourseId, mItemType, mCourseTitle,mIsProgramme,mIsDownloaded);
                Utils.releaseAudioPlayer();
                Utils.callEventLogApi("closed<b> " + eClass.getTitle() + "</b> audio player");
                LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcast(new Intent(Constants.BROADCAST_REFRESH_LIST));
            }
        });

    }


    @Override
    public int getItemCount() {
        return mEClassesList.size();
    }

    //   * View holder class for items fo recycler view
    private class MyViewHolder extends RecyclerView.ViewHolder {
        private PlayerControlView controlView;
        public TextView tvTitle;
        public ImageView ivChapterIcon;
        public ImageView ivChapterPlayIcon,ivDownloadGreyIcon,ivDownloadOrangeIcon;
        public TextView tvDuration;
        private Context mContext;
        public FrameLayout cancelAudioPlayer;
        public LinearLayout mLLEclassContainer;
        private LinearLayout mInactiveLayout, mLlEclassDescription ;
        private FrameLayout audioActiveLayout;
        private PlayerView mPlayerView;
        private Chapters.EClasses mEClassesItem;
        private TextView mEclassDescription;
        private ProgressBar mProgressBar,mDownloadprogresBar;





        //  initialize view
        public MyViewHolder(View view, Context context) {
            super(view);
            tvTitle = view.findViewById(R.id.tv_e_classes_name);
            ivChapterIcon = view.findViewById(R.id.iv_e_classes_icon);
            ivChapterPlayIcon = view.findViewById(R.id.iv_e_classes_play_icon);
            tvDuration = view.findViewById(R.id.tv_e_classes_duration);
            cancelAudioPlayer = view.findViewById(R.id.exo_cancel_button);
            mEclassDescription=view.findViewById(R.id.tv_eclass_description);
            cancelAudioPlayer.setVisibility(View.VISIBLE);
            mPlayerView = view.findViewById(R.id.exoplayer);
            mLlEclassDescription = view.findViewById(R.id.ll_eclass_detail);
            controlView = mPlayerView.findViewById(R.id.exo_controller);
            mInactiveLayout = view.findViewById(R.id.list_item_inactive);
            mLLEclassContainer = view.findViewById(R.id.ll_eclass_container);
            audioActiveLayout = view.findViewById(R.id.item_active);
            ivDownloadGreyIcon=view.findViewById(R.id.tv_download_grey);
            ivDownloadOrangeIcon=view.findViewById(R.id.tv_download_orange);
            mProgressBar=view.findViewById(R.id.download_bar);
            mDownloadprogresBar=view.findViewById(R.id.progressBar);


            mContext = context;
        }


        // Set values to the list items
        void bind(final Chapters.EClasses item, final int position, int selectedIndex) {



            mEClassesItem = item;
            tvTitle.setText(item.getTitle());

//            ivChapterIcon.setImageURI(item.getIcon());
            Glide.with(mContext).load(item.getIcon()).into(ivChapterIcon);
            audioActiveLayout.setVisibility(View.GONE);
            mInactiveLayout.setVisibility(View.VISIBLE);
            switch (item.getType()) {
                case Constants.TYPE_DETAIL_AUDIO:

                    if (item.isPlayAudioActive()) {
                        audioActiveLayout.setVisibility(View.VISIBLE);
                        mInactiveLayout.setVisibility(View.GONE);
                        Utils.releaseAudioPlayer();
                        Utils.initilizeAudioPlayer(mContext, item.getPlay_url());
                        initAudioPlayer();
                        initCancelButton();
                    } else {
                        audioActiveLayout.setVisibility(View.GONE);
                        mInactiveLayout.setVisibility(View.VISIBLE);
                    }

                    ivChapterPlayIcon.setVisibility(View.VISIBLE);
                    ivChapterPlayIcon.setImageResource(R.drawable.ic_play_icon);
                    ivDownloadOrangeIcon.setVisibility(View.INVISIBLE);
                    mEclassDescription.setVisibility(View.INVISIBLE);
                    break;

                case Constants.TYPE_DETAIL_QUIZ:
                    ivChapterPlayIcon.setVisibility(View.VISIBLE);
                    ivChapterPlayIcon.setImageResource(R.drawable.ic_quiz);
                    ivDownloadOrangeIcon.setVisibility(View.INVISIBLE);
                    mEclassDescription.setVisibility(View.INVISIBLE);
                    break;

                case Constants.TYPE_DETAIL_VIDEO:

                    ivChapterPlayIcon.setVisibility(View.VISIBLE);
                    ivChapterPlayIcon.setImageResource(R.drawable.ic_play_icon);
                    showDonwloadIcon(item,mProgressBar,mDownloadprogresBar,ivDownloadGreyIcon,ivDownloadOrangeIcon);


                    break;

                default:
                    ivChapterPlayIcon.setVisibility(View.VISIBLE);
                    ivChapterPlayIcon.setImageResource(R.drawable.view_icon);
                    ivDownloadOrangeIcon.setVisibility(View.INVISIBLE);

                    break;


            }






            if (!item.getFree_paid().equals("Paid")) {
                ivChapterPlayIcon.setVisibility(VISIBLE);

                if (item.isAction()) {
                    ivChapterPlayIcon.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary));
                    ivChapterPlayIcon.setEnabled(true);
                    ivChapterPlayIcon.setClickable(true);
                } else {
                    ivChapterPlayIcon.setColorFilter(ContextCompat.getColor(mContext, R.color.backgroundGray));
                    ivChapterPlayIcon.setEnabled(false);
                }
            } else {
                ivChapterPlayIcon.setVisibility(GONE);

            }



            if (selectedIndex == position && !item.getFree_paid().equals("Paid")) {
                tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                tvDuration.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                ivChapterIcon.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary));
            } else {
                tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.textColorblack));
                tvDuration.setTextColor(ContextCompat.getColor(mContext, R.color.textColorblack));
                ivChapterIcon.setColorFilter(ContextCompat.getColor(mContext, R.color.textColorblack));
            }

            tvDuration.setText(item.getDuration());



            if(item.getType().equals(Constants.TYPE_DETAIL_AUDIO) || item.getType().equals(Constants.TYPE_VIDEO)) {
                if (TextUtils.isEmpty(item.getPlay_url()) || item.getPlay_url().equals("NoVideoExist")) {
                    ivChapterPlayIcon.setVisibility(GONE);
                }
            }


        }

        private void initAudioPlayer() {
            SimpleExoPlayer player = Utils.getAudioPlayer();
            mPlayerView.setPlayer(player);
            player.setPlayWhenReady(true);


            player.addListener(new Player.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                }

                @Override
                public void onLoadingChanged(boolean isLoading) {

                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    if (playbackState == Player.STATE_READY) {
                        if (playWhenReady) {
                            Utils.callEventLogApi("played <b>" + mEClassesItem.getTitle() + "</b> audio ");
                        } else {
                            Utils.callEventLogApi("paused <b>" + mEClassesItem.getTitle() + "</b> audio ");
                        }
                    }
                }

                @Override
                public void onRepeatModeChanged(int repeatMode) {

                }

                @Override
                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {

                }

                @Override
                public void onPositionDiscontinuity(int reason) {

                }

                @Override
                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                }

                @Override
                public void onSeekProcessed() {

                }
            });
        }


        private void initCancelButton() {


            mPlayerView.setControllerShowTimeoutMs(-1);
            mPlayerView.setControllerAutoShow(false);
            mPlayerView.setControllerHideOnTouch(false);

            cancelAudioPlayer = mPlayerView.findViewById(R.id.exo_cancel_button);
            cancelAudioPlayer.setVisibility(View.VISIBLE);


        }

    }


    private class startImageDownload extends AsyncTask<Chapters.EClasses, Integer, Void> {


        File outputFile;
        private Chapters.EClasses eClass;


        @Override
        protected void onPostExecute(Void aVoid) {

            if(!eClass.isDownloadedStopped()) {
                Request request = eClass.getmReuest();

                mDownLoadTask = new DownloadFile(
                        request.getFile(),
                        eClass.getTitle(),
                        mPunchLine,
                        mMentorName,
                        0,
                        null,
                        eClass.getPlay_url(),
                        eClass.getId(),
                        Constants.TYPE_COURSE,
                        mBannerImageUrl,
                        mBannerImagePath,
                        eClass.getDuration(),
                        mCourseTitle,
                        mBannerImagePath,
                        Constants.TYPE_COURSE

                );


                request.setGroupId(Integer.valueOf(mCourseId));

                request.setExtras(getExtrasForRequest(mDownLoadTask));
                Utils.addItemToFetchList(request);
            }



            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Chapters.EClasses... eClasses) {
            eClass = eClasses[0];
            try {

                URL url = new URL(mBannerImageUrl);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection


                outputFile = new File(getApplicationContext().getFilesDir(),  eClass.getId());//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e("Downlaod", "File Created");
                }


                mBannerImagePath = outputFile.getPath();


                FileOutputStream fos = getApplicationContext().openFileOutput( eClass.getId(), Context.MODE_PRIVATE);//Get OutputStream for NewFile Location


                InputStream is = c.getInputStream();//Get InputStream for connection
                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {

                    fos.write(buffer, 0, len1);//Write new file

                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {


                e.printStackTrace();
                outputFile = null;
                Log.e("Downlaod", "Download Error Exception " + e.getMessage());


            }

            return null;
        }

            }


    private Extras getExtrasForRequest(DownloadFile request) {
        final MutableExtras extras = new MutableExtras();
        if(!TextUtils.isEmpty(request.getTitle())) {
            extras.putString(Constants.TITLE, request.getTitle());

        }
        if(!TextUtils.isEmpty(request.getId())) {
            extras.putString(Constants.ID, request.getId());

        }

        if(!TextUtils.isEmpty(request.getmBannerAddress())) {
            extras.putString(Constants.BANNER_PATH, request.getmBannerAddress());

        }

        if(!TextUtils.isEmpty(request.getDescription())) {
            extras.putString(Constants.PUNCH_LINE, request.getDescription());

        }

        if(!TextUtils.isEmpty(request.getMentorName())) {
            extras.putString(Constants.MENTOR_NAME, request.getMentorName());

        }

        if(!TextUtils.isEmpty(request.getVideoUrl())) {
            extras.putString(Constants.VIDEO_URL, request.getVideoUrl());

        }

        if(!TextUtils.isEmpty(request.getBanner())) {
            extras.putString(Constants.IMAGE_URL, request.getBanner());

        }

        if(!TextUtils.isEmpty(request.getVideo_address())) {
            extras.putString(Constants.VIDEO_PATH, request.getVideo_address());

        }

        if(!TextUtils.isEmpty(request.getmGroupBannerAddress())) {
            extras.putString(Constants.GROUP_IMAGE_PATH, request.getmGroupBannerAddress());

        }

        if(!TextUtils.isEmpty(request.getmGroupTitle())) {
            extras.putString(Constants.GROUP_TITLE, request.getmGroupTitle());

        }

        if(!TextUtils.isEmpty(request.getmGroupType())) {
            extras.putString(Constants.GROUP_TYPE, request.getmGroupType());

        }

        extras.putString(Constants.TYPE, request.getType());

        extras.putString(Constants.DATA, new Gson().toJson(mDashBaordCourseDetails));


        if(!TextUtils.isEmpty(request.getmVideoDuration())){
            extras.putString(Constants.VIDEO_DURATION,request.getmVideoDuration());
        }
        return extras;
    }


    private final FetchListener fetchListener = new AbstractFetchListener() {

        @Override
        public void onCompleted(@NotNull Download download) {




            for (int i = 0; i < mEClassesList.size(); i++) {

                Chapters.EClasses eClasses = mEClassesList.get(i);

                if (download.getExtras().getString(Constants.ID, "0").equals(eClasses.getId())) {
                    mEClassesList.get(i).setDownloadedVideoAddress(download.getFile());
                    mEClassesList.get(i).setDownloadedStopped(false);

                    mEClassesList.get(i).setDownloadedStatus(Constants.FINISH);
                    RecyclerView.ViewHolder viewHolder = mRecycleview.findViewHolderForLayoutPosition(i);
                    MyViewHolder holder = (MyViewHolder) viewHolder;
                    Toast.makeText(getApplicationContext(), R.string.download_complete, Toast.LENGTH_SHORT).show();
                    Utils.callEventLogApi("downloading eclass completed  <b>" +mEClassesList.get(i).getTitle()+" </b>" + "from Course Detail eclass" );

                    if (holder !=null && holder.mProgressBar!=null) {
                        holder.mProgressBar.setVisibility(GONE);
                        holder.ivDownloadGreyIcon.setVisibility(VISIBLE);
                        holder.ivDownloadOrangeIcon.setVisibility(GONE);
                        holder.mDownloadprogresBar.setVisibility(GONE);

                    }
                    mShowDownloadIcon.onDownloaded(mChapter.isAllChaptersDownloaded());

                }




            }

        }


        @Override
        public void onError(@NotNull Download download, @NotNull Error error, @org.jetbrains.annotations.Nullable Throwable throwable) {


            for (int i = 0; i < mEClassesList.size(); i++) {
                Chapters.EClasses eClasses = mEClassesList.get(i);

                if (download.getExtras().getString(Constants.ID, "0").equals(eClasses.getId())) {

                    mFetch.delete(download.getId());
                    mEClassesList.get(i).setDownloadedStatus(Constants.STARTED);
                    mEClassesList.get(i).setDownloadedVideoAddress(null);
                    mEClassesList.get(i).setDownloadedStopped(true);

                    RecyclerView.ViewHolder viewHolder = mRecycleview.findViewHolderForLayoutPosition(i);
                    MyViewHolder holder = (MyViewHolder) viewHolder;
                    Toast.makeText(getApplicationContext(), R.string.download_failed, Toast.LENGTH_SHORT).show();
                    Utils.callEventLogApi("downloading eclass failed  <b>" +mEClassesList.get(i).getTitle()+" </b>" + "from Course Detail eclass" );

                    if (holder !=null && holder.mProgressBar!=null) {
                        holder.mProgressBar.setVisibility(GONE);
                        holder.ivDownloadOrangeIcon.setVisibility(VISIBLE);
                        holder.ivDownloadGreyIcon.setVisibility(GONE);
                        holder.mDownloadprogresBar.setVisibility(GONE);
                    }

                    mShowDownloadIcon.onDownloaded(mChapter.isAllChaptersDownloaded());

                }

            }
        }


        @Override
        public void onProgress(@NotNull Download download, long etaInMilliSeconds, long downloadedBytesPerSecond) {


            for (int i = 0; i < mEClassesList.size(); i++) {

                Chapters.EClasses eClasses = mEClassesList.get(i);


                if (download.getExtras().getString(Constants.ID, "0").equals(eClasses.getId())) {


                    RecyclerView.ViewHolder viewHolder = mRecycleview.findViewHolderForLayoutPosition(i);
                    MyViewHolder holder = (MyViewHolder) viewHolder;
                    if (holder !=null && holder.mProgressBar!=null) {
                        if (!eClasses.isDownloadedStopped()) {
                            mEClassesList.get(i).setDownloadedStopped(false);

                            mEClassesList.get(i).setDownloadedStatus(Constants.PENDING);
                            mEClassesList.get(i).setDownloadedVideoAddress(download.getFile());
                            holder.mDownloadprogresBar.setVisibility(GONE);
                            holder.mProgressBar.setVisibility(VISIBLE);
                            holder.ivDownloadGreyIcon.setVisibility(GONE);
                            holder.ivDownloadOrangeIcon.setVisibility(GONE);
                            holder.mProgressBar.setProgress(download.getProgress());
                            mShowDownloadIcon.onDownloaded(mChapter.isAllChaptersDownloaded());


                        } else {
                            mFetch.delete(download.getRequest().getId());
                            mEClassesList.get(i).setDownloadedStatus(Constants.STARTED);
                            mEClassesList.get(i).setDownloadedStopped(true);

                            holder.mDownloadprogresBar.setVisibility(GONE);
                            holder.mProgressBar.setVisibility(GONE);
                            holder.ivDownloadGreyIcon.setVisibility(GONE);
                            holder.ivDownloadOrangeIcon.setVisibility(VISIBLE);
                        }
                    }


                }

            }

            }

        @Override
        public void onQueued(@NotNull Download download, boolean waitingOnNetwork) {

        }


        @Override
        public void onRemoved(@NotNull Download download) {


            for (int i = 0; i < mEClassesList.size(); i++) {

                Chapters.EClasses eClasses = mEClassesList.get(i);

                if (download.getExtras().getString(Constants.ID, "0").equals(eClasses.getId())) {
                    mEClassesList.get(i).setDownloadedStatus(Constants.STARTED);
                    mEClassesList.get(i).setDownloadedVideoAddress(null);
                    mEClassesList.get(i).setDownloadedStopped(true);

                    RecyclerView.ViewHolder viewHolder = mRecycleview.findViewHolderForLayoutPosition(i);
                    MyViewHolder holder = (MyViewHolder) viewHolder;
                    if (holder !=null && holder.mProgressBar!=null) {

                        holder.mProgressBar.setVisibility(GONE);
                        holder.ivDownloadOrangeIcon.setVisibility(VISIBLE);
                        holder.ivDownloadGreyIcon.setVisibility(GONE);
                        holder.mDownloadprogresBar.setVisibility(GONE);

                    }

                    mShowDownloadIcon.onDownloaded(mChapter.isAllChaptersDownloaded());


                }

            }

        }

        @Override
        public void onDeleted(@NotNull Download download) {


            for (int i = 0; i < mEClassesList.size(); i++) {

                Chapters.EClasses eClasses = mEClassesList.get(i);

                if (download.getExtras().getString(Constants.ID, "0").equals(eClasses.getId())) {
                    mEClassesList.get(i).setDownloadedStatus(Constants.STARTED);
                    mEClassesList.get(i).setDownloadedStopped(true);

                    mEClassesList.get(i).setDownloadedVideoAddress(null);
                    RecyclerView.ViewHolder viewHolder = mRecycleview.findViewHolderForLayoutPosition(i);
                    MyViewHolder holder = (MyViewHolder) viewHolder;
                    if (holder !=null && holder.mProgressBar!=null) {
                        holder.mProgressBar.setVisibility(GONE);
                        holder.ivDownloadOrangeIcon.setVisibility(VISIBLE);
                        holder.ivDownloadGreyIcon.setVisibility(GONE);
                        holder.mDownloadprogresBar.setVisibility(GONE);
                    }

                    mShowDownloadIcon.onDownloaded(mChapter.isAllChaptersDownloaded());


                }

            }
        }
    };



}
