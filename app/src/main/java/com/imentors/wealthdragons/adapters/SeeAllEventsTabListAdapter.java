package com.imentors.wealthdragons.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.models.Event;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.EmptyViewHolder;
import com.imentors.wealthdragons.views.EventView;
import com.imentors.wealthdragons.views.FooterViewHolder;

import java.util.ArrayList;
import java.util.List;


/**
 * Created on 02-02-2016.
 */
public class SeeAllEventsTabListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //article list view
    private static final int TYPE_EVENT = 0;

    //Type footer and empty
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_EMPTY = 2;



    //ref to the item click listener
    private final OnItemClickListener mOnItemClickListener;


    //ref to the Mentors items
    private List<Event> mItemsSeeAllEvents;


    private String mTasteId , mSeeAllApiKey;

    private Context mContext;

    private int mTabPosition,mItemsCountOnServer, mNextPage;

    private boolean isTablet , mServerError ,mIsSubCategory = false;
    private PagingListener mPagingListener;






    public SeeAllEventsTabListAdapter(OnItemClickListener onItemClickListener, PagingListener pagingListener, Context context ) {
        mOnItemClickListener = onItemClickListener;
        mContext = context;
        mPagingListener = pagingListener;
        isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);

    }


    /**
     * set the article items
     *
     * @param itemsEvent               article items

     */
    public void setItems(final List<Event> itemsEvent ,  int tabPosition, final int nextPage, final int itemsCountOnServer  , final String tasteId , final String seeAllApiKey) {


        if(itemsEvent != null){
            mItemsSeeAllEvents = new ArrayList<>();
            mItemsSeeAllEvents.addAll(itemsEvent);
        }



        mTabPosition = tabPosition;

        mNextPage = nextPage;
        mTasteId = tasteId;
        mItemsCountOnServer = itemsCountOnServer;
        mSeeAllApiKey = seeAllApiKey;
        notifyDataSetChanged();
    }



    //  add the items
    public void addItems(final List<Event> itemsEvent , final int nextPage, final int itemsCountOnServer  , final String tasteId, final String seeAllApiKey){
        if(mItemsSeeAllEvents !=null){
            mItemsSeeAllEvents.addAll(itemsEvent);
        }

        mIsSubCategory = false;

        mNextPage = nextPage;
        mTasteId = tasteId;
        mItemsCountOnServer = itemsCountOnServer;
        mSeeAllApiKey = seeAllApiKey;

        notifyDataSetChanged();

    }


    //  set server error
    public void setServerError(){
        mServerError = true;
        notifyDataSetChanged();
    }


    //  clear data
    public void clearData(int position){

        if(position == Constants.SEE_ALL_COURSES_TAB) {

            if (mItemsSeeAllEvents != null) {
                mItemsSeeAllEvents.clear();
            }
        }


        notifyDataSetChanged();

    }


    public void isSubCategoryFilterData(){
        mIsSubCategory = true;
    }

    /**
     * get the article items
     *
     * @return article items
     */
    public List<Event> getEventItems() {

        if(mItemsSeeAllEvents !=null){
            return mItemsSeeAllEvents;
        }

        return  null;
    }





    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {



        View view;
        switch (viewType) {
            case TYPE_EVENT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.event_banner_layout, parent, false);
                return new EventViewHolder(view, mOnItemClickListener);
            case TYPE_FOOTER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_article_list_item_footer, parent, false);
                return new FooterViewHolder(view);

            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_article_list_item_empty, parent, false);
                return new EmptyViewHolder(view);
        }




    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof SeeAllEventsTabListAdapter.EventViewHolder) {
            Event item = mItemsSeeAllEvents.get(position);
            //cast holder to ItemViewHolder and set data for header.
            EventViewHolder viewHolder = (EventViewHolder) holder;
            viewHolder.bind(item, position, mItemsSeeAllEvents.size(),mContext);

        }else if (holder instanceof FooterViewHolder) {
            //cast holder to FooterViewHolder and set data for header.
            FooterViewHolder viewHolder = (FooterViewHolder) holder;
            showFooterViewHolder(position, viewHolder);

        }


    }


    @Override
    public int getItemViewType(int position) {
        if (position >= mItemsSeeAllEvents.size()) {
            final int size = mItemsSeeAllEvents.size();
            if (size == 0) {
                if(mIsSubCategory){
                    return TYPE_FOOTER;

                }
                return TYPE_EMPTY;
            } else {
                return (size >= mItemsCountOnServer) ? TYPE_EMPTY : TYPE_FOOTER;
            }
        } else {
            return TYPE_EVENT;
        }
    }

    @Override
    public int getItemCount() {

        if(mItemsSeeAllEvents !=null){

            // plus one for footer
          return   mItemsSeeAllEvents.size()+1;
        }

        return 0;

    }


    /**
     * different click handled by onItemClick , onNetworkClick and onBookmarkClick
     */
    public interface OnItemClickListener {
        void onEventClick(Event item, int position);
        void setHeader(String title, String imageHeader, int listSize, int tabPosition);

    }




    /**
     * View holder class for items fo recycler view
     */
    private static class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //view on click listener need to forward click events
        private final SeeAllEventsTabListAdapter.OnItemClickListener mOnItemClickListener;
        private final EventView mEventBannerLayout;
        // current bind to view holder
        private Event mCurrentItem;
        //position
        private int mPosition;



        EventViewHolder(@NonNull View view, final SeeAllEventsTabListAdapter.OnItemClickListener listener) {
            super(view);
            mOnItemClickListener = listener;
            view.setOnClickListener(this);

            mEventBannerLayout =  view.findViewById(R.id.dashboard_event_banner);

        }

        @Override
        public void onClick(View view) {
            if (mCurrentItem != null && mOnItemClickListener != null) {
                mOnItemClickListener.onEventClick(mCurrentItem, mPosition);
            }
        }

        /**
         * Bind the the values of the view holder
         *
         * @param item     article item
         * @param position position
         */
        void bind(final Event item, final int position , int totalItems , Context mContext) {
            mCurrentItem = item;
            mPosition = position;

            if(totalItems-1 == position && totalItems>1){
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins((int)mContext.getResources().getDimension(R.dimen.margin_default),0,(int)mContext.getResources().getDimension(R.dimen.margin_default),0);
                itemView.findViewById(R.id.card_view).setLayoutParams(params);

            }

            mEventBannerLayout.bind(item, null);
        }
    }





    private void showFooterViewHolder(int position, FooterViewHolder viewHolder) {
      if(mItemsSeeAllEvents !=null){
            //check whether to need to call next page from the server
            if (mPagingListener != null && mNextPage != 0 && position >= mItemsSeeAllEvents.size() && mItemsSeeAllEvents.size() < mItemsCountOnServer && !mIsSubCategory) {
                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {

                    // if there is server error
                    if(mServerError){

                        viewHolder.showServerError(0);

                        //set the click listener to reload adapter on user click
                        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                    // set it to false again
                                    mServerError = false;
                                    notifyDataSetChanged();
                                    Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                                }
                            }
                        });

                    }else {

                        //remove the click event if loading items
                        viewHolder.itemView.setOnClickListener(null);
                        //show loading indicator
                        viewHolder.showLoading(0);

                        //fetch the new page from the server
                        mPagingListener.onGetItemFromApi(mNextPage, mTasteId, Constants.TYPE_SEE_ALL_EVENTS,mSeeAllApiKey);
                    }
                } else {
                    //show the no internet error
                    viewHolder.showNoInternetError(0);

                    //set the click listener to reload adapter on user click
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                notifyDataSetChanged();
                                Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                            }
                        }
                    });
                }

            }else if(mPagingListener != null && mIsSubCategory){
                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {

                    // if there is server error
                    if(mServerError){

                        viewHolder.showServerError((int)Utils.getCourseImageHeight((Activity)mContext));

                        //set the click listener to reload adapter on user click
                        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                    // set it to false again
                                    mServerError = false;
                                    mPagingListener.onGetItemFromApi(mNextPage, mTasteId, Constants.TYPE_SEE_ALL_EVENTS,mSeeAllApiKey);
                                    notifyDataSetChanged();
                                    Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");


                                }
                            }
                        });

                    }else {

                        //remove the click event if loading items
                        viewHolder.itemView.setOnClickListener(null);
                        //show loading indicator
                        viewHolder.showLoading((int)Utils.getCourseImageHeight((Activity)mContext));
                    }
                } else {
                    //show the no internet error
                    viewHolder.showNoInternetError((int)Utils.getCourseImageHeight((Activity)mContext));

                    //set the click listener to reload adapter on user click
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                mPagingListener.onGetItemFromApi(mNextPage, mTasteId, Constants.TYPE_SEE_ALL_EVENTS,mSeeAllApiKey);
                                notifyDataSetChanged();
                                Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                            }
                        }
                    });
                }
            }
        }
    }



}
