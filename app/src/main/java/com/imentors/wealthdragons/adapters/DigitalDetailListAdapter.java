package com.imentors.wealthdragons.adapters;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.models.DigitalCoaching;
import com.imentors.wealthdragons.models.DownloadFile;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.EmptyViewHolder;
import com.imentors.wealthdragons.views.ReviewFooterViewHolder;
import com.tonyodev.fetch2.AbstractFetchListener;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2.Status;
import com.tonyodev.fetch2core.Extras;
import com.tonyodev.fetch2core.MutableExtras;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.facebook.FacebookSdk.getApplicationContext;

public class DigitalDetailListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private int mItemsCountOnServer;
    private List<DigitalCoaching> mDigitalCoaching = new ArrayList<>();
    private Context mContext;
    private OnListItemClicked mOnItemClicked;
    private PagingListener mPagingListener;
    private boolean mServerError;
    private boolean mIsDownloaded;
    private boolean mIsSubscribed;

    private static final int TYPE_FOOTER = 1;
    private int mNextPage;
    private static final int TYPE_EMPTY = 2;
    private static final int TYPE_MOBILE = 3;
    private static final int TYPE_TABLET = 4;

    private String mBannerImageUrl;
    private String mBannerImagePath;
    private String mDescription;
    private String mMentorName;
    private String mGroupBannerImagePath;
    private String mSelectedId;

    private float mImageWidth;
    private Fetch mFetch;
    private List<Download> mDownLoadItemList;
    private String mMentorId;
    private String mGroupBannerImage;

    private RecyclerView mRecycleView;
    private DashBoardMentorDetails mDashBoardMentorDetails;



    public DigitalDetailListAdapter(Context context, PagingListener pagingListener, List<DigitalCoaching> digitalCoachings, OnListItemClicked onListItemClicked, int itemsCountOnServer, int nextPage, String selectedId,
                                    String description, String mentorName,
                                    String bannerImage,String mentorId, String groupBannerImage,
                                    RecyclerView recycleview, boolean isDownloaded, DashBoardMentorDetails dashBoardDetails,
                                    boolean isSubscribed) {

        mContext = context;
        mOnItemClicked = onListItemClicked;
        mDigitalCoaching.addAll(digitalCoachings);
        mPagingListener = pagingListener;
        mItemsCountOnServer = itemsCountOnServer;
        mNextPage = nextPage;
        mSelectedId = selectedId;
        mDescription = description;
        mMentorName = mentorName;
        mBannerImageUrl = bannerImage;
        mRecycleView = recycleview;
        mFetch = Utils.getFetchInstance();
        mFetch.addListener(fetchListener);
        mMentorId = mentorId;
        mGroupBannerImage = groupBannerImage;
        mIsDownloaded = isDownloaded;
        mDashBoardMentorDetails = dashBoardDetails;
        mIsSubscribed = isSubscribed;
    }

    //  set server error
    public void setServerError() {
        mServerError = true;
        notifyDataSetChanged();
    }

    public void setSelectedUrl(String selectedId) {
        mSelectedId = selectedId;
        notifyDataSetChanged();
    }


    /**
     * different click handled by onItemClick
     */
    //  interface
    public interface OnListItemClicked {
        void onItemClicked(DigitalCoaching item, int postion);
    }

    //  add items
    public void addItems(List<DigitalCoaching> digitalCoachings, int itemsCountOnServer, int nextPage) {
        if (itemsCountOnServer>=mDigitalCoaching.size()) {
            Log.e("Digital", String.valueOf(mDigitalCoaching.size()) + nextPage);
            mDigitalCoaching.addAll(digitalCoachings);
            mNextPage = nextPage;
            mItemsCountOnServer = itemsCountOnServer;
            notifyDataSetChanged();
        }
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view;
        switch (viewType) {

            case TYPE_FOOTER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.review_footer_item, parent, false);
                return new ReviewFooterViewHolder(view);

            case TYPE_EMPTY:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.review_footer_list_item_empty, parent, false);
                return new EmptyViewHolder(view);

            case TYPE_TABLET:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.digital_coaching_tablet_list_item, parent, false);
                return new MyTabletViewHolder(view, mContext);


            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.digital_coaching_mobile_list_item, parent, false);

                return new MyMobileViewHolder(view, mContext);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {



        if (holder instanceof ReviewFooterViewHolder) {
            //cast holder to FooterViewHolder and set data for header.
            ReviewFooterViewHolder viewHolder = (ReviewFooterViewHolder) holder;
            showFooterViewHolder(position, viewHolder);

        } else if (holder instanceof MyTabletViewHolder) {


            final MyTabletViewHolder viewHolder = (MyTabletViewHolder) holder;

            final DigitalCoaching digitalCoaching = mDigitalCoaching.get(position);


            viewHolder.mIvBannerImage.post(()-> {
                    mImageWidth = viewHolder.mIvBannerImage.getWidth();
                    mImageWidth = viewHolder.mIvBannerImage.getMeasuredWidth();
            });



            viewHolder.bind(mDigitalCoaching.get(position), mContext, mDigitalCoaching.get(position).getId().equals(mSelectedId), mImageWidth,mIsDownloaded);



            viewHolder.mCardView.setOnClickListener(view ->  {
                    mOnItemClicked.onItemClicked(mDigitalCoaching.get(position),position);
                    notifyDataSetChanged();
            });

            viewHolder.mDownloadIconOrange.setOnClickListener(view ->  {


                    if(Utils.isNetworkAvailable(mContext)){
                        new StartGroupImageDownload().execute();
                        digitalCoaching.setDownloadedStopped(false);

                        viewHolder.mProgressDownloadBar.setProgress(0);
                        viewHolder.mProgressDownloadBar.setVisibility(GONE);
                        viewHolder.mProgressBar.setVisibility(VISIBLE);
                        viewHolder.mDownloadIconGrey.setVisibility(GONE);
                        viewHolder.mDownloadIconOrange.setVisibility(GONE);

                        Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.download_added), Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi(getApplicationContext().getString(R.string.download_added)+"  "+Constants.STARTING_BOLD +mDigitalCoaching.get(position).getTitle()+" "+Constants.ENDING_BOLD+ R.string.from_digital_coaching );

                        new StartImageDownload().execute(digitalCoaching);
                    }else{
                        Toast.makeText(mContext, R.string.error_api_no_internet_connection, Toast.LENGTH_SHORT).show();
                    }


            });

            viewHolder.mProgressBar.setOnClickListener(view ->

                    Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                        @Override
                        public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                            Toast.makeText(getApplicationContext(), R.string.download_canceled, Toast.LENGTH_SHORT).show();
                            Utils.callEventLogApi(R.string.download_canceled+"  "+Constants.STARTING_BOLD +mDigitalCoaching.get(position).getTitle()+" "+Constants.ENDING_BOLD+ R.string.from_digital_coaching );
                            mFetch.delete(digitalCoaching.getmReuest().getId());
                            digitalCoaching.setDownloadedStopped(true);

                            dialog.dismiss();
                            viewHolder.mProgressDownloadBar.setVisibility(GONE);
                            viewHolder.mProgressBar.setVisibility(GONE);
                            viewHolder.mDownloadIconGrey.setVisibility(GONE);
                            viewHolder.mDownloadIconOrange.setVisibility(VISIBLE);

                        }

                        @Override
                        public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                            dialog.dismiss();
                        }
                    }, mContext, mContext.getString(R.string.cancel), mContext.getString(R.string.cancel_message), mContext.getString(R.string.yes), mContext.getString(R.string.no))

            );

            viewHolder.mProgressDownloadBar.setOnClickListener(view ->

                    Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                        @Override
                        public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                            Toast.makeText(getApplicationContext(), R.string.download_canceled, Toast.LENGTH_SHORT).show();
                            Utils.callEventLogApi(R.string.download_canceled+"  "+Constants.STARTING_BOLD +mDigitalCoaching.get(position).getTitle()+" "+Constants.ENDING_BOLD+ R.string.from_digital_coaching );

                            mFetch.delete(digitalCoaching.getmReuest().getId());
                            digitalCoaching.setDownloadedStopped(true);

                            dialog.dismiss();
                            viewHolder.mProgressDownloadBar.setVisibility(GONE);
                            viewHolder.mDownloadIconGrey.setVisibility(GONE);
                            viewHolder.mDownloadIconOrange.setVisibility(VISIBLE);
                            viewHolder.mProgressBar.setVisibility(GONE);
                        }

                        @Override
                        public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                            dialog.dismiss();
                        }
                    }, mContext, mContext.getString(R.string.cancel), mContext.getString(R.string.cancel_message), mContext.getString(R.string.yes), mContext.getString(R.string.no))
            );

            viewHolder.mDownloadIconGrey.setOnClickListener(view ->

                    Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                        @Override
                        public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                            Toast.makeText(getApplicationContext(), R.string.download_removed, Toast.LENGTH_SHORT).show();
                            Utils.callEventLogApi( R.string.download_removed+" "+Constants.STARTING_BOLD +mDigitalCoaching.get(position).getTitle()+" "+Constants.ENDING_BOLD+ R.string.from_digital_coaching);


                            mFetch.delete(digitalCoaching.getmReuest().getId());
                            digitalCoaching.setDownloadedStopped(true);

                            dialog.dismiss();
                            viewHolder.mProgressDownloadBar.setVisibility(GONE);
                            viewHolder.mDownloadIconGrey.setVisibility(GONE);
                            viewHolder.mDownloadIconOrange.setVisibility(VISIBLE);
                            viewHolder.mProgressBar.setVisibility(GONE);

                        }

                        @Override
                        public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                            dialog.dismiss();
                        }
                    }, mContext, mContext.getString(R.string.remove), mContext.getString(R.string.remove_message), mContext.getString(R.string.yes), mContext.getString(R.string.cancel))
            );

        } else if (holder instanceof MyMobileViewHolder) {

            final MyMobileViewHolder viewHolder = (MyMobileViewHolder) holder;

            final DigitalCoaching digitalCoaching = mDigitalCoaching.get(position);


            viewHolder.mCardView.post(()-> {
                    mImageWidth = viewHolder.mIvBannerImage.getWidth();
                    mImageWidth = viewHolder.mIvBannerImage.getMeasuredWidth();
            });


            viewHolder.bind(mDigitalCoaching.get(position), mContext, mDigitalCoaching.get(position).getId().equals(mSelectedId), mImageWidth,mIsDownloaded);



            viewHolder.mCardView.setOnClickListener(view ->  {


                    mSelectedId = mDigitalCoaching.get(position).getId();
                        mOnItemClicked.onItemClicked(mDigitalCoaching.get(position), position);
                        notifyDataSetChanged();
            });

            viewHolder.mDownloadIconOrange.setOnClickListener(view ->  {

                    if(Utils.isNetworkAvailable(mContext)) {

                        new StartGroupImageDownload().execute();
                        digitalCoaching.setDownloadedStopped(false);

                        viewHolder.mProgressDownloadBar.setProgress(0);
                        viewHolder.mProgressDownloadBar.setVisibility(GONE);
                        viewHolder.mProgressBar.setVisibility(VISIBLE);
                        viewHolder.mDownloadIconGrey.setVisibility(GONE);
                        viewHolder.mDownloadIconOrange.setVisibility(GONE);
                        Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.download_added), Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi(getApplicationContext().getString(R.string.download_added)+" "+Constants.STARTING_BOLD +mDigitalCoaching.get(position).getTitle()+" "+Constants.ENDING_BOLD+ R.string.from_digital_coaching );

                        new StartImageDownload().execute(digitalCoaching);

                    }else{
                        Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.error_api_no_internet_connection), Toast.LENGTH_SHORT).show();

                    }

            });

            viewHolder.mProgressBar.setOnClickListener(view ->
                    Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                        @Override
                        public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                            Toast.makeText(getApplicationContext(), R.string.download_canceled, Toast.LENGTH_SHORT).show();
                            Utils.callEventLogApi(R.string.download_canceled+"  "+Constants.STARTING_BOLD +mDigitalCoaching.get(position).getTitle()+" "+Constants.ENDING_BOLD+ R.string.from_digital_coaching );


                            mFetch.delete(digitalCoaching.getmReuest().getId());
                            digitalCoaching.setDownloadedStopped(true);

                            dialog.dismiss();
                            viewHolder.mProgressDownloadBar.setVisibility(GONE);
                            viewHolder.mProgressBar.setVisibility(GONE);
                            viewHolder.mDownloadIconGrey.setVisibility(GONE);
                            viewHolder.mDownloadIconOrange.setVisibility(VISIBLE);

                        }

                        @Override
                        public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                            dialog.dismiss();
                        }
                    }, mContext, mContext.getString(R.string.cancel), mContext.getString(R.string.cancel_message), mContext.getString(R.string.yes), mContext.getString(R.string.no))

            );

            viewHolder.mProgressDownloadBar.setOnClickListener(view ->

                    Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                        @Override
                        public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                            Toast.makeText(getApplicationContext(), R.string.download_canceled, Toast.LENGTH_SHORT).show();
                            Utils.callEventLogApi(R.string.download_canceled+"  "+Constants.STARTING_BOLD +mDigitalCoaching.get(position).getTitle()+" "+Constants.ENDING_BOLD+ R.string.from_digital_coaching );


                            mFetch.delete(digitalCoaching.getmReuest().getId());
                            digitalCoaching.setDownloadedStopped(true);

                            dialog.dismiss();
                            viewHolder.mProgressDownloadBar.setVisibility(GONE);
                            viewHolder.mDownloadIconGrey.setVisibility(GONE);
                            viewHolder.mDownloadIconOrange.setVisibility(VISIBLE);
                            viewHolder.mProgressBar.setVisibility(GONE);

                        }

                        @Override
                        public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                            dialog.dismiss();
                        }
                    }, mContext, mContext.getString(R.string.cancel), mContext.getString(R.string.cancel_message), mContext.getString(R.string.yes), mContext.getString(R.string.no))
            );

            viewHolder.mDownloadIconGrey.setOnClickListener(view ->

                    Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                        @Override
                        public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                            Toast.makeText(getApplicationContext(), R.string.download_removed, Toast.LENGTH_SHORT).show();
                            Utils.callEventLogApi(R.string.download_removed+"  "+Constants.STARTING_BOLD +mDigitalCoaching.get(position).getTitle()+" "+Constants.ENDING_BOLD+ R.string.from_digital_coaching );


                            mFetch.delete(digitalCoaching.getmReuest().getId());
                            digitalCoaching.setDownloadedStopped(true);

                            dialog.dismiss();
                            viewHolder.mProgressDownloadBar.setVisibility(GONE);
                            viewHolder.mDownloadIconGrey.setVisibility(GONE);
                            viewHolder.mDownloadIconOrange.setVisibility(VISIBLE);
                            viewHolder.mProgressBar.setVisibility(GONE);
                        }

                        @Override
                        public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                            dialog.dismiss();
                        }
                    }, mContext, mContext.getString(R.string.remove), mContext.getString(R.string.remove_message), mContext.getString(R.string.yes), mContext.getString(R.string.no))

            );
        }

    }



private class StartImageDownload extends AsyncTask<DigitalCoaching, Void, Void> {


        File outputFile;
        private DigitalCoaching digitalCoaching;


        @Override
        protected Void doInBackground(DigitalCoaching... digitalCoachings) {
            digitalCoaching = digitalCoachings[0];
            try {

                URL url = new URL(digitalCoaching.getmDownloadBanner());//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection


                outputFile = new File(getApplicationContext().getFilesDir(), digitalCoaching.getId());//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    boolean isFileCreated = outputFile.createNewFile();
                    if(isFileCreated){
                        Log.e(Constants.DOWNLOAD, "File Created");

                    }else{
                        Log.e(Constants.DOWNLOAD, "File not Created");

                    }

                }


                mBannerImagePath = outputFile.getPath();


                FileOutputStream fos = getApplicationContext().openFileOutput(digitalCoaching.getId(), Context.MODE_PRIVATE);//Get OutputStream for NewFile Location


                InputStream is = c.getInputStream();//Get InputStream for connection
                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {

                    fos.write(buffer, 0, len1);//Write new file

                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {
                Logger.getLogger(Constants.LOG, e.toString());
                outputFile = null;
                Log.e(Constants.DOWNLOAD, "Download Error Exception " + e.getMessage());


            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(!digitalCoaching.isDownloadedStopped()) {

                Request request = digitalCoaching.getmReuest();

                DownloadFile mDownLoadTask = new DownloadFile(
                        request.getFile(),
                        digitalCoaching.getTitle(),
                        mDescription,
                        mMentorName,
                        0,
                        null,
                        digitalCoaching.getVideo_url(),
                        digitalCoaching.getId(),
                        Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING,
                        mBannerImageUrl,
                        mBannerImagePath,
                        digitalCoaching.getVideo_duration(),
                        mMentorName,
                        mGroupBannerImagePath,
                        Constants.TYPE_MENTOR
                );


                request.setGroupId(Integer.valueOf(mMentorId));
                request.setExtras(getExtrasForRequest(mDownLoadTask));
                Utils.addItemToFetchList(request);
            }



            super.onPostExecute(aVoid);
        }


        private Extras getExtrasForRequest(DownloadFile request) {
            final MutableExtras extras = new MutableExtras();
            if (!TextUtils.isEmpty(request.getTitle())) {
                extras.putString(Constants.TITLE, request.getTitle());

            }
            if (!TextUtils.isEmpty(request.getId())) {
                extras.putString(Constants.ID, request.getId());

            }

            if (!TextUtils.isEmpty(request.getmBannerAddress())) {
                extras.putString(Constants.BANNER_PATH, request.getmBannerAddress());

            }

            if (!TextUtils.isEmpty(request.getDescription())) {
                extras.putString(Constants.PUNCH_LINE, request.getDescription());

            }

            if (!TextUtils.isEmpty(request.getMentorName())) {
                extras.putString(Constants.MENTOR_NAME, request.getMentorName());

            }

            if (!TextUtils.isEmpty(request.getVideoUrl())) {
                extras.putString(Constants.VIDEO_URL, request.getVideoUrl());

            }

            if (!TextUtils.isEmpty(request.getBanner())) {
                extras.putString(Constants.IMAGE_URL, request.getBanner());

            }

            if (!TextUtils.isEmpty(request.getVideo_address())) {
                extras.putString(Constants.VIDEO_PATH, request.getVideo_address());

            }

            if(!TextUtils.isEmpty(request.getmGroupBannerAddress())) {
                extras.putString(Constants.GROUP_IMAGE_PATH, request.getmGroupBannerAddress());

            }

            if(!TextUtils.isEmpty(request.getmGroupTitle())) {
                extras.putString(Constants.GROUP_TITLE, request.getmGroupTitle());

            }

            if(!TextUtils.isEmpty(request.getmGroupType())) {
                extras.putString(Constants.GROUP_TYPE, request.getmGroupType());

            }

            extras.putString(Constants.DATA,  new Gson().toJson(mDashBoardMentorDetails));

            extras.putString(Constants.TYPE, request.getType());
            if (!TextUtils.isEmpty(request.getmVideoDuration())) {
                extras.putString(Constants.VIDEO_DURATION, request.getmVideoDuration());
            }
            return extras;
        }

    }


    private class StartGroupImageDownload extends AsyncTask<Void, Void, Void> {

        private File outputFile= null;

        @Override
        protected Void doInBackground(Void... voids) {
            try {

                URL url = new URL(mGroupBannerImage);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection


                outputFile = new File(getApplicationContext().getFilesDir(),  mMentorId);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    boolean isCreated = outputFile.createNewFile();


                    if(isCreated){
                        Log.e(Constants.DOWNLOAD, "File Created");

                    }else{
                        Log.e(Constants.DOWNLOAD, "File not Created");

                    }
                }


                mGroupBannerImagePath = outputFile.getPath();


                FileOutputStream fos = getApplicationContext().openFileOutput(mMentorId, Context.MODE_PRIVATE);//Get OutputStream for NewFile Location


                InputStream is = c.getInputStream();//Get InputStream for connection
                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {

                    fos.write(buffer, 0, len1);//Write new file

                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {

                Logger.getLogger(Constants.LOG, e.toString());

                outputFile = null;
                Log.e(Constants.DOWNLOAD, "Download Error Exception " + e.getMessage());


            }
            return null;
        }

    }


    private void showDonwloadIcon(final DigitalCoaching items , final ProgressBar progrssBar, final ImageView downloadIcon, final ImageView downloadIconOrange, final ProgressBar mProgressBar) {


        if (!TextUtils.isEmpty(items.getVideo_url())
                && !items.getVideo_url().equals("NoVideoExist")
                && (mIsSubscribed || !items.getFree_paid().equals("Paid"))) {

            mFetch.getDownloads(result-> {
                    mDownLoadItemList = result;
                    checkForCourse(items, progrssBar, downloadIcon, downloadIconOrange,mProgressBar);
            });
        } else {
            progrssBar.setVisibility(GONE);
            downloadIconOrange.setVisibility(GONE);
        }
    }

    private void checkForCourse(DigitalCoaching mItem, ProgressBar mDownloadProgressBar, ImageView mIvDownloadGrey, ImageView mIvDownloadOrange,ProgressBar mProgressBar) {

        if (mDownLoadItemList != null && !mDownLoadItemList.isEmpty()) {
            for (Download downloadTask : mDownLoadItemList) {
                if (downloadTask.getExtras().getString(Constants.ID, "1").equals(mItem.getId())) {
                    if (downloadTask.getStatus() == Status.COMPLETED) {
                        mDownloadProgressBar.setVisibility(GONE);
                        mIvDownloadGrey.setVisibility(VISIBLE);
                        mIvDownloadOrange.setVisibility(GONE);
                        mProgressBar.setVisibility(GONE);
                    } else if (downloadTask.getStatus() == Status.FAILED
                            || downloadTask.getStatus() == Status.CANCELLED
                            || downloadTask.getStatus() == Status.DELETED
                            || downloadTask.getStatus() == Status.REMOVED
                    ) {
                        mDownloadProgressBar.setVisibility(GONE);
                        mIvDownloadGrey.setVisibility(GONE);
                        mIvDownloadOrange.setVisibility(VISIBLE);
                        mProgressBar.setVisibility(GONE);

                    } else {
                        mDownloadProgressBar.setVisibility(VISIBLE);
                        mIvDownloadGrey.setVisibility(GONE);
                        mIvDownloadOrange.setVisibility(GONE);
                        mProgressBar.setVisibility(GONE);

                    }
                    break;
                } else {
                    mDownloadProgressBar.setVisibility(GONE);
                    mIvDownloadGrey.setVisibility(GONE);
                    mIvDownloadOrange.setVisibility(VISIBLE);
                    mProgressBar.setVisibility(GONE);

                }
            }
        } else {
            mDownloadProgressBar.setVisibility(GONE);
            mIvDownloadGrey.setVisibility(GONE);
            mIvDownloadOrange.setVisibility(VISIBLE);
            mProgressBar.setVisibility(GONE);

        }

    }





    @Override
    public int getItemCount() {
        if(!mIsDownloaded){
            return mDigitalCoaching.size() + 1;

        }else{
            return mDigitalCoaching.size();

        }
    }

    //   * View holder class for items fo recycler view

    private class MyMobileViewHolder extends RecyclerView.ViewHolder {
        private TextView mDescription;
        private TextView mTitle;

        private ImageView mIvBannerImage;
        private Context mContext;
        private LinearLayout mCardView;
        private TextView mStripFreePaid;
        private ImageView mDownloadIconGrey;
        private ImageView mDownloadIconOrange;

        private ProgressBar mProgressDownloadBar;
        private ProgressBar mProgressBar;

        //  initialize data
        public MyMobileViewHolder(View view, Context context) {
            super(view);
            mContext = context;
            mTitle = view.findViewById(R.id.tv_title);
            mDescription = view.findViewById(R.id.tv_desc);
            mIvBannerImage = view.findViewById(R.id.iv_banner_image);
            mIvBannerImage.getLayoutParams().height = (int) Utils.getVideoImageHeight((Activity) context);
            mCardView = view.findViewById(R.id.ll_payment_contatiner);
            mStripFreePaid = view.findViewById(R.id.tv_strip_detail);
            mDownloadIconGrey = view.findViewById(R.id.tv_download_icon);
            mProgressDownloadBar = view.findViewById(R.id.download_progres_bar);
            mDownloadIconOrange = view.findViewById(R.id.tv_download_orange);
            mProgressBar=view.findViewById(R.id.progressBar);

        }


        // Set values to the list items
        void bind(final DigitalCoaching item, Context context, boolean isSelected, double imageWidth, boolean isDownloaded) {

            mIvBannerImage.getLayoutParams().height = (int) Utils.getVideoImageHeight((Activity) context);

            double height = mIvBannerImage.getLayoutParams().height;
            double width = imageWidth;


            String bigImageUrl = Utils.getImageUrl(item.getBanner(), width, height);




            if(isDownloaded){
                Uri uri = Uri.fromFile(new File(item.getmDownloadBanner()));
                Glide.with(mContext).load(uri).error(R.drawable.no_img_video).dontAnimate().into(mIvBannerImage);

            }else{
                item.setmDownloadBanner(bigImageUrl);
                Glide.with(mContext).load(bigImageUrl).error(R.drawable.no_img_video).dontAnimate().into(mIvBannerImage);

            }

            mTitle.setText(item.getTitle());
            if (!TextUtils.isEmpty(item.getDescription()) && !item.getDescription().equals("."))
            {
                mDescription.setVisibility(VISIBLE);
                mDescription.setText(item.getDescription());

            }
            else
            {
                mDescription.setVisibility(GONE);

            }

            if (!TextUtils.isEmpty(item.getFree_paid()) && item.getFree_paid().equals("Free")) {
                mStripFreePaid.setVisibility(View.VISIBLE);
                mStripFreePaid.setText(item.getFreePaidText());
                showDonwloadIcon(item, mProgressDownloadBar, mDownloadIconGrey, mDownloadIconOrange,mProgressBar);

            } else {
                mStripFreePaid.setVisibility(View.INVISIBLE);
                mDownloadIconGrey.setVisibility(GONE);
                mDownloadIconOrange.setVisibility(GONE);
                mProgressDownloadBar.setVisibility(GONE);
                mProgressBar.setVisibility(GONE);
                if(mIsSubscribed){
                    showDonwloadIcon(item, mProgressDownloadBar, mDownloadIconGrey, mDownloadIconOrange,mProgressBar);
                }
            }



            if(isSelected){

               mCardView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.selected_background_gray));

            }else{

                mCardView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorWhite));

            }

        }




    }

    private class MyTabletViewHolder extends RecyclerView.ViewHolder {
        private TextView mTitle;
        private TextView  mDescription;

        private ImageView mIvBannerImage;
        private Context mContext;
        private LinearLayout mCardView;
        private TextView mStripFreePaid;
        private ImageView mDownloadIconOrange;
        private ImageView mDownloadIconGrey;
        private ProgressBar mProgressBar;
        private ProgressBar mProgressDownloadBar;

        //  initialize data
        public MyTabletViewHolder(View view, Context context) {
            super(view);
            mContext = context;
            mTitle = view.findViewById(R.id.tv_title);
            mDescription = view.findViewById(R.id.tv_desc);
            mIvBannerImage = view.findViewById(R.id.iv_banner_image);
            mCardView = view.findViewById(R.id.ll_payment_contatiner);
            mStripFreePaid = view.findViewById(R.id.tv_strip_detail);
            mDownloadIconGrey = view.findViewById(R.id.tv_download_grey_icon);
            mProgressDownloadBar = view.findViewById(R.id.download_bar);
            mDownloadIconOrange = view.findViewById(R.id.tv_download_icon);
            mProgressBar=view.findViewById(R.id.progressBar);


        }


        // Set values to the list items
        void bind(final DigitalCoaching item, Context context, boolean isSelected, double imageWidth, boolean isDownloaded) {

            mIvBannerImage.getLayoutParams().height = (int) Utils.getVideoImageHeight((Activity) context);

            double height = mIvBannerImage.getLayoutParams().height;
            double width = imageWidth;

            String bigImageUrl = Utils.getImageUrl(item.getBanner(), width, height);


            if(isDownloaded){
                Glide.with(mContext).load(item.getmDownloadBanner()).error(R.drawable.no_img_video).dontAnimate().into(mIvBannerImage);

            }else{
                item.setmDownloadBanner(bigImageUrl);

                Glide.with(mContext).load(bigImageUrl).error(R.drawable.no_img_video).dontAnimate().into(mIvBannerImage);

            }


            if (!TextUtils.isEmpty(item.getFree_paid()) && item.getFree_paid().equals("Free")) {
                mStripFreePaid.setVisibility(View.VISIBLE);
                mStripFreePaid.setText(item.getFreePaidText());
                showDonwloadIcon(item, mProgressDownloadBar, mDownloadIconGrey, mDownloadIconOrange,mProgressBar);

            } else {
                mStripFreePaid.setVisibility(View.INVISIBLE);
                mDownloadIconGrey.setVisibility(GONE);
                mDownloadIconOrange.setVisibility(GONE);
                mProgressDownloadBar.setVisibility(GONE);
                mProgressBar.setVisibility(GONE);
                if(mIsSubscribed){
                    showDonwloadIcon(item, mProgressDownloadBar, mDownloadIconGrey, mDownloadIconOrange,mProgressBar);
                }
            }


            if (!TextUtils.isEmpty(item.getTitle())) {
                mTitle.setText(item.getTitle());

            }

            if (!TextUtils.isEmpty(item.getDescription()) && !item.getDescription().equals(".")) {
                mDescription.setVisibility(VISIBLE);
                mDescription.setText(item.getDescription());

            }
            else
            {
                mDescription.setVisibility(GONE);

            }

            if (isSelected) {
                mCardView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.selected_background_gray));

            } else {
                mCardView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhite));

            }

        }


    }


    private void showFooterViewHolder(int position, ReviewFooterViewHolder viewHolder) {
            //check whether to need to call next page from the server
            if (mDigitalCoaching != null && mPagingListener != null && mNextPage != 0 && position >= mDigitalCoaching.size() && mDigitalCoaching.size() < mItemsCountOnServer) {
                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {

                    // if there is server error
                    if (mServerError) {

                        viewHolder.showServerError();

                        //set the click listener to reload adapter on user click
                        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                    // set it to false again
                                    mServerError = false;
                                    notifyDataSetChanged();
                                    Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                                }
                            }
                        });

                    } else {

                        //remove the click event if loading items
                        viewHolder.itemView.setOnClickListener(null);
                        //show loading indicator
                        viewHolder.showLoading();

                        //fetch the new page from the server
                        mPagingListener.onGetItemFromApi(mNextPage, null, Constants.TYPE_SEARCH, null);
                    }
                } else {
                    //show the no internet error
                    viewHolder.showNoInternetError();

                    //set the click listener to reload adapter on user click
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                notifyDataSetChanged();
                                Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                            }
                        }
                    });
                }
        }


    }


    @Override
    public int getItemViewType(int position) {

        if (mDigitalCoaching != null) {
            final int size = mDigitalCoaching.size();

            if (position >= size) {
                if (size == 0) {
                    return TYPE_EMPTY;
                } else {
                    return (size >= mItemsCountOnServer) ? TYPE_EMPTY : TYPE_FOOTER;
                }
            } else {
                if (Utils.isTablet()) {
                    return TYPE_TABLET;

                }

                return TYPE_MOBILE;
            }


        }


        return super.getItemViewType(position);
    }


    private final FetchListener fetchListener = new AbstractFetchListener() {


        @Override
        public void onCompleted(@NotNull Download download) {

            int pos = mDigitalCoaching.size();

            for (int i = 0; i < pos; i++) {

                if (download.getExtras().getString(Constants.ID, "0").equals(mDigitalCoaching.get(i).getId())) {
                    RecyclerView.ViewHolder viewHolder = mRecycleView.findViewHolderForAdapterPosition(i);
                    if (!Utils.isTablet()) {
                        DigitalDetailListAdapter.MyMobileViewHolder holder = (DigitalDetailListAdapter.MyMobileViewHolder) viewHolder;
                        if (holder!=null && holder.mProgressDownloadBar != null) {
                            holder.mProgressDownloadBar.setVisibility(GONE);
                            holder.mDownloadIconGrey.setVisibility(VISIBLE);
                            holder.mDownloadIconOrange.setVisibility(GONE);
                            holder.mProgressBar.setVisibility(GONE);

                        }
                        Toast.makeText(getApplicationContext(), R.string.download_complete, Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi(R.string.download_complete+" "+Constants.STARTING_BOLD + mDigitalCoaching.get(i).getTitle() + " "+Constants.ENDING_BOLD + R.string.from_digital_coaching);


                    } else {
                        DigitalDetailListAdapter.MyTabletViewHolder holder = (DigitalDetailListAdapter.MyTabletViewHolder) viewHolder;
                        if (holder!=null && holder.mProgressDownloadBar != null) {
                            holder.mProgressDownloadBar.setVisibility(GONE);
                            holder.mDownloadIconGrey.setVisibility(VISIBLE);
                            holder.mDownloadIconOrange.setVisibility(GONE);
                            holder.mProgressBar.setVisibility(GONE);


                        }
                        Utils.callEventLogApi(R.string.download_complete+" "+Constants.STARTING_BOLD + mDigitalCoaching.get(i).getTitle() + " "+Constants.ENDING_BOLD + R.string.from_digital_coaching);
                        Toast.makeText(getApplicationContext(), R.string.download_complete, Toast.LENGTH_SHORT).show();

                    }


                }

            }

        }


        @Override
        public void onError(@NotNull Download download, @NotNull Error error, @org.jetbrains.annotations.Nullable Throwable throwable) {


            int pos = mDigitalCoaching.size();

            for (int i = 0; i < pos; i++) {

                if (download.getExtras().getString(Constants.ID, "0").equals(mDigitalCoaching.get(i).getId())) {
                    RecyclerView.ViewHolder viewHolder = mRecycleView.findViewHolderForLayoutPosition(i);
                    if (!Utils.isTablet()) {
                        DigitalDetailListAdapter.MyMobileViewHolder holder = (DigitalDetailListAdapter.MyMobileViewHolder) viewHolder;
                        if (holder!=null && holder.mProgressDownloadBar != null) {
                            holder.mProgressDownloadBar.setVisibility(GONE);
                            holder.mDownloadIconOrange.setVisibility(VISIBLE);
                            holder.mDownloadIconGrey.setVisibility(GONE);
                            holder.mProgressBar.setVisibility(GONE);


                        }

                        Toast.makeText(getApplicationContext(), R.string.download_failed, Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi(R.string.download_failed+" "+Constants.STARTING_BOLD + mDigitalCoaching.get(i).getTitle() + " "+Constants.ENDING_BOLD + R.string.digital_coaching);

                    } else {
                        DigitalDetailListAdapter.MyTabletViewHolder holder = (DigitalDetailListAdapter.MyTabletViewHolder) viewHolder;
                        if (holder!=null && holder.mProgressDownloadBar != null) {

                            holder.mProgressDownloadBar.setVisibility(GONE);
                            holder.mDownloadIconOrange.setVisibility(VISIBLE);
                            holder.mDownloadIconGrey.setVisibility(GONE);
                            holder.mProgressBar.setVisibility(GONE);


                        }
                        Toast.makeText(getApplicationContext(), R.string.download_failed, Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi(R.string.download_failed+" "+Constants.STARTING_BOLD + mDigitalCoaching.get(i).getTitle() + " "+Constants.ENDING_BOLD + R.string.digital_coaching);


                    }


                }

            }
        }


        @Override
        public void onProgress(@NotNull Download download, long etaInMilliSeconds, long downloadedBytesPerSecond) {

            int pos = mDigitalCoaching.size();

            for (int i = 0; i < pos; i++) {
                if (download.getExtras().getString(Constants.ID, "0").equals(mDigitalCoaching.get(i).getId())) {

                    RecyclerView.ViewHolder viewHolder = mRecycleView.findViewHolderForLayoutPosition(i);
                    if (!Utils.isTablet()) {
                        DigitalDetailListAdapter.MyMobileViewHolder holder = (DigitalDetailListAdapter.MyMobileViewHolder) viewHolder;
                        if (holder!=null && holder.mProgressDownloadBar != null) {

                            holder.mProgressBar.setVisibility(GONE);
                            holder.mProgressDownloadBar.setVisibility(VISIBLE);
                            holder.mDownloadIconGrey.setVisibility(GONE);
                            holder.mDownloadIconOrange.setVisibility(GONE);
                            holder.mProgressDownloadBar.setProgress(download.getProgress());
                        }
                    } else {
                        DigitalDetailListAdapter.MyTabletViewHolder holder = (DigitalDetailListAdapter.MyTabletViewHolder) viewHolder;
                        if (holder!=null && holder.mProgressDownloadBar != null) {
                            holder.mProgressBar.setVisibility(GONE);
                            holder.mProgressDownloadBar.setVisibility(VISIBLE);
                            holder.mDownloadIconGrey.setVisibility(GONE);
                            holder.mDownloadIconOrange.setVisibility(GONE);
                            holder.mProgressDownloadBar.setProgress(download.getProgress());
                        }
                    }

                }
            }

        }

        @Override
        public void onRemoved(@NotNull Download download) {

            int pos = mDigitalCoaching.size();

            for (int i = 0; i < pos; i++) {
                if (download.getExtras().getString(Constants.ID, "0").equals(mDigitalCoaching.get(i).getId())) {

                    RecyclerView.ViewHolder viewHolder = mRecycleView.findViewHolderForLayoutPosition(i);
                    if (!Utils.isTablet()) {
                        DigitalDetailListAdapter.MyMobileViewHolder holder = (DigitalDetailListAdapter.MyMobileViewHolder) viewHolder;
                        if (holder!=null && holder.mProgressDownloadBar != null) {
                            holder.mProgressDownloadBar.setVisibility(GONE);
                            holder.mDownloadIconOrange.setVisibility(GONE);
                            holder.mDownloadIconGrey.setVisibility(GONE);
                            holder.mDownloadIconOrange.setVisibility(VISIBLE);


                        }
                    } else {
                        DigitalDetailListAdapter.MyTabletViewHolder holder = (DigitalDetailListAdapter.MyTabletViewHolder) viewHolder;
                        if (holder!=null && holder.mProgressDownloadBar != null) {
                            holder.mProgressDownloadBar.setVisibility(GONE);
                            holder.mDownloadIconOrange.setVisibility(GONE);
                            holder.mDownloadIconGrey.setVisibility(GONE);
                            holder.mDownloadIconOrange.setVisibility(VISIBLE);

                        }
                    }


                }

            }

        }

        @Override
        public void onDeleted(@NotNull Download download) {

            int pos = mDigitalCoaching.size();

            for (int i = 0; i < pos; i++) {
                if (download.getExtras().getString(Constants.ID, "0").equals(mDigitalCoaching.get(i).getId())) {
                    RecyclerView.ViewHolder viewHolder = mRecycleView.findViewHolderForLayoutPosition(i);
                    if (!Utils.isTablet()) {
                        DigitalDetailListAdapter.MyMobileViewHolder holder = (DigitalDetailListAdapter.MyMobileViewHolder) viewHolder;
                        if (holder!=null && holder.mProgressDownloadBar != null) {

                            holder.mProgressDownloadBar.setVisibility(GONE);
                            holder.mDownloadIconOrange.setVisibility(GONE);
                            holder.mDownloadIconGrey.setVisibility(GONE);
                            holder.mDownloadIconOrange.setVisibility(VISIBLE);


                        }
                    } else {
                        DigitalDetailListAdapter.MyTabletViewHolder holder = (DigitalDetailListAdapter.MyTabletViewHolder) viewHolder;
                        if (holder!=null && holder.mProgressDownloadBar != null) {

                            holder.mProgressDownloadBar.setVisibility(GONE);
                            holder.mDownloadIconOrange.setVisibility(GONE);
                            holder.mDownloadIconGrey.setVisibility(GONE);
                            holder.mDownloadIconOrange.setVisibility(VISIBLE);


                        }


                    }

                }
            }
        }




    };
}
