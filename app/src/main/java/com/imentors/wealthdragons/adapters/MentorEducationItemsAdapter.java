package com.imentors.wealthdragons.adapters;


import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.List;

public class MentorEducationItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    private List<DashBoardMentorDetails.Education> mNewItemList;
    private Context mContext;


    public MentorEducationItemsAdapter(List<DashBoardMentorDetails.Education> newItemList, Context context) {
        this.mNewItemList = newItemList;
        mContext = context;
    }






        @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view;

            view = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.education_list_item, parent, false);
            return new MyViewHolder(view);


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


            MyViewHolder viewHolder = (MyViewHolder) holder;
            viewHolder.bind(mNewItemList.get(position),mContext);

    }




    @Override
    public int getItemCount() {
        return mNewItemList.size();
    }

    //   * View holder class for items fo recycler view
    private static class MyViewHolder extends RecyclerView.ViewHolder{

        private WealthDragonTextView mTvDescription,mTvDegreeName,mTvOrganisation,mTvEducationTime;


        //  initialize view
        public MyViewHolder(View view ) {
            super(view);
            mTvDegreeName =  view.findViewById(R.id.tv_degree_name);
            mTvOrganisation =  view.findViewById(R.id.tv_organisation_name);
            mTvEducationTime = view.findViewById(R.id.tv_education_time);
            mTvDescription = view.findViewById(R.id.description);

        }



        // Set values to the list items
        void bind(DashBoardMentorDetails.Education item , Context context){

            if(TextUtils.isEmpty(item.getOrganisation_name())){
                mTvOrganisation.setVisibility(View.GONE);

            }else{
                mTvOrganisation.setVisibility(View.VISIBLE);

                mTvOrganisation.setText(item.getOrganisation_name());

            }


            if(!TextUtils.isEmpty(item.getEducation_from()) && !TextUtils.isEmpty(item.getEducation_to())){
                mTvEducationTime.setVisibility(View.VISIBLE);

                mTvEducationTime.setText(item.getEducation_from() +" to "+ item.getEducation_to());

            }else{
                mTvEducationTime.setVisibility(View.GONE);

            }


            if(TextUtils.isEmpty(item.getDegree_name())){
                mTvDegreeName.setVisibility(View.GONE);
            }else{
                mTvDegreeName.setVisibility(View.VISIBLE);

                mTvDegreeName.setText(item.getDegree_name());

            }

            if(TextUtils.isEmpty(item.getDescription())){
                mTvDescription.setVisibility(View.GONE);

            }else{
                mTvDescription.setVisibility(View.VISIBLE);
                mTvDescription.setText(item.getDescription());

            }




        }


    }


}
