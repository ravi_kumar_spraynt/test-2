package com.imentors.wealthdragons.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.DashBoardSlider;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.util.ArrayList;
import java.util.List;


public class SlidingImageAdapter extends RecyclerView.Adapter<SlidingImageAdapter.SliderViewHolder> {


    private List<DashBoardSlider> mSliderImagesList;
    private LayoutInflater inflater;
    private Context mContext;
    //ref to the item click listener
    private final OnItemClickListener mOnItemClickListener ;
    private boolean isTablet;

    public SlidingImageAdapter(SlidingImageAdapter.OnItemClickListener onItemClickListener, Context context) {
        mOnItemClickListener =  onItemClickListener;
        mContext= context;
        isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);
    }



    /**
     * different click handled by onItemClick , onNetworkClick and onBookmarkClick
     */
    public interface OnItemClickListener {
        void onSliderImageClick(DashBoardSlider item, int position);
    }

    public void setItems( final List<DashBoardSlider> sliderImageList) {

       if(sliderImageList.size()>0){

           if(isTablet){

               if(sliderImageList.size()>2) {

                   mSliderImagesList = new ArrayList<>();
                   mSliderImagesList.add(sliderImageList.get(sliderImageList.size() - 2));
                   mSliderImagesList.add(sliderImageList.get(sliderImageList.size() - 1));
                   mSliderImagesList.addAll(sliderImageList);
                   mSliderImagesList.add(sliderImageList.get(0));
                   mSliderImagesList.add(sliderImageList.get(1));
               }else{
                   mSliderImagesList = new ArrayList<>();
                   mSliderImagesList.addAll(sliderImageList);
               }

           }else {

               if(sliderImageList.size()>1) {

                   mSliderImagesList = new ArrayList<>();
                   mSliderImagesList.add(sliderImageList.get(sliderImageList.size() - 1));
                   mSliderImagesList.addAll(sliderImageList);
                   mSliderImagesList.add(sliderImageList.get(0));
               }else{
                   mSliderImagesList = new ArrayList<>();
                   mSliderImagesList.addAll(sliderImageList);
               }
           }

       }
        notifyDataSetChanged();
    }




    @NonNull
    @Override
    public SliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_slider_item, parent, false);

        return new SliderViewHolder(itemView,mOnItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull SliderViewHolder holder, int position) {


        DashBoardSlider item = mSliderImagesList.get(position);
            //cast holder to ItemViewHolder and set data for header.

        holder.bind(item,position);

        holder.sliderImage.getLayoutParams().width = (int) Utils.getSliderImageWidth((Activity) mContext);
        holder.sliderImage.getLayoutParams().height = (int) Utils.getSliderImageHeight((Activity) mContext);


        double height =  holder.sliderImage.getLayoutParams().height;
        double width =  holder.sliderImage.getLayoutParams().width;

        String newImageUrl=Utils.getImageUrl(item.getMobile_banner(),width,height);
        String thumbImage=Utils.getImageUrl(item.getLow_qty_banner(),width,height);

        if(mContext != null) {
            Glide.with(mContext).load(newImageUrl).thumbnail(Glide.with(mContext).load(thumbImage).dontAnimate()).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.no_img_banner).into(holder.sliderImage);
//            holder.sliderImage.setImageURI(newImageUrl);
        }


    }

    @Override
    public int getItemCount() {
        if(mSliderImagesList==null){
            return 0;
        }else {
            return mSliderImagesList.size();
        }
    }


    //  cleared items
    public void clearItems(){
        if(mSliderImagesList!=null){
            mSliderImagesList.clear();
            notifyDataSetChanged();
        }
    }




    /**
     * View holder class for items fo recycler view
     */
    public class SliderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        //view on click listener need to forward click events
        private final SlidingImageAdapter.OnItemClickListener mOnItemClickListener;
        // current bind to view holder
        private DashBoardSlider mCurrentItem;

        public ImageView sliderImage;
        //position
        private int mPosition;



        SliderViewHolder(@NonNull View view, final SlidingImageAdapter.OnItemClickListener listener) {
            super(view);
            mOnItemClickListener = listener;

            sliderImage = view.findViewById(R.id.slider_image);

            view.setOnClickListener(this);



        }

            /**
             * Bind the the values of the view holder
             *
             * @param item     article item
             * @param position position
             */
            void bind(final DashBoardSlider item, final int position) {
                mCurrentItem = item;
                mPosition = position;
                }


        @Override
        public void onClick(View v) {
            if(mCurrentItem != null && mOnItemClickListener!=null){
                mOnItemClickListener.onSliderImageClick(mCurrentItem, mPosition);
            }
        }
    }







}
 
 