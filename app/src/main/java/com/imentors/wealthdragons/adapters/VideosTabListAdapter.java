package com.imentors.wealthdragons.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.models.Video;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.EmptyViewHolder;
import com.imentors.wealthdragons.views.FooterViewHolder;
import com.imentors.wealthdragons.views.VideoView;
import java.util.ArrayList;
import java.util.List;


/**
 * Created on 02-02-2016.
 */
public class VideosTabListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //article list view
    private static final int TYPE_VIDEO = 0;

    //Type footer and empty
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_EMPTY = 2;


    //ref to the item click listener
    private final OnItemClickListener mOnItemClickListener;


    //ref to the Mentors items
    private List<Video> mItemsVideos;


    private String  mTasteId;

    private Context mContext;

    private int  mItemsCountOnServer;
    private int   mNextPage;


    private boolean  mServerError;
    private boolean  mIsSubCategory = false;

    private PagingListener mPagingListener;


    public VideosTabListAdapter(OnItemClickListener onItemClickListener, PagingListener pagingListener, Context context) {
        mOnItemClickListener = onItemClickListener;
        mContext = context;
        mPagingListener = pagingListener;

    }


    /**
     * set the article items
     *
     * @param itemsVideo video items
     */
    public void setItems(final List<Video> itemsVideo, final int nextPage, final int itemsCountOnServer, final String tasteId) {


        if (itemsVideo != null) {
            mItemsVideos = new ArrayList<>();
            mItemsVideos.addAll(itemsVideo);
        }


        mNextPage = nextPage;
        mTasteId = tasteId;
        mItemsCountOnServer = itemsCountOnServer;

        notifyDataSetChanged();
    }


    //  add items
    public void addItems(final List<Video> itemsVideo, final int nextPage, final int itemsCountOnServer, final String tasteId ) {
        if (mItemsVideos != null) {
            mItemsVideos.addAll(itemsVideo);
        }

        mIsSubCategory = false;


        mNextPage = nextPage;
        mTasteId = tasteId;
        mItemsCountOnServer = itemsCountOnServer;

        notifyDataSetChanged();

    }


    //  clear data
    public void clearData(int position) {

        if (position == Constants.VIDEO_TAB && mItemsVideos != null) {

            mItemsVideos.clear();

        }


        notifyDataSetChanged();

    }


    public void isSubCategoryFilterData(){
        mIsSubCategory = true;
    }


    //  set server error
    public void setServerError() {
        mServerError = true;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {

        View view;
        switch (viewType) {
            case TYPE_VIDEO:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.video_list_item, parent, false);
                return new VideoViewHolder(view, mOnItemClickListener);
            case TYPE_FOOTER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_article_list_item_footer, parent, false);
                return new FooterViewHolder(view);

            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_article_list_item_empty, parent, false);
                return new EmptyViewHolder(view);
        }


    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof VideosTabListAdapter.VideoViewHolder) {
            Video item = mItemsVideos.get(position);
            //cast holder to ItemViewHolder and set data for header.
            VideoViewHolder viewHolder = (VideoViewHolder) holder;
            viewHolder.bind(item, position, mItemsVideos.size(), mContext);

        } else if (holder instanceof FooterViewHolder) {
            //cast holder to FooterViewHolder and set data for header.
            FooterViewHolder viewHolder = (FooterViewHolder) holder;
            showFooterViewHolder(position, viewHolder);

        }

    }


    @Override
    public int getItemViewType(int position) {
        if (position >= mItemsVideos.size()) {
            final int size = mItemsVideos.size();
            if (size == 0) {
                if(mIsSubCategory){
                    return TYPE_FOOTER;
                }
                return TYPE_EMPTY;
            } else {
                return (size >= mItemsCountOnServer) ? TYPE_EMPTY : TYPE_FOOTER;
            }
        } else {
            return TYPE_VIDEO;
        }
    }


    @Override
    public int getItemCount() {

        if (mItemsVideos != null) {

            //plus one for footer
            return mItemsVideos.size() + 1;
        }

        return 0;

    }


    /**
     * different click handled by onItemClick , onNetworkClick and onBookmarkClick
     */
    public interface OnItemClickListener {
        void onVideoClick(Video item, int position);

        void setHeader(String title, String imageHeader, int listSize, int tabPosition);

    }


    /**
     * View holder class for items fo recycler view
     */
    private static class VideoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //view on click listener need to forward click events
        private final VideosTabListAdapter.OnItemClickListener mOnItemClickListener;
        private final VideoView mVideoBannerLayout;
        // current bind to view holder
        private Video mCurrentItem;
        //position
        private int mPosition;



        VideoViewHolder(@NonNull View view, final VideosTabListAdapter.OnItemClickListener listener) {
            super(view);
            mOnItemClickListener = listener;
            view.setOnClickListener(this);

            mVideoBannerLayout = view.findViewById(R.id.dashboard_video_banner);


        }

        @Override
        public void onClick(View view) {
            if (mCurrentItem != null && mOnItemClickListener != null) {
                mOnItemClickListener.onVideoClick(mCurrentItem, mPosition);
            }
        }

        /**
         * Bind the the values of the view holder
         *
         * @param item     article item
         * @param position position
         */
        void bind(final Video item, final int position, int totalItems, Context mContext) {
            mCurrentItem = item;
            mPosition = position;

            if (totalItems - 1 == position && totalItems > 1) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins((int) mContext.getResources().getDimension(R.dimen.margin_default), 0, (int) mContext.getResources().getDimension(R.dimen.margin_default), 0);
                itemView.findViewById(R.id.card_view).setLayoutParams(params);
            }
            mVideoBannerLayout.bind(item, null, Constants.VIDEO_TAB);
        }
    }

    private void showFooterViewHolder(int position, FooterViewHolder viewHolder) {
        if (mItemsVideos != null) {
            //check whether to need to call next page from the server
            if (mPagingListener != null && mNextPage != 0 && position >= mItemsVideos.size() && mItemsVideos.size() < mItemsCountOnServer && !mIsSubCategory) {
                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {

                    // if there is server error
                    if (mServerError) {

                        viewHolder.showServerError(0);

                        //set the click listener to reload adapter on user click
                        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                    // set it to false again
                                    mServerError = false;
                                    notifyDataSetChanged();
                                }
                                Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                            }
                        });

                    } else {

                        //remove the click event if loading items
                        viewHolder.itemView.setOnClickListener(null);
                        //show loading indicator
                        viewHolder.showLoading(0);

                        //fetch the new page from the server
                        mPagingListener.onGetItemFromApi(mNextPage, mTasteId, Constants.TYPE_VIDEO,null);
                    }
                } else {
                    //show the no internet error
                    viewHolder.showNoInternetError(0);

                    //set the click listener to reload adapter on user click
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                notifyDataSetChanged();
                            }
                        }
                    });
                }


            }else if(mPagingListener != null && mIsSubCategory){
                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {

                    // if there is server error
                    if(mServerError){

                        viewHolder.showServerError((int)Utils.getVideoImageHeight((Activity)mContext));

                        //set the click listener to reload adapter on user click
                        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                    // set it to false again
                                    mServerError = false;
                                    mPagingListener.onGetItemFromApi(mNextPage, mTasteId, Constants.TYPE_VIDEO,null);
                                    notifyDataSetChanged();

                                }
                            }
                        });

                    }else {

                        //remove the click event if loading items
                        viewHolder.itemView.setOnClickListener(null);
                        //show loading indicator
                        viewHolder.showLoading((int)Utils.getVideoImageHeight((Activity)mContext));
                    }
                } else {
                    //show the no internet error
                    viewHolder.showNoInternetError((int)Utils.getVideoImageHeight((Activity)mContext));

                    //set the click listener to reload adapter on user click
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                mPagingListener.onGetItemFromApi(mNextPage, mTasteId, Constants.TYPE_VIDEO,null);
                                notifyDataSetChanged();
                                Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                            }
                        }
                    });
                }

            }
        }


    }
}
