package com.imentors.wealthdragons.adapters;


import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.SearchAutoComplete;

import java.util.List;

public class AutoSearchListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    private List<SearchAutoComplete> autoSearchItemList;
    private OnTopSearchesClick mOnItemClickListener;
    private Context mContext;



    //  calling adapter
    public AutoSearchListAdapter(List<SearchAutoComplete> autoSearchItemList, OnTopSearchesClick onItemClickListener,Context context) {

        this.autoSearchItemList = autoSearchItemList;
        mOnItemClickListener = onItemClickListener;
        mContext = context;
    }



    /**
     * different click handled by onItemClick
     */
    public interface OnTopSearchesClick {
        void onTopSearchItemClick(SearchAutoComplete item);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.auto_search_list_item, parent, false);

        return new MyViewHolder(itemView,mOnItemClickListener,mContext);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder viewHolder = (MyViewHolder) holder;
        viewHolder.bind(autoSearchItemList.get(position),position,autoSearchItemList.size());

    }


    @Override
    public int getItemCount() {
        return autoSearchItemList.size();
    }

    //   * View holder class for items fo recycler view
    private static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView title;
        private OnTopSearchesClick onItemClickListener;
        private SearchAutoComplete mItem;
        private ImageView mTick;
        private ImageView mIvSearchItemType;
        private View mDivider;
        private Context mContext;


        //  initialize data
        public MyViewHolder(View view , OnTopSearchesClick listener,Context context) {
            super(view);
            title =  view.findViewById(R.id.tv_search_item_name);
            mTick=view.findViewById(R.id.tick);
            mDivider=view.findViewById(R.id.divider);
            mIvSearchItemType = view.findViewById(R.id.iv_search_item_type);

            view.setOnClickListener(this);
            onItemClickListener = listener;
            mContext = context;

        }



        // Set values to the list items
        void bind(final SearchAutoComplete item, final int position,int length){
            if(item!=null) {

                mItem = item;
                int mPosition = position;

                if (!TextUtils.isEmpty(item.getName())) {


                    if (item.getTick().equals("Yes")) {
                        mTick.setVisibility(View.VISIBLE);

                    } else {

                        mTick.setVisibility(View.GONE);

                    }

                    title.setText(item.getName());

                    if (TextUtils.isEmpty(item.getIcon())) {
                        switch (item.getType()) {
                            case "Expert":
                                mIvSearchItemType.setImageResource(R.drawable.icon_user);
                                break;
                            case "Video":
                                mIvSearchItemType.setImageResource(R.drawable.icon_plays);
                                break;
                            case "Course":
                                mIvSearchItemType.setImageResource(R.drawable.icon_book);
                                break;
                            case "Tag":
                                mIvSearchItemType.setImageResource(R.drawable.icon_tags);
                                break;
                            default:
                                mIvSearchItemType.setImageResource(R.drawable.icon_book);
                                break;


                        }
                    } else {
                        Glide.with(mContext).load(item.getIcon()).dontAnimate().into(mIvSearchItemType);
                    }
                }

                if (mPosition == length - 1) {
                    mDivider.setVisibility(View.GONE);

                } else {
                    mDivider.setVisibility(View.VISIBLE);
                }
            }


        }
        @Override
        public void onClick(View v) {
            if(onItemClickListener != null){
                onItemClickListener.onTopSearchItemClick(mItem);
            }
        }
    }
}
