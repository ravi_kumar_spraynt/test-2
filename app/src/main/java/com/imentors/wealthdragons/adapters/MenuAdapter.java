package com.imentors.wealthdragons.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.Categories;
import com.imentors.wealthdragons.models.Category;
import com.imentors.wealthdragons.models.MenuItem;
import com.imentors.wealthdragons.utils.AppPreferences;

import java.util.HashMap;
import java.util.List;


/**
 * Created on 7/10/17.
 */
public class MenuAdapter extends BaseAdapter {

    @SuppressWarnings("CanBeFinal")
    private List<Category> mItems;
    @SuppressWarnings("CanBeFinal")
    private LayoutInflater mInflater;
    private Context mContext = null;
    private boolean isStaticData;
    private int mSizeOfOnlineData;

    public MenuAdapter(Context context) {
        mContext= context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }




    public void setItems(List<Category> items , boolean is_static_data ){
        mItems = items;
        isStaticData = is_static_data;
        mSizeOfOnlineData = AppPreferences.getOnlineCategoryCount();
    }

    /**
     * Return the count
     *
     * @return count
     */
    public int getCount() {
        if (mItems == null) {
            return 0;
        }

        return mItems.size();
    }

    /**
     * Return the Object item
     *
     * @return Object
     * @param position position
     */
    public Object getItem(int position) {

        return mItems.get(position);
    }

    /**
     * Return the Item Id
     *
     * @return Object
     * @param position position
     */


    public long getItemId(int position) {
        return 0;
    }





    public void clearData(){
        if(mItems!=null) {
            mItems.clear();
            notifyDataSetChanged();
        }
    }




    /**
     * Return the View
     *
     * @return View
     * @param position position
     * @param convertView convert view
     * @param parent parent
     */

    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.fragment_menu_item, parent, false);
            viewHolder = new ViewHolder();

            // Initialize different views
            viewHolder.nameView = convertView.findViewById(R.id.txt_name);
            viewHolder.nameView.setAllCaps(true);
            viewHolder.imageView = convertView.findViewById(R.id.imageView);
            // Set tag
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Set values to the list items
        final Category item = mItems.get(position);

        // static data is added in the int location form in string thus converting back to int
        if(isStaticData) {
            viewHolder.nameView.setText(mContext.getString(Integer.parseInt(item.getTitle())));
            if(mContext!=null) {
                Glide.with(mContext).load(Integer.parseInt(item.getBanner())).into(viewHolder.imageView);
            }
        }else{

            if(position>=mSizeOfOnlineData){
                viewHolder.nameView.setText(mContext.getString(Integer.parseInt(item.getTitle())));
                if(mContext!=null) {
                    Glide.with(mContext).load(Integer.parseInt(item.getBanner())).into(viewHolder.imageView);
                }
            }else{
                viewHolder.nameView.setText(item.getTitle());
                if(mContext!=null) {
                    Glide.with(mContext).load(item.getBanner()).into(viewHolder.imageView);
                }
            }

        }
        return convertView;
    }

    /**
     * View Holder class
     */
    private static class ViewHolder implements View.OnClickListener{
        TextView nameView;
        ImageView imageView;

        @Override
        public void onClick(View v) {

        }
    }
}
