package com.imentors.wealthdragons.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.models.SubCategory;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.util.List;

public class SubCategoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    private List<SubCategory> mSubCategoryList;
    private onSubCategoryClick mOnItemClickListener;
    private PagingListener mPagingListener;
    private Context mContext;
    private String mSeeAllApiKey , mCatId;
    private String mViewType;
    private boolean mIsSeeAllFragment , isTablet;

    public SubCategoryListAdapter(onSubCategoryClick onItemClickListener, PagingListener pagingListener, Context context , boolean isSeeAllFragment) {
        mOnItemClickListener = onItemClickListener;
        mPagingListener = pagingListener;
        mContext = context;
        // required for not calling on
        mIsSeeAllFragment = isSeeAllFragment;
        isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);

    }


    //  set the items
    public void setItems(List<SubCategory> subCategoryList, final String seeAllApiKey ,final String viewType , final String catID){
        this.mSubCategoryList = subCategoryList;
        this.mSeeAllApiKey = seeAllApiKey;
        this.mViewType = viewType;
        this.mCatId = catID;
        notifyDataSetChanged();
    }



    /**
     * different click handled by onItemClick
     */
    public interface onSubCategoryClick {
        void onSubCategoryItemClick(SubCategory item,String viewType);
        void onSubCategorySeeAllFilterClick(SubCategory item , String parentId);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_subcategory_list_item, parent, false);

        return new MyViewHolder(itemView,mOnItemClickListener,mPagingListener,mSeeAllApiKey,mViewType,mIsSeeAllFragment,mCatId);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder viewHolder = (MyViewHolder) holder;
        viewHolder.bind(mSubCategoryList.get(position),position,mSubCategoryList.size(),isTablet);
    }


    @Override
    public int getItemCount() {
        return mSubCategoryList.size();
    }

    private static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title;
        private onSubCategoryClick onItemClickListener;
        private SubCategory mItem;
        private int mPosition ,size;
        private PagingListener mPagingListener;
        private String mSeeAllApiKey;
        private String mViewType , mCatId;
        private boolean mIsSeeAllFragment;
        private View mOrangeLine;


        //  initialize view
        public MyViewHolder(View view , onSubCategoryClick listener , PagingListener pagingListener , String seeAllKey ,String viewType, boolean isSeeAllFragment ,String catId) {
            super(view);
            title =  view.findViewById(R.id.txt_name);
            mOrangeLine = view.findViewById(R.id.below_line);
            view.setOnClickListener(this);
            onItemClickListener = listener;
            mPagingListener = pagingListener;
            mSeeAllApiKey = seeAllKey;
            mViewType = viewType;
            mIsSeeAllFragment = isSeeAllFragment;
            mCatId = catId;
        }



        // Set values to the list items
        void bind(final SubCategory item, final int position , final int size , final boolean isTablet){
            mItem= item;
            mPosition = position;
            title.setText(item.getTitle());
            if(isTablet) {
                //hide line below both items
                if(size%2==0){
                    if (position == size - 1 || position == size - 2) {
                        mOrangeLine.setVisibility(View.INVISIBLE);
                    } else {
                        mOrangeLine.setVisibility(View.VISIBLE);
                    }
                }else{
                    if (position == size - 1) {
                        mOrangeLine.setVisibility(View.INVISIBLE);
                    } else {
                        mOrangeLine.setVisibility(View.VISIBLE);
                    }
                }

            }else{
                if (position == size - 1) {
                    mOrangeLine.setVisibility(View.INVISIBLE);
                } else {
                    mOrangeLine.setVisibility(View.VISIBLE);
                }
            }
        }


        @Override
        public void onClick(View v) {
            if(onItemClickListener != null){
                if(mIsSeeAllFragment){
                    onItemClickListener.onSubCategorySeeAllFilterClick(mItem , mCatId);
                }else{
                    onItemClickListener.onSubCategoryItemClick(mItem,mViewType);
                    mPagingListener.onGetItemFromApi(1,mItem.getId(),mViewType,mSeeAllApiKey);
                }
            }

        }
    }
}
