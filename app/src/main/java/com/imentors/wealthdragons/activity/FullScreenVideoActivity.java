package com.imentors.wealthdragons.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.DashBoardVideoDetails;
import com.imentors.wealthdragons.models.VideoData;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FullScreenVideoActivity extends AppCompatActivity {


    private int mResumeWindow;
    private long mResumePosition;
    private String mVideoUrl = null;
    private PlayerView mExoPlayerView;
    private SimpleExoPlayer mPlayer;
    private boolean isVideoLoadingError = false;
    private boolean  mIsCourseDetail = false;
    private TextView mVideoError;
    private RelativeLayout mLlHeadingContainer;
    private List<String> mVideoUrls = new ArrayList<>();
    private List<Chapters.EClasses> eClassesArrayList = new ArrayList<>();
    private LinearLayout nextVideoFrame;
    private TextView mTvEClassName;
    private String mVideoTitle;
    private Timer mTimerNextVideo = new Timer();
    final Handler mNextVideoHandler = new Handler();
    private boolean mShowNextVideoWithImageFrame;
    private boolean mShowController;
    private boolean mShowProgressBarDefinite = true;
    private DashBoardCourseDetails mDashBoardCourseDetails;
    private DashBoardVideoDetails mDashBoardVideoDetails;
    private Timer saveSeekTime;
    private String saveVideoTimeResponse = null;
    private ProgressBar mProgressBar;
    private ProgressBar mDefiniteProgressBar;
    private View mDecorView;
    private ImageView mPosterImage;
    private int mProgressBarPosition;
    private Thread mProgreeBarThread;
    private Handler mProgressBarHandler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppPreferences.setVideoFullScreenActivityState(true);


        Intent intentData = getIntent();
        //checking for extras in form of bundle
        Bundle bundleData = intentData.getExtras();


        mResumeWindow = bundleData.getInt(Constants.STATE_RESUME_WINDOW);
        mResumePosition = bundleData.getLong(Constants.STATE_RESUME_POSITION);
        mVideoUrls = getIntent().getStringArrayListExtra(Constants.VIDEO_URL_LIST);
        mDashBoardCourseDetails = (DashBoardCourseDetails) getIntent().getSerializableExtra(Constants.DASHBOARD_COURSE_DATA);
        mDashBoardVideoDetails = (DashBoardVideoDetails) getIntent().getSerializableExtra(Constants.DASHBOARD_VIDEO_DATA);
        mVideoTitle = bundleData.getString(Constants.VIDEO_TITLE);
        mIsCourseDetail = getIntent().getBooleanExtra(Constants.IS_COURSEDETAIL, false);
        String mThumbUrl = bundleData.getString(Constants.THUMB_URL);
        String mPosterImageUrl = bundleData.getString(Constants.POSTER_IMAGE_URL);

        if (mDashBoardCourseDetails != null && mDashBoardCourseDetails.getCourseDetail().getChapters() != null
                && !mDashBoardCourseDetails.getCourseDetail().getChapters().isEmpty()) {


            for (Chapters chapters : mDashBoardCourseDetails.getCourseDetail().getChapters()) {
                for (Chapters.EClasses eClasses : chapters.getE_classes()) {
                    if (eClasses.getType().equals(Constants.TYPE_DETAIL_VIDEO) && !TextUtils.isEmpty(eClasses.getPlay_url())) {
                        eClassesArrayList.add(eClasses);
                    }
                }
            }
        }

        mDecorView = getWindow().getDecorView();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        hideSystemUI();


        setContentView(R.layout.activity_video_fullscreen);
        mPosterImage = findViewById(R.id.iv_poster_image);
        mDefiniteProgressBar = findViewById(R.id.circularProgressbar);
        Glide.with(this).load(mPosterImageUrl).thumbnail(Glide.with(this).load(mThumbUrl).dontAnimate()).into(mPosterImage);

        mVideoError = findViewById(R.id.video_error);
        mExoPlayerView = findViewById(R.id.exoplayer_fullScreen);
        RelativeLayout videoPlayerView = mExoPlayerView.findViewById(R.id.ll_video_playerview);
        LinearLayout videoPlayerNavigaitonContorller = videoPlayerView.findViewById(R.id.ll_video_nav_controler);

        boolean hasSoftKey = ViewConfiguration.get(this).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        boolean hasHomeKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_HOME);
        int res = this.getResources().getIdentifier("navigation_bar_height", "dimen", "android");

        if(!hasSoftKey && !hasHomeKey && !hasBackKey && res>0) {
            // for navigation bar which are included in the screen
            int navHeight = this.getResources().getDimensionPixelSize(res);
            int sideMargin = navHeight + Utils.dpToPx((int) getResources().getDimension(R.dimen.margin_default));
            videoPlayerNavigaitonContorller.setPadding(navHeight, 0, sideMargin, 0);
        }




        mProgressBar = findViewById(R.id.progress_bar);
        ImageView mIvBack = findViewById(R.id.iv_back_button);
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mLlHeadingContainer = findViewById(R.id.header_holder);
        initExoPlayer();

        // for handling when to show progress bar definite
        if (mResumePosition > mPlayer.getDuration() - 10 * 1000 && mPlayer.getNextWindowIndex() != -1 && mPlayer.getRepeatMode() == Player.REPEAT_MODE_ALL) {
            mShowProgressBarDefinite = false;
        }


        nextVideoFrame = findViewById(R.id.next_video_image_holder);
        mTvEClassName = findViewById(R.id.tv_eclasses_title);
        String videoUrl = mVideoUrls.get(mExoPlayerView.getPlayer().getCurrentWindowIndex());

        if (mIsCourseDetail) {
            for (Chapters.EClasses eClass : eClassesArrayList) {
                if (videoUrl.equals(eClass.getPlay_url())) {
                    mVideoTitle = eClass.getTitle();
                }
            }
            mTvEClassName.setText(mVideoTitle);

        } else {
            mTvEClassName.setText(mDashBoardVideoDetails.getVideoDetail().getTitle());

        }




        ImageView previewImage = findViewById(R.id.iv_video_image);

        if (!TextUtils.isEmpty(mThumbUrl)) {
            Glide.with(this).load(mThumbUrl).into(previewImage);
        }

        // on Next Click
        nextVideoFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer.getNextWindowIndex() != -1) {
                    Utils.callEventLogApi("clicked Next Video from <b> " + mVideoTitle + "</b>");
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());
                }
            }
        });


        //  if video loading error
        if (isVideoLoadingError) {

            mVideoError.setVisibility(VISIBLE);
            mExoPlayerView.setVisibility(View.INVISIBLE);
        }


        LocalBroadcastManager.getInstance(this).registerReceiver(mVideoErrorListener, new IntentFilter(Constants.VIDEO_ERROR));
        LocalBroadcastManager.getInstance(this).registerReceiver(mSaveVideoPosition, new IntentFilter(Constants.BROADCAST_SAVE_VIDEO_POSITION));
        LocalBroadcastManager.getInstance(this).registerReceiver(mChangeEclassName, new IntentFilter(Constants.BROADCAST_MARK_RUNNING_TRACK));
        LocalBroadcastManager.getInstance(this).registerReceiver(mBufferingListener, new IntentFilter(Constants.BROADCAST_VIDEO_BUFFERING));
        LocalBroadcastManager.getInstance(this).registerReceiver(mBufferingStopListener, new IntentFilter(Constants.BROADCAST_VIDEO_BUFFERING_STOP));
        LocalBroadcastManager.getInstance(this).registerReceiver(mPositionChangedListener, new IntentFilter(Constants.BROADCAST_VIDEO_POSITION_CHANGED));


    }


    @Override
    protected void onResume() {

        super.onResume();

        initFullscreenButton();

        startNextVideoTimer();
        startTimerToSendVideoSeekTime();
        AppPreferences.setMainActivityState(true);


        getProgressBarThread();

        try {
            if (mProgreeBarThread.getState() == Thread.State.NEW)
                mProgreeBarThread.start();

        }catch (Exception e){
            Logger.getLogger(Constants.LOG,e.toString());
        }


    }

    private Thread getProgressBarThread() { mProgreeBarThread = new Thread(()-> {
                while (mProgressBarPosition < 100) {
                  mProgressBarPosition += 1;

                    mProgressBarHandler.post(()-> {
                            mDefiniteProgressBar.setProgress(mProgressBarPosition);
                            mShowProgressBarDefinite = false;
                    });
                    try {
                        // Sleep for 100 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep(100); //thread will take approx 10 seconds to finish
                    } catch (InterruptedException e) {
                        Logger.getLogger(Constants.LOG,e.toString());
                    }
                }
        });

       return mProgreeBarThread;
    }

    @Override
    protected void onPause() {
        mPlayer.setPlayWhenReady(false);
        AppPreferences.setMainActivityState(false);
        super.onPause();

    }

    //  opened full screen mode
    private void initFullscreenButton() {


        TextView mNextVideo = mExoPlayerView.findViewById(R.id.tv_nxtvideo);
        mNextVideo.setVisibility(VISIBLE);

        //  click next video
        mNextVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mPlayer.getNextWindowIndex() != -1) {
                    Utils.callEventLogApi("clicked Next Video from <b> " + mVideoTitle + "</b>");
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());
                }

            }
        });


        mExoPlayerView.setControllerVisibilityListener(visibility-> {

                if (visibility == VISIBLE) {
                    mShowController = true;
                    showSystemUI();
                    mLlHeadingContainer.setVisibility(VISIBLE);

                    if (mPlayer.getNextWindowIndex() != -1 && mPlayer.getRepeatMode() == Player.REPEAT_MODE_ALL) {
                        mNextVideo.setVisibility(VISIBLE);
                    } else {
                        mNextVideo.setVisibility(GONE);
                    }
                    disableNextVideoLabel(true);
                } else {
                    mShowController = false;
                    hideSystemUI();
                    mLlHeadingContainer.setVisibility(GONE);
                    if (mShowNextVideoWithImageFrame) {
                        showNextVideoLabel();
                    } else {
                        disableNextVideoLabel(true);
                    }
            }
        });


    }


    @Override
    public void onBackPressed() {

        if (mIsCourseDetail) {
            Utils.callEventLogApi("closed<b> video player from </b>" + mDashBoardCourseDetails.getCourseDetail().getTitle());
        } else {
            Utils.callEventLogApi("closed <b> video player from from </b>" + mDashBoardVideoDetails.getVideoDetail().getTitle());
        }
        Intent returnIntent = new Intent();
        if (mIsCourseDetail) {
            returnIntent.putExtra(Constants.STATE_VIDEO_DATA, saveVideoTimeResponse);
        }
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private BroadcastReceiver mVideoErrorListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isVideoLoadingError = true;
            cancelNextVideoTimer();
            if (mPlayer != null && mPlayer.getPlayWhenReady()) {

                    try {

                        mVideoError.setVisibility(VISIBLE);
                        mExoPlayerView.setVisibility(View.INVISIBLE);
                        mPosterImage.setVisibility(GONE);
                        mProgressBar.setVisibility(GONE);
                        showSystemUI();

                    } catch (Exception e) {
                        Logger.getLogger(Constants.LOG,e.toString());
                    }

            }
        }
    };

    private BroadcastReceiver mBufferingListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mProgressBar.setVisibility(VISIBLE);
        }
    };


    // in case when user drag progress bar to android then also dont show progress bar definite
    private BroadcastReceiver mPositionChangedListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mPlayer.getContentPosition() > mPlayer.getDuration() - 10 * 1000 && mPlayer.getNextWindowIndex() != -1 && mPlayer.getRepeatMode() == Player.REPEAT_MODE_ALL) {
                mShowProgressBarDefinite = false;
                mProgressBarPosition = 0;
                mDefiniteProgressBar.setProgress(mProgressBarPosition);
            } else {
                mShowProgressBarDefinite = true;

            }
        }
    };


    private BroadcastReceiver mBufferingStopListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mProgressBar.setVisibility(GONE);
            mExoPlayerView.setControllerAutoShow(true);
            mExoPlayerView.setUseController(true);
            mPosterImage.setVisibility(GONE);

        }
    };

    //  if showed next video option
    private void startNextVideoTimer() {

        mTimerNextVideo.schedule(new TimerTask() {
            @Override
            public void run() {
                mNextVideoHandler.post(()-> {
                        if (mPlayer != null) {
                            // next video option will be shown before 10 seconds from end
                            if (mPlayer.getContentPosition() > mPlayer.getDuration() - 10 * 1000 && mPlayer.getNextWindowIndex() != -1 && mPlayer.getRepeatMode() == Player.REPEAT_MODE_ALL) {
                                mShowNextVideoWithImageFrame = true;
                                if (!mShowController) {
                                    showNextVideoLabel();
                                }

                            } else {
                                mShowNextVideoWithImageFrame = false;
                                disableNextVideoLabel(false);
                            }
                        }
                });
            }
        }, 0, 500);

    }


    private void cancelNextVideoTimer() {
        mTimerNextVideo.cancel();
        disableNextVideoLabel(false);
    }

    private void showNextVideoLabel() {
        nextVideoFrame.setVisibility(VISIBLE);
        if (mShowProgressBarDefinite) {
                mProgressBarPosition = 0;
                mDefiniteProgressBar.setProgress(mProgressBarPosition);
                if(!mProgreeBarThread.isAlive()){
                    mProgreeBarThread = getProgressBarThread();
                    mProgreeBarThread.start();
                }

        }
    }

    private void disableNextVideoLabel(boolean isFromControllerVisibility) {

        nextVideoFrame.setVisibility(GONE);

        // do not disable in case of controller visibilty
        if(!isFromControllerVisibility) {
            mShowProgressBarDefinite = true;
        }

    }


    public VideoData getCurrentCourseVideoData() {
        VideoData videoDataToBeSent = new VideoData();
        String seekTime;

        try {

            if (mPlayer != null) {
                if (mPlayer.getContentPosition() > mPlayer.getDuration() - 5 * 1000) {
                    videoDataToBeSent.setTime("0");
                    seekTime = "0";
                } else {
                    videoDataToBeSent.setTime(Long.toString(mPlayer.getContentPosition() / 1000));
                    seekTime = Long.toString(mPlayer.getContentPosition() / 1000);
                }
                if (mDashBoardCourseDetails.isProgrammes()) {
                    if (mDashBoardCourseDetails.isEclassVisible()) {
                        videoDataToBeSent.setType(Constants.CHAPTER);
                        videoDataToBeSent.setVideoId(eClassesArrayList.get(mPlayer.getCurrentWindowIndex()).getId());
                        if (!TextUtils.isEmpty(seekTime)) {
                            for (Chapters chapters : mDashBoardCourseDetails.getCourseDetail().getChapters()) {
                                for (Chapters.EClasses eClasses : chapters.getE_classes()) {
                                    if (eClasses.getId().equals(videoDataToBeSent.getVideoId())) {
                                        eClassesArrayList.get(mPlayer.getCurrentWindowIndex()).setSeek_time(seekTime);
                                    }
                                }
                            }

                        }


                    } else {
                        videoDataToBeSent.setType(Constants.TYPE_PROGRAMMES);
                        videoDataToBeSent.setVideoId(mDashBoardCourseDetails.getCourseDetail().getId());
                        if (!TextUtils.isEmpty(seekTime)) {
                            mDashBoardCourseDetails.getCourseDetail().setSeek_time(seekTime);
                        }
                    }
                } else {
                    if (mDashBoardCourseDetails.isEclassVisible()) {
                        videoDataToBeSent.setType(Constants.CHAPTER);
                        videoDataToBeSent.setVideoId(eClassesArrayList.get(mPlayer.getCurrentWindowIndex()).getId());
                        if (!TextUtils.isEmpty(seekTime)) {
                            for (Chapters chapters : mDashBoardCourseDetails.getCourseDetail().getChapters()) {
                                for (Chapters.EClasses eClasses : chapters.getE_classes()) {
                                    if (eClasses.getId().equals(videoDataToBeSent.getVideoId())) {
                                        eClassesArrayList.get(mPlayer.getCurrentWindowIndex()).setSeek_time(seekTime);
                                    }
                                }
                            }
                        }
                    } else {
                        videoDataToBeSent.setType(Constants.TYPE_COURSE);
                        videoDataToBeSent.setVideoId(mDashBoardCourseDetails.getCourseDetail().getId());
                        if (!TextUtils.isEmpty(seekTime)) {
                            mDashBoardCourseDetails.getCourseDetail().setSeek_time(seekTime);
                        }
                    }
                }

            }
        } catch (Exception e) {
            Logger.getLogger(Constants.LOG,e.toString());
        }

        return videoDataToBeSent;

    }


    public VideoData getCurrentVideoData() {
        VideoData videoDataToBeSent = new VideoData();

        try {

            if (mPlayer != null) {
                videoDataToBeSent.setTime(Long.toString(mPlayer.getContentPosition() / 1000));

                videoDataToBeSent.setType(Constants.TYPE_VIDEO);

                videoDataToBeSent.setVideoId(mDashBoardVideoDetails.getVideoDetail().getVideo_id());

                mDashBoardVideoDetails.getVideoDetail().setSeek_time(Long.toString(mPlayer.getContentPosition() / 1000));

            }
        } catch (Exception e) {
            Logger.getLogger(Constants.LOG,e.toString());
        }

        return videoDataToBeSent;

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.O || android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.O_MR1) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mVideoErrorListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mSaveVideoPosition);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mChangeEclassName);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mPositionChangedListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBufferingListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBufferingStopListener);


        cancelNextVideoTimer();
        if (mExoPlayerView != null && mPlayer != null) {
            mResumeWindow = mPlayer.getCurrentWindowIndex();
            AppPreferences.setVideoResumeWindow(mVideoUrl, mResumeWindow);
            mResumePosition = Math.max(0, mPlayer.getContentPosition());
            AppPreferences.setVideoResumePosition(mVideoUrl, mResumePosition);
            mPlayer.release();
            mPlayer = null;
            Utils.releaseDetailScreenPlayer();

        }

        if (saveSeekTime != null) {
            saveSeekTime.cancel();
        }

        AppPreferences.setVideoFullScreenActivityState(false);


    }


    // for internal use
    private void initExoPlayer() {

        // get position from preferences
        String mType;
        mPlayer = Utils.getDetailScreenConcatinatedPlayer();
        if (mPlayer == null) {
            if (mIsCourseDetail) {
                if (mDashBoardCourseDetails.isProgrammes()) {
                    if (mDashBoardCourseDetails.isEclassVisible()) {
                        mType = Constants.CHAPTER;
                    } else {
                        mType = Constants.TYPE_PROGRAMMES;
                    }
                } else {
                    if (mDashBoardCourseDetails.isEclassVisible()) {
                        mType = Constants.CHAPTER;
                    } else {
                        mType = Constants.TYPE_COURSE;
                    }
                }

                Utils.initilizeDetailScreenConcatinatedPlayer(this, mVideoUrls, eClassesArrayList, mDashBoardCourseDetails.isAutoPlay(), mType,null,false);
                mPlayer = Utils.getDetailScreenConcatinatedPlayer();

            } else {
                Utils.initilizeDetailScreenConcatinatedPlayer(this, mVideoUrls, eClassesArrayList, false, Constants.TYPE_DETAIL_VIDEO,mVideoTitle,false);
                mPlayer = Utils.getDetailScreenConcatinatedPlayer();
            }

        }
        mPlayer.clearVideoSurface();
        mExoPlayerView.setPlayer(mPlayer);

        if (mIsCourseDetail) {
            if (!mDashBoardCourseDetails.isEclassVisible()) {
                if (!TextUtils.isEmpty(mDashBoardCourseDetails.getCourseDetail().getSeek_time())) {
                    mResumePosition = Utils.getTimeInMilliSeconds(mDashBoardCourseDetails.getCourseDetail().getSeek_time());
                } else {
                    mResumePosition = 0;
                }
            }


            // also us in repeat mode for logs in exo player listener
            if (mDashBoardCourseDetails.isAutoPlay()) {
                mPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);
            } else {
                mPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
            }
        } else {
            mPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);

            if (!TextUtils.isEmpty(mDashBoardVideoDetails.getVideoDetail().getSeek_time())) {
                mResumePosition = Utils.getTimeInMilliSeconds(mDashBoardVideoDetails.getVideoDetail().getSeek_time());
            } else {
                mResumePosition = 0;
            }
        }

        boolean haveResumePosition = mResumeWindow != C.INDEX_UNSET;

        if (haveResumePosition) {
            mPlayer.seekTo(mResumeWindow, mResumePosition);
        }

        mPlayer.setPlayWhenReady(true);
    }


    private BroadcastReceiver mChangeEclassName = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String videoId = intent.getStringExtra(Constants.ECLASSES_VIDEO_ID);
            boolean isVideoIconTapped = intent.getBooleanExtra(Constants.IS_VIDEO_PLAYED, false);

            if (!isVideoIconTapped && !TextUtils.isEmpty(videoId)) {

                String videoUrl = mVideoUrls.get(mPlayer.getCurrentWindowIndex());
                for (Chapters.EClasses eClass : eClassesArrayList) {
                    if (videoUrl.equals(eClass.getPlay_url())) {
                        mVideoTitle = eClass.getTitle();
                    }
                }

                mTvEClassName.setText(mVideoTitle);
            }


        }
    };


    private BroadcastReceiver mSaveVideoPosition = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            boolean isDetailOnTop = intent.getBooleanExtra(Constants.IS_TOP_FRAGMENT, false);
            cancelNextVideoTimer();

            if (mExoPlayerView != null && mPlayer != null) {
                mResumeWindow = mPlayer.getCurrentWindowIndex();
                AppPreferences.setVideoResumeWindow(mVideoUrl, mResumeWindow);
                mResumePosition = Math.max(0, mPlayer.getContentPosition());
                AppPreferences.setVideoResumePosition(mVideoUrl, mResumePosition);
            }

            if (!isDetailOnTop) {
                Utils.releaseDetailScreenPlayer();
                Utils.releaseFullScreenPlayer();
                if (mExoPlayerView != null) {
                    mExoPlayerView.getVideoSurfaceView().setVisibility(GONE);
                }
                mExoPlayerView = null;
                mPlayer = null;
            }


        }
    };

    private void startTimerToSendVideoSeekTime() {
        saveSeekTime = new Timer();
        saveSeekTime.schedule(new TimerTask() {
            @Override
            public void run() {
                saveSeekTime();
            }
        }, 100, 1000);

    }


    public void saveSeekTime() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SAVE_VIDEO_TIME_API, response -> {
            //nothing to do
        }
                 , error -> {
            //nothing to do
        } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                VideoData mVideoData = null;

                if (mIsCourseDetail) {
                    mVideoData = getCurrentCourseVideoData();
                    if (mDashBoardCourseDetails.isEclassVisible()) {
                        for (int i = 0; i < mDashBoardCourseDetails.getCourseDetail().getChapters().size(); i++) {

                            Chapters chapters = mDashBoardCourseDetails.getCourseDetail().getChapters().get(i);

                            for (int j = 0; j < chapters.getE_classes().size(); j++) {
                                Chapters.EClasses eClasses = mDashBoardCourseDetails.getCourseDetail().getChapters().get(i).getE_classes().get(j);
                                if (eClasses.getId().equals(mVideoData.getVideoId())) {
                                    mDashBoardCourseDetails.getCourseDetail().getChapters().get(i).getE_classes().get(j).setSeek_time(mVideoData.getTime());
                                    break;
                                }
                            }
                        }
                    } else {
                        mDashBoardCourseDetails.getCourseDetail().setSeek_time(mVideoData.getTime());

                    }

                    saveVideoTimeResponse = new Gson().toJson(mDashBoardCourseDetails);
                } else {
                    mVideoData = getCurrentVideoData();
                }


                if (mVideoData != null) {
                    params.put(Keys.VIDEOS_ID, mVideoData.getVideoId());
                    params.put(Keys.COUNTRY, mVideoData.getCountry());
                    params.put(Keys.TIME, mVideoData.getTime());
                    params.put(Keys.TYPE, mVideoData.getType());
                }


                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    // This snippet hides the system bars.
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    // This snippet shows the system bars. It does this by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }


}
