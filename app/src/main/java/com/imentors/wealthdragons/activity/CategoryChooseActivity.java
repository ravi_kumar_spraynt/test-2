package com.imentors.wealthdragons.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.fragments.CategoryChooserFragment;


public class CategoryChooseActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_choose);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fm_main, CategoryChooserFragment.newInstance());
        ft.commit();

    }

    @Override
    public void onBackStackChanged() {
        // no use
    }
}
