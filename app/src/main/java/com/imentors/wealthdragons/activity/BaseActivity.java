package com.imentors.wealthdragons.activity;

import android.app.ProgressDialog;
import android.os.Bundle;


import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.fragments.BaseFragment;
import com.imentors.wealthdragons.utils.Utils;


import java.util.List;

/**
 * Created on 7/03/17.
 * Base class for all the activities
 */
public abstract class BaseActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener, BaseFragment.ProgressDialogInteraction {

    //instance to progress dialog
    private ProgressDialog mProgressDialog;

    private boolean mIsStopped = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }


    /**
     * @return the top fragment in the fragment manager by id fragment_container
     */
    @Nullable
    BaseFragment getTopFragment() {
        return (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.fm_main);
    }


    @Override
    protected void onResume() {
        super.onResume();
        //sync the arrow state of button if activity is restarted or restored
        onBackStackChanged();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mIsStopped = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mIsStopped = true;
    }

    @Override
    public void onBackPressed() {
        if (!mIsStopped) {
            super.onBackPressed();
            Utils.hideKeyboard(this);
        }
    }


    /**
     * hide toolbar from screen layout
     */
    public void hideToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    @Override
    protected void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }

    @Override
    public void showProgressDialog(String message) {
        //create the instance of progress dialog if not created
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
        }

        //set the message and show the dialog
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


    public boolean isFragmentInBackStack(Fragment frag) {
        List<Fragment> fragentList = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragentList) {

            if (fragment.getClass().isAssignableFrom(frag.getClass())) {
                return true;
            }

        }
        return false;

    }

    public Fragment getFragmentFromBackStack(Fragment frag) {
        List<Fragment> fragentList = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragentList) {

            if (fragment.getClass().isAssignableFrom(frag.getClass())) {
                return fragment;
            }

        }
        return null;
    }


}
