package com.imentors.wealthdragons.activity;

import android.os.Bundle;
import android.text.Html;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;

import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;

import com.imentors.wealthdragons.models.Maintenance;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.util.HashMap;
import java.util.Map;

public class MaintenanceActivity extends BaseActivity {

    //click of terms and condition
    ClickableSpan contactUsSpan = new ClickableSpan() {
        @Override
        public void onClick(View widget) {

            Utils.openEmail(null, MaintenanceActivity.this);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    };
    private ImageView mIvMaintenanceLogo;
    private TextView mTvHeading;
    private ProgressBar mProgressBar;
    private TextView mMaintenanceMessage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maintenance);

        mIvMaintenanceLogo = findViewById(R.id.iv_logo_icon);
        mTvHeading = findViewById(R.id.tv_maintenance_heading);
        mProgressBar = findViewById(R.id.progress_bar);
        mMaintenanceMessage = findViewById(R.id.tv_maintenance_message);


    }

    public void callingMaintenanceApi() {
        mProgressBar.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.MAINTENANCE_PAGE, response -> {
            mProgressBar.setVisibility(View.GONE);

            try {
                Maintenance maintenance = new Gson().fromJson(response, Maintenance.class);
                Glide.with(getApplicationContext()).load(maintenance.getLogo()).into(mIvMaintenanceLogo);
                mTvHeading.setText(maintenance.getHeading());

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    mMaintenanceMessage.setText(Html.fromHtml(maintenance.getText(), Html.FROM_HTML_MODE_LEGACY));
                } else {
                    mMaintenanceMessage.setText(Html.fromHtml(maintenance.getText()));
                }

                String[] clikableLinks = {getString(R.string.contact_us)};
                ClickableSpan[] clickableSpan = {contactUsSpan};
                Utils.makeLinks(mMaintenanceMessage, clikableLinks, clickableSpan);

                if (maintenance.getStatus().equals("2")) {
                    AppPreferences.setMaintainceMode(false);
                    WealthDragonsOnlineApplication.sharedInstance().redirectToSplashActivity(MaintenanceActivity.this);
                } else {
                    AppPreferences.setMaintainceMode(true);
                }
            } catch (Exception e) {
                // no use
            }
        }, error -> {
            // no use
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                if (WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet)) {
                    params.put(Keys.DEVICE, Keys.TABLET);

                } else {
                    params.put(Keys.DEVICE, Keys.ANDROID);

                }

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onBackStackChanged() {
        // no use
    }


    @Override
    protected void onResume() {
        super.onResume();
        callingMaintenanceApi();
    }
}
