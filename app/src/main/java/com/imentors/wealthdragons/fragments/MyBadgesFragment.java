package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.BadgesListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.models.Badges;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


public class MyBadgesFragment extends BaseFragment implements BadgesListAdapter.OnListItemClicked{


    private LinearLayout mProgressBar , mNoDataFound;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private BadgesListAdapter mPaymentHistoryAdapter;


    public static MyBadgesFragment newInstance() {
        MyBadgesFragment fragment = new MyBadgesFragment();
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_badges, container, false);


        mProgressBar = view.findViewById(R.id.progressBar);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mNoDataFound = view.findViewById(R.id.no_data);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mPaymentHistoryAdapter = new BadgesListAdapter(mContext,this);

        callBadgesApi();


        return view;
    }

    public void callBadgesApi() {

        mProgressBar.setVisibility(View.VISIBLE);

        // calling badges api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.BADGES_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                        mProgressBar.setVisibility(View.GONE);
                        onGetDataAPIServerResponse(response);

                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>My Badges </b> error in api");

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>My Badges </b> error in api");

                error.printStackTrace();
                Utils.showNetworkError(mContext, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    //handling badges api response
    @Override
    public void showDataOnView(String response)  {
        super.showDataOnView(response);
        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response) {


        //  parse JSON to java
        Badges paymentHistory = new Gson().fromJson(response, Badges.class);

        if(paymentHistory.getBadge() !=null){
            if(paymentHistory.getBadge().size()>0){
                mNoDataFound.setVisibility(View.GONE);
                mPaymentHistoryAdapter.setNewItems(paymentHistory.getBadge());
                mRecyclerView.setAdapter(mPaymentHistoryAdapter);
            }else{
                mNoDataFound.setVisibility(View.VISIBLE);
            }
        }else{
            mNoDataFound.setVisibility(View.VISIBLE);
        }

    }


    //  opened badges detail
    @Override
    public void onItemClicked(Badges.Badge item) {

        Utils.callEventLogApi("clicked <b>"+item.getCourse_title()+"</b> Badge Detail from <b>My Badges</b>");

        addFragment(BadgesDetailFragment.newInstance(item),true);

    }

    //  closed badges page
    @Override
    public void onStop() {
        super.onStop();
        Utils.callEventLogApi("closed <b>My Badges </b> page");

    }
}
