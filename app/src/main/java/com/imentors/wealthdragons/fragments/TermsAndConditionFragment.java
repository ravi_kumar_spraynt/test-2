package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.TermsAndPrivacyListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.TermsAndPrivacy;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.HashMap;
import java.util.Map;


public class TermsAndConditionFragment extends BaseFragment implements TermsAndPrivacyListAdapter.OnSpanClickListener {

    private boolean mIsPrivacy = false , mIsMain;
    private static final String  IS_PRIVACY = "IS_PRIVACY";
    private static final String  IS_MAIN = "IS_MAIN";

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TermsAndPrivacyListAdapter mTermsAndPrivacyListAdapter = null;
    private ListView mListView;
    private ViewGroup header;
    private TermsAndPrivacy mData = null;
    private Context mContext;
    private WealthDragonTextView mHeadingText , mDescription;
    private View topView,bottomView;
    private LinearLayout mLowerLayout;


    public static TermsAndConditionFragment newInstance(boolean isPrivacy , boolean isMainActivity) {


        // put data in bundle
        Bundle bundle = new Bundle();
        bundle.putBoolean(IS_PRIVACY,isPrivacy);
        bundle.putBoolean(IS_MAIN,isMainActivity);


        TermsAndConditionFragment fragment = new TermsAndConditionFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mContext=getActivity();

        if (getArguments() != null) {
            mIsPrivacy = getArguments().getBoolean(IS_PRIVACY);
            mIsMain = getArguments().getBoolean(IS_MAIN);
            mData = null;
        }

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_privacy_policy, container, false);

        if (mIsPrivacy)
        {
            Utils.callEventLogApi("visited<b> Privacy Policy</b>");

        }
        else {
            Utils.callEventLogApi("visited<b> Terms And Condition</b>");

        }

        mLowerLayout = view.findViewById(R.id.ll_screen_footer);

        Toolbar mToolbar = view.findViewById(R.id.login_toolbar);

        TextView txtStatus = mToolbar.findViewById(R.id.tv_singing_status);
        txtStatus.setVisibility(View.GONE);

        mToolbar.setNavigationIcon(R.drawable.back_arrow);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });



        if(mIsMain){
            mToolbar.setVisibility(View.GONE);
            mLowerLayout.setVisibility(View.GONE);
        }else{
            mToolbar.setVisibility(View.VISIBLE);
            mLowerLayout.setVisibility(View.VISIBLE);
        }

        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);

        mListView = view.findViewById(R.id.listView);


        if(mData!=null){
            mData = null;
        }


        if(mTermsAndPrivacyListAdapter == null) {

            //  calling Terms And Privacy List Adapter constructor
            mTermsAndPrivacyListAdapter = new TermsAndPrivacyListAdapter(this.getContext(),this,mIsPrivacy,mIsMain);
        }




        header = (ViewGroup)inflater.inflate(R.layout.header_terms_subscription_privacy, mListView, false);

        header.setPadding(Utils.dpToPx(16),0,Utils.dpToPx(16),0);

        mHeadingText = header.findViewById(R.id.tv_subscription_main_heading);
        mDescription = header.findViewById(R.id.tv_subscription_sub_heading);
        topView = header.findViewById(R.id.view_top_sub_heading);
        bottomView = header.findViewById(R.id.view_bottom_sub_heading);

        mListView.addHeaderView(header);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(mIsPrivacy){
                    getDataFromApi();
                    Utils.callEventLogApi("refreshed <b>Privacy Policy Screen</b>");

                }else{
                    getDataFromApi();
                    Utils.callEventLogApi("refreshed <b>Terms And Condition Screen</b>");

                }

            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mSwipeRefreshLayout.setRefreshing(false);
        


        if(mIsPrivacy){
            mData = AppPreferences.getPrivacyPolicy();
        }else{
            mData = AppPreferences.getTermsAndCondition();
        }

        if(!TextUtils.isEmpty(mData.getTitle())){
            intializeDataView(mData);
        }else{
            getDataFromApi();
        }

        return view;

    }

    private void getDataFromApi() {


        mTermsAndPrivacyListAdapter.clearData();

        String url = null;

        if(mIsPrivacy){
            url = Api.PRIVACY_POLICY_API;
        }else{
            url = Api.TERMS_AND_CONDITION_API;
        }


        // show progress Bar

        mSwipeRefreshLayout.setRefreshing(true);


        // calling privacy policy and terms and condition  api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    onGetDataAPIServerResponse(response);
                    mSwipeRefreshLayout.setRefreshing(false);
                } catch (Exception e) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    if(isAdded()) {
                        Utils.showAlertDialog(mContext, getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                    }
                    if(mIsPrivacy){
                        Utils.callEventLogApi("getting <b>Privacy Policy</b>  error in api");

                    }else{
                        Utils.callEventLogApi("getting <b>Terms And Condition</b>  error in api");

                    }

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b>Terms And Condition</b>  error in api");
                error.printStackTrace();
                if(isAdded()) {

                    Utils.showAlertDialog(mContext, getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                }
                mSwipeRefreshLayout.setRefreshing(false);

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet)) {
                    params.put(Keys.DEVICE, Keys.TABLET);

                } else {
                    params.put(Keys.DEVICE, Keys.ANDROID);

                }

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    // handling privacy policy and terms and condition api response
    @Override
    public void showDataOnView(String response)  {

        super.showDataOnView(response);

        // parse json to java
        TermsAndPrivacy termsAndPrivacy = new Gson().fromJson(response, TermsAndPrivacy.class);


        //  setting data in app preferences
        if(mIsPrivacy){
            AppPreferences.setPrivacyPolicy(termsAndPrivacy);

        }else{
            AppPreferences.setTermsAndConditon(termsAndPrivacy);

        }
        intializeDataView(termsAndPrivacy);
        mSwipeRefreshLayout.setRefreshing(false);


    }

    //  setting data on view
    private void intializeDataView(TermsAndPrivacy termsAndPrivacy ) {


        mTermsAndPrivacyListAdapter.clearData();

        if(!TextUtils.isEmpty(termsAndPrivacy.getTitle())){
            mHeadingText.setText(termsAndPrivacy.getTitle());
        }

        if(!TextUtils.isEmpty(termsAndPrivacy.getEffectiveDate())){
            mDescription.setText(termsAndPrivacy.getEffectiveDate());
            topView.setVisibility(View.VISIBLE);
            bottomView.setVisibility(View.VISIBLE);
        }


        mTermsAndPrivacyListAdapter.setItems(termsAndPrivacy.getContent());
        mListView.setAdapter(mTermsAndPrivacyListAdapter);

       }

    @Override
    public void onSpanClicked(Fragment frag, boolean addToBackStack) {
        addFragment(frag,addToBackStack);
    }

    //  closed privacy policy and terms and condition
    @Override
    public void onStop() {
        super.onStop();
        if (mIsPrivacy)
        {
            Utils.callEventLogApi("closed<b> Privacy Policy</b>");

        }
        else {
            Utils.callEventLogApi("closed<b> Terms And Condition</b>");

        }
    }
}
