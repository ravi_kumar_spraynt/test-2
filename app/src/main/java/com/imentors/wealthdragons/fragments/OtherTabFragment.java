package com.imentors.wealthdragons.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.models.Article;
import com.imentors.wealthdragons.models.DashBoardCourses;
import com.imentors.wealthdragons.models.DashBoardEvents;
import com.imentors.wealthdragons.models.DashBoardItemOderingArticle;
import com.imentors.wealthdragons.models.DashBoardItemOderingEvent;
import com.imentors.wealthdragons.models.DashBoardItemOderingMentors;
import com.imentors.wealthdragons.models.DashBoardItemOderingVideo;
import com.imentors.wealthdragons.models.DashBoardMentors;
import com.imentors.wealthdragons.models.DashBoardVideos;
import com.imentors.wealthdragons.models.Event;
import com.imentors.wealthdragons.models.Mentors;
import com.imentors.wealthdragons.models.Video;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.DashBoardOtherTabView;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.Map;

public class OtherTabFragment extends BaseFragment  {


    // in some cases taste Id is coming in  Id too

    private Context mContext;
    private RecyclerView mViewPager;
    private LinearLayout mProgressBar;
    private LinearLayout mLlScreenContainer,mNoRecordFound;
    private SwipeRefreshLayout mSwipeRefershLayout;
    private int mTabPosition;
    private String mSeeAllApikey;
    private WealthDragonTextView mSearchLayout;
    private boolean isTablet;
    private FrameLayout mNoInternetConnection;


    public static OtherTabFragment newInstance(int tabPosition ,String key) {

        //  put data in bundle
        Bundle args = new Bundle();
        args.putInt(Constants.TAB_POSITION,tabPosition);
        if(!TextUtils.isEmpty(key)){
            args.putString(Constants.SEE_ALL_API,key);
        }

        OtherTabFragment fragment = new OtherTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);

        if(getArguments()!=null){
            mTabPosition = getArguments().getInt(Constants.TAB_POSITION);

            if(getArguments().containsKey(Constants.SEE_ALL_API)){
                mSeeAllApikey = getArguments().getString(Constants.SEE_ALL_API);
            }

        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        mProgressBar = view.findViewById(R.id.progressBar);
        mLlScreenContainer = view.findViewById(R.id.ll_screen_container);
        mViewPager = view.findViewById(R.id.pager);
        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);
        mSearchLayout = view.findViewById(R.id.tv_search);
        mNoRecordFound = view.findViewById(R.id.no_record_available);
        mSearchLayout.setVisibility(View.VISIBLE);
        mNoInternetConnection = view.findViewById(R.id.no_internet);

        view.findViewById(R.id.tv_downloads).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addFragment(MyGroupDownloadFragment.newInstance(),true);

            }
        });


        view.findViewById(R.id.tv_retry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callTabApi();
            }
        });

        callTabApi();

        mSwipeRefershLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callTabApi();

                switch (mTabPosition){
                    case Constants.MENTOR_TAB:
                        callTabApi();
                        Utils.callEventLogApi("refreshed <b>Mentor </b>Tab");
                        break;
                    case Constants.VIDEO_TAB:
                        callTabApi();
                        Utils.callEventLogApi("refreshed <b>Video </b>Tab");
                        break;
                    case Constants.COURSE_TAB:
                        callTabApi();
                        Utils.callEventLogApi("refreshed <b>Course </b>Tab");
                        break;
                    case Constants.SEE_ALL_EVENT_TAB:
                        Utils.callEventLogApi("refreshed <b>See All Event </b>page");
                        callTabApi();
                        break;
                    case Constants.SEE_ALL_COURSES_TAB:
                        Utils.callEventLogApi("opened <b>See All Course</b> page");

                        if(!TextUtils.isEmpty(mSeeAllApikey)){
                            switch (mSeeAllApikey){
                                case Constants.ALL_PROGRAMMES_KEY:
                                    callTabApi();
                                    break;
                                case Constants.ALL_UPCOMING_COURSE:
                                    callTabApi();
                                    break;
                                default:
                                    callTabApi();

                            }
                        }

                }
            }
        });


        LocalBroadcastManager.getInstance(mContext).registerReceiver(mReloadPayResponseListener, new IntentFilter(Constants.BROADCAST_PAY_FINSIH));


        mViewPager.setVisibility(View.GONE);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!Utils.isNetworkAvailable(mContext)){
            callTabApi();
        }
    }

    public void callTabApi() {

        mNoInternetConnection.setVisibility(View.GONE);

        //  set refreshing
        if (mSwipeRefershLayout.isRefreshing()) {
            mSwipeRefershLayout.setRefreshing(true);
            mProgressBar.setVisibility(View.GONE);
        } else {
            mProgressBar.setVisibility(View.VISIBLE);
        }


        String api = null;

        switch (mTabPosition){
            case Constants.MENTOR_TAB:
                mSearchLayout.setText("Loading All Mentors");
                api = Api.MENTORS_API;

                break;
            case Constants.VIDEO_TAB:
                mSearchLayout.setText("Loading Events");
                api = Api.VIDEOS_API;
                break;
            case Constants.COURSE_TAB:
                mSearchLayout.setText("Loading All Courses");
                api= Api.COURSES_API;
                break;
            case Constants.SEE_ALL_EVENT_TAB:
                Utils.callEventLogApi("opened <b>See All Event </b> page");

                api= Api.ALL_EVENTS_API;
                break;
            case Constants.SEE_ALL_COURSES_TAB:
                Utils.callEventLogApi("opened <b>See All Course</b> page");

                if(!TextUtils.isEmpty(mSeeAllApikey)){
                    switch (mSeeAllApikey){
                        case Constants.ALL_PROGRAMMES_KEY:
                            api= Api.ALL_PROGRAMMES_API;
                            break;
                        case Constants.ALL_UPCOMING_COURSE:
                            api=Api.UPCOMING_COURSES_API;
                            break;
                        default:
                            api=Api.ON_SALE_API;

                    }
                }

        }




        // calling  api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST,api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);
                if (mSwipeRefershLayout.isRefreshing()) {
                    mSwipeRefershLayout.setRefreshing(false);
                }
                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>Tab api </b> error");

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>Tab api </b> error");
                error.printStackTrace();
                mSwipeRefershLayout.setRefreshing(false);

                if(!Utils.isNetworkAvailable(mContext)){
                    mNoInternetConnection.setVisibility(View.VISIBLE);
                }else{
                    Utils.showNetworkError(mContext, error);
                }


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);


                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    //  handling api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response) {

        Intent intent = new Intent(Constants.BROADCAST_OTHER_TAB_LIST_UPDATED);

        intent.putExtra(Constants.TAB_POSITION,mTabPosition);


        LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);

        mLlScreenContainer.removeAllViews();


        if(mTabPosition == Constants.MENTOR_TAB) {
            // Parse JSON to Java
            final DashBoardMentors dashBoard = new Gson().fromJson(response, DashBoardMentors.class);


            AppPreferences.setMentorDashBoard(dashBoard);

            //  for each loop
            for (final DashBoardItemOderingMentors dashBoardItemOdering : dashBoard.getAll_Mentors()) {


                if (dashBoardItemOdering.getMentors().size() > 0) {


                    for(Mentors mentors:dashBoardItemOdering.getMentors()){
                        Utils.setImagesInMemCache(Utils.getImageUrl(mentors.getBanner(),Utils.getMentorImageWidth(this.getActivity()),Utils.getMentorImageHeight(this.getActivity())));
                    }


                    mLlScreenContainer.addView(new DashBoardOtherTabView(mContext, dashBoardItemOdering,dashBoardItemOdering.getTotal_items(),dashBoardItemOdering.getId(),dashBoardItemOdering.getNext_page()));


                }

                continue;

            }
        }else if(mTabPosition == Constants.VIDEO_TAB){

            // Parse JSON to Java
            final  DashBoardVideos dashBoard = new Gson().fromJson(response, DashBoardVideos.class);

            AppPreferences.setDashboardVideos(dashBoard);


            //  for each loop
            for (final DashBoardItemOderingVideo dashBoardItemOdering : dashBoard.getAll_Videos()) {


                if (dashBoardItemOdering.getVideos().size() > 0) {

                    for (Video videos : dashBoardItemOdering.getVideos()) {
                        Utils.setImagesInMemCache(Utils.getImageUrl(videos.getMobile_small_banner(), Utils.getVideoImageWidth(this.getActivity()), Utils.getVideoImageHeight(this.getActivity())));
                    }

                    mLlScreenContainer.addView(new DashBoardOtherTabView(mContext, dashBoardItemOdering, dashBoardItemOdering.getTotal_items(), dashBoardItemOdering.getId(), dashBoardItemOdering.getNext_page()));


                }

                continue;

            }

        }else if(mTabPosition == Constants.SEE_ALL_EVENT_TAB){

            // Parse JSON to Java
            final DashBoardEvents dashBoard = new Gson().fromJson(response, DashBoardEvents.class);



            //  for each loop
            for (final DashBoardItemOderingEvent dashBoardItemOdering : dashBoard.getAll_Events()) {


                if (dashBoardItemOdering.getEvents().size() > 0) {

                    for (Event article : dashBoardItemOdering.getEvents()) {

                        Utils.setImagesInMemCache(Utils.getImageUrl(article.getBanner(),Utils.getCourseImageWidth(getActivity()),Utils.getCourseImageHeight(getActivity())));
                    }

                    mLlScreenContainer.addView(new DashBoardOtherTabView(mContext, dashBoardItemOdering, true, dashBoardItemOdering.getTotal_items(), mSeeAllApikey, dashBoardItemOdering.getId(), dashBoardItemOdering.getNext_page()));


                }

                continue;

            }

        }


        else if(mTabPosition == Constants.SEE_ALL_COURSES_TAB){

            // Parse JSON to Java
            final DashBoardCourses dashBoard = new Gson().fromJson(response, DashBoardCourses.class);

                AppPreferences.setDashboardCourses(dashBoard);
            // in case if it is programmes
            if(dashBoard.getAll_Course()!=null) {

                //  for each loop
                for (final DashBoardItemOderingArticle dashBoardItemOdering : dashBoard.getAll_Course()) {


                    if (dashBoardItemOdering.getCourses().size() > 0) {

                        for (Article article : dashBoardItemOdering.getCourses()) {

                            Utils.setImagesInMemCache(Utils.getImageUrl(article.getBanner(), Utils.getCourseImageWidth(this.getActivity()), Utils.getCourseImageHeight(this.getActivity())));
                        }

                        mLlScreenContainer.addView(new DashBoardOtherTabView(mContext, dashBoardItemOdering, true, dashBoardItemOdering.getTotal_items(), mSeeAllApikey, dashBoardItemOdering.getId(), dashBoardItemOdering.getNext_page(), false));


                    }

                    continue;
                }
            }else{
                //  for each loop
                for (final DashBoardItemOderingArticle dashBoardItemOdering : dashBoard.getAll_Programme()) {


                    if (dashBoardItemOdering.getProgrammes().size() > 0) {

                        for (Article article : dashBoardItemOdering.getProgrammes()) {

                            Utils.setImagesInMemCache(Utils.getImageUrl(article.getBanner(), Utils.getCourseImageWidth(this.getActivity()), Utils.getCourseImageHeight(this.getActivity())));
                        }

                        mLlScreenContainer.addView(new DashBoardOtherTabView(mContext, dashBoardItemOdering, true, dashBoardItemOdering.getTotal_items(), mSeeAllApikey, dashBoardItemOdering.getId(), dashBoardItemOdering.getNext_page(), true));


                    }

                    continue;

                }
            }

        } else {
            // Parse JSON to Java
            final DashBoardCourses dashBoard = new Gson().fromJson(response, DashBoardCourses.class);

            AppPreferences.setDashboardCourses(dashBoard);


            //  for each loop
                for (final DashBoardItemOderingArticle dashBoardItemOdering : dashBoard.getAll_Course()) {


                    if (dashBoardItemOdering.getCourses().size() > 0) {

                        for (Article article : dashBoardItemOdering.getCourses()) {

                            Utils.setImagesInMemCache(Utils.getImageUrl(article.getBanner(), Utils.getCourseImageWidth(this.getActivity()), Utils.getCourseImageHeight(this.getActivity())));
                        }

                        mLlScreenContainer.addView(new DashBoardOtherTabView(mContext, dashBoardItemOdering, false, dashBoardItemOdering.getTotal_items(), null, dashBoardItemOdering.getId(), dashBoardItemOdering.getNext_page(), false));


                    }

                    continue;

            }
        }


        if(mLlScreenContainer.getChildCount()>0){
            mNoRecordFound.setVisibility(View.GONE);

        }else{
            mLlScreenContainer.addView(mNoRecordFound);
            mNoRecordFound.setVisibility(View.VISIBLE);
        }

    }

    private BroadcastReceiver mReloadPayResponseListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            callTabApi();
        }
    };

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mReloadPayResponseListener);
    }
}
