package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.BaseActivity;

import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONException;
import org.json.JSONObject;



/**
 * Created on 7/03/17.
 * Base class for all the fragment
 */

public abstract class BaseFragment extends Fragment {

    public interface ProgressDialogInteraction {
        void showProgressDialog(String message);

        void dismissProgressDialog();
    }

    static final String ARG_TITLE = "title";

    @Nullable
    private String mTitle;

    boolean mIsViewCreatedFirstTime = true;

    private ProgressDialogInteraction mProgressDialogInteraction;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTitle = getArguments().getString(ARG_TITLE);
        }


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!mIsViewCreatedFirstTime) {
            getActivity().invalidateOptionsMenu();
        }

        mIsViewCreatedFirstTime = false;

    }


    /**
     * update the title on actionbar/toolbar
     *
     * @param title to be set on action bar
     */
    public void setToolbarTitle(String title) {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.setTitle(title);
        }
    }

    /**
     * @return get the fragment title
     */
    @Nullable
    public String getTitle() {
        return mTitle;
    }

    /**
     * @param title set the fragment title
     */
    public void setTitle(String title) {
        mTitle = title;
    }

//    /**
//     * Make a standard toast that just contains a text view with the text from a resource.
//     *
//     * @param resId    The resource id of the string resource to use.  Can be formatted text.
//     * @param duration duration
//     */
//    protected void showToast(int resId, int duration) {
//        View view = getView();
//        if (view == null) {
//            Toast.makeText(getActivity(), resId, Toast.LENGTH_LONG).show();
//        } else {
//            Snackbar.make(getView(), resId, duration).show();
//        }
//    }

//    /**
//     * Make a standard toast that just contains a text view with the text from a resource.
//     *
//     * @param resId The resource id of the string resource to use.  Can be formatted text.
//     */
//    protected void showToast(int resId) {
//        View view = getView();
//        if (view == null) {
//            Toast.makeText(getActivity(), resId, Toast.LENGTH_SHORT).show();
//        } else {
//            Snackbar.make(getView(), resId, Snackbar.LENGTH_SHORT).show();
//        }
//    }


    /**
     * Make a standard toast that just contains a text view with the text from a resource.
     *
     * @param text     The text to show.  Can be formatted text.
     * @param duration duration
     */
//    void showToast(CharSequence text, @SuppressWarnings("SameParameterValue") int duration) {
//        View view = getView();
//        if (view == null) {
//            Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
//        } else {
//            Snackbar.make(getView(), text, duration).show();
//        }
//    }

    /**
     * Make a standard toast that just contains a text view with the text from a resource.
     *
     * @param text The text to show.  Can be formatted text.
     */
//    protected void showToast(CharSequence text) {
//        View view = getView();
//        if (view == null) {
//            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
//        } else {
//            Snackbar.make(getView(), text, Snackbar.LENGTH_SHORT).show();
//        }
//    }

    /**
     * Hide the toolbar
     */
    protected void hideToolbar() {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.hideToolbar();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProgressDialogInteraction) {
            mProgressDialogInteraction = (ProgressDialogInteraction) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mProgressDialogInteraction = null;
    }

    /**
     * Show the progress dialog
     *
     * @param message message
     */
    void showProgressDialog(String message) {
        if (mProgressDialogInteraction != null) {
            mProgressDialogInteraction.showProgressDialog(message);
        }
    }

    /**
     * dismiss the progress dialog
     */
    void dismissProgressDialog() {
        if (mProgressDialogInteraction != null) {
            mProgressDialogInteraction.dismissProgressDialog();
        }
    }

    /**
     * Push the new fragment in the activity
     *
     * @param fragment fragment
     */
    void pushFragment(Fragment fragment , boolean addToBackStack) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if(addToBackStack) {
            ft.setCustomAnimations(
                    R.anim.slide_in_from_right, R.anim.slide_out_to_right);
            ft.addToBackStack(null);
        }else{
            ft.setCustomAnimations(
                    R.anim.slide_in_from_bottom, 0);
        }
        ft.replace(R.id.fm_main, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    void addFragment(Fragment fragment , boolean addToBackStack) {
        FragmentManager fm = getActivity().getSupportFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();

        if(addToBackStack) {
            ft.setCustomAnimations(
                    R.anim.slide_in_from_right, R.anim.no_animation);
            ft.addToBackStack(null);
        }else{
            ft.setCustomAnimations(
                    R.anim.slide_in_from_bottom, 0);
        }

        ft.add(R.id.fm_main, fragment);


        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }


    /**
     * remove the current fragment
     */
    void popFragment() {
        getActivity().onBackPressed();
    }

//
//    /**
//     * call event log api
//     */
//    void callEventLogApi(final String message ){
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.LOG_EVENT_API, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                // nothing to respond it will run in background
//                Log.d("LOG_EVENT_API",response.toString());
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//            }
//        }){
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//
//                Map<String, String> params = Utils.getParams(false);
//                params.put(Keys.LOG_MESSAGE,message);
//
//                return params;
//            }
//        };
//
//
//        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
//    }




    /**
     * pop all the fragment except the first in the stack
     */
    void popToRootFragment() {
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onResume() {
        super.onResume();
        enableTouch();

    }


    public void enableTouch() {
        if(getActivity()!=null) {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }


    public void disableTouch(){
        if(getActivity()!=null) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }


    public void onGetDataAPIServerResponse(String response) throws JSONException {


        enableTouch();
        //TODO uncomment this when _maintaince is activated for all apis
//        AppPreferences.setMaintainceMode(false);

        JSONObject jsonObject = new JSONObject(response);


        if(!TextUtils.isEmpty(jsonObject.optString(Keys.ERROR))){

            String message = jsonObject.getString(Keys.ERROR);

            if(this instanceof SubscriptionFragment){
                Utils.showAlertDialog(getContext(), getString(R.string.error), message, getString(R.string.okay_text), true,true);

            }else{
                Utils.showAlertDialog(getContext(), getString(R.string.error), message, getString(R.string.okay_text), false,true);

            }



        }else if(!TextUtils.isEmpty(jsonObject.optString(Keys.INACTIVE))){

            String message = jsonObject.getString(Keys.INACTIVE);

            if((this instanceof SubscriptionFragment) || (this instanceof WelcomeFragment) ){
                Utils.logoutFbSession();
                Utils.showAlertDialog(getContext(), getString(R.string.account_inactive), message, getString(R.string.error_contact_support), true,true);

            }else if(this instanceof SignInFragment){
                Utils.logoutFbSession();
                Utils.showAlertDialog(getContext(), getString(R.string.account_inactive), message, getString(R.string.error_contact_support), true,true);
            }else{
                Utils.showAlertDialog(getContext(), getString(R.string.account_inactive), message, getString(R.string.error_contact_support), false,true);

            }


        }else if(!TextUtils.isEmpty(jsonObject.optString(Keys._INACTIVE))){

            String message = jsonObject.getString(Keys._INACTIVE);

            if((this instanceof SubscriptionFragment) || (this instanceof WelcomeFragment) || (this instanceof SignInFragment)){
                Utils.showAlertDialog(getContext(), getString(R.string.inactive), message, getString(R.string.error_contact_support), true,true);

            }else{
                Utils.showAlertDialog(getContext(), getString(R.string.inactive), message, getString(R.string.error_contact_support), false,true);

            }

        } else if(!TextUtils.isEmpty(jsonObject.optString(Keys._ERROR))){

            String message = jsonObject.getString(Keys._ERROR);

            if(this instanceof SubscriptionFragment){
                Utils.showAlertDialog(getContext(), getString(R.string.error), message, getString(R.string.okay_text), true,true);

            }else{
                Utils.showAlertDialog(getContext(), getString(R.string.error), message, getString(R.string.okay_text), false,true);

            }
        }else if(!TextUtils.isEmpty(jsonObject.optString(Keys._MAINTAINANCE))){

            String message = jsonObject.getString(Keys._MAINTAINANCE);
            if(message.equals(Keys.ON)){
                AppPreferences.setMaintainceMode(true);

                WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
            }

        }else if(!TextUtils.isEmpty(jsonObject.optString(Keys._CATEGORY))) {

            String message = jsonObject.getString(Keys._CATEGORY);

            WealthDragonsOnlineApplication.sharedInstance().redirectToCategoryChooserActivity(getActivity());


        }

        else{

            showDataOnView(response);

        }
    }


    public void showDataOnView(String response)  {
       enableTouch();
    }


}
