package com.imentors.wealthdragons.fragments;

import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.EClassesListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.courseDetailView.CourseListNewItems;
import com.imentors.wealthdragons.dialogFragment.PayDialogFragment;
import com.imentors.wealthdragons.dialogFragment.PaymentCardListDialog;
import com.imentors.wealthdragons.digitalCoachingDetailView.DigitalCoachingBelowHeading;
import com.imentors.wealthdragons.digitalCoachingDetailView.DigitalCoachingHeadingBelowVideo;
import com.imentors.wealthdragons.digitalCoachingDetailView.DigitalCoachingHeader;
import com.imentors.wealthdragons.episodeDetailView.EpisodeDetailBelowVideo;
import com.imentors.wealthdragons.episodeDetailView.EpisodeDetailHeader;
import com.imentors.wealthdragons.eventDetailView.EventDetailBelowVideo;
import com.imentors.wealthdragons.eventDetailView.EventDetailDescription;
import com.imentors.wealthdragons.eventDetailView.EventDetailDescriptionBelow;
import com.imentors.wealthdragons.eventDetailView.EventDetailHeader;
import com.imentors.wealthdragons.mentorDetailview.MentorDetailHeader;
import com.imentors.wealthdragons.mentorDetailview.MentorDetailVideo;
import com.imentors.wealthdragons.mentorDetailview.MentorDetailMentorDescription;
import com.imentors.wealthdragons.mentorDetailview.MentorDigitalDescription;
import com.imentors.wealthdragons.mentorDetailview.MentorDigitalVideo;
import com.imentors.wealthdragons.mentorDetailview.MentorEducation;
import com.imentors.wealthdragons.mentorDetailview.MentorJobDescriptionItems;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.CreditCard;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.DashBoardEpisodeDetails;
import com.imentors.wealthdragons.models.DashBoardEventDetails;
import com.imentors.wealthdragons.models.DashBoardItemOdering;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.models.DashBoardVideoDetails;
import com.imentors.wealthdragons.models.DigitalCoaching;
import com.imentors.wealthdragons.models.DigitalCoachingDetails;
import com.imentors.wealthdragons.models.EclassesType;
import com.imentors.wealthdragons.models.VideoData;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.ItemTypeDeserilization;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.courseDetailView.CourseDetailBelowVideo;
import com.imentors.wealthdragons.courseDetailView.CourseDetailDescription;
import com.imentors.wealthdragons.courseDetailView.CourseDetailEClassesview;
import com.imentors.wealthdragons.courseDetailView.CourseDetailHeader;
import com.imentors.wealthdragons.courseDetailView.CourseDetailMentorDescription;
import com.imentors.wealthdragons.courseDetailView.CourseDetailRatings;
import com.imentors.wealthdragons.courseDetailView.CourseDetailReviewList;
import com.imentors.wealthdragons.courseDetailView.DetailItemListView;
import com.imentors.wealthdragons.videoDetailView.VideoDetailBelowVideo;
import com.imentors.wealthdragons.videoDetailView.VideoDetailHeader;
import com.imentors.wealthdragons.views.WealthDragonTextView;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2core.Func;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class DashBoardDetailFragment extends BaseFragment implements CourseDetailBelowVideo.OnItemClickListener, MentorDetailMentorDescription.OnItemClickListener, MentorDetailVideo.OnFavClickListener {


    private Context mContext;
    private LinearLayout mProgressBar;
    private LinearLayout mLlScreenContainer;
    private NestedScrollView mNestedScrollView;
    private LinearLayoutManager layoutManager;
    private SwipeRefreshLayout mSwipeRefershLayout;
    private String mCatId, mDetailKey, mEpisodeKey;
    private CourseDetailHeader mCourseDetailHeader = null;
    private VideoDetailHeader mVideoDetailHeader = null;
    private EventDetailHeader mEventDetailHeader = null;
    private EpisodeDetailHeader mEpisodeDetailHeader = null;
    private MentorDetailVideo mMentorDetailVideo = null;
    private MentorDigitalVideo mMentorDigitalVideo = null;

    private DashBoardCourseDetails mDashBoardDetails;
    private DashBoardMentorDetails mMentorDetails;

    private String mResponse;
    private WealthDragonTextView mLoaderTitle;
    private Timer saveSeekTime;
    // used for free paid condition in runnning videos
    private String mPlayedEclassesId = null,mSavedResponse;
    private DashBoardVideoDetails mVideoDetail;
    private DigitalCoachingHeader digitalCoachingHeader;
    private boolean mIsDownloaded;
    private CourseDetailEClassesview eClassesview;
    private CourseDetailDescription courseDetailDescription;



    public static DashBoardDetailFragment newInstance(String categoryId, String detialType, String episodeId, boolean isDownloaded, String savedResponse) {


        //  put data in bundle
        Bundle args = new Bundle();

        args.putString(Constants.CAT_ID, categoryId);
        args.putString(Constants.DETAIL_KEY, detialType);
        if (!TextUtils.isEmpty(episodeId)) {
            args.putString(Constants.EPISODE_KEY, episodeId);
        }

        args.putBoolean(Constants.IS_DOWNLOADED,isDownloaded);

        if (!TextUtils.isEmpty(savedResponse)) {
            args.putString(Constants.SAVED_RESPONSE, savedResponse);
        }

        DashBoardDetailFragment fragment = new DashBoardDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //callEClassesImagesAPi(Api.ECLASSES_ICON_API);
        mCatId = getArguments().getString(Constants.CAT_ID);
        mDetailKey = getArguments().getString(Constants.DETAIL_KEY);
        boolean hasEpisode = getArguments().containsKey(Constants.EPISODE_KEY);
        boolean hasSavedResponse = getArguments().containsKey(Constants.SAVED_RESPONSE);

        mIsDownloaded = getArguments().getBoolean(Constants.IS_DOWNLOADED);

        if(hasSavedResponse){
            mSavedResponse = getArguments().getString(Constants.SAVED_RESPONSE);
        }

        if (hasEpisode) {
            mEpisodeKey = getArguments().getString(Constants.EPISODE_KEY);
        }
        Utils.releaseDetailScreenPlayer();
        Utils.releaseFullScreenPlayer();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_dashboard_details, container, false);

        mProgressBar = view.findViewById(R.id.progressBar);
        mLlScreenContainer = view.findViewById(R.id.ll_screen_container);
        mNestedScrollView = view.findViewById(R.id.nested_scroll_view);
        layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);
        mLoaderTitle = view.findViewById(R.id.tv_search);
        callExistingCardListApi();

        if (!TextUtils.isEmpty(mCatId)) {
            if(mIsDownloaded){
                mSwipeRefershLayout.setEnabled(false);
                handleDashBoardResponse(mSavedResponse);
            }else{
                callDetailApi();
            }
        }

        switch (mDetailKey) {
            case Constants.TYPE_COURSE:
                mLoaderTitle.setText(getString(R.string.loading_course_details));
                break;
            case Constants.TYPE_MENTOR:
                mLoaderTitle.setText(getString(R.string.loading_expert_channel));
                break;
            case Constants.TYPE_EXPERT:
                mDetailKey = Constants.TYPE_MENTOR;
                mLoaderTitle.setText(getString(R.string.loading_expert_channel));
                break;
            case Constants.TYPE_EVENT:
                mLoaderTitle.setText(getString(R.string.loading_event_details));
                break;
            case Constants.TYPE_PROGRAMMES:
                mLoaderTitle.setText(getString(R.string.loading_programmes_details));
                break;
            case Constants.TYPE_EPISODE:
                mLoaderTitle.setText(getString(R.string.loading_episode_details));
                break;
            case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING:
                mLoaderTitle.setText(getString(R.string.loading_vip_club));
                break;
            default:
                mLoaderTitle.setText(getString(R.string.loading_video_details));
                break;
        }


        mSwipeRefershLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                switch (mDetailKey) {
                    case Constants.TYPE_COURSE:
                        callDetailApi();
                        Utils.callEventLogApi("refreshed <b>Course Detail </b> screen");
                        break;
                    case Constants.TYPE_MENTOR:
                        callDetailApi();
                        Utils.callEventLogApi("refreshed <b>Mentor Detail </b> screen");
                        break;
                    case Constants.TYPE_EXPERT:
                        callDetailApi();
                        Utils.callEventLogApi("refreshed <b>Expert Detail </b> screen");
                        break;
                    case Constants.TYPE_EVENT:
                        callDetailApi();
                        Utils.callEventLogApi("refreshed <b>Event Detail </b> screen");
                        break;
                    case Constants.TYPE_PROGRAMMES:
                        callDetailApi();
                        Utils.callEventLogApi("refreshed <b>Programmes Detail </b> screen");
                        break;
                    case Constants.TYPE_EPISODE:
                        callDetailApi();
                        Utils.callEventLogApi("refreshed <b>Episode Detail </b> screen");
                        break;
                    case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING:
                        callDetailApi();
                        Utils.callEventLogApi("refreshed <b>Mentor Digital Coaching Detail </b> screen");
                        break;
                    default:
                        callDetailApi();
                        Utils.callEventLogApi("refreshed <b>Video Detail </b> screen");
                        break;
                }
            }
        });

        return view;
    }

    // used always except refresh
    public void callDetailApi() {

        String api = null;

        if (mSwipeRefershLayout.isRefreshing()) {
            mSwipeRefershLayout.setRefreshing(true);
            mProgressBar.setVisibility(View.GONE);
        } else {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        switch (mDetailKey) {
            case Constants.TYPE_COURSE:
                api = Api.COURSE_DETAIL_API;
                break;
            case Constants.TYPE_MENTOR:
                api = Api.MENTOR_DETAIL_API;
                break;
            case Constants.TYPE_EXPERT:
                api = Api.MENTOR_DETAIL_API;
                break;
            case Constants.TYPE_EVENT:
                api = Api.EVENT_DETAIL_API;
                break;
            case Constants.TYPE_PROGRAMMES:
                api = Api.PROGRAMMES_DETAIL_API;
                break;
            case Constants.TYPE_EPISODE:
                api = Api.EPISODES_DETAIL_API;
                break;
            case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING:
                api = Api.MENTOR_DETAIL_DIGITAL_COACHING_DETAIL_PAGE;
                break;

            default:
                api = Api.VIDEO_DETAIL_API;
                break;
        }

        // calling dashboard detail api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);
                if (mSwipeRefershLayout.isRefreshing()) {
                    mSwipeRefershLayout.setRefreshing(false);
                }
                try {
                    if (mDetailKey.equals(Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING)) {
                        showDataOnView(response);
                    } else {
                        onGetDataAPIServerResponse(response);
                    }
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>Dashboard Detail </b> error in api");

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>Dashboard Detail </b> error in api");
                error.printStackTrace();
                if(!mIsDownloaded){
                    Utils.showNetworkError(mContext, error);

                }
                mSwipeRefershLayout.setRefreshing(false);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                if (!TextUtils.isEmpty(mCatId)) {
                    switch (mDetailKey) {
                        case Constants.TYPE_COURSE:
                            params.put(Keys.COURSE_ID, mCatId);
                            break;

                        case Constants.TYPE_MENTOR:
                            params.put(Keys.MENTOR_ID, mCatId);
                            break;

                        case Constants.TYPE_EXPERT:
                            params.put(Keys.MENTOR_ID, mCatId);
                            break;

                        case Constants.TYPE_VIDEO:
                            params.put(Keys.VIDEOS_ID, mCatId);
                            break;

                        case Constants.TYPE_EPISODE:
                            params.put(Keys.VIDEOS_ID, mCatId);
                            params.put(Keys.EPISODE_ID, mEpisodeKey);

                            break;
                        case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING:
                            params.put(Keys.MENTOR_ID, mCatId);
                            break;

                        case Constants.TYPE_EVENT:
                            params.put(Keys.EVENT_ID, mCatId);
                            break;

                        case Constants.TYPE_PROGRAMMES:
                            params.put(Keys.PROGRAMMES_ID, mCatId);
                            break;
                    }
                }

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    // handling dashboard detail api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
        // empty app preferences data because now article view can adjust right visibility of courses data with null condition
        AppPreferences.setMentorDashBoardDetail(null);
        AppPreferences.setCourseDashBoardDetail(null);
        AppPreferences.setVideoDashBoardDetail(null);
        AppPreferences.setEpisodeDashBoardDetail(null);
        AppPreferences.setCourseDashBoardWishList(null);
        handleDashBoardResponse(response);
        mPlayedEclassesId = null;
    }

    private void handleDashBoardResponse(String response) {

        mResponse = response;
        final GsonBuilder gsonBuilder = new GsonBuilder();

        // Parse JSON to Java
        gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
        final Gson gson = gsonBuilder.create();

        switch (mDetailKey) {
            case Constants.TYPE_COURSE:
                showCoursesInDetailPage(response, gson);
                break;
            case Constants.TYPE_MENTOR:
                showMentorInDetailPage(response, gson);
                break;
            case Constants.TYPE_EXPERT:
                showMentorInDetailPage(response, gson);
                break;
            case Constants.TYPE_EVENT:
                showEventInDetailPage(response, gson);
                break;
            case Constants.TYPE_PROGRAMMES:
                showCoursesInDetailPage(response, gson);
                break;
            case Constants.TYPE_EPISODE:
                showEpisodeInDetailPage(response, gson);
                break;
            case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING:
                showDigitalCoachingInDetailPage(response, gson);
                break;

            default:
                showVideoInDetailPage(response, gson);
                break;
        }


        startTimerToSendVideoSeekTime();

    }

    private void startTimerToSendVideoSeekTime() {
        saveSeekTime = new Timer();

        saveSeekTime.schedule(new TimerTask() {
            @Override
            public void run() {
                sendSeekTime();
                setSaveSeekTimeInCourses();
            }
        }, 100, 1000);

    }

    // handling courses api response
    private void showCoursesInDetailPage(String response, Gson gson) {
        mLlScreenContainer.removeAllViews();

        //  parse json to java
        final DashBoardCourseDetails dashBoard = gson.fromJson(response, DashBoardCourseDetails.class);

        AppPreferences.setCourseDashBoardDetail(dashBoard);

        try {
            callScrapApi(dashBoard.getCourseDetail().getShare_url());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mDashBoardDetails = dashBoard;

        mLlScreenContainer.removeAllViews();


        // calling constructor in course detail header
        mCourseDetailHeader = new CourseDetailHeader(mContext, dashBoard, getActivity(), new CourseDetailHeader.OnDashBoardDetailReload() {
            @Override
            public void onItemClicked() {
                clearResponse();
                reload();
            }
        }, this,mIsDownloaded);

        mLlScreenContainer.addView(mCourseDetailHeader);

        if (dashBoard.isBelowVideoVisible() && !mIsDownloaded) {

            // calling constructor in course detail below video
            mLlScreenContainer.addView(new CourseDetailBelowVideo(mContext, dashBoard, this, mDetailKey.equals(Constants.TYPE_COURSE)));
        }


        // calling constructor in course detail description
        courseDetailDescription = new CourseDetailDescription(mContext, dashBoard, new CourseDetailEClassesview.OnAllDownload() {
            @Override
            public void onDownnloadClick() {
                eClassesview.downloadAllVideos();
            }

            @Override
            public void onCancelDownloadClick() {
                eClassesview.cancelAllVideos();
            }
        },mIsDownloaded);
        mLlScreenContainer.addView(courseDetailDescription);


        if (mDetailKey.equals(Constants.TYPE_COURSE)) {
            if (dashBoard.getCourseDetail().getChapters().size() > 0 && dashBoard.getCourseDetail().getChapters().get(0).getE_classes().size() > 0) {

                // calling constructor in course detail Eclasses view
                eClassesview = new CourseDetailEClassesview(mContext, dashBoard.getCourseDetail().getChapters().get(0), getString(R.string.e_classes), dashBoard.isEclassVisible()
                        && dashBoard.getCourseDetail().getChapters().get(0).getE_classes().get(0).isAction(),
                        dashBoard.isEclassVisible(), mCatId, dashBoard.getCourseDetail().getFinal_cost(), mDetailKey,
                        dashBoard.getCourseDetail().getTitle(), mPlayedEclassesId, mDashBoardDetails.isProgrammes(),
                        mDashBoardDetails.getCourseDetail().getBanner(),mDashBoardDetails.getCourseDetail().getPunch_line(),
                        mDashBoardDetails.getCourseDetail().getMentors().get(0).getName(),mIsDownloaded,mDashBoardDetails, new EClassesListAdapter.ShowDownlaodIcon() {
                    @Override
                    public void onDownloaded(String show) {
                        courseDetailDescription.onDownloaded(show);
                    }
                });
                mLlScreenContainer.addView(eClassesview);
            }
        } else {
            if (dashBoard.getCourseDetail().getChapters().size() > 0) {
                boolean selectedEClassesSend = true;
                for (Chapters chapters : dashBoard.getCourseDetail().getChapters()) {
                    if (selectedEClassesSend) {

                        // calling constructor in course detail Eclasses view
                        eClassesview = new CourseDetailEClassesview(mContext, chapters, chapters.getTitle(),
                                dashBoard.isEclassVisible() &&
                                        dashBoard.getCourseDetail().getChapters().get(0).getE_classes().get(0).isAction(), dashBoard.isEclassVisible(), mCatId,
                                dashBoard.getCourseDetail().getFinal_cost(), mDetailKey, dashBoard.getCourseDetail().getTitle(),
                                mPlayedEclassesId, mDashBoardDetails.isProgrammes(),
                                mDashBoardDetails.getCourseDetail().getBanner(),mDashBoardDetails.getCourseDetail().getPunch_line(),
                                mDashBoardDetails.getCourseDetail().getMentors().get(0).getName(),mIsDownloaded,mDashBoardDetails ,new EClassesListAdapter.ShowDownlaodIcon() {
                            @Override
                            public void onDownloaded(String show) {
                                courseDetailDescription.onDownloaded(show);
                            }
                        });
                        mLlScreenContainer.addView(eClassesview);
                        selectedEClassesSend = false;
                    } else {

                        eClassesview = new CourseDetailEClassesview(mContext, chapters,
                                chapters.getTitle(), false,
                                dashBoard.isEclassVisible(), mCatId,
                                dashBoard.getCourseDetail().getFinal_cost(),
                                mDetailKey, dashBoard.getCourseDetail().getTitle(),
                                mPlayedEclassesId, mDashBoardDetails.isProgrammes(),
                                mDashBoardDetails.getCourseDetail().getBanner(),
                                mDashBoardDetails.getCourseDetail().getPunch_line(),
                                mDashBoardDetails.getCourseDetail().getMentors().get(0).getName(),mIsDownloaded,mDashBoardDetails , new EClassesListAdapter.ShowDownlaodIcon() {
                            @Override
                            public void onDownloaded(String show) {

                                courseDetailDescription.onDownloaded(show);


                            }
                        });
                        // calling constructor in course detail Eclasses view
                        mLlScreenContainer.addView(eClassesview);
                    }
                }
            }
        }


        // calling constructor in course detail learning
        if (mDashBoardDetails.getCourseDetail().getCourseLearning() != null && mDashBoardDetails.getCourseDetail().getCourseLearning().size() > 0) {
            mLlScreenContainer.addView(new CourseListNewItems(mContext, dashBoard.getCourseDetail().getCourseLearning(), getString(R.string.course_learning), Constants.COURSE_LEARNING));
        }


        // calling constructor in course detail requirement
        if (mDashBoardDetails.getCourseDetail().getCourseRequirement() != null && mDashBoardDetails.getCourseDetail().getCourseRequirement().size() > 0) {
            if (mDashBoardDetails.isProgrammes()) {
                mLlScreenContainer.addView(new CourseListNewItems(mContext, dashBoard.getCourseDetail().getCourseRequirement(), getString(R.string.programme_requirement), Constants.COURSE_REQUIREMENTS));

            } else {
                mLlScreenContainer.addView(new CourseListNewItems(mContext, dashBoard.getCourseDetail().getCourseRequirement(), getString(R.string.course_requirement), Constants.COURSE_REQUIREMENTS));

            }
        }

        // calling constructor in course detail target audience
        if (mDashBoardDetails.getCourseDetail().getCourseTargetAudience() != null && mDashBoardDetails.getCourseDetail().getCourseTargetAudience().size() > 0) {
            mLlScreenContainer.addView(new CourseListNewItems(mContext, dashBoard.getCourseDetail().getCourseTargetAudience(), getString(R.string.target_audeince_heading), Constants.COURSE_TARGET_AUDINECE));
        }

        // calling constructor in course detail region
        if (mDashBoardDetails.getCourseDetail().getCourseReagon() != null && mDashBoardDetails.getCourseDetail().getCourseReagon().size() > 0) {
            mLlScreenContainer.addView(new CourseListNewItems(mContext, dashBoard.getCourseDetail().getCourseReagon(), null, Constants.COURSE_REGION));
        }

        // calling constructor in course faqs
        if (mDashBoardDetails.getCourseDetail().getQuestionAnswers() != null && mDashBoardDetails.getCourseDetail().getQuestionAnswers().size() > 0) {
            if (mDashBoardDetails.isProgrammes()) {
                mLlScreenContainer.addView(new CourseListNewItems(mContext, dashBoard.getCourseDetail().getQuestionAnswers(), getString(R.string.programme_faqs), Constants.COURSE_FAQS));

            } else {
                mLlScreenContainer.addView(new CourseListNewItems(mContext, dashBoard.getCourseDetail().getQuestionAnswers(), getString(R.string.course_faqs), Constants.COURSE_FAQS));

            }
        }


        // calling constructor in course detail mentor description
        mLlScreenContainer.addView(new CourseDetailMentorDescription(mContext, dashBoard));


        View v = new View(mContext);
        LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Utils.dpToPx(1));
        Params.setMargins(Utils.dpToPx(4), Utils.dpToPx(16), Utils.dpToPx(4), 0);
        v.setLayoutParams(Params);
        v.setBackgroundColor(getResources().getColor(R.color.light_background_gray));

        if ((dashBoard.getCourseDetail().getRelated_Courses() != null && dashBoard.getCourseDetail().getRelated_Courses().getCourses() != null && dashBoard.getCourseDetail().getRelated_Courses().getCourses().size() > 0)
                || (dashBoard.getCourseDetail().getRelated_Programme() != null && dashBoard.getCourseDetail().getRelated_Programme().getProgrammes() != null && dashBoard.getCourseDetail().getRelated_Programme().getProgrammes().size() > 0)
                && !mIsDownloaded) {

            mLlScreenContainer.addView(v);
        }


        if (dashBoard.getCourseDetail().getRelated_Courses() != null && !mIsDownloaded) {
            if (dashBoard.getCourseDetail().getRelated_Courses().getCourses() != null) {
                if (dashBoard.getCourseDetail().getRelated_Courses().getCourses().size() > 0) {
                    mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getCourseDetail().getRelated_Courses().getCourses(), dashBoard.getCourseDetail().getMentor_id(),
                            (dashBoard.getCourseDetail().getRelated_Courses().getCourses().size() > 1) ? mContext.getString(R.string.courses_from_title) + " " +
                                    dashBoard.getCourseDetail().getRelated_Courses().getCourses().get(0).getMentor_name() : mContext.getString(R.string.course_from_title) + " " +
                                    dashBoard.getCourseDetail().getRelated_Courses().getCourses().get(0).getMentor_name(),
                            dashBoard.getCourseDetail().getRelated_Courses().getNext_page(),
                            dashBoard.getCourseDetail().getRelated_Courses().getTotal_items(), dashBoard.getCourseDetail().getId(),
                            Constants.TYPE_COURSE_DASHBOARD_DETAIL_RELATED_COURSE, false,false));

                }
            }
        }

        if (dashBoard.getCourseDetail().getRelated_Programme() != null && !mIsDownloaded) {
            if (dashBoard.getCourseDetail().getRelated_Programme().getProgrammes() != null) {
                if (dashBoard.getCourseDetail().getRelated_Programme().getProgrammes().size() > 0) {
                    mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getCourseDetail().getRelated_Programme().getProgrammes(), dashBoard.getCourseDetail().getMentor_id(), getString(R.string.buy_complete_programme), dashBoard.getCourseDetail().getRelated_Programme().getNext_page(), dashBoard.getCourseDetail().getRelated_Programme().getTotal_items(), null, Constants.TYPE_COURSE_DASHBOARD_DETAIL_PROGRAMME_COURSE, true,false));

                }
            }
        }

        if (dashBoard.isShowRatings()) {


            // calling constructor in course detail ratings
            mLlScreenContainer.addView(new CourseDetailRatings(mContext, dashBoard));

            // calling constructor in course detail review list
            mLlScreenContainer.addView(new CourseDetailReviewList(mContext, dashBoard));
        }

    }


    public void startNewVideo(String id, int position) {
        //only for courses detail page
        if (mCourseDetailHeader != null) {
            mCourseDetailHeader.startNewPlayer(id, position);
            mNestedScrollView.post(new Runnable() {
                @Override
                public void run() {

                    ObjectAnimator.ofInt(mNestedScrollView, "scrollY", 0).setDuration(400).start();
//                    mNestedScrollView.scrollTo(0,0);
                }
            });
        }

    }

    public void stopVideo() {
        //only for courses detail page
        if (mCourseDetailHeader != null) {
            mCourseDetailHeader.closePlayer();
        }

    }


    public void clearScreen() {
        //only for courses detail page
        if (mCourseDetailHeader != null) {
            mCourseDetailHeader.clearPlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (saveSeekTime != null) {
            saveSeekTime.cancel();
            saveSeekTime = null;
        }
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mReloadPayResponseListener);
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mClearResponse);
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mFreePaidResponseListener);


    }


    public void reload() {
        if (TextUtils.isEmpty(mResponse)) {
            callDetailApi();
        } else {
            handleDashBoardResponse(mResponse);
        }
    }

    // so that detail page always get fully refresh after payment
    public void clearResponse() {
        mResponse = null;
    }


    @Override
    public void onPause() {
        super.onPause();
        pauseVideos();
    }


    public void pauseVideos() {
        //only for courses detail page
        if (mCourseDetailHeader != null) {
            mCourseDetailHeader.pauseVideo();
        }

        if (mVideoDetailHeader != null) {
            mVideoDetailHeader.pauseVideo();

        }

        if (mEpisodeDetailHeader != null) {
            mEpisodeDetailHeader.pauseVideo();
        }

        if (mEventDetailHeader != null) {
            mEventDetailHeader.pauseVideo();
        }

        if (mMentorDetailVideo != null) {
            mMentorDetailVideo.pauseVideo();
        }

        if (mMentorDigitalVideo != null) {
            mMentorDigitalVideo.pauseVideo();
        }

        if (digitalCoachingHeader != null) {
            digitalCoachingHeader.pauseVideo();
        }


        // to inform audio player in case of pause
        LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcast(new Intent(Constants.BROADCAST_IS_AUDIO_ACTIVE));

    }


    // handling mentor detail api response
    private void showMentorInDetailPage(String response, Gson gson) {

        //  parse json to java
        final DashBoardMentorDetails dashBoard = gson.fromJson(response, DashBoardMentorDetails.class);

        AppPreferences.setMentorDashBoardDetail(dashBoard);

        mLlScreenContainer.removeAllViews();
        mMentorDetails = dashBoard;
        try {
            callScrapApi(dashBoard.getProfile().getShare_url());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // calling constructor mentor detail video
        mLlScreenContainer.addView(new MentorDetailHeader(mContext, dashBoard, mIsDownloaded));

        // calling constructor mentor detail video

//        if(!TextUtils.isEmpty(dashBoard.getProfile().getExpert_intro_video()) && !TextUtils.equals(dashBoard.getProfile().getExpert_intro_video(),"NoVideoExist")) {






        List<DashBoardMentorDetails.Ranking> rankingList = dashBoard.getProfile().getSectionOrdering();

        boolean isIntroVideoAvaialble = false;


        for(DashBoardMentorDetails.Ranking rank: rankingList){
            if(rank.getId().equals(Constants.MENTOR_INTRODUCTION_VIDEO)){
                isIntroVideoAvaialble = true;
            }
        }


            mMentorDetailVideo = new MentorDetailVideo(mContext, dashBoard, getActivity(), this, this, new MentorDigitalVideo.OnVideoPlayListener() {
                @Override
                public void onVideoPlayed() {
                    mMentorDigitalVideo.stopVideo();
                }
            },isIntroVideoAvaialble,mIsDownloaded);

            mLlScreenContainer.addView(mMentorDetailVideo);


            if( (dashBoard.getProfile().getDigi_subscription().equals("No") && dashBoard.getProfile().getJoin_digi_btn().equals("Yes")) || dashBoard.getProfile().getFeeds_btn().equals("Yes") || dashBoard.getProfile().getExpert_subscription_btn()) {
                View v = new View(mContext);
                LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Utils.dpToPx(1));
                Params.setMargins(Utils.dpToPx(4), Utils.dpToPx(4), Utils.dpToPx(4), 0);
                v.setLayoutParams(Params);
                v.setBackgroundColor(getResources().getColor(R.color.light_background_gray));

                mLlScreenContainer.addView(v);
            }








        for (DashBoardMentorDetails.Ranking rank : rankingList) {

            switch (rank.getId()) {
                case Constants.MENTOR_ABOUT:
                    // calling constructor mentor detail mentor description
                    mLlScreenContainer.addView(new MentorDetailMentorDescription(mContext, dashBoard, new MentorDetailMentorDescription.OnLiveClickListener() {
                        @Override
                        public void onLiveButtonClick() {
                            clearResponse();

                        }
                    }));
                    break;
                case Constants.MENTOR_COURSES_LIST:


                    if (dashBoard.getCourses() != null && !mIsDownloaded) {
                        if (dashBoard.getCourses().getCourses().size() > 0) {
                            //  calling constructor detail item list view
                            mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getCourses().getCourses(), dashBoard.getProfile().getId(), (dashBoard.getCourses().getCourses().size() > 1) ? mContext.getString(R.string.title_course) : Constants.TYPE_COURSE, dashBoard.getCourses().getNext_page(), dashBoard.getCourses().getTotal_items(), null, Constants.TYPE_MENTOR_DASHBOARD_DETAIL_COURSE, false,false));
                        }

                    }

                    break;
                case Constants.MENTOR_EVENTS_LIST:
                    if (dashBoard.getEvents() != null && !mIsDownloaded) {
                        if (dashBoard.getEvents().getSmallLeterEvents().size() > 0) {
                            //  calling constructor detail item list view
                            mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getEvents(), dashBoard.getEvents().getId(), (dashBoard.getEvents().getSmallLeterEvents().size() > 1) ? mContext.getString(R.string.title_video) : Constants.TYPE_EVENT, dashBoard.getEvents().getNext_page(), dashBoard.getEvents().getTotal_items(), Constants.TYPE_MENTOR_DASHBOARD_DETAIL_EVENT));
                        }

                    }


                    if (dashBoard.getVideos() != null && !mIsDownloaded) {
                        if (dashBoard.getVideos().getVideos().size() > 0) {

                            //  calling constructor detail item list view
                            mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getVideos().getVideos(), dashBoard.getProfile().getId(), (dashBoard.getVideos().getVideos().size() > 1) ? mContext.getString(R.string.title_video) : Constants.TYPE_EVENT, dashBoard.getVideos().getNext_page(), dashBoard.getVideos().getTotal_items(), Constants.TYPE_MENTOR_DASHBOARD_DETAIL_VIDEO));
                        }

                    }


                    break;

                case Constants.MENTOR_DIGITAL_COACHING_INFO:
                    // digital coaching mentor description


                    mLlScreenContainer.addView(new MentorDigitalDescription(mContext, dashBoard));


                    break;


                case Constants.MENTOR_DIGITAL_COACHING_INTRODUCTION_VIDEO:
                    // mentor digital video

                    mMentorDigitalVideo = new MentorDigitalVideo(mContext, dashBoard, getActivity(), new MentorDigitalVideo.OnVideoPlayListener() {
                        @Override
                        public void onVideoPlayed() {
                            if(mMentorDetailVideo!=null)
                                mMentorDetailVideo.stopVideo();
                        }
                    },mIsDownloaded);
                    mLlScreenContainer.addView(mMentorDigitalVideo);


                    break;


                case Constants.MENTOR_EDUCATION:

                    if (dashBoard.getProfile().getEducations() != null) {
                        if (dashBoard.getProfile().getEducations().size() > 0) {
                            mLlScreenContainer.addView(new MentorEducation(mContext, dashBoard.getProfile().getEducations(), getString(R.string.mentor_education)));

                        }
                    }


                    break;

                case Constants.MENTOR_ABOUT_SKILLS:


                    // calling constructor in Mentor SKills
                    if (dashBoard.getProfile().getSkillEndorsements() != null && dashBoard.getProfile().getSkillEndorsements().size() > 0) {
                        mLlScreenContainer.addView(new CourseListNewItems(mContext, dashBoard.getProfile().getSkillEndorsements(), getString(R.string.mentor_skills), Constants.MENTOR_SKILLS));
                    }




                    break;

                case Constants.MENTOR_ABOUT_HONORS:


                    // calling constructor in Mentor Honors
                    if (dashBoard.getProfile().getHonorsAwards() != null && dashBoard.getProfile().getHonorsAwards().size() > 0) {
                        mLlScreenContainer.addView(new CourseListNewItems(mContext, dashBoard.getProfile().getHonorsAwards(), getString(R.string.mentor_honors), Constants.MENTOR_HONORS));
                    }


                    break;

                case Constants.MENTOR_DIGITAL_COACHING_LIST:



                    final List<DigitalCoaching> videoList = new ArrayList<>();

                    final List<DigitalCoaching> actualList = new ArrayList<>();


                    if(mIsDownloaded){

                        actualList.addAll(dashBoard.getDigitalCoaching().getCoachings());

                        Fetch fetch = Utils.getFetchInstance();

                        fetch.getDownloadsInGroup(Integer.valueOf(dashBoard.getProfile().getId()), new Func<List<Download>>() {
                            @Override
                            public void call(@NotNull List<Download> result) {

                                if(result!=null && result.size()>0) {

                                    videoList.clear();



                                    for(Download download:result){
                                        if(download.getExtras().getString(Constants.TYPE,Constants.TYPE).equals(Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING)) {

                                            DigitalCoaching coaching = null;
                                            for(DigitalCoaching digitalCoaching:actualList){
                                                if(digitalCoaching.getTitle().equals(download.getExtras().getString(Constants.TITLE, Constants.TYPE_COURSE))){
                                                    coaching = digitalCoaching;
                                                }
                                            }


                                                videoList.add(new DigitalCoaching(
                                                    download.getExtras().getString(Constants.TITLE, Constants.TYPE_COURSE),
                                                    download.getExtras().getString(Constants.ID, Constants.TYPE_COURSE),
                                                    dashBoard.getProfile().getId(),
                                                    download.getExtras().getString(Constants.VIDEO_URL, Constants.TYPE_COURSE),
                                                    download.getExtras().getString(Constants.BANNER_PATH, Constants.TYPE_COURSE),
                                                    download.getFile(),coaching.getFree_paid(),coaching.getFreePaidText()
                                            ));
                                        }
                                    }

                                    if(videoList.size()>0){
                                        mLlScreenContainer.addView(new DetailItemListView(mContext, videoList, dashBoard.getProfile().getId(), dashBoard.getProfile().getDigital_coaching_section_heading(),dashBoard.getProfile().getDigital_coaching_section_heading_orange(), dashBoard.getDigitalCoaching().getNext_page(), dashBoard.getDigitalCoaching().getTotal_items(), dashBoard.getProfile().getDigi_subscription(), dashBoard.getProfile().getNext_video(),dashBoard.getProfile().getDigi_coaching_screen(),dashBoard.getProfile().getName(),dashBoard.getProfile().getBanner(), new DetailItemListView.OnDigiVideoPlayListener() {
                                            @Override
                                            public void onDigiVideoPlayed() {

                                                if(mMentorDetailVideo!=null)
                                                    mMentorDetailVideo.stopVideo();

                                                if(mMentorDigitalVideo !=null)
                                                    mMentorDigitalVideo.stopVideo();

                                            }
                                        },mIsDownloaded,dashBoard));
                                    }

                                }



                            }
                        });


                    }else {

                        if (dashBoard.getDigitalCoaching() != null ) {
                            if (dashBoard.getDigitalCoaching().getCoachings().size() > 0) {

                                //  calling constructor detail item list view
                                mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getDigitalCoaching().getCoachings(), dashBoard.getProfile().getId(), dashBoard.getProfile().getDigital_coaching_section_heading(),dashBoard.getProfile().getDigital_coaching_section_heading_orange(), dashBoard.getDigitalCoaching().getNext_page(), dashBoard.getDigitalCoaching().getTotal_items(), dashBoard.getProfile().getDigi_subscription(), dashBoard.getProfile().getNext_video(),dashBoard.getProfile().getDigi_coaching_screen(),dashBoard.getProfile().getName(),dashBoard.getProfile().getBanner(), new DetailItemListView.OnDigiVideoPlayListener() {
                                    @Override
                                    public void onDigiVideoPlayed() {

                                        if(mMentorDetailVideo!=null)
                                            mMentorDetailVideo.stopVideo();

                                        if(mMentorDigitalVideo !=null)
                                            mMentorDigitalVideo.stopVideo();

                                    }
                                },mIsDownloaded,dashBoard));
                            }

                        }
                    }



                    break;

                case Constants.MENTOR_PROGRAMMES_LIST:


                    if (dashBoard.getProgrammes() != null && !mIsDownloaded) {
                        if (dashBoard.getProgrammes().getProgrammes().size() > 0) {

                            //  calling constructor detail item list view
                            mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getProgrammes().getProgrammes(), dashBoard.getProfile().getId(), (dashBoard.getProgrammes().getProgrammes().size() > 1) ? mContext.getString(R.string.buy_complete_programmes) : mContext.getString(R.string.buy_complete_programme), dashBoard.getProgrammes().getNext_page(), dashBoard.getProgrammes().getTotal_items(), null, Constants.TYPE_MENTOR_DASHBOARD_DETAIL_PROGRAMME, true,false));
                        }

                    }

                    break;

                case Constants.MENTOR_INTRODUCTION_VIDEO:



                    break;

                case Constants.MENTOR_EXPERINECE:

                    if (dashBoard.getProfile().getJobHistory() != null) {
                        if (dashBoard.getProfile().getJobHistory().size() > 0) {
                            for (int i = 0; i < dashBoard.getProfile().getJobHistory().size(); i++) {
                                boolean showDivider = true;
                                if (i == dashBoard.getProfile().getJobHistory().size() - 1) {
                                    showDivider = false;
                                }
                                if (i == 0) {
                                    mLlScreenContainer.addView(new MentorJobDescriptionItems(mContext, dashBoard.getProfile().getJobHistory().get(i), getString(R.string.mentor_job_experience), Constants.MENTOR_EXPERIENCE, showDivider));
                                } else {
                                    mLlScreenContainer.addView(new MentorJobDescriptionItems(mContext, dashBoard.getProfile().getJobHistory().get(i), null, Constants.MENTOR_EXPERIENCE, showDivider));
                                }

                            }
                        }
                    }
                    break;
            }

        }


    }


    // handling video detail api response
    private void showVideoInDetailPage(String response, Gson gson) {


        //  parse json to java
        final DashBoardVideoDetails dashBoard = gson.fromJson(response, DashBoardVideoDetails.class);

        AppPreferences.setVideoDashBoardDetail(dashBoard);
        mLlScreenContainer.removeAllViews();

        mVideoDetail = dashBoard;

        try {
            callScrapApi(dashBoard.getVideoDetail().getShare_url());
        } catch (Exception e) {
            e.printStackTrace();
        }


        // calling constructor in video detail header
        mVideoDetailHeader = new VideoDetailHeader(mContext, dashBoard, getActivity());


        mLlScreenContainer.addView(mVideoDetailHeader);

        // calling constructor in video detail below video
        mLlScreenContainer.addView(new VideoDetailBelowVideo(mContext, dashBoard));


        if (dashBoard.getGiven().getCourse() != null) {
            if (dashBoard.getGiven().getCourse().getCourses().size() > 0) {
                mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getGiven().getCourse().getCourses(), dashBoard.getGiven().getCourse().getCourses().get(0).getMentor_id(), (dashBoard.getGiven().getCourse().getCourses().size() > 1) ? mContext.getString(R.string.courses_from_title) + " " + dashBoard.getGiven().getCourse().getCourses().get(0).getMentor_name() : mContext.getString(R.string.course_from_title) + " " + dashBoard.getGiven().getCourse().getCourses().get(0).getMentor_name(), dashBoard.getGiven().getCourse().getNext_page(), dashBoard.getGiven().getCourse().getTotal_items(), null, Constants.TYPE_MENTOR_DASHBOARD_DETAIL_COURSE, false,false));
            }

        }

        if (dashBoard.getGiven().getProgramme() != null) {
            if (dashBoard.getGiven().getProgramme().getProgrammes().size() > 0) {
                mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getGiven().getProgramme().getProgrammes(), dashBoard.getGiven().getProgramme().getProgrammes().get(0).getMentor_id(), (dashBoard.getGiven().getProgramme().getProgrammes().size() > 1) ? mContext.getString(R.string.programmes_from_title) + " " + dashBoard.getGiven().getProgramme().getProgrammes().get(0).getMentor_name() : mContext.getString(R.string.programme_from_title) + " " + dashBoard.getGiven().getProgramme().getProgrammes().get(0).getMentor_name(), dashBoard.getGiven().getProgramme().getNext_page(), dashBoard.getGiven().getProgramme().getTotal_items(), null, Constants.TYPE_MENTOR_DASHBOARD_DETAIL_PROGRAMME, true,false));
            }

        }


        if (dashBoard.getTaken().getCourse() != null) {
            if (dashBoard.getTaken().getCourse().getCourses().size() > 0) {
                mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getTaken().getCourse().getCourses(), dashBoard.getTaken().getCourse().getCourses().get(0).getMentor_id(), (dashBoard.getTaken().getCourse().getCourses().size() > 1) ? mContext.getString(R.string.courses_from_title) + " " + dashBoard.getTaken().getCourse().getCourses().get(0).getMentor_name() : mContext.getString(R.string.course_from_title) + " " + dashBoard.getTaken().getCourse().getCourses().get(0).getMentor_name(), dashBoard.getTaken().getCourse().getNext_page(), dashBoard.getTaken().getCourse().getTotal_items(), null, Constants.TYPE_MENTOR_DASHBOARD_DETAIL_COURSE, false,false));
            }

        }

        if (dashBoard.getTaken().getProgramme() != null) {
            if (dashBoard.getTaken().getProgramme().getProgrammes().size() > 0) {
                mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getTaken().getProgramme().getProgrammes(), dashBoard.getTaken().getProgramme().getProgrammes().get(0).getMentor_id(), (dashBoard.getTaken().getProgramme().getProgrammes().size() > 1) ? mContext.getString(R.string.programmes_from_title) + " " + dashBoard.getTaken().getProgramme().getProgrammes().get(0).getMentor_name() : mContext.getString(R.string.programme_from_title) + " " + dashBoard.getTaken().getProgramme().getProgrammes().get(0).getMentor_name(), dashBoard.getTaken().getProgramme().getNext_page(), dashBoard.getTaken().getProgramme().getTotal_items(), null, Constants.TYPE_MENTOR_DASHBOARD_DETAIL_PROGRAMME, true,false));
            }

        }

//
//            if (dashBoard.getRelatedVideos() != null) {
//                if (dashBoard.getRelatedVideos().getVideos().size() > 0) {
//                        String[] firstMentorId = dashBoard.getRelatedVideos().getVideos().get(0).getMentor_id().split(",");
//                        mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getRelatedVideos().getVideos(), firstMentorId[0], mContext.getString(R.string.more_videos_title) + " " + dashBoard.getVideoDetail().getFirst_mentor_name(), dashBoard.getRelatedVideos().getNext_page(), dashBoard.getRelatedVideos().getTotal_items(), Constants.TYPE_MENTOR_DASHBOARD_DETAIL_VIDEO));
//                }
//
//            }

    }


    // handling episode detail api response
    private void showEpisodeInDetailPage(String response, Gson gson) {


        //  parse json to java
        final DashBoardEpisodeDetails dashBoard = gson.fromJson(response, DashBoardEpisodeDetails.class);

        AppPreferences.setEpisodeDashBoardDetail(dashBoard);
        mLlScreenContainer.removeAllViews();

        try {
            callScrapApi(dashBoard.getEpisodeDetail().getShare_url());
        } catch (Exception e) {
            e.printStackTrace();
        }
        // calling constructor in episode detail header
        mEpisodeDetailHeader = new EpisodeDetailHeader(mContext, dashBoard, getActivity());


        mLlScreenContainer.addView(mEpisodeDetailHeader);

        // calling constructor in episode detail below video
        mLlScreenContainer.addView(new EpisodeDetailBelowVideo(mContext, dashBoard));


//        if (dashBoard.getRelatedVideos() != null) {
//                if (dashBoard.getRelatedVideos().size()>0) {
//                    String[] firstMentorId = dashBoard.getRelatedVideos().get(0).getMentor_id().split(",");
//                    mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getRelatedVideos(), firstMentorId[0], mContext.getString(R.string.more_videos_title) + " this " + Constants.TYPE_EPISODE,0, 0, Constants.TYPE_MENTOR_DASHBOARD_DETAIL_VIDEO));
//                }
//
//        }

    }

    // handling event detail api response
    private void showEventInDetailPage(String response, Gson gson) {


        //  parse json to java
        final DashBoardEventDetails dashBoard = gson.fromJson(response, DashBoardEventDetails.class);

        mLlScreenContainer.removeAllViews();

        try {
            callScrapApi(dashBoard.getEvent().getShare_url());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mEventDetailHeader = new EventDetailHeader(mContext, dashBoard, getActivity());

        mLlScreenContainer.addView(mEventDetailHeader);
        if (dashBoard.isBelowVideoVisible()) {

            // calling constructor in event detail below video
            mLlScreenContainer.addView(new EventDetailBelowVideo(mContext, dashBoard, this));
        }

        // calling constructor in event detail description
        mLlScreenContainer.addView(new EventDetailDescription(mContext, dashBoard));

        // calling constructor in event detail description below
        mLlScreenContainer.addView(new EventDetailDescriptionBelow(mContext, dashBoard));

    }


    // handling digital coaching detail api response
    private void showDigitalCoachingInDetailPage(String response, Gson gson) {


        //  parse json to java
        Gson digitalGson = new Gson();
        Type listType = new TypeToken<List<DigitalCoachingDetails>>() {
        }.getType();
        List<DigitalCoachingDetails> digitalCoachingDetailsList = digitalGson.fromJson(response, listType);


        mLlScreenContainer.removeAllViews();

        AppPreferences.setVideoResumePosition(digitalCoachingDetailsList.get(0).getMentor_id(), Utils.getTimeInMilliSeconds(digitalCoachingDetailsList.get(0).getDigi_intro_video_seektime()));


        digitalCoachingHeader = new DigitalCoachingHeader(mContext, digitalCoachingDetailsList.get(0), getActivity());

        mLlScreenContainer.addView(digitalCoachingHeader);

        mLlScreenContainer.addView(new DigitalCoachingHeadingBelowVideo(mContext, null, digitalCoachingDetailsList.get(0).getMain_heading()));


        mLlScreenContainer.addView(new DigitalCoachingBelowHeading(mContext, digitalCoachingDetailsList.get(0).getContent(), digitalCoachingDetailsList.get(0).getCurrency() + digitalCoachingDetailsList.get(0).getCost(), digitalCoachingDetailsList.get(0).getMentor_id(), digitalCoachingDetailsList.get(0).getDigital_coaching_btn_name()));


    }

    // opening subscription page
    @Override
    public void onSubScriptionButtonClick(boolean isVideoEnded) {
//        Utils.callEventLogApi("clicked <b>Subscription Button  </b> ");
//        addFragment(SubscriptionFragment.newInstance(false, null), true);
        clearResponse();
        if (!isVideoEnded) {
            pauseVideos();
        }
        if (AppPreferences.getCreditCardList() != null && AppPreferences.getCreditCardList().size() > 0) {
            openDialog(PaymentCardListDialog.newInstance(mCatId, mDetailKey, null));
        } else {
            openDialog(PayDialogFragment.newInstance("Ruppee", false, false, mCatId, mDetailKey, null));
        }

    }


    public void openFullVideoScreen(int rotationAngle) {
        if (mCourseDetailHeader != null) {
            if (mCourseDetailHeader.getPlayBackState()) {
                mCourseDetailHeader.openFullScreenVideo(rotationAngle);
            }
        }

        if (mVideoDetailHeader != null) {
            if (mVideoDetailHeader.getPlayBackState()) {
                mVideoDetailHeader.openFullScreenVideo(rotationAngle);

            }
        }

        if (mEpisodeDetailHeader != null) {
            if (mEpisodeDetailHeader.getPlayBackState()) {
                mEpisodeDetailHeader.openFullScreenVideo(rotationAngle);

            }
        }

        if (mEventDetailHeader != null) {
            if (mEventDetailHeader.getPlayBackState()) {
                mEventDetailHeader.openFullScreenVideo(rotationAngle);

            }
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        if (saveSeekTime == null) {
            startTimerToSendVideoSeekTime();
        }
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mReloadPayResponseListener, new IntentFilter(Constants.BROADCAST_PAY_FINSIH));
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mFreePaidResponseListener, new IntentFilter(Constants.BROADCAST_CHECK_FREE_PAID_TRACK));
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mClearResponse, new IntentFilter(Constants.BROADCAST_CLEAR_REPONSE));
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mSetWishlist, new IntentFilter(Constants.BROADCAST_DETAIL_SET));

    }

    private void setSaveSeekTimeInCourses() {

        if (mCourseDetailHeader != null) {
            VideoData videoData = mCourseDetailHeader.returnSeekTimeForCourse();
            if (videoData != null) {
                if (mDashBoardDetails.isEclassVisible()) {
                    for (int i = 0; i < mDashBoardDetails.getCourseDetail().getChapters().size(); i++) {

                        Chapters chapters = mDashBoardDetails.getCourseDetail().getChapters().get(i);

                        for (int j = 0; j < chapters.getE_classes().size(); j++) {
                            Chapters.EClasses eClasses = mDashBoardDetails.getCourseDetail().getChapters().get(i).getE_classes().get(j);
                            if (eClasses.getId().equals(videoData.getVideoId())) {
                                mDashBoardDetails.getCourseDetail().getChapters().get(i).getE_classes().get(j).setSeek_time(videoData.getTime());
                                break;
                            }
                        }
                    }
                } else {
                    mDashBoardDetails.getCourseDetail().setSeek_time(videoData.getTime());

                }

                if (!TextUtils.isEmpty(mResponse)) {
                    mResponse = new Gson().toJson(mDashBoardDetails);
                }

                mCourseDetailHeader.emptyVideoData();
            }
        }
    }


    private void sendSeekTime() {


        //TODO implemented maintenance mode
        // calling sendSeekTime api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SAVE_VIDEO_TIME_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

//                Log.e("seek_reponse",response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("seek_reponse",error.toString());

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                VideoData mVideoData = null;

                if (mCourseDetailHeader != null) {
                    mVideoData = mCourseDetailHeader.getCurrentVideoData();

                }

                if (mVideoDetailHeader != null) {
                    mVideoData = mVideoDetailHeader.getCurrentVideoData();

                }

                if (mEpisodeDetailHeader != null) {
                    mVideoData = mEpisodeDetailHeader.getCurrentVideoData();

                }

                if (mEventDetailHeader != null) {
                    mVideoData = mEventDetailHeader.getCurrentEventVideoData();

                }


                if (mMentorDetailVideo != null) {
                    mVideoData = mMentorDetailVideo.getCurrentVideoData();
                }

                if (digitalCoachingHeader != null) {
                    mVideoData = digitalCoachingHeader.getCurrentVideoData();
                }

                if(mMentorDigitalVideo != null){
                    if(TextUtils.isEmpty(mVideoData.getTime())){
                        mVideoData = mMentorDigitalVideo.getCurrentVideoData();
                    }
                }


                if (mVideoData != null) {
                    params.put(Keys.VIDEOS_ID, mVideoData.getVideoId());
                    params.put(Keys.COUNTRY, mVideoData.getCountry());
                    params.put(Keys.TIME, mVideoData.getTime());
                    params.put(Keys.TYPE, mVideoData.getType());
                }


                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    public void closeFullScreenDialog() {

        if (mCourseDetailHeader != null) {

            mCourseDetailHeader.pressFullScreenDialogButton();

        }


        if (mVideoDetailHeader != null) {

            mVideoDetailHeader.pressFullScreenDialogButton();


        }

        if (mMentorDetailVideo != null) {

            mMentorDetailVideo.pressFullScreenDialogButton();


        }

        if (digitalCoachingHeader != null) {
            digitalCoachingHeader.pressFullScreenDialogButton();
        }


    }


    // for fb app link
    private void callScrapApi(String shareUrl) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("scrape", "true");
        jsonObject.put("id", shareUrl);

        GraphRequest request = GraphRequest.newPostRequest(
                AccessToken.getCurrentAccessToken(),
                "/",
                jsonObject,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        // Insert your code here
                        response.toString();
                    }
                });
        request.executeAsync();
    }

    //TODO implemented maintenance mode
    private void callEClassesImagesAPi(final String api) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {

                JSONObject jsonObject = null;
                try {

                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
                        return;
                    }

                    jsonObject = new JSONObject(response);
                    List<EclassesType> eclassesTypeList = new Gson().fromJson(jsonObject.getJSONArray("EclassTypes").toString(), new TypeToken<List<EclassesType>>() {
                    }.getType());

                    if (eclassesTypeList.size() > 0) {
                        for (EclassesType eclassesType : eclassesTypeList) {
                            if (!TextUtils.isEmpty(eclassesType.getBanner())) {
                                Utils.setImagesInMemCache(eclassesType.getBanner());
                            }
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet)) {
                    params.put(Keys.DEVICE, Keys.TABLET);

                } else {
                    params.put(Keys.DEVICE, Keys.ANDROID);

                }

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    private void callExistingCardListApi() {


        // calling badges api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.PAYMENT_CARD_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    setCardListInAppPreferences(response);

                } catch (Exception e) {

                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
                if(!mIsDownloaded){
                    Utils.showNetworkError(mContext, error);
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    private void setCardListInAppPreferences(String response) {


        //  parse JSON to java
        List<CreditCard> creditCardTypeList = null;

        if (!TextUtils.isEmpty(response)) {

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response);
                creditCardTypeList = new Gson().fromJson(jsonObject.getJSONArray("CardDetails").toString(), new TypeToken<List<CreditCard>>() {
                }.getType());
                AppPreferences.setCreditCardList(creditCardTypeList);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }


    // opening payment dialog
    private void openDialog(DialogFragment dialogFragment) {
        dialogFragment.setCancelable(false);
        dialogFragment.show(getActivity().getSupportFragmentManager(), "dialog");

    }

    private BroadcastReceiver mReloadPayResponseListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String videoId = intent.getStringExtra(Constants.ECLASSES_VIDEO_ID);

            if (!TextUtils.isEmpty(videoId)) {
                mPlayedEclassesId = videoId;
            }

            reload();

        }
    };


    private BroadcastReceiver mClearResponse = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mDetailKey.equals(Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING)) {
                getActivity().onBackPressed();
            } else {
                clearResponse();
            }
        }
    };

    // setting wishlist in response to maintain their wishlist icon
    private BroadcastReceiver mSetWishlist = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String selectedId = intent.getStringExtra(Keys.ARTICLE_ID);
            String selectedValue = intent.getStringExtra(Keys.WISHLIST_VALUE);


            // for course detail
            if (mDashBoardDetails != null) {
                if (mDashBoardDetails.getCourseDetail().getRelated_Courses() != null) {
                    if (mDashBoardDetails.getCourseDetail().getRelated_Courses().getCourses() != null) {
                        if (mDashBoardDetails.getCourseDetail().getRelated_Courses().getCourses().size() > 0) {

                            for (int j = 0; j < mDashBoardDetails.getCourseDetail().getRelated_Courses().getCourses().size(); j++) {

                                if (mDashBoardDetails.getCourseDetail().getRelated_Courses().getCourses().get(j).getId().equals(selectedId)) {
                                    mDashBoardDetails.getCourseDetail().getRelated_Courses().getCourses().get(j).setWishlist(selectedValue);
                                }
                            }
                        }
                    }
                }

                if (mDashBoardDetails.getCourseDetail().getRelated_Programme() != null) {
                    if (mDashBoardDetails.getCourseDetail().getRelated_Programme().getProgrammes() != null) {
                        if (mDashBoardDetails.getCourseDetail().getRelated_Programme().getProgrammes().size() > 0) {
                            for (int j = 0; j < mDashBoardDetails.getCourseDetail().getRelated_Programme().getProgrammes().size(); j++) {

                                if (mDashBoardDetails.getCourseDetail().getRelated_Programme().getProgrammes().get(j).getId().equals(selectedId)) {
                                    mDashBoardDetails.getCourseDetail().getRelated_Programme().getProgrammes().get(j).setWishlist(selectedValue);
                                }
                            }
                        }
                    }
                }

                mResponse = new Gson().toJson(mDashBoardDetails);

            }


            // for mentor detail

            if (mMentorDetails != null) {


                if (mMentorDetails.getEvents() != null) {
                    if (mMentorDetails.getEvents().getSmallLeterEvents().size() > 0) {

                        for (int j = 0; j < mMentorDetails.getEvents().getSmallLeterEvents().size(); j++) {

                            if (mMentorDetails.getEvents().getSmallLeterEvents().get(j).getId().equals(selectedId)) {
                                mMentorDetails.getEvents().getSmallLeterEvents().get(j).setWishlist(selectedValue);
                            }
                        }
                    }

                }


                if (mMentorDetails.getCourses() != null) {
                    if (mMentorDetails.getCourses().getCourses().size() > 0) {

                        for (int j = 0; j < mMentorDetails.getCourses().getCourses().size(); j++) {

                            if (mMentorDetails.getCourses().getCourses().get(j).getId().equals(selectedId)) {
                                mMentorDetails.getCourses().getCourses().get(j).setWishlist(selectedValue);
                            }
                        }

                    }

                }

                if (mMentorDetails.getProgrammes() != null) {
                    if (mMentorDetails.getProgrammes().getProgrammes().size() > 0) {

                        for (int j = 0; j < mMentorDetails.getProgrammes().getProgrammes().size(); j++) {

                            if (mMentorDetails.getProgrammes().getProgrammes().get(j).getId().equals(selectedId)) {
                                mMentorDetails.getProgrammes().getProgrammes().get(j).setWishlist(selectedValue);
                            }
                        }
                    }

                }


                if (mMentorDetails.getVideos() != null) {
                    if (mMentorDetails.getVideos().getVideos().size() > 0) {

                        for (int j = 0; j < mMentorDetails.getVideos().getVideos().size(); j++) {

                            if (mMentorDetails.getVideos().getVideos().get(j).getId().equals(selectedId)) {
                                mMentorDetails.getVideos().getVideos().get(j).setWishlist(selectedValue);
                            }
                        }
                    }

                }


                mResponse = new Gson().toJson(mMentorDetails);


            }


            // for handling video detail pages

            if (mVideoDetail != null) {

                if (mVideoDetail.getGiven().getCourse() != null) {
                    if (mVideoDetail.getGiven().getCourse().getCourses().size() > 0) {

                        for (int j = 0; j < mVideoDetail.getGiven().getCourse().getCourses().size(); j++) {

                            if (mVideoDetail.getGiven().getCourse().getCourses().get(j).getId().equals(selectedId)) {
                                mVideoDetail.getGiven().getCourse().getCourses().get(j).setWishlist(selectedValue);
                            }
                        }
                    }

                }

                if (mVideoDetail.getGiven().getProgramme() != null) {
                    if (mVideoDetail.getGiven().getProgramme().getProgrammes().size() > 0) {

                        for (int j = 0; j < mVideoDetail.getGiven().getProgramme().getProgrammes().size(); j++) {

                            if (mVideoDetail.getGiven().getProgramme().getProgrammes().get(j).getId().equals(selectedId)) {
                                mVideoDetail.getGiven().getProgramme().getProgrammes().get(j).setWishlist(selectedValue);
                            }
                        }
                    }

                }


                if (mVideoDetail.getTaken().getCourse() != null) {
                    if (mVideoDetail.getTaken().getCourse().getCourses().size() > 0) {

                        for (int j = 0; j < mVideoDetail.getTaken().getCourse().getCourses().size(); j++) {

                            if (mVideoDetail.getTaken().getCourse().getCourses().get(j).getId().equals(selectedId)) {
                                mVideoDetail.getTaken().getCourse().getCourses().get(j).setWishlist(selectedValue);
                            }
                        }
                    }

                }

                if (mVideoDetail.getTaken().getProgramme() != null) {
                    if (mVideoDetail.getTaken().getProgramme().getProgrammes().size() > 0) {

                        for (int j = 0; j < mVideoDetail.getTaken().getProgramme().getProgrammes().size(); j++) {

                            if (mVideoDetail.getTaken().getProgramme().getProgrammes().get(j).getId().equals(selectedId)) {
                                mVideoDetail.getTaken().getProgramme().getProgrammes().get(j).setWishlist(selectedValue);
                            }
                        }
                    }

                }

                mResponse = new Gson().toJson(mVideoDetail);

            }


        }
    };

    private BroadcastReceiver mFreePaidResponseListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String videoId = intent.getStringExtra(Constants.ECLASSES_VIDEO_ID);


            if (!TextUtils.isEmpty(videoId)) {
                mPlayedEclassesId = videoId;
            }


            if (mCourseDetailHeader != null) {
                if (mCourseDetailHeader.isFullScreen()) {

                    mCourseDetailHeader.pressFullScreenDialogButton();
                    reload();

                } else {
                    reload();

                }
            }


        }
    };


    @Override
    public void onFeedsButtonClick(String mentorId) {
        addFragment(FeedFragment.newInstance(mentorId), true);

    }

    public String getDetailPageType() {
        return mDetailKey;
    }

    @Override
    public void onFavButtonClick(String favClicked) {
        if (mMentorDetails != null) {
            mMentorDetails.getProfile().setWishlist(favClicked);
            mResponse = new Gson().toJson(mMentorDetails);
        }
    }
}
