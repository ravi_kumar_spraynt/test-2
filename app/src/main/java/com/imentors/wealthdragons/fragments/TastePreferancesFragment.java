package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.interfaces.TasteClickListener;
import com.imentors.wealthdragons.models.Categories;
import com.imentors.wealthdragons.models.Category;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.TastePreferencesView;

import org.json.JSONException;

import java.util.Map;

import static com.imentors.wealthdragons.adapters.TastePreferencesListAdapter.TASTE_OFTEN;


public class TastePreferancesFragment extends BaseFragment implements TastePreferencesView.onExpandClick ,TasteClickListener{


    private LinearLayout mProgressBar,mLlItemContainer;
    private Context mContext;
    private Categories tastePreferencesData;
    private SwipeRefreshLayout mSwipeRefreshLayout;




    public static TastePreferancesFragment newInstance() {


        TastePreferancesFragment fragment = new TastePreferancesFragment();
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_taste_preferances, container, false);

        mProgressBar = view.findViewById(R.id.progressBar);
        mLlItemContainer = view.findViewById(R.id.ll_item_container);
        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callTastePreferancesApi();
                Utils.callEventLogApi("refreshed <b>Taste Preferences Screen </b>");

            }
        });


        callTastePreferancesApi();

        return view;
    }


    //  refreshing taste preferences api
    public void callTastePreferancesApi() {

        String api = Api.GET_PREFERENCES_API;

        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(true);
            mProgressBar.setVisibility(View.GONE);
        } else {
            mProgressBar.setVisibility(View.VISIBLE);
        }


        // calling get taste preferences api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);

                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>Test Preferances</b> error in api");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>Test Preferances</b> error in api");
                error.printStackTrace();
                Utils.showNetworkError(mContext, error);
                mSwipeRefreshLayout.setRefreshing(false);



            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }

     // handling get taste preferences api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response) {

        //  parse json to java
        Categories tasteData = new Gson().fromJson(response, Categories.class);
        tastePreferencesData = tasteData;

        if(tastePreferencesData != null){


            if(tastePreferencesData.getCategory().size()>0) {
                tastePreferencesData.getCategory().get(0).setExpanded(true);
                setDataInRecyclerView(tastePreferencesData);
            }
        }

    }

    // set data in recycler view
    private void setDataInRecyclerView(Categories tasteData ) {

        mLlItemContainer.removeAllViews();

        for(int i=0;i<tasteData.getCategory().size();i++){
            Category category = tasteData.getCategory().get(i);
                mLlItemContainer.addView(new TastePreferencesView(mContext, category, this, i, category.isExpanded(),this));
        }

    }


    @Override
    public void onExpandClick(int position, boolean isExpanded) {
        tastePreferencesData.getCategory().get(position).setExpanded(isExpanded);
        setDataInRecyclerView(tastePreferencesData);
    }



    private void setTastePrefernces(final String cat_id , final String taste) {

        String api = Api.SET_PREFERENCES_API;


        //TODO implemented maintenance mode
        // calling set taste preferences api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b>Test Preferances</b> error in api");

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.CAT_ID,cat_id);
                params.put(Keys.TASTE,taste);

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    //  selected often or never and set taste preferences
    @Override
    public void onTasteClick(String id, String taste, int groupPosition,  int subPosition) {


        setTastePrefernces(id,taste);

        if(subPosition == -1){

            if(taste.equals(TASTE_OFTEN)){
                Utils.callEventLogApi("selected <b>often</b> for <b>"+tastePreferencesData.getCategory().get(groupPosition).getTitle()+"</b> Category from <b>Taste Preferences</b>");

            }else{
                Utils.callEventLogApi("selected <b>never</b> for <b>"+tastePreferencesData.getCategory().get(groupPosition).getTitle()+"</b> Category from <b>Taste Preferences</b>");

            }

            tastePreferencesData.getCategory().get(groupPosition).setTaste(taste);

        }else{

            if(taste.equals(TASTE_OFTEN)){
                Utils.callEventLogApi("selected <b>often</b> for <b>"+tastePreferencesData.getCategory().get(groupPosition).getSub().get(subPosition).getTitle()+"</b> Category from <b>Taste Preferences</b>");

            }else{
                Utils.callEventLogApi("selected <b>never</b> for <b>"+tastePreferencesData.getCategory().get(groupPosition).getSub().get(subPosition).getTitle()+"</b> Category from <b>Taste Preferences</b>");

            }

            tastePreferencesData.getCategory().get(groupPosition).getSub().get(subPosition).setTaste(taste);

        }
        setDataInRecyclerView(tastePreferencesData);

    }

    //  closed test preferences
    @Override
    public void onStop() {
        super.onStop();
        Utils.callEventLogApi("closed <b>Test Preferances</b> page");

    }

    @Override
    public void onPause() {
        super.onPause();

        Intent intent = new Intent(Constants.BROADCAST_PAY_FINSIH);
        LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);

    }
}
