package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.Badges;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.WealthDragonTextView;


public class BadgesDetailFragment extends BaseFragment {


    private WealthDragonTextView mTvCongrts, mTvEarnedBadges, mTvShareNow ;
    private ImageView mIvBadgeImage;
    private Context mContext;
    private Badges.Badge mBadgesData;


    public static BadgesDetailFragment newInstance(Badges.Badge orderData) {

        //  put data in bundle
        Bundle args= new Bundle();

        args.putSerializable(Constants.BADGE_DATA,orderData);

        BadgesDetailFragment fragment = new BadgesDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBadgesData = (Badges.Badge) getArguments().getSerializable(Constants.BADGE_DATA);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_badges_detail, container, false);

        Utils.callEventLogApi("opened <b>Badges Detail</b> page" );

        mTvEarnedBadges = view.findViewById(R.id.tv_badges_earned);
        mTvCongrts = view.findViewById(R.id.tv_congrts);
        mTvShareNow = view.findViewById(R.id.tv_share_now);
        mIvBadgeImage = view.findViewById(R.id.iv_badge);


        //  set data on view
        mTvEarnedBadges.setText(getString(R.string.badge_earned) + " "+ mBadgesData.getCourse_title() + " Badge");
        mTvCongrts.setText(getString(R.string.congratulations) + " "+ mBadgesData.getLogin_user_name() +"," );

        Glide.with(mContext).load(mBadgesData.getBadge()).into(mIvBadgeImage);
        mTvShareNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTvShareNow.setEnabled(false);
                Utils.callEventLogApi("clicked <b>Share</b> from <b>"+mBadgesData.getCourse_title()+"</b> "+ " badge ");
                Utils.shareArticleUrl(mBadgesData.getUrl(),mContext,getString(R.string.share_badges));
            }
        });



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mTvShareNow.setEnabled(true);

    }

    //  closed badges detail
    @Override
    public void onStop() {
        super.onStop();
        Utils.callEventLogApi("closed <b>Badges Detail</b> page" );

    }
}
