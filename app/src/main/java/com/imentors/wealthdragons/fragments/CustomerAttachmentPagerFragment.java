package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.tabs.TabLayout;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.SupportType;
import com.imentors.wealthdragons.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomerAttachmentPagerFragment extends BaseFragment  {


    private Context mContext;
    private ViewPager mViewPager;
    private PagerCustomAdapter mPagerAdapter;
    private static final String SUPPORT_MESSAGE_ITEM = "SUPPORT_MESSAGE_ITEM";
    private SupportType.ExistingSupportMessage mSupportMessage;
    private List<String> imagesList = new ArrayList<>();


    //  put data in bundle
    public static CustomerAttachmentPagerFragment newInstance(SupportType.ExistingSupportMessage item) {

        Bundle args = new Bundle();
        args.putSerializable(SUPPORT_MESSAGE_ITEM,item);

        CustomerAttachmentPagerFragment fragment = new CustomerAttachmentPagerFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSupportMessage = (SupportType.ExistingSupportMessage) getArguments().getSerializable(SUPPORT_MESSAGE_ITEM);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_customer_attachment_viewpager, container, false);
        mViewPager = view.findViewById(R.id.fragments_attach_viewpager);
        imagesList = Arrays.asList(mSupportMessage.getAttachments().split(","));

        if(imagesList.size()>1){
            TabLayout tabLayout = view.findViewById(R.id.tabDots);
            tabLayout.setVisibility(View.VISIBLE);
            tabLayout.setupWithViewPager(mViewPager, true);
        }



        mPagerAdapter = new PagerCustomAdapter(imagesList,mContext);

        mViewPager.setAdapter(mPagerAdapter);

        mViewPager.setCurrentItem(0);
        Utils.hideKeyboard(mContext);


        return view;
    }


    public boolean isItemMoreThanOne(){

        return imagesList != null && imagesList.size() > 1;

    }


    public static class PagerCustomAdapter extends PagerAdapter {
        private List<String> mImagesList = new ArrayList<>();
        private Context mContext;
        private LayoutInflater inflater;

        public PagerCustomAdapter(List<String> imagesList, Context context) {
            super();
            mImagesList = imagesList;
            mContext = context;
        }



        //  return the image list size
        @Override
        public int getCount() {
            return mImagesList.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view.equals(object);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {

            inflater=(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view=inflater.inflate(R.layout.item_customer_attachment,container,false);
            ImageView imageView= view.findViewById(R.id.images);
            final ProgressBar mProgressbar = view.findViewById(R.id.progress_bar);
            Glide.with(mContext).load(mImagesList.get(position)).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    mProgressbar.setVisibility(View.GONE);
                    return false;
                }
            }).into(imageView);
            container.addView(view,0);
            return view;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);        }
    }





}
