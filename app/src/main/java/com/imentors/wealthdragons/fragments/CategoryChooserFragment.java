package com.imentors.wealthdragons.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.adapters.CategoryChooserAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.UserCategory;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class CategoryChooserFragment extends BaseFragment implements CategoryChooserAdapter.OnListItemClicked {

    private RecyclerView mRecycleview;
    private NestedScrollView mNestedScrollView;
    private CategoryChooserAdapter mCategoryChooserAdapter;
    private Context mContext;
    private FrameLayout mSubmit,mSkip;
    private boolean isAutoPlay;
    private String mCatId;
    private List<UserCategory.category> mUserCategory = new ArrayList<>();
    private WealthDragonTextView mCountHeading,mCounterChoose;
    private int mCountChoose=6,mTotalItem,mCountNeedToBeSelected;
    private HashMap<String, Boolean> mSelectedItemList;
    private LinearLayout mProgressBar;






    public static CategoryChooserFragment newInstance() {

        //  put data in bundle
        Bundle args = new Bundle();
//        args.putSerializable(Constants.MENTORS_KEY, mentorId);

        CategoryChooserFragment fragment = new CategoryChooserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mMentorId = (String) getArguments().getSerializable(Constants.MENTORS_KEY);
        mCatId = (String) getArguments().getSerializable(Constants.CAT_ID);

    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.category_chooser, container, false);

        Utils.callEventLogApi("opened <b>category</b> screen");

        mNestedScrollView = view.findViewById(R.id.nested_scroll_view);
        mRecycleview = view.findViewById(R.id.listView);
        mProgressBar=view.findViewById(R.id.progressBar);
        mSubmit=view.findViewById(R.id.fl_submit);
        mCountHeading=view.findViewById(R.id.tv_choose_count_heading);
        mSkip=view.findViewById(R.id.fl_btn_skip);
        mRecycleview.setNestedScrollingEnabled(false);
        mRecycleview.setHasFixedSize(true);
        mSubmit.setEnabled(true);


        mSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callCategorySkipApi();
                Utils.callEventLogApi("clicked <b>skip</b> button from category screen");

            }
        });


        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCategoryChooserAdapter!=null) {
                    mTotalItem = mCategoryChooserAdapter.getItemCount() - mCountChoose;

                    if (mTotalItem > mCategoryChooserAdapter.getItemCount() - 1) {
                        saveUserCategory();
                    } else {


                        Utils.showSimpleAlertDialog(getContext(), getString(R.string.confirmation), getString(R.string.confirmation_message)+" " + mCountNeedToBeSelected +" categories.", getString(R.string.okay_text),true);
                    }
                }
                Utils.callEventLogApi("clicked <b>submit</b> button from category screen");


            }
        });


//        mCategoryChooserAdapter = new CategoryChooserAdapter(getContext(),this);
//        mRecycleview.setAdapter(mCategoryChooserAdapter);
//
        if (Utils.isTablet())
        {

            double reuiredButtonWidth = Utils.getScreenWidth((Activity)mContext)/1.5;

            int marginFromStart = (int)(Utils.getScreenWidth((Activity)mContext) - reuiredButtonWidth)/2;

            LayoutParams params4 = new LayoutParams( ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params4.setMarginStart(marginFromStart-30);

            LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams((int) (reuiredButtonWidth), Utils.dpToPx(60));
            Params.gravity = Gravity.CENTER_HORIZONTAL ;
            Params.setMargins(0,0,0,8);
            mSkip.setLayoutParams(Params);
            mSubmit.setLayoutParams(Params);


        }
        else {
            LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Utils.dpToPx(50));
            Params.gravity = Gravity.CENTER_HORIZONTAL ;
            mSkip.setLayoutParams(Params);
            Params.setMargins(0,0,0,4);
            mSubmit.setLayoutParams(Params);

        }

    GridLayoutManager manager = null;
        if (Utils.isTablet()) {
             manager = new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false);
        }
        else
        {
             manager = new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false);
        }

        mRecycleview.setLayoutManager(manager);

        callUserCategories();



        return view;
    }

    private void callUserCategories() {

        // calling badges api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.USER_CATEGORIES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    onGetDataAPIServerResponse(response);

                } catch (Exception e) {

                    Utils.callEventLogApi("getting <b>User Category </b> error in api");

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b>User Category  </b> error in api");
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);


                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void showDataOnView(String response)  {
        super.showDataOnView(response);
        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response)  {


        try{
            JSONObject jsonObject = new JSONObject(response);
            if(!TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS))){


                Intent intent=new Intent(getContext(),MainActivity.class);
                startActivity(intent);
                getActivity().finish();


            }else{
                //  parse JSON to java
                UserCategory userCategory = new Gson().fromJson(response, UserCategory.class);
                mCountNeedToBeSelected = mCountChoose = Integer.parseInt(userCategory.getCategory_choose());
                mCategoryChooserAdapter = new CategoryChooserAdapter(getContext(),  userCategory.getCategory(),this,mCountNeedToBeSelected);
                mRecycleview.setAdapter(mCategoryChooserAdapter);
                mCountHeading.setText(mCountChoose+" out of "+mCountNeedToBeSelected+" remaining");
            }

        }catch (Exception e){

            mSubmit.setEnabled(true);
            mProgressBar.setVisibility(View.GONE);

        }


    }




    @Override
    public void onItemClicked(UserCategory.category item, int postion, int totalItem, HashMap<String, Boolean> selectedItemList) {
        mCountChoose=totalItem;
        mSelectedItemList = selectedItemList;
        if(mCountChoose>-1) {
            mCountHeading.setText(mCountChoose + " out of "+mCountNeedToBeSelected+" remaining");
        }
    }

    private void saveUserCategory() {

        mProgressBar.setVisibility(View.VISIBLE);
        mSubmit.setEnabled(false);

        // calling badges api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SAVE_USER_CATEGORIES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    mProgressBar.setVisibility(View.GONE);

                    onGetDataAPIServerResponse(response);

                } catch (Exception e) {
                    mSubmit.setEnabled(true);
                    mProgressBar.setVisibility(View.GONE);

                    Utils.callEventLogApi("getting <b>Save User Category </b> error in api");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mSubmit.setEnabled(true);
                mProgressBar.setVisibility(View.GONE);

                Utils.callEventLogApi("getting <b>>Save User Category </b> error in api");
                error.printStackTrace();
                Utils.showNetworkError(mContext, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                String selectedItemList = "";
                Map<String, String> params = Utils.getParams(false);
                if(mSelectedItemList!=null){

                    Iterator hmIterator = mSelectedItemList.entrySet().iterator();

                    while(hmIterator.hasNext()){
                        Map.Entry<String,Boolean> mapElement = (Map.Entry)hmIterator.next();
                        if(!mapElement.getValue()){
                            selectedItemList += mapElement.getKey() +",";
                        }
                    }


                  selectedItemList.subSequence(0,selectedItemList.length()-1);
                }

                params.put(Keys.CATEGORIES,selectedItemList);



                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    private void callCategorySkipApi() {

        // calling badges api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.CATEGORY_SKIP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                        JSONObject jsonObject = new JSONObject(response);
                        if(!TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS))){


                            Intent intent=new Intent(getContext(),MainActivity.class);
                            intent.putExtra(Constants.IS_CATEGORY_CHOOSER,true);

                            startActivity(intent);
                            getActivity().finish();

                        }



                } catch (Exception e) {

                    Utils.callEventLogApi("getting <b>User Category Skip error </b> error in api");

                    Toast.makeText(getContext(),R.string.try_again,Toast.LENGTH_SHORT).show();

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b>User Category  Skip </b> error in api");
                Toast.makeText(getContext(),R.string.try_again,Toast.LENGTH_SHORT).show();
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);


                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }









}

