package com.imentors.wealthdragons.fragments;


import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.LoginActivity;
import com.imentors.wealthdragons.adapters.WelcomeViewPagerAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.dialogFragment.FingerPrintAuthenticationDialog;
import com.imentors.wealthdragons.models.UserCredentials;
import com.imentors.wealthdragons.models.WelcomeModel;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


public class WelcomePagerFragment extends BaseFragment{


    private ViewPager mViewPager ;
    private WelcomeViewPagerAdapter mWelcomeViewPagerAdapter;
    private TabLayout tabLayout;
    private int mPagerPosition;
    private WelcomeModel mWelcomeModel;
    private TextView mTitleToolbarSigningStatus;
    private boolean isNewPager = false;
    private Serializable mNotificationData = null;

    private Context mContext;
    private boolean mIsSplashRedirection;


    public static WelcomePagerFragment newInstance(WelcomeModel welcomeModelData , boolean isNewPager , int pagerPosition, Serializable notificationData,boolean isSpashRedirection) {

        //  put data in bundle
        Bundle args = new Bundle();
        args.putSerializable(Constants.WELCOME_MODEL_DATA,welcomeModelData);
        args.putBoolean(Constants.IS_NEW_PAGER,isNewPager);
        args.putInt(Constants.PAGE_POSITION,pagerPosition);
        args.putBoolean(Constants.IS_SPLASH,isSpashRedirection);
        if(notificationData!=null){
            args.putSerializable(Keys.NOTIFICATION,notificationData);

        }
        WelcomePagerFragment fragment = new WelcomePagerFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null){
            mWelcomeModel = (WelcomeModel) getArguments().getSerializable(Constants.WELCOME_MODEL_DATA);
            isNewPager = getArguments().getBoolean(Constants.IS_NEW_PAGER);
            mPagerPosition = getArguments().getInt(Constants.PAGE_POSITION);
            mNotificationData = getArguments().getSerializable(Keys.NOTIFICATION);
            mIsSplashRedirection = getArguments().getBoolean(Constants.IS_SPLASH);
        }

        LocalBroadcastManager.getInstance(this.getContext()).registerReceiver(mSetTabLayoutPosition,
                new IntentFilter(Constants.BROADCAST_TABLAYOUT_POSITION));
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_welcome_viewpager, container, false);


            Toolbar toolbar = view.findViewById(R.id.login_toolbar);
            mTitleToolbarSigningStatus = toolbar.findViewById(R.id.tv_singing_status);



        // check if sign in option avaialble
        if(isNewPager){
            if (mPagerPosition == 0) {
                if (mWelcomeModel.getUserGroup().isSignInVisible() && mWelcomeModel.getUserGroup().isSignUpButtonVisible()) {
                    mTitleToolbarSigningStatus.setText(getString(R.string.sign_in));
                } else {
                    mTitleToolbarSigningStatus.setText(null);

                    // sign in case show fingerprint

//                    UserCredentials user = AppPreferences.getUserCredentials();
//
//                    if(AppPreferences.getUserCredentials().isCredentailsSaved())
//                    {
//
//                        if (Utils.isFingerPrintAvailable(getActivity())) {
//                            fingerPrintAuthenticationDialog
//                                    = new FingerPrintAuthenticationDialog();
//                            fingerPrintAuthenticationDialog.setListener(WelcomePagerFragment.this);
//                            fingerPrintAuthenticationDialog.show(getActivity().getFragmentManager(), DIALOG_FRAGMENT_TAG);
//                        }
//                    }


                }

            } else {
                if (mWelcomeModel.getUserGroup().isSignUpButtonVisible()) {
                    mTitleToolbarSigningStatus.setText(getString(R.string.sign_up));
                } else {
                    mTitleToolbarSigningStatus.setText(null);
                }

//                UserCredentials user = AppPreferences.getUserCredentials();
//
//                if(AppPreferences.getUserCredentials().isCredentailsSaved())
//                {
//
//                    if (Utils.isFingerPrintAvailable(getActivity())) {
//                        fingerPrintAuthenticationDialog
//                                = new FingerPrintAuthenticationDialog();
//                        fingerPrintAuthenticationDialog.setListener(WelcomePagerFragment.this);
//                        fingerPrintAuthenticationDialog.show(getActivity().getFragmentManager(), DIALOG_FRAGMENT_TAG);
//                    }
//                }
            }


        }else {

            if (mWelcomeModel.getUserGroup().isSignInVisible()) {
                mTitleToolbarSigningStatus.setText(getString(R.string.sign_in));
            } else {
                mTitleToolbarSigningStatus.setText(null);
            }
        }


        mTitleToolbarSigningStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = mTitleToolbarSigningStatus.getText().toString();

                if(isNewPager){

                    switch (title){
                        case "Sign In":
                            if(mWelcomeModel.getUserGroup().isSignUpButtonVisible()){
                                mViewPager.setCurrentItem(1,true);
                            }else{
                                mViewPager.setCurrentItem(0,true);

                            }
                            return;

                        default:
                            mViewPager.setCurrentItem(0,true);
                            return;

                    }


                }else {

                    switch (title) {
                        case "Sign In":
                            if (mViewPager.getCurrentItem() < mWelcomeModel.getNumberOfFragmentsInSlider()) {
                                // open new fragment
                                openNextSliderFragment();
                            } else {
                                openSignInFragment();
                            }
                            return;

                        default:
                            mViewPager.setCurrentItem(mWelcomeModel.getNumberOfFragmentsInSlider(), true);
                            return;

                    }
                }
            }
        });



        mViewPager = view.findViewById(R.id.fragments_viewpager);
        mWelcomeViewPagerAdapter = new WelcomeViewPagerAdapter(getActivity().getSupportFragmentManager(), mWelcomeModel  , isNewPager,mNotificationData);
        mViewPager.setAdapter(mWelcomeViewPagerAdapter);


        // for notificaiton case open direct sign in fragment
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivity()!=null)
                {
                if(((LoginActivity)getActivity()).getNotificationData()!=null){
                    openSignInFragment();
                }
            }}
        }, 200);

        if(isNewPager) {
                mViewPager.setCurrentItem(mPagerPosition);
        }

        tabLayout = view.findViewById(R.id.tab_layout);


        setLayoutAccordingToData();
        AppPreferences.setFingerprintDialogStatus(false);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if(!isNewPager) {
                    if (position < mWelcomeModel.getNumberOfFragmentsInSlider()) {
                        tabLayout.setScrollPosition(position, positionOffset, true);
                    }
                }


            }

            @Override
            public void onPageSelected(int position) {

                if(isNewPager){

                    // set title on toolbar

                    // checking for sign up visibility

                    if (mWelcomeViewPagerAdapter.getItem(position) instanceof RegisterFragment) {
                        if (mWelcomeModel.getUserGroup().isSignInVisible()) {
                            mTitleToolbarSigningStatus.setText(getString(R.string.sign_in));
                        } else {
                            mTitleToolbarSigningStatus.setText(null);
                        }
                    } else {
                        if (mWelcomeModel.getUserGroup().isSignUpButtonVisible()) {
                            mTitleToolbarSigningStatus.setText(getString(R.string.sign_up));
                        } else {
                            mTitleToolbarSigningStatus.setText(null);
                        }
                    }



                }else {

                    if (position < mWelcomeModel.getNumberOfFragmentsInSlider() && mWelcomeModel.getNumberOfFragmentsInSlider()>1) {
                        tabLayout.setVisibility(View.VISIBLE);
                    } else {
                        tabLayout.setVisibility(View.INVISIBLE);
                    }


                    // set title on toolbar


//                // checking for sign up visibility
                    if (mWelcomeModel.getUserGroup().isSignUpButtonVisible()) {
                        if (position < mWelcomeModel.getNumberOfFragmentsInSlider() + 1) {

                            // check if sign in option avaialble
                            if (mWelcomeModel.getUserGroup().isSignInVisible()) {
                                mTitleToolbarSigningStatus.setText(getString(R.string.sign_in));
                            } else {
                                mTitleToolbarSigningStatus.setText(null);
                            }

                        } else {

                            mTitleToolbarSigningStatus.setText(getString(R.string.sign_up));

                        }
                    } else {
                        if (position < mWelcomeModel.getNumberOfFragmentsInSlider()) {

                            // check if sign in option available
                            if (mWelcomeModel.getUserGroup().isSignInVisible()) {
                                mTitleToolbarSigningStatus.setText(getString(R.string.sign_in));
                            } else {
                                mTitleToolbarSigningStatus.setText(null);
                            }
                        } else {
                            mTitleToolbarSigningStatus.setText(null);
                        }
                    }
                }


                if (mWelcomeViewPagerAdapter.getItem(position) instanceof SignInFragment) {

                    UserCredentials user = AppPreferences.getUserCredentials();
                    AppPreferences.setFingerprintDialogStatus(false);

//                    if(AppPreferences.getUserCredentials().isCredentailsSaved())
//                    {
//
//                        if (Utils.isFingerPrintAvailable(getActivity())) {
//                                fingerPrintAuthenticationDialog
//                                        = new FingerPrintAuthenticationDialog();
//                                fingerPrintAuthenticationDialog.setListener(WelcomePagerFragment.this);
//                                fingerPrintAuthenticationDialog.show(getActivity().getFragmentManager(), DIALOG_FRAGMENT_TAG);
//                            }
//                        }
//
                    }

                }



            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return view;

    }

    private void setLayoutAccordingToData() {
        if(mWelcomeModel.getNumberOfFragmentsInSlider()>1){
        // tablayout need to show only in slider
        if(!isNewPager) {

            tabLayout.setVisibility(View.VISIBLE);



            // adding tabs to tab layout
            tabLayout.removeAllTabs();
            for (int i = 0; i < mWelcomeModel.getNumberOfFragmentsInSlider(); i++) {
                tabLayout.addTab(tabLayout.newTab());
            }


            // disabling tab click listener
            LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
            for (int i = 0; i < tabStrip.getChildCount(); i++) {
                tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return true;
                    }

                });
            }
        }
        }else{
            tabLayout.setVisibility(View.GONE);
        }
    }


//    // on fingerPrint dialog dismiss
//    @Override
//    public void onDialogDismiss() {
//        if(fingerPrintAuthenticationDialog !=  null) {
//            if (fingerPrintAuthenticationDialog.isVisible()) {
//                fingerPrintAuthenticationDialog.dismiss();
//            }
//
//            if (mWelcomeViewPagerAdapter.getItem(mViewPager.getCurrentItem()) instanceof SignInFragment) {
//                List<Fragment> fragentList = getActivity().getSupportFragmentManager().getFragments();
//                for(Fragment fragment:fragentList){
//
//                    if(fragment.getClass().getName().equals(SignInFragment.class.getName())){
//                        SignInFragment signInFragment = (SignInFragment) fragment;
//                        signInFragment.callSignInAPI(AppPreferences.getUserCredentials().getEmail(),AppPreferences.getUserCredentials().getPassword(),true);
//                    }
//
//                }
//            }
//        }
//
//    }

    // open Sign In Fragment
    public void openSignInFragment () {

        if(isNewPager){
            if(mWelcomeModel.getUserGroup().isSignUpButtonVisible()){
                mViewPager.setCurrentItem(1,true);
            }else{
                mViewPager.setCurrentItem(0,true);
            }
        }else {

            if (mWelcomeModel.getUserGroup().isSignUpButtonVisible()) {
                mViewPager.setCurrentItem(mWelcomeModel.getNumberOfFragmentsInSlider() + 1, true);
            } else {
                mViewPager.setCurrentItem(mWelcomeModel.getNumberOfFragmentsInSlider(), true);
            }
        }
    }

    // open Forgot Password Fragment
    public void openForgotPasswordFragment () {
        if(isNewPager){
            mViewPager.setCurrentItem(mWelcomeViewPagerAdapter.getCount(),true);
        }else {
            mViewPager.setCurrentItem(mWelcomeModel.getTotalNumberOfFragments(), true);
        }
    }

    // open Sign up Fragment
    public void openSignUpFragment () {
        if(isNewPager) {
            mViewPager.setCurrentItem(0,true);
        }else{
            mViewPager.setCurrentItem(mWelcomeModel.getNumberOfFragmentsInSlider(), true);
        }
    }




    private void openNextSliderFragment () {

        WelcomePagerFragment fragment;

        if (mWelcomeModel.getUserGroup().isSignUpButtonVisible()) {
             fragment = WelcomePagerFragment.newInstance(mWelcomeModel,true,1,mNotificationData,false);

        } else {
            fragment = WelcomePagerFragment.newInstance(mWelcomeModel,true,0,mNotificationData,false);

        }
        pushFragment(fragment,false);

    }

    // to handle every error case and getting back to slider screen
    private BroadcastReceiver mSetTabLayoutPosition = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int postion = intent.getIntExtra(Constants.TABLAYOUT_POSITION, 0);
            setTabLayoutPosition(postion);
        }
    };

    private void setTabLayoutPosition(int position) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        layoutParams.topMargin = position;

        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        tabLayout.setLayoutParams(layoutParams);
    }

    @Override
    public void onStop() {
        super.onStop();
        mIsSplashRedirection = false;
        LocalBroadcastManager.getInstance(this.getActivity()).unregisterReceiver(mSetTabLayoutPosition);
    }

    @Override
    public void onResume() {
        super.onResume();
        getWelcomePageDetails();
    }




    private void getWelcomePageDetails() {
        // show progress Bar

        // url for homepage
        String url = Api.HOMEPAGE_API;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{

                    JSONObject jsonObject = new JSONObject(response);


                    if(!TextUtils.isEmpty(jsonObject.optString(Keys._MAINTAINANCE))){

                        String message = jsonObject.getString(Keys._MAINTAINANCE);
                        if(message.equals(Keys.ON)){
                            AppPreferences.setMaintainceMode(true);
                            WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
                            return;
                        }

                    } else{

                        onGetDataAPIServerResponse(new Gson().fromJson(response, WelcomeModel.class));


                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet)) {
                    params.put(Keys.DEVICE, Keys.TABLET);

                } else {
                    params.put(Keys.DEVICE, Keys.ANDROID);

                }

                return params;
            }
        };
        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);


    }



    private void onGetDataAPIServerResponse(WelcomeModel response) throws Exception {


        // send user to login screen if data comes here
        if (response != null) {

            //set data in bundle


            if(!isNewPager && WealthDragonsOnlineApplication.sharedInstance().getBackGroundState() && !mIsSplashRedirection){
                mWelcomeModel = response;
               //mPagerPosition = mViewPager.getCurrentItem();
//                mWelcomeViewPagerAdapter = new WelcomeViewPagerAdapter(getActivity().getSupportFragmentManager(), mWelcomeModel  , isNewPager,mNotificationData);
//                mViewPager.setAdapter(mWelcomeViewPagerAdapter);
//                mViewPager.setCurrentItem(mPagerPosition);
//                setLayoutAccordingToData();
//                AppPreferences.setTabLayoutResized(false);


//                if (mWelcomeModel.getUserGroup().isSignInVisible()) {
//                    mTitleToolbarSigningStatus.setText(getString(R.string.sign_in));
//                } else {
//                    mTitleToolbarSigningStatus.setText(null);
//                }




                // perfect

                WelcomePagerFragment fragment = WelcomePagerFragment.newInstance(mWelcomeModel, false, 0,mNotificationData,false);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(
                        R.anim.no_animation, R.anim.slide_out_to_right);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

                fragmentTransaction.replace(R.id.fm_main, fragment);
                fragmentTransaction.commit();
                setLayoutAccordingToData();
                AppPreferences.setTabLayoutResized(false);





                // another option
//                WealthDragonsOnlineApplication.sharedInstance().redirectToSplashActivity(getActivity());
//                FragmentTransaction ft = getFragmentManager().beginTransaction();
//                ft.detach(this).attach(this).commit();






                //                mViewPager.clearOnPageChangeListeners();


//                Intent intent = new Intent();
//
//                Bundle bundle = new Bundle();
//                bundle.putSerializable(Constants.SIGNING_MODEL_DATA, response);
//                if (bundle != null) {
//                    intent.putExtras(bundle);
//                }
//                intent.putExtra(Keys.NOTIFICATION, mNotificationData);
//
//
//                intent.setClass(getContext(), LoginActivity.class);
////                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                AppPreferences.setTabLayoutResized(false);
//
//                startActivity(intent);
//                getActivity().overridePendingTransition(R.anim.slide_in_from_right,R.anim.no_animation);
//                getActivity().finish();

            }



        }
    }



}
