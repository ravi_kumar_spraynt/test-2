package com.imentors.wealthdragons.fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.LoginActivity;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.dialogFragment.PayDialogFragment;
import com.imentors.wealthdragons.models.Subscription;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class SubscriptionFragment extends BaseFragment implements Utils.DialogInteraction {


    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressBar mProgressBar;
    private TextView mTvHeading , mTvSubHeading , mTvContentHeading, mTvCourseHeading , mTvCourse , mTvWdoTvHeading , mTvWdoTv , mTvFooterHeading , mTvFooter , mTvOtherBenfitsHeading , mTvOtherBenfits ,mtvTermsAndCondition  , mTvOrangeButton , mTvBlueButton;
    private  FrameLayout mFlBlue , mFlOrange;
    private View mViewCourseHeading , mViewWdoTv , mViewOtherBenfeits , mViewTopSubHeading , mViewBottomSubHeading;
    private boolean mIsLoginActivity;
    private LinearLayout mFooterLayout;
    private Serializable mNotificationData = null;
    private String mCurrency;

    public static SubscriptionFragment newInstance(boolean isLoginActivity, Serializable notificationData) {


        // put data in bundle
        Bundle args = new Bundle();

        args.putBoolean(Constants.IS_LOGIN_ACTIVITY,isLoginActivity);

        if(notificationData!=null){
            args.putSerializable(Keys.NOTIFICATION,notificationData);

        }

        SubscriptionFragment fragment = new SubscriptionFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsLoginActivity =  getArguments().getBoolean(Constants.IS_LOGIN_ACTIVITY);
        mNotificationData = getArguments().getSerializable(Keys.NOTIFICATION);
        LocalBroadcastManager.getInstance(this.getContext()).registerReceiver(mDismissAlertDialog,
                new IntentFilter(Constants.BROADCAST_DISMISS_ALERT_DIALOG));

    }





    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_subscription, container, false);

        Toolbar toolbar = view.findViewById(R.id.login_toolbar);

        Utils.callEventLogApi("<b>Subscription </b> page opened");
        Utils.hideKeyboard(getContext());

        if(mIsLoginActivity){
            toolbar.setVisibility(View.VISIBLE);
            TextView mTitleToolbarSigningStatus = toolbar.findViewById(R.id.tv_singing_status);
            mTitleToolbarSigningStatus.setText(getString(R.string.sign_out));

            mTitleToolbarSigningStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showSignOutDialogBox();
                }
            });
        }else{
            toolbar.setVisibility(View.GONE);
        }





        mProgressBar = view.findViewById(R.id.progressBar);
        mViewTopSubHeading = view.findViewById(R.id.view_top_sub_heading);
        mTvHeading = view.findViewById(R.id.tv_subscription_main_heading);
        mViewBottomSubHeading = view.findViewById(R.id.view_bottom_sub_heading);
        mTvSubHeading = view.findViewById(R.id.tv_subscription_sub_heading);
        mTvCourseHeading = view.findViewById(R.id.tv_subscription_course_heading);
        mTvCourse = view.findViewById(R.id.tv_subscription_course);
        mTvContentHeading = view.findViewById(R.id.tv_subscription_content_heading);
        mTvWdoTvHeading = view.findViewById(R.id.tv_subscription_wdo_tv_heading);
        mTvWdoTv = view.findViewById(R.id.tv_subscription_wdo_tv);
        mTvFooterHeading = view.findViewById(R.id.tv_subscription_footer_heading);
        mTvFooter = view.findViewById(R.id.tv_subscription_footer_text);
        mTvOtherBenfitsHeading = view.findViewById(R.id.tv_subscription_other_benifits_heading);
        mTvOtherBenfits = view.findViewById(R.id.tv_subscription_other_benifits);
        mtvTermsAndCondition = view.findViewById(R.id.tv_subscription_terms_and_conditions_line);
        mTvOrangeButton = view.findViewById(R.id.tv_orange_button);
        mTvBlueButton = view.findViewById(R.id.tv_blue_button);
        mFlOrange = view.findViewById(R.id.fl_btn_orange);
        mFlBlue =  view.findViewById(R.id.fl_btn_blue);
        mViewCourseHeading = view.findViewById(R.id.view_course_heading);
        mViewOtherBenfeits = view.findViewById(R.id.view_other_benefits);
        mViewWdoTv = view.findViewById(R.id.view_wdo_tv);

        mFooterLayout = view.findViewById(R.id.ll_screen_footer);

        mFlOrange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("<b>Subscription</b> button clicked");
                openPaymentDialog();

            }
        });

        mFlBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("<b>Subscription</b> button clicked");
               openPaymentDialog();
            }
        });


        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callSubscriptionApi();
                Utils.callEventLogApi("refreshed <b>Subscription Screen </b>");

            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);


        Subscription subscription = AppPreferences.getSubscription();
        if(!TextUtils.isEmpty(subscription.getMainHeading())){
            setDataInView(subscription);
        }else{
            callSubscriptionApi();
        }


        if(mIsLoginActivity) {
            mFooterLayout.setVisibility(View.VISIBLE);
        }else{
            mFooterLayout.setVisibility(View.GONE);

        }




        return view;
    }

    // opening payment dialog
    private void openPaymentDialog() {
        PayDialogFragment dialogFrag= PayDialogFragment.newInstance(mCurrency,mIsLoginActivity,false,null,Constants.SUBSCRIPTION,null);
        dialogFrag.setCancelable(false);
        dialogFrag.show(getActivity().getSupportFragmentManager(), "dialog");

    }

    //  show alert dialog
    private void showSignOutDialogBox() {

        Utils.callEventLogApi("clicked on <b>Sign Out </b>");


        Utils.showTwoButtonAlertDialog(this,this.getActivity(),getString(R.string.dialog_title),getString(R.string.dialog_message_logout),null,null);
    }



    private void callSignOutApi() {

        // show progress Bar



        disableTouch();
        mProgressBar.setVisibility(View.VISIBLE);


        // calling sign out api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SIGN_OUT_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    mProgressBar.setVisibility(View.GONE);
                    mSwipeRefreshLayout.setRefreshing(false);
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    if(isAdded()) {

                        Utils.showAlertDialog(WealthDragonsOnlineApplication.sharedInstance(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                    }
                    mSwipeRefreshLayout.setRefreshing(false);
                    enableTouch();
                    Utils.callEventLogApi("getting <b>Sign Out </b> error in api");

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b>Sign Out </b> error in api");
                error.printStackTrace();
                mSwipeRefreshLayout.setRefreshing(false);
                mProgressBar.setVisibility(View.GONE);
                if(isAdded()) {
                    Utils.showAlertDialog(WealthDragonsOnlineApplication.sharedInstance(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                }
                enableTouch();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    @Override
    public void onDestroyView() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.clearAnimation();
        }
        super.onDestroyView();
    }



    private void callSubscriptionApi() {

        // show progress Bar

        if(!mSwipeRefreshLayout.isRefreshing()){
            mProgressBar.setVisibility(View.VISIBLE);
        }


        // calling subscription api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SUBSCRIPTION_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    mProgressBar.setVisibility(View.GONE);
                    mSwipeRefreshLayout.setRefreshing(false);
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    Utils.callEventLogApi("getting <b>Subscription</b> error in api");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b>Subscription</b> error in api");
                error.printStackTrace();
                mSwipeRefreshLayout.setRefreshing(false);
                mProgressBar.setVisibility(View.GONE);
                if(isAdded()) {

                    Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    // handling subscription api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);


        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);

            // subscription condition
            if (TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS))) {

                //  parse JSON to java
                Subscription subscription = new Gson().fromJson(response, Subscription.class);


                //  setting subscription data in app preferences
                AppPreferences.setSubscriptionData(subscription);
                setDataInView(subscription);

            }else{
                // logout condition
                enableTouch();


                // check if user is redirected through login activity or Splash Activity

                redirectUserOnCondition();

            }
        }catch (JSONException e){
                e.printStackTrace();

        }


    }

    private void redirectUserOnCondition() {
        FragmentManager fm =  getActivity().getSupportFragmentManager();
        List<Fragment> activeFragmentsList = fm.getFragments();
        Fragment fr = null;

        for(Fragment frag : activeFragmentsList){
            if(frag instanceof WelcomePagerFragment){
                fr = frag;
            }
        }

        if(fr instanceof WelcomePagerFragment) {
            AppPreferences.logout();
            getActivity().onBackPressed();
        }else{
            WealthDragonsOnlineApplication.sharedInstance().redirectToSplashActivity(getActivity());
        }
    }

     // setting data in view
    private void setDataInView(Subscription subscription) {

        mCurrency = subscription.getCurrency();

        if(!TextUtils.isEmpty(subscription.getMainHeading())){
            mTvHeading.setVisibility(View.VISIBLE);
            mTvHeading.setText(subscription.getMainHeading());
        }else{
            mTvHeading.setVisibility(View.GONE);
        }


        if(!TextUtils.isEmpty(subscription.getSubHeading())){
            mTvSubHeading.setVisibility(View.VISIBLE);
            mViewBottomSubHeading.setVisibility(View.VISIBLE);
            mViewTopSubHeading.setVisibility(View.VISIBLE);
            mTvSubHeading.setText(subscription.getSubHeading());
        }else{
            mTvSubHeading.setVisibility(View.GONE);
            mViewBottomSubHeading.setVisibility(View.GONE);
            mViewTopSubHeading.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(subscription.getContentHeading())){
            mTvContentHeading.setVisibility(View.VISIBLE);
            mTvContentHeading.setText(subscription.getContentHeading());
        }else{
            mTvContentHeading.setVisibility(View.GONE);
        }


        if(!TextUtils.isEmpty(subscription.getCourseHeading())){
            mTvCourseHeading.setVisibility(View.VISIBLE);
            mViewCourseHeading.setVisibility(View.VISIBLE);
            mTvCourseHeading.setText(subscription.getCourseHeading());
        }else{
            mTvCourseHeading.setVisibility(View.GONE);
            mViewCourseHeading.setVisibility(View.GONE);

        }



        if(!TextUtils.isEmpty(subscription.getCourse())){
            mTvCourse.setVisibility(View.VISIBLE);
            mTvCourse.setText(subscription.getCourse());
        }else{
            mTvCourse.setVisibility(View.GONE);
        }




        if(!TextUtils.isEmpty(subscription.getWdoTvHeading())){
            mTvWdoTvHeading.setVisibility(View.VISIBLE);
            mViewWdoTv.setVisibility(View.VISIBLE);
            mTvWdoTvHeading.setText(subscription.getWdoTvHeading());
        }else{
            mTvWdoTvHeading.setVisibility(View.GONE);
            mViewWdoTv.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(subscription.getWdoTv())){
            mTvWdoTv.setVisibility(View.VISIBLE);
            mTvWdoTv.setText(subscription.getWdoTv());
        }else{
            mTvWdoTv.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(subscription.getOtherBenifitsHeading())){
            mTvOtherBenfitsHeading.setVisibility(View.VISIBLE);
            mViewOtherBenfeits.setVisibility(View.VISIBLE);
            mTvOtherBenfitsHeading.setText(subscription.getOtherBenifitsHeading());
        }else{
            mTvOtherBenfitsHeading.setVisibility(View.GONE);
            mViewOtherBenfeits.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(subscription.getOtherBenifits())){
            mTvOtherBenfits.setVisibility(View.VISIBLE);
            mTvOtherBenfits.setText(subscription.getOtherBenifits());
        }else{
            mTvOtherBenfits.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(subscription.getFooterHeading())){
            mTvFooterHeading.setVisibility(View.VISIBLE);
            mTvFooterHeading.setText(subscription.getFooterHeading());
        }else{
            mTvFooterHeading.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(subscription.getFooterText())){
            mTvFooter.setVisibility(View.VISIBLE);
            mTvFooter.setText(subscription.getFooterText());
        }else{
            mTvFooter.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(subscription.getTermsAndConditionsLine())){
            mtvTermsAndCondition.setVisibility(View.VISIBLE);
            mtvTermsAndCondition.setText(subscription.getTermsAndConditionsLine());
            String[] clikableLinks = {getString(R.string.terms_condition),getString(R.string.privacy_policy)};
            ClickableSpan[] clickableSpan = {termsAndConditionSpan,privacyPolicySpan};
            Utils.makeLinks(mtvTermsAndCondition,clikableLinks,clickableSpan);
        }else{
            mtvTermsAndCondition.setVisibility(View.GONE);
        }

        if(subscription.isButtonVisible()){
            mFlBlue.setVisibility(View.VISIBLE);
            mFlOrange.setVisibility(View.VISIBLE);
            mTvBlueButton.setText(subscription.getButtonText());
            mTvOrangeButton.setText(subscription.getButtonText());
        }else{
            mFlBlue.setVisibility(View.GONE);
            mFlOrange.setVisibility(View.GONE);
        }

    }


    // to handle every error case and getting back to slider screen
    private BroadcastReceiver mDismissAlertDialog = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            redirectUserOnCondition();
        }
    };

    @Override
    public void onStop() {
        Utils.callEventLogApi("<b>Subscription </b>page left");
        super.onStop();
        if(mProgressBar.isShown()) {
            mProgressBar.setVisibility(View.GONE);
        }
        LocalBroadcastManager.getInstance(this.getActivity()).unregisterReceiver(mDismissAlertDialog);
    }


    //click of terms and condition
    ClickableSpan termsAndConditionSpan = new ClickableSpan() {
        @Override
        public void onClick(View widget) {

            TermsAndConditionFragment termFragment  = TermsAndConditionFragment.newInstance(false,!mIsLoginActivity);
            addFragment(termFragment,true);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    };


    // click of privacy policy
    ClickableSpan privacyPolicySpan = new ClickableSpan() {
        @Override
        public void onClick(View widget) {

            TermsAndConditionFragment termFragment  = TermsAndConditionFragment.newInstance(true,!mIsLoginActivity);
            addFragment(termFragment,true);

        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    };



     // calling dashboard
    private void moveToDashBoard(){
        Intent intent = new Intent();
        Serializable notificationHashMap = ((LoginActivity)getActivity()).getNotificationData();
        if(notificationHashMap != null) {
            intent.putExtra(Keys.NOTIFICATION, notificationHashMap);
        }
        intent.setClass(this.getActivity(),MainActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_from_right,R.anim.no_animation);
        getActivity().finish();
    }

    // dismiss dialog
    @Override
    public void onPositiveButtonClick(AlertDialog dialog,boolean isNotification) {
        Utils.callEventLogApi("clicked <b>Logout</b> from <b>Free trial Subscription Screen</b>");


        callSignOutApi();
            dialog.dismiss();

    }

    // dismiss dialog
    @Override
    public void onNegativeButtonClick(AlertDialog dialog,boolean isNotification) {
        Utils.callEventLogApi("clicked <b>Cancel</b> for <b>Logout Confirmation</b>");
        dialog.dismiss();

    }
}
