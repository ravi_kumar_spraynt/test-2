package com.imentors.wealthdragons.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.LoginActivity;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.Slider;
import com.imentors.wealthdragons.models.User;
import com.imentors.wealthdragons.models.WelcomeModel;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class WelcomeFragment extends BaseFragment {


    private WelcomeModel mWelcomeModel;
    private int mItemPosition;
    private ProgressBar mProgressBar;
    private Slider sliderData;
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private String name, email, firstName, lastName , fbImage;
    private ProfileTracker profileTracker;
    private TextView tvFbSign, tvSignUp;

    private Serializable mNotificationData = null;


    public static WelcomeFragment newInstance(WelcomeModel mWelcomeModelData, int position, Serializable notificationData) {


        //  put data in bundle
        Bundle args = new Bundle();
        args.putSerializable(Constants.WELCOME_MODEL_DATA, mWelcomeModelData);
        args.putInt(Constants.ITEM_POSITION, position);
        if(notificationData!=null){
            args.putSerializable(Keys.NOTIFICATION,notificationData);

        }
        WelcomeFragment fragment = new WelcomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mWelcomeModel = (WelcomeModel) getArguments().getSerializable(Constants.WELCOME_MODEL_DATA);
            mItemPosition = getArguments().getInt(Constants.ITEM_POSITION);
            mNotificationData = getArguments().getSerializable(Keys.NOTIFICATION);

        }

        callbackManager = CallbackManager.Factory.create();

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {

                if (newProfile != null) {
                    firstName = newProfile.getFirstName();
                    lastName = newProfile.getLastName();
                }
            }
        };
        profileTracker.startTracking();


    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_welcome, container, false);
        Utils.callEventLogApi("opened <b>WelCome</b> page");

        sliderData = mWelcomeModel.getSliderDataPerPosition(mItemPosition);
        mProgressBar = view.findViewById(R.id.progressBar);
        TextView tvTermsAndCondition = view.findViewById(R.id.tv_terms_condition);


        tvTermsAndCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>Terms And Condition</b>");

                TermsAndConditionFragment termFragment  = TermsAndConditionFragment.newInstance(false,false);
                addFragment(termFragment,true);
            }
        });

        TextView tvPrivacyPolicy = view.findViewById(R.id.tv_privacy_policy);
        tvPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>Privacy Policy</b>");

                TermsAndConditionFragment termFragment  = TermsAndConditionFragment.newInstance(true,false);
                addFragment(termFragment,true);
            }
        });


        initViews(view);

        return view;
    }

    private void initViews(final View view) {


        final SimpleDraweeView ivWelcomeScreen = view.findViewById(R.id.iv_background);

        String url = sliderData.getBanner();
        String bigUrl = sliderData.getBig_banner();


        // set background

        boolean isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);

        if(isTablet){
            if (!TextUtils.isEmpty(bigUrl)) {
                ivWelcomeScreen.setImageURI(Uri.parse(bigUrl));


//                Glide.with(this).load(bigUrl).thumbnail(Glide.with(this).load(sliderData.getLow_qty_big_banner()).dontAnimate().diskCacheStrategy(DiskCacheStrategy.NONE)).dontAnimate().centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE).into(ivWelcomeScreen);
            }
        }else{
            if (!TextUtils.isEmpty(url)) {

                ivWelcomeScreen.setImageURI(Uri.parse(url));

//                Glide.with(this).load(url).thumbnail(Glide.with(this).load(sliderData.getLow_qty_banner()).dontAnimate().diskCacheStrategy(DiskCacheStrategy.NONE)).dontAnimate().centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE).into(ivWelcomeScreen);
            }

        }

        loadOtherView(view);


    }


    // load other view after background has been established

    private void loadOtherView(View view) {

        TextView tvHeading = view.findViewById(R.id.tv_heading);
        TextView tvDescription = view.findViewById(R.id.tv_description);
        tvSignUp = view.findViewById(R.id.tv_orange_button);
        tvFbSign = view.findViewById(R.id.tv_blue_button);
        final FrameLayout flOrange = view.findViewById(R.id.fl_btn_orange);
        final FrameLayout flBlue = view.findViewById(R.id.fl_btn_blue);
        loginButton = view.findViewById(R.id.login_button);
        loginButton.setFragment(this);
        // setting heading and description data
        if (!TextUtils.isEmpty(sliderData.getHeading())) {
            tvHeading.setVisibility(View.VISIBLE);
            tvHeading.setText(sliderData.getHeading());
        } else {
            tvHeading.setVisibility(View.INVISIBLE);
        }

        if (!TextUtils.isEmpty(sliderData.getDescription())) {
            tvDescription.setVisibility(View.VISIBLE);
            tvDescription.setText(sliderData.getDescription());
        } else {
            tvDescription.setVisibility(View.INVISIBLE);
        }


        // setting button data
        WelcomeModel.UserGroup userGroup = mWelcomeModel.getUserGroup();
        if (userGroup.isSignUpButtonVisible()) {
            flOrange.setVisibility(View.VISIBLE);
            tvSignUp.setText(userGroup.getSignUpBtnText());
        } else {
            flOrange.setVisibility(View.INVISIBLE);
        }

        if (userGroup.isFbButtonVisible()) {
            flBlue.setVisibility(View.VISIBLE);
            tvFbSign.setText(userGroup.getFbBtnText());
        } else {
            flBlue.setVisibility(View.INVISIBLE);
        }


        flOrange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                WelcomePagerFragment fragment = WelcomePagerFragment.newInstance(mWelcomeModel, true, 0,mNotificationData,false);
                pushFragment(fragment, false);

            }
        });

        flBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isNetworkAvailable(getContext())){
                    mProgressBar.setVisibility(View.VISIBLE);
                    loginButton.performClick();
                }else{
                    Utils.showErrorDialog(getContext(), getString(R.string.error_api_no_internet_connection), getString(R.string.error_api_no_internet_connection_message), R.string.okay_text, -1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    },null);
                }
            }
        });

        if(flBlue.getVisibility() != View.GONE){

            flBlue.post(new Runnable() {
                @Override
                public void run() {
                    Rect rectf = new Rect();
                    flBlue.getGlobalVisibleRect(rectf);
                    if(!AppPreferences.isTabLayoutResized()){

                        Intent intent = new Intent(Constants.BROADCAST_TABLAYOUT_POSITION);
                        // You can also include some extra data.
                        intent.putExtra(Constants.TABLAYOUT_POSITION, rectf.bottom);

                        LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);

                        AppPreferences.setTabLayoutResized(true);

                    }
                }
            });

        }



        // fb setup

        List<String> permissionNeeds = Arrays.asList("email",
                "public_profile");

        loginButton.setReadPermissions(permissionNeeds);

        loginButton.registerCallback(callbackManager, callback);


    }


    @Override
    public void onActivityResult(int requestCode, int responseCode,
                                 Intent data) {
        super.onActivityResult(requestCode, responseCode, data);

        callbackManager.onActivityResult(requestCode, responseCode, data);
    }


    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            mProgressBar.setVisibility(View.VISIBLE);

            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object,
                                                GraphResponse response) {

                            if(getActivity()!=null && !getActivity().isDestroyed()) {
                                Log.i("LoginActivity",
                                        response.toString());
                                mProgressBar.setVisibility(View.GONE);

                                try {
                                    name = object.getString("name");
                                    email = object.getString("email");
//                                    firstName = object.getString("first_name");
//                                    lastName = object.getString("last_name");
                                    tvFbSign.setText(getString(R.string.signing_in));
                                    JSONObject pictureData = object.getJSONObject("picture");
                                    JSONObject imageData = pictureData.getJSONObject("data");
                                    String imageUrl = imageData.getString("url");


                                    fbImage = imageUrl;

                                    disableTouch();
                                    callLoginApi();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    if (isAdded()) {
                                        Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                                    }
                                    enableTouch();

                                }finally {

                                }
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields",
                    "name,email,first_name,last_name,picture");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            System.out.println("onCancel");
            mProgressBar.setVisibility(View.GONE);
            enableTouch();

        }

        @Override
        public void onError(FacebookException exception) {
            System.out.println("onError");
            mProgressBar.setVisibility(View.GONE);
            if(isAdded()) {
                Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
            }
            enableTouch();

        }
    };


    private void callLoginApi() {

        Utils.hideKeyboard(getContext());
//        Utils.logoutFbSession();
        mProgressBar.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.FB_LOG_IN_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    mProgressBar.setVisibility(View.GONE);
                    onGetDataAPIServerResponse(response);
                    tvFbSign.setText(mWelcomeModel.getUserGroup().getFbBtnText());

                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    if(isAdded()) {
                        Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                    }
                        tvFbSign.setText(mWelcomeModel.getUserGroup().getFbBtnText());
                    enableTouch();
                    e.printStackTrace();
                    Utils.logoutFbSession();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                mProgressBar.setVisibility(View.GONE);
                Utils.logoutFbSession();
                if(isAdded()) {
                    Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                }
                tvFbSign.setText(mWelcomeModel.getUserGroup().getFbBtnText());
                enableTouch();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                Map<String, String> params = Utils.getParams(true);

                params.put(Keys.EMAIL, email);
                params.put(Keys.FIRST_NAME, name);
//                params.put(Keys.LAST_NAME, lastName);
                if(!TextUtils.isEmpty(fbImage)){
                    params.put(Keys.IMAGE,fbImage);
                }

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);


    }


    //  handling privacy policy and terms and condition api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);

        if (response != null) {

            //  parse json to java
            User user = new Gson().fromJson(response, User.class);

            AppPreferences.setUserData(user);

            if(!TextUtils.isEmpty(fbImage)) {
                callFbProfileApi();
            }

            if (user.isDashboard()) {
                Intent intent = new Intent();
                Serializable notificationHashMap = ((LoginActivity)getActivity()).getNotificationData();
                if(notificationHashMap != null) {
                        intent.putExtra(Keys.NOTIFICATION, notificationHashMap);
                    }

                intent.setClass(getContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_from_right,R.anim.no_animation);
                getActivity().finish();
            } else {
                SubscriptionFragment fragment = SubscriptionFragment.newInstance(true,mNotificationData);
                addFragment(fragment, true);
            }

            setAllFields();


        } else {
            if(isAdded()) {
                Utils.showAlertDialog(getContext(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
            }
                tvFbSign.setText(mWelcomeModel.getUserGroup().getFbBtnText());

        }

    }


    @Override
    public void onStop() {
        super.onStop();
        profileTracker.stopTracking();
    }


    //  setting data in view
    private void setAllFields() {
        WelcomeModel.UserGroup userGroup = mWelcomeModel.getUserGroup();
        if (userGroup.isSignUpButtonVisible()) {
            tvSignUp.setText(userGroup.getSignUpBtnText());
        }
        if (userGroup.isFbButtonVisible()) {
            tvFbSign.setText(userGroup.getFbBtnText());
        }
    }

    private void callFbProfileApi() {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SAVE_FB_IMAGE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    String res = response;
                } catch (Exception e) {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                Map<String, String> params = Utils.getParams(false);

                if(!TextUtils.isEmpty(fbImage)){
                    params.put(Keys.IMAGE,fbImage);
                }

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);


    }





}
