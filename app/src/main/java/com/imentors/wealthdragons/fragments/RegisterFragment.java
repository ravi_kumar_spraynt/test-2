package com.imentors.wealthdragons.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.LoginActivity;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.interfaces.LoginFragmentChangingListener;
import com.imentors.wealthdragons.models.User;
import com.imentors.wealthdragons.models.WelcomeModel;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.io.Serializable;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterFragment extends BaseFragment {

    private WelcomeModel mWelcomeModel;
    private int mItemPosition;
    private LoginFragmentChangingListener mListener;
    private EditText mEtEmail, mEtPassword, mEtFirstName;
    private TextView mRegisterButton;
    private ImageView mIvAgreeTerms;
    private boolean isAgreeTermsClicked = false;
    private ProgressBar mProgressBar;
    private Serializable mNotificationData = null;


    public static RegisterFragment newInstance(WelcomeModel mWelcomeModelData, int position, Serializable notificationData) {

        // put data in bundle
        Bundle args = new Bundle();
        args.putSerializable(Constants.WELCOME_MODEL_DATA, mWelcomeModelData);
        args.putInt(Constants.ITEM_POSITION, position);
        if(notificationData!=null){
            args.putSerializable(Keys.NOTIFICATION,notificationData);

        }


        RegisterFragment fragment = new RegisterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mWelcomeModel = (WelcomeModel) getArguments().getSerializable(Constants.WELCOME_MODEL_DATA);
            mItemPosition = getArguments().getInt(Constants.ITEM_POSITION);
            mNotificationData = getArguments().getSerializable(Keys.NOTIFICATION);

        }

        if (getActivity() instanceof LoginFragmentChangingListener) {
            mListener = (LoginFragmentChangingListener) getActivity();
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register, container, false);

        LinearLayout llSignIn = view.findViewById(R.id.ll_sign_in);

        TextView txtSignInNow = view.findViewById(R.id.tv_sign_in_now);

        mProgressBar = view.findViewById(R.id.progressBar);


        mEtFirstName = view.findViewById(R.id.et_register_first_name);


//        mEtLastName = view.findViewById(R.id.et_register_last_name);

        mEtEmail = view.findViewById(R.id.et_register_email_address);

//        mEtConfirmEmailAddress = view.findViewById(R.id.et_register_confirm_email_address);

        mEtPassword = view.findViewById(R.id.et_register_password);

//        mEtConfirmPassword = view.findViewById(R.id.et_register_confirm_password);

        mIvAgreeTerms = view.findViewById(R.id.iv_register_agree_terms);

        mRegisterButton = view.findViewById(R.id.tv_orange_button);

        mRegisterButton.setText(getString(R.string.register_button_text));

        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callRegisterAPI();
            }
        });

//
//        mEtFirstName.setText("Ravi");
//        mEtLastName.setText("Kumar");
//        mEtEmail.setText("programming.devteam@gmail.com");
//        mEtConfirmEmailAddress.setText("programming.devteam@gmail.com");
//        mEtConfirmPassword.setText("123456");
//        mEtPassword.setText("123456");


        if (mWelcomeModel.getUserGroup().isSignInVisible()) {
            llSignIn.setVisibility(View.VISIBLE);
        } else {
            llSignIn.setVisibility(View.GONE);
        }

        txtSignInNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSignInClicked();
            }
        });

        mIvAgreeTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isAgreeTermsClicked) {
                    mIvAgreeTerms.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent));
                    isAgreeTermsClicked = true;
                } else {
                    mIvAgreeTerms.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorWhite));
                    isAgreeTermsClicked = false;

                }
            }
        });

        TextView tvTermsAndCondition = view.findViewById(R.id.tv_terms_condition);

        String[] clikableLinks = {getString(R.string.terms_condition),getString(R.string.privacy_policy)};
        ClickableSpan[] clickableSpan = {termsAndConditionSpan,privacyPolicySpan};
        Utils.makeLinks(tvTermsAndCondition,clikableLinks,clickableSpan);

        view.getViewTreeObserver().addOnWindowFocusChangeListener(new ViewTreeObserver.OnWindowFocusChangeListener() {
            @Override
            public void onWindowFocusChanged(final boolean hasFocus) {
                // do your stuff here
                if(hasFocus){
                    if(mEtEmail.hasFocus()){
                        Utils.showKeyboard(getContext(),mEtEmail);
                        return;
                    }
                    if(mEtPassword.hasFocus()){
                        Utils.showKeyboard(getContext(),mEtPassword);
                        return;
                    }

//                    if(mEtConfirmEmailAddress.hasFocus()){
//                        Utils.showKeyboard(getContext(),mEtConfirmEmailAddress);
//                        return;
//                    }

//                    if(mEtConfirmPassword.hasFocus()){
//                        Utils.showKeyboard(getContext(),mEtConfirmPassword);
//                        return;
//                    }

                    if(mEtFirstName.hasFocus()){
                        Utils.showKeyboard(getContext(),mEtFirstName);
                        return;
                    }

//                    if(mEtLastName.hasFocus()){
//                        Utils.showKeyboard(getContext(),mEtLastName);
//                        return;
//                    }

                    Utils.hideKeyboard(getContext());



                }
            }
        });



        mEtFirstName.setFilters(new InputFilter[]{getEditTextFilter()});

//        mEtLastName.setFilters(new InputFilter[]{getEditTextFilter()});


        return view;
    }


    public static InputFilter getEditTextFilter() {
        return new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                boolean keepOriginal = true;
                StringBuilder sb = new StringBuilder(end - start);
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (isCharAllowed(c)) // put your condition here
                        sb.append(c);
                    else
                        keepOriginal = false;
                }
                if (keepOriginal)
                    return null;
                else {
                    if (source instanceof Spanned) {
                        SpannableString sp = new SpannableString(sb);
                        TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                        return sp;
                    } else {
                        return sb;
                    }
                }
            }

            private boolean isCharAllowed(char c) {
                Pattern ps = Pattern.compile("^[a-zA-Z ]+$");
                Matcher ms = ps.matcher(String.valueOf(c));
                return ms.matches();
            }
        };
    }


    private void callRegisterAPI() {

        Utils.hideKeyboard(getContext());


        // check for validation

        String email = null;
        String password = null;
        String firstName = null;
        String lastName = null;


        if (TextUtils.isEmpty(mEtFirstName.getText().toString().trim())) {

            mEtFirstName.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_name_blank), getString(R.string.okay_text), false,true);
            return;

        }else if (!Utils.isValidName(mEtFirstName.getText().toString().trim())) {

            mEtFirstName.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_first_name_incorrect), getString(R.string.okay_text), false,true);
            return;

//        }  else if (TextUtils.isEmpty(mEtLastName.getText().toString().trim())) {
//
//            mEtLastName.requestFocus();
//            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_last_name_blank), getString(R.string.okay_text), false,true);
//            return;
//
//        } else if (!Utils.isValidName(mEtLastName.getText().toString().trim())) {

//            mEtLastName.requestFocus();
//            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_last_name_incorrect), getString(R.string.okay_text), false,true);
//            return;

        }else if (TextUtils.isEmpty(mEtEmail.getText().toString().trim())) {

            mEtEmail.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_email_blank), getString(R.string.okay_text), false,true);
            return;

        } else if (!Utils.isValidEmail(mEtEmail.getText().toString().trim())) {

            mEtEmail.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_incorrect_email), getString(R.string.okay_text), false,true);
            return;

//        } else if (TextUtils.isEmpty(mEtConfirmEmailAddress.getText().toString().trim())) {
//
//            mEtConfirmEmailAddress.requestFocus();
//            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_confirm_email_blank), getString(R.string.okay_text), false,true);
//            return;
//
//        } else if (!(mEtConfirmEmailAddress.getText().toString()).equals(mEtEmail.getText().toString().trim())) {

//            mEtConfirmEmailAddress.requestFocus();
//            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_incorrect_confirm_email), getString(R.string.okay_text), false,true);
//            return;

        } else if (TextUtils.isEmpty(mEtPassword.getText().toString().trim())) {

            mEtPassword.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_password_blank), getString(R.string.okay_text), false,true);
            return;

        }  else if (mEtPassword.getText().length()<8 || !Utils.isValidPassword(mEtPassword.getText().toString().trim())) {

            mEtPassword.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_valid_password), getString(R.string.okay_text), false,true);
            return;

//        } else if (TextUtils.isEmpty(mEtConfirmPassword.getText().toString().trim())) {
//
//            mEtConfirmPassword.requestFocus();
//            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_confirm_password_blank), getString(R.string.okay_text), false,true);
//            return;
//
//        } else if (!(mEtConfirmPassword.getText().toString()).equals(mEtPassword.getText().toString().trim())) {

//            mEtConfirmPassword.requestFocus();
//            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_incorrect_confirm_password), getString(R.string.okay_text), false,true);
//            return;

        } else if (!isAgreeTermsClicked) {
            mIvAgreeTerms.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_select_agree_terms), getString(R.string.okay_text), false,true);
            return;

        }


        firstName = mEtFirstName.getText().toString();
//        lastName = mEtLastName.getText().toString();
        email = mEtEmail.getText().toString();
        password = mEtPassword.getText().toString();

        mProgressBar.setVisibility(View.VISIBLE);

        final String finalEmail = email;
        final String finalPassword = password;
        final String finalLastName = lastName;
        final String finalFirstName = firstName;

        mRegisterButton.setText(getString(R.string.processing));
        disableTouch();


        // calling register api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.REGISTER_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    mProgressBar.setVisibility(View.GONE);

                    onGetDataAPIServerResponse(response);
                    mRegisterButton.setText(getString(R.string.register_button_text));
                    enableTouch();
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);

                    mRegisterButton.setText(getString(R.string.register_button_text));
                    enableTouch();

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                error.printStackTrace();
                mRegisterButton.setText(getString(R.string.register_button_text));
                enableTouch();
                if(isAdded()) {
                    Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                Map<String, String> params = Utils.getParams(true);

                params.put(Keys.EMAIL, finalEmail);
                params.put(Keys.PASSWORD, finalPassword);
                params.put(Keys.FIRST_NAME, finalFirstName);
//                params.put(Keys.LAST_NAME, finalLastName);

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    // handling register api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
        if (response != null) {

            //  parse json to java
            User user = new Gson().fromJson(response, User.class);

            //  set user data in app preferences
            AppPreferences.setUserData(user);

            if (user.isDashboard()) {
                Intent intent = new Intent();
                Serializable notificationHashMap = ((LoginActivity)getActivity()).getNotificationData();
                if(notificationHashMap != null) {
                    intent.putExtra(Keys.NOTIFICATION,notificationHashMap);
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setClass(getContext(), MainActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_from_right,R.anim.no_animation);

                getActivity().finish();
            } else {
                SubscriptionFragment fragment = SubscriptionFragment.newInstance(true,mNotificationData);
                addFragment(fragment, true);
                setAllFields();
            }


        } else {

            mRegisterButton.setText(getString(R.string.register_button_text));
            enableTouch();
            if(isAdded()) {
                Utils.showAlertDialog(getContext(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
            }
        }

    }



    // set all register fields
    private void setAllFields(){
        mEtFirstName.setText("");
//        mEtLastName.setText("");
        mEtEmail.setText("");
//        mEtConfirmEmailAddress.setText("");
//        mEtConfirmPassword.setText("");
        mEtPassword.setText("");
        mRegisterButton.setText(getString(R.string.register_button_text));
        mIvAgreeTerms.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorWhite));

    }

    //click of terms and condition
    ClickableSpan termsAndConditionSpan = new ClickableSpan() {
        @Override
        public void onClick(View widget) {

            TermsAndConditionFragment termFragment  = TermsAndConditionFragment.newInstance(false,false);
            addFragment(termFragment,true);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    };


    // click of privacy policy
    ClickableSpan privacyPolicySpan = new ClickableSpan() {
        @Override
        public void onClick(View widget) {

            TermsAndConditionFragment termFragment  = TermsAndConditionFragment.newInstance(true,false);
            addFragment(termFragment,true);

        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    };


}
