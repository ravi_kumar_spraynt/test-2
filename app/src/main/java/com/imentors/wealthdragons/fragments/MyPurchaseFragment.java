package com.imentors.wealthdragons.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.courseDetailView.DetailItemListView;
import com.imentors.wealthdragons.models.DashBoardItemOdering;
import com.imentors.wealthdragons.models.DashBoardWishList;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.ItemTypeDeserilization;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.Map;

public class MyPurchaseFragment extends BaseFragment {


    private Context mContext;
    private LinearLayout mProgressBar , mNoData;
    private LinearLayout mLlScreenContainer;
    private SwipeRefreshLayout mSwipeRefershLayout;
    private WealthDragonTextView mLoaderTitle , mTvNoData;
    private FrameLayout mNoInternetConnection;

    public static MyPurchaseFragment newInstance( ) {



        MyPurchaseFragment fragment = new MyPurchaseFragment();
        return fragment;
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {



        View view = inflater.inflate(R.layout.fragment_dashboard_details, container, false);


        mProgressBar = view.findViewById(R.id.progressBar);
        mLlScreenContainer = view.findViewById(R.id.ll_screen_container);
        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);
        mLoaderTitle = view.findViewById(R.id.tv_search);
        mNoData = view.findViewById(R.id.no_data);
        mTvNoData = view.findViewById(R.id.tv_no_Data);
        mLoaderTitle.setText(getString(R.string.loading_my_purchase));
        mNoInternetConnection = view.findViewById(R.id.no_internet);



        view.findViewById(R.id.tv_downloads).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addFragment(MyGroupDownloadFragment.newInstance(),true);

            }
        });


        view.findViewById(R.id.tv_retry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myPurchaseApi();
            }
        });

        myPurchaseApi();

        LocalBroadcastManager.getInstance(mContext).registerReceiver(mReloadPayResponseListener, new IntentFilter(Constants.BROADCAST_PAY_FINSIH));

        mSwipeRefershLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                myPurchaseApi();
                Utils.callEventLogApi("refreshed <b>My Purchase Screen</b>");

            }
        });

        return view;
    }

    // used always except refresh

    public void myPurchaseApi() {

        String api=null;
        mNoInternetConnection.setVisibility(View.GONE);

        if(mSwipeRefershLayout.isRefreshing()){
            mSwipeRefershLayout.setRefreshing(true);
            mProgressBar.setVisibility(View.GONE);
        }else{
            mProgressBar.setVisibility(View.VISIBLE);
        }



        // calling wishlist api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.MY_PURCHASE_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);
                if(mSwipeRefershLayout.isRefreshing()){
                    mSwipeRefershLayout.setRefreshing(false);
                }
                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>My Purchase</b> error in api");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>My Purchase</b>  error in api");
                error.printStackTrace();
                mSwipeRefershLayout.setRefreshing(false);

                if(!Utils.isNetworkAvailable(mContext)){
                    mNoInternetConnection.setVisibility(View.VISIBLE);
                }else{
                    Utils.showNetworkError(mContext, error);
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }



     //handling wishlist api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);

        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response) {

        final GsonBuilder gsonBuilder = new GsonBuilder();

        //  parse json to java
        gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
        final Gson gson = gsonBuilder.create();

        showCoursesInMyPurchase(response, gson);



    }


     // showing courses in wishlist
    private void showCoursesInMyPurchase(String response, Gson gson) {

        final DashBoardWishList dashBoard = gson.fromJson(response, DashBoardWishList.class);

        // empty app preferances data because now article view can adjust right visibility of courses data with null condition
        AppPreferences.setMentorDashBoardDetail(null);
        AppPreferences.setCourseDashBoardDetail(null);
        AppPreferences.setVideoDashBoardDetail(null);
        AppPreferences.setEpisodeDashBoardDetail(null);
        AppPreferences.setCourseDashBoardWishList(null);

        AppPreferences.setCourseDashBoardWishList(dashBoard);


        mLlScreenContainer.removeAllViews();

        if( dashBoard.getFeaturedCourse().getTotal_items() ==0 && dashBoard.getFeaturedProgramme().getTotal_items() == 0){

            mNoData.setVisibility(View.VISIBLE);
            mLlScreenContainer.setVisibility(View.INVISIBLE);

            mTvNoData.setText(mContext.getString(R.string.no_data_mypurchase));

        }else {
            mNoData.setVisibility(View.GONE);
            mLlScreenContainer.setVisibility(View.VISIBLE);

            if (dashBoard.getFeaturedCourse() != null) {
                if (dashBoard.getFeaturedCourse().getCourses() != null) {
                    if (dashBoard.getFeaturedCourse().getCourses().size() > 0) {
                        mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getFeaturedCourse().getCourses(), null,mContext.getString(R.string.purchase), dashBoard.getFeaturedCourse().getNext_page(), dashBoard.getFeaturedCourse().getTotal_items(), null, Constants.TYPE_PURCHASE_FEATURED_COURSES,false,false));

                    }
                }
            }

            if (dashBoard.getFeaturedProgramme() != null) {
                if (dashBoard.getFeaturedProgramme().getProgrammes() != null) {
                    if (dashBoard.getFeaturedProgramme().getProgrammes().size() > 0) {
                        if(mLlScreenContainer.getChildCount()>0) {

                            if (dashBoard.getFeaturedProgramme().getProgrammes().size() > 1) {
                                mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getFeaturedProgramme().getProgrammes(), null, mContext.getString(R.string.featured_programmes), dashBoard.getFeaturedProgramme().getNext_page(), dashBoard.getFeaturedProgramme().getTotal_items(), null, Constants.TYPE_PURCHASE_FEATURED_PROGRAMMES, true,false));

                            } else {
                                mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getFeaturedProgramme().getProgrammes(), null, mContext.getString(R.string.featured_programme), dashBoard.getFeaturedProgramme().getNext_page(), dashBoard.getFeaturedProgramme().getTotal_items(), null, Constants.TYPE_PURCHASE_FEATURED_PROGRAMMES, true,false));

                            }
                        }else{
                            mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getFeaturedProgramme().getProgrammes(), null, mContext.getString(R.string.purchase), dashBoard.getFeaturedProgramme().getNext_page(), dashBoard.getFeaturedProgramme().getTotal_items(), null, Constants.TYPE_PURCHASE_FEATURED_PROGRAMMES, true,false));

                        }

                    }
                }
            }




        }

    }

    private BroadcastReceiver mReloadPayResponseListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            myPurchaseApi();
        }
    };

    //  closed My Purchase page
    @Override
    public void onStop() {
        super.onStop();
//        Utils.callEventLogApi("closed <b>my purchase</b> page");

    }

    @Override
    public void onResume() {
        super.onResume();
        if(!Utils.isNetworkAvailable(mContext)){
            myPurchaseApi();
        }
    }
}
