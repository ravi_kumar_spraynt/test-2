package com.imentors.wealthdragons.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.courseDetailView.DetailItemListView;
import com.imentors.wealthdragons.models.DashBoardItemOdering;
import com.imentors.wealthdragons.models.DashBoardWishList;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.ItemTypeDeserilization;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.Map;

public class WishListFragment extends BaseFragment {


    private Context mContext;
    private LinearLayout mProgressBar , mNoData;
    private LinearLayout mLlScreenContainer;
    private LinearLayoutManager layoutManager;
    private SwipeRefreshLayout mSwipeRefershLayout;
    private WealthDragonTextView mLoaderTitle , mTvNoData;
    private boolean isWishlistHeadingVisible;


    public static WishListFragment newInstance( ) {



        WishListFragment fragment = new WishListFragment();
        return fragment;
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        LocalBroadcastManager.getInstance(context).registerReceiver(mReloadWishlist, new IntentFilter(Constants.BROADCAST_WISHLIST_RELOAD));

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {



        View view = inflater.inflate(R.layout.fragment_dashboard_details, container, false);


        mProgressBar = view.findViewById(R.id.progressBar);
        mLlScreenContainer = view.findViewById(R.id.ll_screen_container);
        layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);
        mLoaderTitle = view.findViewById(R.id.tv_search);
        mNoData = view.findViewById(R.id.no_data);
        mTvNoData = view.findViewById(R.id.tv_no_Data);

        mLoaderTitle.setText(getString(R.string.loading_wishlist));


        callWishListApi();




        mSwipeRefershLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callWishListApi();
                Utils.callEventLogApi("refreshed <b>My WishList Screen</b>");

            }
        });

        return view;
    }

    // used always except refresh

    public void callWishListApi() {

        String api=null;
        if(mSwipeRefershLayout.isRefreshing()){
            mSwipeRefershLayout.setRefreshing(true);
            mProgressBar.setVisibility(View.GONE);
        }else{
            mProgressBar.setVisibility(View.VISIBLE);
        }



        // calling wishlist api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.WISHLIST_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);
                if(mSwipeRefershLayout.isRefreshing()){
                    mSwipeRefershLayout.setRefreshing(false);
                }
                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>WishList</b> error in api");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>WishList</b>  error in api");
                error.printStackTrace();
                Utils.showNetworkError(mContext, error);
                mSwipeRefershLayout.setRefreshing(false);


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }



     //handling wishlist api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);

        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response) {

        final GsonBuilder gsonBuilder = new GsonBuilder();

        //  parse json to java
        gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
        final Gson gson = gsonBuilder.create();

        showCoursesInWishlist(response, gson);



    }


     // showing courses in wishlist
    private void showCoursesInWishlist(String response, Gson gson) {

        isWishlistHeadingVisible = false;

        final DashBoardWishList dashBoard = gson.fromJson(response, DashBoardWishList.class);

        // empty app preferances data because now article view can adjust right visibility of courses data with null condition
        AppPreferences.setMentorDashBoardDetail(null);
        AppPreferences.setCourseDashBoardDetail(null);
        AppPreferences.setVideoDashBoardDetail(null);
        AppPreferences.setEpisodeDashBoardDetail(null);
        AppPreferences.setCourseDashBoardWishList(null);

        AppPreferences.setCourseDashBoardWishList(dashBoard);


        mLlScreenContainer.removeAllViews();

        if(dashBoard.getFeaturedProgramme().getTotal_items() == 0 && dashBoard.getFeaturedCourse().getTotal_items() ==0 && dashBoard.getVideo().getTotal_items()==0 && dashBoard.getExpert().getTotal_items()==0){

            mNoData.setVisibility(View.VISIBLE);
            mTvNoData.setText(mContext.getString(R.string.no_data_wishlist));

        }else {
            mNoData.setVisibility(View.GONE);

            if (dashBoard.getFeaturedCourse() != null) {
                if (dashBoard.getFeaturedCourse().getCourses() != null) {
                    if (dashBoard.getFeaturedCourse().getCourses().size() > 0) {
                        isWishlistHeadingVisible = true;
                        mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getFeaturedCourse().getCourses(), null,mContext.getString(R.string.my_wishlist), dashBoard.getFeaturedCourse().getNext_page(), dashBoard.getFeaturedCourse().getTotal_items(), null, Constants.TYPE_WISHLLIST_FEATURED_COURSES,false,false));

                    }
                }
            }

            if (dashBoard.getFeaturedProgramme() != null) {
                if (dashBoard.getFeaturedProgramme().getProgrammes() != null) {
                    if (dashBoard.getFeaturedProgramme().getProgrammes().size() > 0) {
                        mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getFeaturedProgramme().getProgrammes(), null, isWishlistHeadingVisible?dashBoard.getFeaturedProgramme().getTitle():mContext.getString(R.string.my_wishlist), dashBoard.getFeaturedProgramme().getNext_page(), dashBoard.getFeaturedProgramme().getTotal_items(), null, Constants.TYPE_WISHLLIST_FEATURED_PROGRAMMES,true,false));
                        isWishlistHeadingVisible = true;

                    }
                }
            }


//            if (dashBoard.getEvent() != null) {
//                if (dashBoard.getEvent().getSmallLeterEvents() != null) {
//                    if (dashBoard.getEvent().getSmallLeterEvents().size() > 0) {
//                        mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getEvent(), null,dashBoard.getEvent().getTitle(),dashBoard.getEvent().getNext_page(), dashBoard.getEvent().getTotal_items(), Constants.TYPE_WISHLLIST_FEATURED_EVENTS));
//
//                    }
//                }
//            }


            if (dashBoard.getVideo() != null) {
                if (dashBoard.getVideo().getVideos() != null) {
                    if (dashBoard.getVideo().getVideos().size() > 0) {

                        if(dashBoard.getVideo().getVideos().size()>1){
                            mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getVideo().getVideos(), "null",isWishlistHeadingVisible?dashBoard.getVideo().getTitle():mContext.getString(R.string.my_wishlist),dashBoard.getVideo().getNext_page(), dashBoard.getVideo().getTotal_items(), Constants.TYPE_WISHLLIST_FEATURED_VIDEOS));

                        }else{
                            mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getVideo().getVideos(), "null",isWishlistHeadingVisible?Constants.TYPE_EVENT:mContext.getString(R.string.my_wishlist),dashBoard.getVideo().getNext_page(), dashBoard.getVideo().getTotal_items(), Constants.TYPE_WISHLLIST_FEATURED_VIDEOS));

                        }
                        isWishlistHeadingVisible = true;


                    }
                }
            }

            if (dashBoard.getExpert() != null) {
                if (dashBoard.getExpert().getExperts() != null) {
                    if (dashBoard.getExpert().getExperts().size() > 0) {
                        isWishlistHeadingVisible = true;

                        if(dashBoard.getExpert().getExperts().size()>1){
                            mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getExpert(), null,isWishlistHeadingVisible?"Mentors":mContext.getString(R.string.my_wishlist),dashBoard.getExpert().getNext_page(), dashBoard.getExpert().getTotal_items(), Constants.TYPE_WISHLLIST_FEATURED_MENTORS,null));
                        }else{
                            mLlScreenContainer.addView(new DetailItemListView(mContext, dashBoard.getExpert(), null,isWishlistHeadingVisible?Constants.TYPE_MENTOR:mContext.getString(R.string.my_wishlist),dashBoard.getExpert().getNext_page(), dashBoard.getExpert().getTotal_items(), Constants.TYPE_WISHLLIST_FEATURED_MENTORS,null));

                        }
                        isWishlistHeadingVisible = true;

                    }
                }
            }
        }

    }

    //  closed wishlist page
    @Override
    public void onStop() {
        super.onStop();
        Utils.callEventLogApi("closed <b>WishList</b> page");

    }


    private BroadcastReceiver mReloadWishlist = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
           callWishListApi();
        }
    };

}
