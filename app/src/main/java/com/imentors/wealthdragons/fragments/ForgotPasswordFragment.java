package com.imentors.wealthdragons.fragments;

import android.app.AlertDialog;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.interfaces.LoginFragmentChangingListener;
import com.imentors.wealthdragons.models.WelcomeModel;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class ForgotPasswordFragment extends BaseFragment {


    private WelcomeModel mWelcomeModel;
    private int mItemPosition;
    private LoginFragmentChangingListener mListener;
    private EditText mEtForgotPasswordEmail;
    private TextView tvForgotPasswordButton;
    private ProgressBar mProgressBar;

    public static ForgotPasswordFragment newInstance(WelcomeModel mWelcomeModelData, int position ) {

        //  put data in bundle
        Bundle args = new Bundle();
        args.putSerializable(Constants.WELCOME_MODEL_DATA,mWelcomeModelData);
        args.putInt(Constants.ITEM_POSITION,position);

        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null){
            mWelcomeModel = (WelcomeModel) getArguments().getSerializable(Constants.WELCOME_MODEL_DATA);
            mItemPosition = getArguments().getInt(Constants.ITEM_POSITION);
        }
        if (getActivity() instanceof LoginFragmentChangingListener) {
            mListener = (LoginFragmentChangingListener) getActivity();
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);



        LinearLayout ll_sign_in = view.findViewById(R.id.ll_sign_in);
        TextView txtSignIn = view.findViewById(R.id.tv_sign_in_now);

        mProgressBar = view.findViewById(R.id.progressBar);


        tvForgotPasswordButton = view.findViewById(R.id.tv_orange_button);
        FrameLayout flOrange = view.findViewById(R.id.fl_btn_orange);

        tvForgotPasswordButton.setText(getString(R.string.retrive_now));


        mEtForgotPasswordEmail = view.findViewById(R.id.et_forgot_password_email);
//        mEtForgotPasswordEmail.setText("programming.devteam@gmail.com");


        if(!mWelcomeModel.getUserGroup().isSignInVisible()){
            ll_sign_in.setVisibility(View.GONE);
        }else{
            ll_sign_in.setVisibility(View.VISIBLE);
        }

        txtSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSignInClicked();
            }
        });


        mEtForgotPasswordEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    tvForgotPasswordButton.requestFocus();

                    callForgotPasswordApi();
                    return true;
                }
                return false;
            }
        });

        flOrange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.callEventLogApi("clicked <b>Gain Access Now</b> " );

                callForgotPasswordApi();
            }
        });


        return view;
    }


    private void callForgotPasswordApi() {



        Utils.hideKeyboard(getContext());


        // check for validation

        String email = null;
        String password = null;

        if(TextUtils.isEmpty(mEtForgotPasswordEmail.getText().toString())){

            mEtForgotPasswordEmail.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_email_blank), getString(R.string.okay_text), false,true);
            return;


        }else if(!Utils.isValidEmail(mEtForgotPasswordEmail.getText().toString())){

            mEtForgotPasswordEmail.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_incorrect_email), getString(R.string.okay_text), false,true);
            return;

        }
        mProgressBar.setVisibility(View.VISIBLE);



        tvForgotPasswordButton.setText(getString(R.string.loading));





        disableTouch();



        email = mEtForgotPasswordEmail.getText().toString();

        final String finalEmail = email;


        // calling forgot password api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.FORGOT_PASSWORD_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);

                try {

                   onGetDataAPIServerResponse(response);

                   tvForgotPasswordButton.setText(getString(R.string.retrive_now));



                } catch (Exception e) {
                    enableTouch();

                    tvForgotPasswordButton.setText(getString(R.string.retrive_now));


                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                enableTouch();
                mProgressBar.setVisibility(View.GONE);

                tvForgotPasswordButton.setText(getString(R.string.retrive_now));



                error.printStackTrace();
                if(isAdded()) {
                    Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.EMAIL, finalEmail);

                return params;
            }
        };



        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    //handling forgot password api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
        if(response!=null) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response);
                String message = jsonObject.getString(Keys.SUCCESS);
                if(isAdded()) {
                    Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                        @Override
                        public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                            dialog.dismiss();
                            setAllFields();
                        }

                        @Override
                        public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                            dialog.dismiss();
                            callForgotPasswordApi();
                        }
                    }, getActivity(), getString(R.string.retrive_your_password), message, getString(R.string.okay_text), "Resend");
                }

            } catch (JSONException e) {
                enableTouch();

                tvForgotPasswordButton.setText(getString(R.string.retrive_now));


                e.printStackTrace();
                if(isAdded()) {
                    Utils.showAlertDialog(getContext(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                }
            }
               }

    }

     // setting data on view
    private void setAllFields(){

            mEtForgotPasswordEmail.setText("");
            tvForgotPasswordButton.setText(getString(R.string.retrive_now));


    }


}
