package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.models.SupportType;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.util.Map;

public class ExistingSupportRequestDetailFragment extends BaseFragment {

    private TextView mTvFirstName, mTvTicketNumber, mTvMobileNumber, mTvMessage, mTvEmail, mTvServiceCode, mTvCountryCode, mTvSericeType,mTvMessageDate,mTvReplyButton;
    private FrameLayout mBtReply , mBtBack;
    private LinearLayout mProgressbarLayout;
    private Context mContext;
    private SupportType.ExistingSupportMessage mSupportType;


    public static ExistingSupportRequestDetailFragment newInstance(SupportType.ExistingSupportMessage item) {


        ExistingSupportRequestDetailFragment fragment = new ExistingSupportRequestDetailFragment();
        //  put data in bundle
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.SUPPORT,item);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSupportType = (SupportType.ExistingSupportMessage) getArguments().getSerializable(Constants.SUPPORT);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_existing_support_request_detail, container, false);

        mTvMessage = view.findViewById(R.id.et_support_message);
        mTvMessageDate = view.findViewById(R.id.et_support_message_date);
        mTvFirstName = view.findViewById(R.id.et_support_first_name);
        mTvTicketNumber = view.findViewById(R.id.et_support_ticket_number);
        mTvEmail = view.findViewById(R.id.et_support_email_address);
        mTvMobileNumber = view.findViewById(R.id.et_support_mobile_number);
        mTvServiceCode = view.findViewById(R.id.et_support_service_code);
        mTvSericeType = view.findViewById(R.id.et_support_service_type);
        mProgressbarLayout = view.findViewById(R.id.progressBar);
        mTvReplyButton = view.findViewById(R.id.tv_reply_button_text);
        mBtReply = view.findViewById(R.id.fl_reply);
        mBtBack = view.findViewById(R.id.fl_back);

        mBtBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.callEventLogApi("clicked <b>Back</b> button from "+ mSupportType.getTicket_no() +" ticket of customer support");

                popFragment();
            }
        });

        mBtReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>Reply</b> button from "+ mSupportType.getTicket_no() +" ticket of customer support");
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.add(R.id.fm_customer, ExistingSupportReplyListFragment.newInstance(mSupportType.getId()));
                ft.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_right);
                ft.addToBackStack(null);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            }
        });

        setDataOnScreen(mSupportType);



        return view;
    }


    //TODO implemented maintenance mode
    private void callDetailApi() {


        // calling reply support list detail api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.REPLY_SUPPORT_LIST_DETAIL_API, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
                        return;
                    }

                } catch (Exception e) {
                    Utils.callEventLogApi("getting <b>Existing Support</b> error in api");
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressbarLayout.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>Existing Support</b> error in api");
                error.printStackTrace();
                Utils.showCustomerSupportNetworkError(mContext, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

// handling reply support list detail api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
    }


    //  set data on view
    private void setDataOnScreen(SupportType.ExistingSupportMessage mCustomerData) {

            mTvFirstName.setText(mCustomerData.getFull_name());

            mTvTicketNumber.setText(mCustomerData.getTicket_no());

            mTvEmail.setText(mCustomerData.getContact_email());

            mTvMobileNumber.setText(mCustomerData.getContact_mobile());

            mTvServiceCode.setText(mCustomerData.getService_code());

            mTvMessage.setText(mCustomerData.getContact_message());

            mTvSericeType.setText(mCustomerData.getSupport_type_title());

            mTvMessageDate.setText(mCustomerData.getDate());

    }

    //  closed existing support page
    @Override
    public void onStop() {
        super.onStop();
        Utils.callEventLogApi("closed <b>Existing Support</b> page");

    }
}
