package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.DashBoard;
import com.imentors.wealthdragons.models.DashBoardItemOdering;
import com.imentors.wealthdragons.models.DashBoardItemOderingArticle;
import com.imentors.wealthdragons.models.DashBoardItemOderingEvent;
import com.imentors.wealthdragons.models.DashBoardItemOderingMentors;
import com.imentors.wealthdragons.models.DashBoardItemOderingVideo;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.ItemTypeDeserilization;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.SeeAllTabView;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.Map;

public class SeeAllFragment extends BaseFragment {


    /**
     * use always for slider while getting position  = position % itemList.size()
     */


    private Context mContext;
    private RecyclerView mRecyclerViewpager;
    private LinearLayout mProgressBar, mNoDataContainer;
    private LinearLayout mLlScreenContainer;
    private LinearLayoutManager layoutManager;
    private SwipeRefreshLayout mSwipeRefershLayout;

    private WealthDragonTextView mSearchLayout;
    private String mSelectedSubCategoryName , mCatId , mSeeAllKey , mCategoryName, mTabSelectedName, mPaginationId;



    public static SeeAllFragment newInstance(String categoryId , String seeAll ,String categoryName, String type) {


        //  add data in bundle
        Bundle args = new Bundle();

        args.putString(Constants.CAT_ID,categoryId);
        args.putString(Constants.SEE_ALL_KEY,seeAll);
        args.putString(Constants.CATEGORY_NAME,categoryName);
        args.putString(Constants.SELECTED_TAB,type);

        SeeAllFragment fragment = new SeeAllFragment();
        fragment.setArguments(args);
        return fragment;
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCatId =  getArguments().getString(Constants.CAT_ID);
        mPaginationId = mCatId;
        mSeeAllKey =  getArguments().getString(Constants.SEE_ALL_KEY);
        mCategoryName =  getArguments().getString(Constants.CATEGORY_NAME);
        mTabSelectedName =  getArguments().getString(Constants.SELECTED_TAB);



    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);


        mProgressBar = view.findViewById(R.id.progressBar);
        mLlScreenContainer = view.findViewById(R.id.ll_screen_container);
        mRecyclerViewpager =  view.findViewById(R.id.pager);
        layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);
        mSearchLayout = view.findViewById(R.id.tv_search);
        mRecyclerViewpager.setVisibility(View.GONE);
        mNoDataContainer = view.findViewById(R.id.no_data_available);
        callSeeAllApi();


        mSearchLayout.setText(getString(R.string.loading_see_all)+" "+ mCategoryName +"...");

        mSwipeRefershLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callSeeAllApi();
                Utils.callEventLogApi("refreshed <b>See All Screen</b>");

            }
        });


        return view;
    }



    // used always except refresh

    public void callSeeAllApi(String api , final String tasteId , final String parentCatId, final String selectedSubCategoryName) {

        mSelectedSubCategoryName = selectedSubCategoryName;

        if(mSwipeRefershLayout.isRefreshing()){
            mSwipeRefershLayout.setRefreshing(true);
            mProgressBar.setVisibility(View.GONE);
        }else{
            mProgressBar.setVisibility(View.VISIBLE);
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);
                if(mSwipeRefershLayout.isRefreshing()){
                    mSwipeRefershLayout.setRefreshing(false);
                }
                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>See All</b> error in api");

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>See All</b> error in api");
                error.printStackTrace();
                Utils.showNetworkError(mContext, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                if(!TextUtils.isEmpty(tasteId)){
                    params.put(Keys.TASTE_ID,tasteId);
                    mPaginationId = tasteId;

                }



                if(!TextUtils.isEmpty(parentCatId)){
                    params.put(Keys.PARENT_ID,parentCatId);
                }
                else{
                    mPaginationId = mCatId;

                }


                    if (!TextUtils.isEmpty(mTabSelectedName)) {
                        if(!mTabSelectedName.equals(Constants.TYPE_OLD)) {
                            params.put(Keys.TYPE, mTabSelectedName);
                        }
                    }


                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }




    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response) {



        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
        final Gson gson = gsonBuilder.create();

        // Parse JSON to Java
        final DashBoard dashBoard = gson.fromJson(response, DashBoard.class);
        AppPreferences.setDashBoard(dashBoard);


        String title = dashBoard.getCategory_title();

        // to replace title on first active array
        int findFirstActiveArray = 0;


        mLlScreenContainer.removeAllViews();

        if(dashBoard.getHeadingOrdering().size()>0) {

            for (DashBoardItemOdering dashBoardItemOdering : dashBoard.getHeadingOrdering()) {

                String layoutType = dashBoardItemOdering.getLayout();

                switch (layoutType) {
                    case Constants.TYPE_COURSE:

                        DashBoardItemOderingArticle dashBoardItemOderingArticle = (DashBoardItemOderingArticle) dashBoardItemOdering;
                        if (dashBoardItemOderingArticle.getItems().size() > 0) {
                            findFirstActiveArray++;

                            if (findFirstActiveArray == 1) {
                                if (!TextUtils.isEmpty(mCatId)) {
                                    mLlScreenContainer.addView(new SeeAllTabView(mContext, dashBoardItemOderingArticle, title, mCatId, mSeeAllKey, dashBoard.getSubcategories(), mSelectedSubCategoryName, false,mPaginationId));
                                } else {
                                    mLlScreenContainer.addView(new SeeAllTabView(mContext, dashBoardItemOderingArticle, title, mCatId, mSeeAllKey, dashBoard.getSubcategories(), mSelectedSubCategoryName, false,mPaginationId));
                                }

                            } else {

                                if (!TextUtils.isEmpty(mCatId)) {
                                    mLlScreenContainer.addView(new SeeAllTabView(mContext, dashBoardItemOderingArticle, null, mCatId, mSeeAllKey, null, null, false,mPaginationId));
                                } else {
                                    mLlScreenContainer.addView(new SeeAllTabView(mContext, dashBoardItemOderingArticle, null, mCatId, mSeeAllKey, null, null, false,mPaginationId));
                                }

                            }

                        }

                        continue;

                    case Constants.TYPE_PROGRAMMES:

                        DashBoardItemOderingArticle dashBoardItemOderingProgrammes = (DashBoardItemOderingArticle) dashBoardItemOdering;
                        if (dashBoardItemOderingProgrammes.getItems().size() > 0) {
                            findFirstActiveArray++;

                            if (findFirstActiveArray == 1) {
                                if (!TextUtils.isEmpty(mCatId)) {
                                    mLlScreenContainer.addView(new SeeAllTabView(mContext, dashBoardItemOderingProgrammes, title, mCatId, mSeeAllKey, dashBoard.getSubcategories(), mSelectedSubCategoryName, true,mPaginationId));
                                } else {
                                    mLlScreenContainer.addView(new SeeAllTabView(mContext, dashBoardItemOderingProgrammes, title, mCatId, mSeeAllKey, dashBoard.getSubcategories(), mSelectedSubCategoryName, true,mPaginationId));
                                }

                            } else {

                                if (!TextUtils.isEmpty(mCatId)) {
                                    mLlScreenContainer.addView(new SeeAllTabView(mContext, dashBoardItemOderingProgrammes, null, mCatId, mSeeAllKey, null, null, true,mPaginationId));
                                } else {
                                    mLlScreenContainer.addView(new SeeAllTabView(mContext, dashBoardItemOderingProgrammes, null, mCatId, mSeeAllKey, null, null, true,mPaginationId));
                                }

                            }

                        }

                        continue;


                    case Constants.TYPE_VIDEO:

                        DashBoardItemOderingVideo dashBoardItemOderingVideo = (DashBoardItemOderingVideo) dashBoardItemOdering;


                        if (dashBoardItemOderingVideo.getItems().size() > 0) {
                            findFirstActiveArray++;
                            if (findFirstActiveArray == 1) {
                                mLlScreenContainer.addView(new SeeAllTabView(mContext, dashBoardItemOderingVideo, title, mCatId, dashBoard.getSubcategories(), mSelectedSubCategoryName,mPaginationId));
                            } else {
                                mLlScreenContainer.addView(new SeeAllTabView(mContext, dashBoardItemOderingVideo, null, mCatId, null, null,mPaginationId));

                            }
                        }


                        continue;


                    case Constants.TYPE_MENTOR:
                        DashBoardItemOderingMentors dashBoardItemOderingMentors = (DashBoardItemOderingMentors) dashBoardItemOdering;

                        if (dashBoardItemOderingMentors.getItems().size() > 0) {
                            findFirstActiveArray++;
                            if (findFirstActiveArray == 1) {
                                mLlScreenContainer.addView(new SeeAllTabView(mContext, dashBoardItemOderingMentors, title, mCatId, dashBoard.getSubcategories(), mSelectedSubCategoryName,mPaginationId));
                            } else {
                                mLlScreenContainer.addView(new SeeAllTabView(mContext, dashBoardItemOderingMentors, null, mCatId, null, null,mPaginationId));

                            }
                        }

                        continue;

                    case Constants.TYPE_EVENT:
                        DashBoardItemOderingEvent dashBoardItemOderingEvents = (DashBoardItemOderingEvent) dashBoardItemOdering;

                        if (dashBoardItemOderingEvents.getItems().size() > 0) {
                            findFirstActiveArray++;
                            if (findFirstActiveArray == 1) {
                                mLlScreenContainer.addView(new SeeAllTabView(mContext, dashBoardItemOderingEvents, title, mCatId, dashBoard.getSubcategories(), mSelectedSubCategoryName,mPaginationId));
                            } else {
                                mLlScreenContainer.addView(new SeeAllTabView(mContext, dashBoardItemOderingEvents, null, mCatId, null, null,mPaginationId));

                            }
                        }

                        continue;
                }


            }
        }

        if(findFirstActiveArray==0){
            mNoDataContainer.setVisibility(View.VISIBLE);
        }

    }



    //used only for refresh api in MainActivity

    public void callSeeAllApi() {

        mNoDataContainer.setVisibility(View.GONE);

        String api = null;

        mSelectedSubCategoryName = null;

        if(mSwipeRefershLayout.isRefreshing()){
            mSwipeRefershLayout.setRefreshing(true);
            mProgressBar.setVisibility(View.GONE);
        }else{
            mProgressBar.setVisibility(View.VISIBLE);
        }

        if(!TextUtils.isEmpty(mSeeAllKey)) {
            if (mSeeAllKey.equals(Constants.COURSES_YOU_MAY_LIKE)) {

                api = Api.COURSES_YOU_MAY_LIKE;
            } else {
                api = Api.SEE_ALL_API;
            }
        }else{
            api = Api.SEE_ALL_API;
        }


        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);
                if(mSwipeRefershLayout.isRefreshing()){
                    mSwipeRefershLayout.setRefreshing(false);
                }
                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>See All</b> error in api");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>See All</b> error in api");
                error.printStackTrace();
                Utils.showNetworkError(mContext, error);
                mSwipeRefershLayout.setRefreshing(false);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                if (!TextUtils.isEmpty(mCatId)) {
                    params.put(Keys.TASTE_ID, mCatId);
                    mPaginationId = mCatId;

                }



                if(!TextUtils.isEmpty(mTabSelectedName)){
                    if(!mTabSelectedName.equals(Constants.TYPE_OLD)){
                        params.put(Keys.TYPE,mTabSelectedName);
                    }
                }
                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

}
