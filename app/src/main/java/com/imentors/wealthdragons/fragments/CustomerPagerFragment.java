package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.imentors.wealthdragons.R;

import com.imentors.wealthdragons.utils.Utils;

public class CustomerPagerFragment extends BaseFragment  {


    private Context mContext;
    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;

    public static CustomerPagerFragment newInstance() {

        Bundle args = new Bundle();

        CustomerPagerFragment fragment = new CustomerPagerFragment();
        fragment.setArguments(args);
        return fragment;
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_dashboard_viewpager, container, false);
        mViewPager = view.findViewById(R.id.fragments_viewpager);
        mPagerAdapter = new PagerAdapter(getActivity().getSupportFragmentManager());

        mViewPager.setAdapter(mPagerAdapter);

        mViewPager.setCurrentItem(0);
        Utils.hideKeyboard(mContext);


        return view;
    }


    public static class PagerAdapter extends FragmentPagerAdapter {

        private Fragment mCurrentFragment;


        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            //  opened fragments
            if(position == 0){
                return NewSupportRequestFragment.newInstance();
            }else{
                return ExistingSupportRequestListFragment.newInstance();
            }
        }

        //  returning int
        @Override
        public int getCount() {
            return 2;
        }

        public Fragment getCurrentFragment(){
            return mCurrentFragment;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            if(getCurrentFragment()!=object){
                mCurrentFragment = (Fragment) object;
            }
            super.setPrimaryItem(container, position, object);
        }
    }

    //  set current item in viewpager
    public void setCurrentItem(int position){
        mViewPager.setCurrentItem(position);
        Utils.hideKeyboard(mContext);
    }

    public Fragment getCurrentItemOnPager(){
        return mPagerAdapter.getCurrentFragment();
    }


}
