package com.imentors.wealthdragons.fragments;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.CardItemListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.CreditCard;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static com.facebook.FacebookSdk.getApplicationContext;
public class CardItemListFragment  extends BaseFragment implements CardItemListAdapter.OnListItemClicked {
    private RecyclerView mRecycleview;
    private CardItemListAdapter mCardItemListAdapter;
    private List<CreditCard> mCreditCard=new ArrayList<>();
    private LinearLayout mProgressBar,mNoCardFound;
    private Context mContext;
    public static CardItemListFragment newInstance() {
        Bundle args = new Bundle();
        CardItemListFragment fragment = new CardItemListFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_card_list,container,false);
        mRecycleview=view.findViewById(R.id.recycler_view);
        mProgressBar=view.findViewById(R.id.progressBar);
        mNoCardFound=view.findViewById(R.id.no_data);
        callExistingCardListApi();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecycleview.setLayoutManager(mLayoutManager);
        return view;
    }
    private void deleteCreditCardDetails(final CreditCard creditCard){
        // calling badges api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.DELETE_CARD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new ArrayMap<>();
                params.put(Keys.CARD_ID,creditCard.getId());
                return params;
            }
        };
        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }
    private void callExistingCardListApi() {
        mProgressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.PAYMENT_CARD_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    mProgressBar.setVisibility(View.GONE);
                    setCardListInAppPreferences(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                error.printStackTrace();
                Utils.showNetworkError(getContext(), error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = Utils.getParams(false);
                return params;
            }
        };
        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }
    private void setCardListInAppPreferences(String response) {
        //  parse JSON to java
        List<CreditCard> creditCardTypeList = null;
        if(!TextUtils.isEmpty(response)) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response);
                creditCardTypeList = new Gson().fromJson(jsonObject.getJSONArray("CardDetails").toString(), new TypeToken<List<CreditCard>>() {
                }.getType());
                AppPreferences.setCreditCardList(creditCardTypeList);
                if (creditCardTypeList!=null) {
                    if (creditCardTypeList.size() > 0) {
                        mCreditCard.addAll(creditCardTypeList);
                        mCardItemListAdapter = new CardItemListAdapter(getContext(), mCreditCard, this);
                        mRecycleview.setAdapter(mCardItemListAdapter);
                    } else {
                        mNoCardFound.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    mNoCardFound.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void onItemDelete(int pos) {
        deleteCreditCardDetails(mCreditCard.get(pos));
        mCardItemListAdapter.removeItem(pos);
    }
}

