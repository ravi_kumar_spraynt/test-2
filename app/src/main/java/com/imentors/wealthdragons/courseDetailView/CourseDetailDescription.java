package com.imentors.wealthdragons.courseDetailView;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.EClassesListAdapter;
import com.imentors.wealthdragons.dialogFragment.ChatCommentDialog;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.DownloadFile;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;
import com.tonyodev.fetch2.AbstractFetchListener;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2.Status;
import com.tonyodev.fetch2core.Extras;
import com.tonyodev.fetch2core.Func;
import com.tonyodev.fetch2core.MutableExtras;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

public class CourseDetailDescription extends LinearLayout {


    private Context mContext;
    private FrameLayout mBtShare, mBtFullDetails;
    private WealthDragonTextView mTvCourseShortDescription, mTvCourseFullDescription , mTvFullDetailBtn, mTvCourseDescriptionTitle;
    private boolean isFullVisible = false;
    private String mType,mID;
    private ImageView mIvDownloadGrey, mIvDownloadOrange,mIvEclassDownloadGrey,mIvEclassDownloadOrange;
    private ProgressBar mProgressBar,mDownloadProgresBar,mEclassDownloadProgressBar;
    List<DownloadFile> list = AppPreferences.getDownloadList();
    private DownloadFile mDownLoadTask;
    private Request mRequset;
    private Fetch mFetch;
    private List<Download> mDownLoadItemList;
    private boolean mIsTablet;
    private String imageUrl,mBannerImagePath;
    private DashBoardCourseDetails dashBoardCourseDetails;
    private TextView mPercentage;
    private Thread mProgreeBarThread;
    private Handler mProgressBarHandler = new Handler();
    private int mProgressBarPosition;
    private boolean mShowProgressBarDefinite = true;
    private boolean mIsDelete=true;
    private CourseDetailEClassesview.OnAllDownload mOnAllDownloadLIstener;

    private boolean mIsDownloaded ;



    public CourseDetailDescription(Context context) {
        super(context);
        mContext = context;
    }

    public CourseDetailDescription(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public CourseDetailDescription(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public CourseDetailDescription(Context context, DashBoardCourseDetails dashBoardDetails, CourseDetailEClassesview.OnAllDownload onAllDownload, boolean isDownloaded) {
        super(context);
        dashBoardCourseDetails = dashBoardDetails;
        mFetch = Utils.getFetchInstance();
        mOnAllDownloadLIstener = onAllDownload;
        mIsDownloaded = isDownloaded;
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set data in view
    private void bindView(final DashBoardCourseDetails dashBoardDetails) {

        mID = dashBoardDetails.getCourseDetail().getId();


        mRequset = new Request(dashBoardDetails.getCourseDetail().getPlay_url(), mContext.getApplicationContext().getFilesDir()+ "/video/"  + dashBoardDetails.getCourseDetail().getId());

        imageUrl =dashBoardDetails.getCourseDetail().getBanner();

        showDonwloadIcon();
        mFetch.addListener(fetchListener);

        if (dashBoardDetails.isEclassVisible()
                && dashBoardDetails.getCourseDetail().getChapters()!=null
                && dashBoardDetails.getCourseDetail().getChapters().size()>0
                && dashBoardDetails.getCourseDetail().getChapters().get(0).getE_classes().size()>0)
        {
            mIvEclassDownloadGrey.setVisibility(GONE);
            mIvEclassDownloadOrange.setVisibility(VISIBLE);
            mEclassDownloadProgressBar.setVisibility(GONE);
        }else
        {
            mIvEclassDownloadGrey.setVisibility(GONE);
            mIvEclassDownloadOrange.setVisibility(GONE);
            mEclassDownloadProgressBar.setVisibility(GONE);
        }

//        mIvEclassDownloadOrange.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if(Utils.isNetworkAvailable(mContext)){
//                    mIvEclassDownloadOrange.setVisibility(GONE);
//                    mIvEclassDownloadGrey.setVisibility(GONE);
//                    mEclassDownloadProgressBar.setVisibility(VISIBLE);
//                    mOnAllDownloadLIstener.onDownnloadClick();
//                }else{
//                    Toast.makeText(getContext(), R.string.error_api_no_internet_connection, Toast.LENGTH_SHORT).show();
//
//                }
//
//
//
//            }
//        });

        mIvEclassDownloadOrange.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        if(Utils.isNetworkAvailable(mContext)){
                            mIvEclassDownloadOrange.setVisibility(GONE);
                            mIvEclassDownloadGrey.setVisibility(GONE);
                            dialog.dismiss();
                            mEclassDownloadProgressBar.setVisibility(VISIBLE);
                            mOnAllDownloadLIstener.onDownnloadClick();
                        }else{
                            Toast.makeText(getContext(), R.string.error_api_no_internet_connection, Toast.LENGTH_SHORT).show();

                        }



                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();


                    }
                }, getContext(), getContext().getString(R.string.download), getContext().getString(R.string.all_download), getContext().getString(R.string.yes), getContext().getString(R.string.no));
            }
        });


        mEclassDownloadProgressBar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        Toast.makeText(getContext(), R.string.download_canceled, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        mOnAllDownloadLIstener.onCancelDownloadClick();

                        mEclassDownloadProgressBar.setVisibility(GONE);
                        mIvEclassDownloadOrange.setVisibility(VISIBLE);
                        mIvEclassDownloadGrey.setVisibility(GONE);



                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();
                    }
                }, getContext(), getContext().getString(R.string.cancel), getContext().getString(R.string.cancel_message), getContext().getString(R.string.yes), getContext().getString(R.string.no));
            }
        });

        mIvEclassDownloadGrey.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        Toast.makeText(getContext(), R.string.download_canceled, Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                        Utils.getFetchInstance().deleteGroup(Integer.valueOf(mID));
                        mEclassDownloadProgressBar.setVisibility(GONE);
                        mIvEclassDownloadOrange.setVisibility(VISIBLE);
                        mIvEclassDownloadGrey.setVisibility(GONE);
                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();
                    }
                }, getContext(), getContext().getString(R.string.remove), getContext().getString(R.string.remove_all_download), getContext().getString(R.string.yes), getContext().getString(R.string.no));

            }
        });

        mIvDownloadOrange.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Utils.isNetworkAvailable(mContext)) {
                    mIsDelete=true;
                    mProgressBar.setProgress(0);
                    mProgressBar.setVisibility(GONE);
                    mDownloadProgresBar.setVisibility(VISIBLE);
                    mIvDownloadGrey.setVisibility(GONE);
                    mIvDownloadOrange.setVisibility(GONE);
                    Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.download_added), Toast.LENGTH_SHORT).show();
                    Utils.callEventLogApi("downloading course started  <b>" +dashBoardCourseDetails.getCourseDetail().getTitle()+" </b>" + "from Course Detail" );

                    new startImageDownload().execute();
                }else{
                    Toast.makeText(mContext, mContext.getString(R.string.error_api_no_internet_connection), Toast.LENGTH_SHORT).show();
                }

            }
        });


        mDownloadProgresBar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        Toast.makeText(getContext(), R.string.download_canceled, Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi("downloading course cancelled  <b>" +dashBoardCourseDetails.getCourseDetail().getTitle()+" </b>" + "from Course Detail" );
                        mFetch.delete(mRequset.getId());
                        dialog.dismiss();
                        mProgressBar.setVisibility(GONE);
                        mDownloadProgresBar.setVisibility(GONE);
                        mIvDownloadGrey.setVisibility(GONE);
                        mIvDownloadOrange.setVisibility(VISIBLE);
                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();
                    }
                }, getContext(), getContext().getString(R.string.cancel), getContext().getString(R.string.cancel_message), getContext().getString(R.string.yes), getContext().getString(R.string.no));

            }
        });

        mProgressBar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        Toast.makeText(getContext(), R.string.download_canceled, Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi("downloading course cancelled  <b>" +dashBoardCourseDetails.getCourseDetail().getTitle()+" </b>" + "from Course Detail" );
                        mFetch.delete(mRequset.getId());
                        dialog.dismiss();
                        mProgressBar.setVisibility(GONE);
                        mIvDownloadGrey.setVisibility(GONE);
                        mIvDownloadOrange.setVisibility(VISIBLE);
                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();
                    }
                }, getContext(), getContext().getString(R.string.cancel), getContext().getString(R.string.cancel_message), getContext().getString(R.string.yes), getContext().getString(R.string.no));

            }
        });

        mIvDownloadGrey.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        mFetch.delete(mRequset.getId());
                        dialog.dismiss();
                        mProgressBar.setVisibility(GONE);
                        mIvDownloadGrey.setVisibility(GONE);
                        mIvDownloadOrange.setVisibility(VISIBLE);
                        Utils.callEventLogApi("removed downloaded course  <b>" +dashBoardCourseDetails.getCourseDetail().getTitle()+" </b>" + "from Course Detail" );

                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();
                    }
                }, getContext(), getContext().getString(R.string.remove), getContext().getString(R.string.remove_message), getContext().getString(R.string.yes), getContext().getString(R.string.no));
            }
        });

        if (dashBoardDetails.isProgrammes()) {
            mTvCourseDescriptionTitle.setText(R.string.programme_description);
            if (dashBoardDetails.isEclassVisible()) {
                mType = Constants.CHAPTER;
            }else{
                mType = Constants.TYPE_PROGRAMMES;
            }
        }else {
            mTvCourseDescriptionTitle.setText(R.string.course_description);

            if (dashBoardDetails.isEclassVisible()) {
                mType = Constants.CHAPTER;
            }else{
                mType = Constants.TYPE_COURSE;
            }
        }
      if(!TextUtils.isEmpty(dashBoardDetails.getCourseDetail().getShare_url()) && !mIsDownloaded){
          mBtShare.setVisibility(View.VISIBLE);
      }else{
          mBtShare.setVisibility(View.GONE);
      }

        if(!TextUtils.isEmpty(dashBoardDetails.getCourseDetail().getLong_description())){
            mBtFullDetails.setVisibility(View.VISIBLE);
        }else{
            mBtFullDetails.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(dashBoardDetails.getCourseDetail().getShort_description())){
            mTvCourseShortDescription.setVisibility(View.VISIBLE);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                mTvCourseShortDescription.requestFocus();

                mTvCourseShortDescription.setText(Html.fromHtml(dashBoardDetails.getCourseDetail().getShort_description(),Html.FROM_HTML_MODE_LEGACY));
            } else {
                mTvCourseShortDescription.setText(Html.fromHtml(dashBoardDetails.getCourseDetail().getShort_description()));
            }
        }else{
            mTvCourseShortDescription.setVisibility(View.GONE);
            mBtFullDetails.setVisibility(View.GONE);
        }
        mTvCourseShortDescription.setMovementMethod(LinkMovementMethod.getInstance());

        mBtFullDetails.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                if(!isFullVisible){
                    Utils.callEventLogApi("clicked <b>Full detail button</b> from <b>"+dashBoardDetails.getCourseDetail().getTitle());

                    mTvCourseFullDescription.setVisibility(View.VISIBLE);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        mTvCourseFullDescription.requestFocus();
                        mTvCourseFullDescription.setText(Html.fromHtml(dashBoardDetails.getCourseDetail().getLong_description(),Html.FROM_HTML_MODE_LEGACY));
                    } else {
                        mTvCourseFullDescription.setText(Html.fromHtml(dashBoardDetails.getCourseDetail().getLong_description()));
                    }
                    // no margins required all set in basic layout

                    mTvFullDetailBtn.setText(mContext.getString(R.string.show_less));
                }else{
                    Utils.callEventLogApi("clicked <b>Show Less button</b> from <b>"+dashBoardDetails.getCourseDetail().getTitle());

                    mTvCourseFullDescription.setVisibility(View.GONE);
                    mTvFullDetailBtn.setText(mContext.getString(R.string.full_details));
                }

                isFullVisible = !isFullVisible;
            }
        });

        mTvCourseFullDescription.setMovementMethod(LinkMovementMethod.getInstance());


        if(dashBoardDetails.isSocial_sharing() && !mIsDownloaded){
          mBtShare.setVisibility(View.VISIBLE);

      }else {
          mBtShare.setVisibility(View.GONE);
      }


        mBtShare.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {


              mBtShare.setEnabled(false);
              Utils.callEventLogApi("clicked <b>Share</b> from <b>"+dashBoardDetails.getCourseDetail().getTitle()+"</b> " +mType + " type");
              Utils.shareArticleUrl(dashBoardDetails.getCourseDetail().getShare_url(),mContext,mContext.getString(R.string.share_article));





          }
      });

    }

    private void showDonwloadIcon() {

        mFetch.getDownloads(new Func<List<Download>>() {
            @Override
            public void call(@NotNull List<Download> result) {
                mDownLoadItemList = result;
                checkForCourse();
            }
        });



    }

    private void checkForCourse() {


        if (dashBoardCourseDetails.isIs_intro_video()
                && !dashBoardCourseDetails.isEclassVisible()
                && !TextUtils.isEmpty(dashBoardCourseDetails.getCourseDetail().getVideo())
                && !TextUtils.isEmpty(dashBoardCourseDetails.getCourseDetail().getPlay_url())
                && !dashBoardCourseDetails.getCourseDetail().getPlay_url().equals("NoVideoExist")
                ) {
            if (mDownLoadItemList != null && mDownLoadItemList.size() > 0) {
                for (Download downloadTask : mDownLoadItemList) {

                    if (downloadTask.getExtras().getString(Constants.ID, "1").equals(mID)) {
                        if (downloadTask.getStatus() == Status.COMPLETED) {
                            dashBoardCourseDetails.getCourseDetail().setDownLoadedVideoAddress(downloadTask.getFile());
                            mProgressBar.setVisibility(GONE);
                            mIvDownloadGrey.setVisibility(VISIBLE);
                            mIvDownloadOrange.setVisibility(GONE);

                        } else if (downloadTask.getStatus() == Status.FAILED
                                || downloadTask.getStatus() == Status.CANCELLED
                                || downloadTask.getStatus() == Status.DELETED
                                || downloadTask.getStatus() == Status.REMOVED
                        ) {
                            dashBoardCourseDetails.getCourseDetail().setDownLoadedVideoAddress(null);

                            mProgressBar.setVisibility(GONE);
                            mIvDownloadGrey.setVisibility(GONE);
                            mIvDownloadOrange.setVisibility(VISIBLE);

                        } else {
                            dashBoardCourseDetails.getCourseDetail().setDownLoadedVideoAddress(downloadTask.getFile());

                            mProgressBar.setVisibility(VISIBLE);
                            mIvDownloadGrey.setVisibility(GONE);
                            mIvDownloadOrange.setVisibility(GONE);

                        }
                        break;
                    }
                }
            }else{
                dashBoardCourseDetails.getCourseDetail().setDownLoadedVideoAddress(null);

                mProgressBar.setVisibility(GONE);
                mIvDownloadGrey.setVisibility(GONE);
                mIvDownloadOrange.setVisibility(VISIBLE);
            }
        }else{
            dashBoardCourseDetails.getCourseDetail().setDownLoadedVideoAddress(null);
            mProgressBar.setVisibility(GONE);
            mIvDownloadGrey.setVisibility(GONE);
            mIvDownloadOrange.setVisibility(GONE);

        }



    }


    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if(hasWindowFocus){
            mBtShare.setEnabled(true);
        }
    }


    //  initialize course detail description view
    private void initViews(Context context ) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mIsTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);

        View view = inflater.inflate(R.layout.course_detail_description, this);
        mContext = context;

        mBtShare = view.findViewById(R.id.fl_btn_share);
        mBtFullDetails = view.findViewById(R.id.fl_btn_full_details);

        mIvDownloadGrey = view.findViewById(R.id.iv_download);
        mIvDownloadOrange=view.findViewById(R.id.iv_download_grey);
        mProgressBar = view.findViewById(R.id.download_bar);

        mTvCourseFullDescription = view.findViewById(R.id.tv_course_full_description);
        mTvCourseShortDescription = view.findViewById(R.id.tv_course_small_description);

        mTvFullDetailBtn = view.findViewById(R.id.tv_full_detail_btn);
        mTvCourseDescriptionTitle = view.findViewById(R.id.tv_detail_title);
        mPercentage = view.findViewById(R.id.tv_progress);
        mDownloadProgresBar=view.findViewById(R.id.progressBar);
        mIvEclassDownloadGrey=view.findViewById(R.id.eclass_grey_icon);
        mIvEclassDownloadOrange=view.findViewById(R.id.eclass_orange_icon);
        mEclassDownloadProgressBar=view.findViewById(R.id.eclass_download_bar);

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public void onDownloaded(String show) {

        mIvEclassDownloadGrey.setVisibility(GONE);
        mIvEclassDownloadOrange.setVisibility(GONE);
        mEclassDownloadProgressBar.setVisibility(GONE);

        switch(show){
            case Constants.STARTED:
                mIvEclassDownloadOrange.setVisibility(VISIBLE);
                break;
            case Constants.FINISH:
                mIvEclassDownloadGrey.setVisibility(VISIBLE);
                break;

            default:
                mEclassDownloadProgressBar.setVisibility(VISIBLE);
                break;
        }

    }


    private class startImageDownload extends AsyncTask<Void, Integer, Void> {


        File outputFile;


        @Override
        protected void onPostExecute(Void aVoid) {


                    mDownLoadTask = new DownloadFile(
                            mRequset.getFile(),
                            dashBoardCourseDetails.getCourseDetail().getTitle(),
                            dashBoardCourseDetails.getCourseDetail().getPunch_line(),
                            dashBoardCourseDetails.getCourseDetail().getMentors().get(0).getName(),
                            0,
                            null,
                            dashBoardCourseDetails.getCourseDetail().getPlay_url(),
                            dashBoardCourseDetails.getCourseDetail().getId(),
                            Constants.TYPE_COURSE,
                            dashBoardCourseDetails.getCourseDetail().getBanner(),
                            mBannerImagePath,
                            null,
                            dashBoardCourseDetails.getCourseDetail().getTitle(),
                            mBannerImagePath,
                            Constants.TYPE_COURSE

                            );

                    mRequset.setGroupId(Integer.valueOf(mID));
                    mRequset.setExtras(getExtrasForRequest(mDownLoadTask));
                    Utils.addItemToFetchList(mRequset);



            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {

                URL url = new URL(imageUrl);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection

                int fileLength = c.getContentLength();


                outputFile = new File(getApplicationContext().getFilesDir(),  mID);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e("Downlaod", "File Created");
                }


                mBannerImagePath = outputFile.getPath();


                FileOutputStream fos = getApplicationContext().openFileOutput( mID, Context.MODE_PRIVATE);//Get OutputStream for NewFile Location


                InputStream is = c.getInputStream();//Get InputStream for connection
                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {

                    fos.write(buffer, 0, len1);//Write new file

                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {


                e.printStackTrace();
                outputFile = null;
                Log.e("Downlaod", "Download Error Exception " + e.getMessage());


            }

            return null;
        }
    }


    private Extras getExtrasForRequest(DownloadFile request) {
        final MutableExtras extras = new MutableExtras();
        if(!TextUtils.isEmpty(request.getTitle())) {
            extras.putString(Constants.TITLE, request.getTitle());

        }
        if(!TextUtils.isEmpty(request.getId())) {
            extras.putString(Constants.ID, request.getId());

        }

        if(!TextUtils.isEmpty(request.getmBannerAddress())) {
            extras.putString(Constants.BANNER_PATH, request.getmBannerAddress());

        }

        if(!TextUtils.isEmpty(request.getDescription())) {
            extras.putString(Constants.PUNCH_LINE, request.getDescription());

        }

        if(!TextUtils.isEmpty(request.getMentorName())) {
            extras.putString(Constants.MENTOR_NAME, request.getMentorName());

        }

        if(!TextUtils.isEmpty(request.getVideoUrl())) {
            extras.putString(Constants.VIDEO_URL, request.getVideoUrl());

        }

        if(!TextUtils.isEmpty(request.getBanner())) {
            extras.putString(Constants.IMAGE_URL, request.getBanner());

        }

        if(!TextUtils.isEmpty(request.getVideo_address())) {
            extras.putString(Constants.VIDEO_PATH, request.getVideo_address());

        }

        if(!TextUtils.isEmpty(request.getmGroupBannerAddress())) {
            extras.putString(Constants.GROUP_IMAGE_PATH, request.getmGroupBannerAddress());

        }

        if(!TextUtils.isEmpty(request.getmGroupTitle())) {
            extras.putString(Constants.GROUP_TITLE, request.getmGroupTitle());

        }

        if(!TextUtils.isEmpty(request.getmGroupType())) {
            extras.putString(Constants.GROUP_TYPE, request.getmGroupType());

        }

        extras.putString(Constants.TYPE, request.getType());
        if (!TextUtils.isEmpty(request.getmVideoDuration())) {
            extras.putString(Constants.VIDEO_DURATION, request.getmVideoDuration());
        }

        extras.putString(Constants.DATA, new Gson().toJson(dashBoardCourseDetails));

        return extras;
    }


    private final FetchListener fetchListener = new AbstractFetchListener() {


        @Override
        public void onCompleted(@NotNull Download download) {

            if(download.getExtras().getString(Constants.ID,"0").equals(mID)) {
                dashBoardCourseDetails.getCourseDetail().setDownLoadedVideoAddress(download.getFile());

                mProgressBar.setVisibility(GONE);
                mIvDownloadGrey.setVisibility(VISIBLE);
                mIvDownloadOrange.setVisibility(GONE);
                Toast.makeText(getContext(), R.string.download_complete, Toast.LENGTH_SHORT).show();
                Utils.callEventLogApi("downloading course completed  <b>" +dashBoardCourseDetails.getCourseDetail().getTitle()+" </b>" + "from Course Detail" );


            }

        }


        @Override
        public void onError(@NotNull Download download, @NotNull Error error, @org.jetbrains.annotations.Nullable Throwable throwable) {
            if(download.getExtras().getString(Constants.ID,"0").equals(mID)) {
                dashBoardCourseDetails.getCourseDetail().setDownLoadedVideoAddress(null);

                Toast.makeText(getContext(), R.string.download_failed, Toast.LENGTH_SHORT).show();
                Utils.callEventLogApi("downloading course failed  <b>" +dashBoardCourseDetails.getCourseDetail().getTitle()+" </b>" + "from Course Detail" );
                mProgressBar.setVisibility(GONE);
                mIvDownloadOrange.setVisibility(VISIBLE);
                mIvDownloadGrey.setVisibility(GONE);
            }

        }


        @Override
        public void onProgress(@NotNull Download download, long etaInMilliSeconds, long downloadedBytesPerSecond) {

            if(download.getExtras().getString(Constants.ID,"0").equals(mID)){
                updateProgress(download);
                dashBoardCourseDetails.getCourseDetail().setDownLoadedVideoAddress(download.getFile());

            }

        }

        @Override
        public void onQueued(@NotNull Download download, boolean waitingOnNetwork) {

        }


        @Override
        public void onRemoved(@NotNull Download download) {

            if(download.getExtras().getString(Constants.ID,"0").equals(mID)) {
                dashBoardCourseDetails.getCourseDetail().setDownLoadedVideoAddress(null);

                mProgressBar.setVisibility(GONE);
                mIvDownloadOrange.setVisibility(VISIBLE);
                mIvDownloadGrey.setVisibility(GONE);

            }
        }

        @Override
        public void onDeleted(@NotNull Download download) {

            if(download.getExtras().getString(Constants.ID,"0").equals(mID)) {
                dashBoardCourseDetails.getCourseDetail().setDownLoadedVideoAddress(null);

                mProgressBar.setVisibility(GONE);
                mIvDownloadOrange.setVisibility(VISIBLE);
                mIvDownloadGrey.setVisibility(GONE);
                mDownloadProgresBar.setVisibility(GONE);
                mIsDelete=false;

            }
        }

    };

    private void updateProgress(Download progress) {

        if (mIsDelete) {
            mDownloadProgresBar.setVisibility(GONE);
            mProgressBar.setVisibility(VISIBLE);
            mProgressBar.setProgress(progress.getProgress());
            mIvDownloadOrange.setVisibility(GONE);
            mIvDownloadGrey.setVisibility(GONE);
        }
        else
        {
            mFetch.delete(mRequset.getId());

            mDownloadProgresBar.setVisibility(GONE);
            mProgressBar.setVisibility(GONE);
            mIvDownloadOrange.setVisibility(VISIBLE);
            mIvDownloadGrey.setVisibility(GONE);
        }

    }




//    private Thread getProgressBarThread() {
//        return mProgreeBarThread = new Thread(new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                while (mProgressBarPosition < 100) {
//                    mProgressBarPosition += 1;
//
//                    mProgressBarHandler.post(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            // TODO Auto-generated method stub
//                            mProgressBar.setProgress(mProgressBarPosition);
//                            mShowProgressBarDefinite = false;
//
//                        }
//                    });
//                    try {
//                        // Sleep for 100 milliseconds.
//                        // Just to display the progress slowly
//                        Thread.sleep(100); //thread will take approx 10 seconds to finish
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        });
//    }


}
