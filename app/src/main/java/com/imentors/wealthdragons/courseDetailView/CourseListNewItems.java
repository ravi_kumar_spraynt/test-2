package com.imentors.wealthdragons.courseDetailView;

import android.content.Context;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.CourseNewItemsAdapter;
import com.imentors.wealthdragons.models.CourseItemListModel;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.List;

public class CourseListNewItems extends LinearLayout {


    private Context mContext;
    private WealthDragonTextView mTvNewItemHeading;
    private RecyclerView mItemListRecyclerView;
    private CourseNewItemsAdapter mCourseNewItemsAdapter = null;
    private View mLineBelowHeading;



    public CourseListNewItems(Context context) {
        super(context);
        mContext = context;
    }

    public CourseListNewItems(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public CourseListNewItems(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public CourseListNewItems(Context context, List<CourseItemListModel> newItemList, String listTitle, String newItemType) {
        super(context);
        initViews(context);
        bindView(newItemList,listTitle,newItemType);
    }

    //  set data in view
    private void bindView(List<CourseItemListModel> newItemList, String listTitle, String newItemType) {

        if(TextUtils.isEmpty(listTitle)){
            mTvNewItemHeading.setVisibility(GONE);
            mLineBelowHeading.setVisibility(GONE);

        }else{
            mLineBelowHeading.setVisibility(VISIBLE);
            mTvNewItemHeading.setVisibility(VISIBLE);
            mTvNewItemHeading.setText(listTitle);
        }

        mCourseNewItemsAdapter = new CourseNewItemsAdapter(newItemList,newItemType);

        mItemListRecyclerView.setAdapter(mCourseNewItemsAdapter);


    }

    //  initialize course detail description view
    private void initViews(Context context ) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.course_detail_list, this);
        mContext = context;


        mTvNewItemHeading = view.findViewById(R.id.tv_detail_title);
        mItemListRecyclerView = view.findViewById(R.id.listView);
        mLineBelowHeading = view.findViewById(R.id.view_below_heading);
        mItemListRecyclerView.setLayoutManager (new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        mItemListRecyclerView.setNestedScrollingEnabled(false);
    }



}
