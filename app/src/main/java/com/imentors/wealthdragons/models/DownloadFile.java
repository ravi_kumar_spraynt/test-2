package com.imentors.wealthdragons.models;

import com.imentors.wealthdragons.utils.Constants;

import java.io.Serializable;

public class DownloadFile implements Serializable{


   private String video_address;
   private String title;
   private String description;
   private String mentorName;


   private long seekTime;
   private String fileSize;
   private String videoUrl;
   private String id;
   private String type;
   private String banner;
   private String mBannerAddress;
   private String mVideoDuration;
   private String mGroupTitle;
   private String mGroupBannerAddress;
   private String mGroupType;

   public DownloadFile(
            String video_address,
            String title,
            String description,
            String mentorName,
            long seekTime,
            String fileSize,
            String videoUrl,
            String id,
            String type,
            String banner,
            String bannerAddress,
            String videoDuration,
            String groupTitle,
            String groupBannerAddress,
            String groupType
   ){
      this.video_address = video_address;
      this.description = description;
      this.id= id;
      this.type= type;
      this.fileSize = fileSize;
      this.mentorName = mentorName;
      this.videoUrl= videoUrl;
      this.seekTime= seekTime;
      this.title = title;
      this.banner = banner;
      this.mBannerAddress = bannerAddress;
      this.mVideoDuration=videoDuration;
      this.mGroupTitle = groupTitle;
      this.mGroupBannerAddress = groupBannerAddress;
      this.mGroupType = groupType;
   }

   public String getVideo_address() {
      return video_address;
   }

   public String getTitle() {
      return title;
   }

   public String getDescription() {
      return description;
   }

   public String getMentorName() {
      return mentorName;
   }

   public long getSeekTime() {
      return seekTime;
   }

   public String getFileSize() {
      return fileSize;
   }

   public String getVideoUrl() {
      return videoUrl;
   }

   public String getBanner() {
      return banner;
   }

   public String getType() {
      return type;
   }

   public String getId() {
      return id;
   }

   public void setSeekTime(long seekTime) {
      this.seekTime = seekTime;
   }



    public void setVideoAddress(String address) {
        video_address = address;
    }


   public String getmBannerAddress() {
      return mBannerAddress;
   }

   public String getmVideoDuration() {
      return mVideoDuration;
   }

   public String getmGroupBannerAddress() {
      return mGroupBannerAddress;
   }

   public String getmGroupTitle() {
      return mGroupTitle;
   }

   public String getmGroupType() {
      return mGroupType;
   }
}


