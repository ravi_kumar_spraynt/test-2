package com.imentors.wealthdragons.models;

import java.io.Serializable;

public class RatingDetail implements Serializable {

    private float rating;
    private int rating_persons;
    private int one_percent;
    private int two_percent;
    private int three_percent;
    private int four_percent;
    private int five_percent;
    private int one_count;
    private int two_count;
    private int three_count;
    private int four_count;
    private int five_count;
    private int current_page;
    private int next_page;
    private int per_page_items;
    private int total_items;
    private int total_page;

    public int getCurrent_page() {
        return current_page;
    }

    public int getNext_page() {
        return next_page;
    }

    public int getPer_page_items() {
        return per_page_items;
    }

    public int getTotal_items() {
        return total_items;
    }

    public int getTotal_page() {
        return total_page;
    }





    public float getRating() {
        return rating;
    }

    public int getRating_persons() {
        return rating_persons;
    }

    public int getOne_percent() {
        return one_percent;
    }

    public int getTwo_percent() {
        return two_percent;
    }

    public int getThree_percent() {
        return three_percent;
    }

    public int getFour_percent() {
        return four_percent;
    }

    public int getFive_percent() {
        return five_percent;
    }

    public int getOne_count() {
        return one_count;
    }

    public int getTwo_count() {
        return two_count;
    }

    public int getThree_count() {
        return three_count;
    }

    public int getFour_count() {
        return four_count;
    }

    public int getFive_count() {
        return five_count;
    }


}
