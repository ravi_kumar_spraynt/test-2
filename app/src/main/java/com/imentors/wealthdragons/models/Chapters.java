package com.imentors.wealthdragons.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.tonyodev.fetch2.Request;

import java.io.Serializable;
import java.util.List;

public class Chapters implements Serializable {

    private String title;
    private List<EClasses> e_classes;

    public String getTitle() {
        return title;
    }

    public List<EClasses> getE_classes() {
        return e_classes;
    }

    public String isAllChaptersDownloaded() {

        String completeDownloadedStatus = Constants.FINISH;

        for(int i=0; i<e_classes.size();i++){
            EClasses eClasses = e_classes.get(i);
            if(eClasses.getType().equals(Constants.TYPE_DETAIL_VIDEO)) {
                if (eClasses.getDownloadedStatus().equals(Constants.PENDING)) {
                    completeDownloadedStatus = eClasses.getDownloadedStatus();
                    break;
                } else if (eClasses.getDownloadedStatus().equals(Constants.STARTED)) {
                    completeDownloadedStatus = eClasses.getDownloadedStatus();
                }
            }
        }


        return completeDownloadedStatus;

    }

    public class EClasses implements Serializable{

        private String id;
        private String title;
        private String description;
        private String duration;
        private String preview_video;
        private String file;
        private String chapter_type_id;
        private String quiz_list_id;
        private String free_paid;
        private String icon;
        private String play_url;
        private String type;
        private String subscription;
        private boolean action;
        private String seek_time;
        private String start_time;
        private Request mReuest;

        public String getDownloadedVideoAddress() {
            return downloadedVideoAddress;
        }

        public void setDownloadedVideoAddress(String downloadedVideoAddress) {
            this.downloadedVideoAddress = downloadedVideoAddress;
        }

        private String downloadedVideoAddress;
        private String downloadedStatus = Constants.STARTED;
        private boolean isDownloadedStopped = false;

        private boolean playAudioActive = false;


        public String getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public String getDuration() {
            return duration;
        }

        public String getPreview_video() {
            return preview_video;
        }

        public String getFile() {
            return file;
        }

        public String getChapter_type_id() {
            return chapter_type_id;
        }

        public String getQuiz_list_id() {
            return quiz_list_id;
        }

        public String getFree_paid() {
            return free_paid;
        }

        public String getIcon() {
            return icon;
        }

        public String getPlay_url() {
            return play_url;
        }

        public String getType() {
            return type;
        }

        public String getSubscription() {
            return subscription;
        }

        public boolean isAction() {
            return action;
        }

        public boolean isPlayAudioActive(){
            return playAudioActive;
        }

        public void setIsPlayActive(boolean isPlayAudioActive){
            playAudioActive = isPlayAudioActive;
        }


        public String getSeek_time() {
            return seek_time;
        }



        public void setSeek_time(String seek_time) {
            this.seek_time = seek_time;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String seek_time) {
            this.start_time = seek_time;
        }

        public Request getmReuest() {
            return new Request(getPlay_url(), WealthDragonsOnlineApplication.sharedInstance().getApplicationContext().getFilesDir()+ "/video/"  + getId());
        }

        public String getDownloadedStatus() {
            if(TextUtils.isEmpty(downloadedStatus)){
                return Constants.STARTED;
            }
            return downloadedStatus;
        }

        public void setDownloadedStatus(String downloadedStatus) {
            this.downloadedStatus = downloadedStatus;
        }

        public boolean isDownloadedStopped() {
            return isDownloadedStopped;
        }

        public void setDownloadedStopped(boolean downloadedStopped) {
            isDownloadedStopped = downloadedStopped;
        }
    }




}
