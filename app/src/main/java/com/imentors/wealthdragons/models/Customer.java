package com.imentors.wealthdragons.models;

import java.io.Serializable;

public class Customer implements Serializable {



    private UserDetail  Customer;

    public UserDetail getCustomer() {
        return Customer;
    }


    public static class  UserDetail{
        private String first_name;
        private String last_name;
        private String email;
        private String unique_id;
        private String country_code;
        private String mobile;
        private String chat_image;

        public String getFirst_name() {
            return first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public String getEmail() {
            return email;
        }

        public String getUnique_id() {
            return unique_id;
        }

        public String getCountry_code() {
            return country_code;
        }

        public String getMobile() {
            return mobile;
        }

        public String getChat_image() {
            return chat_image;
        }
    }


}
