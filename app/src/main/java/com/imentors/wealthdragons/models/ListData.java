package com.imentors.wealthdragons.models;

public class ListData {

    int image;
    String name;
    String price;

    public ListData(int image, String name, String price )
    {
       this.image=image;
       this.name=name;
       this.price=price;
    }

    public int getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }
}
