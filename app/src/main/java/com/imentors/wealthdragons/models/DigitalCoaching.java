package com.imentors.wealthdragons.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.tonyodev.fetch2.Request;

import java.io.Serializable;

public class DigitalCoaching implements Parcelable ,Serializable {

    private String title;
    private String banner;
    private String description;
    private String id;
    private String mentor_id;
    private String video_url;
    private String video_duration;
    private String video_seektime;
    private String free_paid;
    private String free_paid_text;
    private Request mReuest;
    private boolean isDownloadedStopped = false;
    private String mDownloadBanner;
    private String mSavedVideoAddress;


    public void setBanner(String banner) {
        this.banner = banner;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMentor_id(String mentor_id) {
        this.mentor_id = mentor_id;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public DigitalCoaching(String title, String id, String mentor_id, String video_url, String mDownloadBanner, String mSavedVideoAddress,String freePaid,String freePaidText){
        this.title = title;
        this.id = id;
        this.mentor_id = mentor_id;
        this.video_url = video_url;
        this.mDownloadBanner = mDownloadBanner;
        this.mSavedVideoAddress = mSavedVideoAddress;
        this.free_paid=freePaid;
        this.free_paid_text=freePaidText;
    }

    protected DigitalCoaching(Parcel in) {

        this.title = in.readString();
        this.banner = in.readString();
        this.description = in.readString();
        this.id = in.readString();
        this.mentor_id = in.readString();
        this.video_url = in.readString();
        this.video_duration = in.readString();
        this.video_seektime = in.readString();
        this.free_paid = in.readString();
        this.free_paid_text = in.readString();
        this.mDownloadBanner = in.readString();
        this.mSavedVideoAddress = in.readString();


    }

    public static final Creator<DigitalCoaching> CREATOR = new Creator<DigitalCoaching>() {
        @Override
        public DigitalCoaching createFromParcel(Parcel in) {
            return new DigitalCoaching(in);
        }

        @Override
        public DigitalCoaching[] newArray(int size) {
            return new DigitalCoaching[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(this.title);
        dest.writeString(this.banner);
        dest.writeString(this.description);
        dest.writeString(this.id);
        dest.writeString(this.mentor_id);
        dest.writeString(this.video_url);
        dest.writeString(this.video_duration);
        dest.writeString(this.video_seektime);
        dest.writeString(this.free_paid);
        dest.writeString(this.free_paid_text);
        dest.writeString(this.mDownloadBanner);
        dest.writeString(this.mSavedVideoAddress);


    }


    public String getBanner() {
        return banner;
    }

    public String getId() {
        return id;
    }


    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getMentor_id() {
        return mentor_id;
    }

    public String getVideo_url() {
        return video_url;
    }

    public String getVideo_duration() {
        return video_duration;
    }

    public String getFreePaidText(){
        return free_paid_text;
    }

    public String getVideo_seektime() {
        return video_seektime;
    }


    public String getFree_paid(){
        return free_paid;
    }

    public Request getmReuest() {
        return new Request(getVideo_url(), WealthDragonsOnlineApplication.sharedInstance().getApplicationContext().getFilesDir()+ "/video/"  + getId());
    }

    public boolean isDownloadedStopped() {
        return isDownloadedStopped;
    }

    public void setDownloadedStopped(boolean downloadedStopped) {
        isDownloadedStopped = downloadedStopped;
    }

    public String getmDownloadBanner() {
        return mDownloadBanner;
    }

    public String getmSavedVideoAddress() {
        return mSavedVideoAddress;
    }

    public void setmSavedVideoAddress(String mSavedVideoAddress) {
        this.mSavedVideoAddress = mSavedVideoAddress;
    }

    public void setmDownloadBanner(String mDownloadBanner) {
        this.mDownloadBanner = mDownloadBanner;
    }

}
