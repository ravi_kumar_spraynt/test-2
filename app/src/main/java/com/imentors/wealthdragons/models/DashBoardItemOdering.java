package com.imentors.wealthdragons.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.imentors.wealthdragons.api.Api;

import java.io.Serializable;
import java.util.List;

public class DashBoardItemOdering implements Serializable{

    private String key;
    private String title;
    private String icon;
    private int limit;
    private String id;
    private String see_all;
    private String cat_id;
    private String pre_fix;
    private int total_items;
    private int total_page;
    private int current_page;
    private int next_page;
    private int per_page_items;
    private String is_subscribed;



    public String getKey() {
        return key;
    }

    public String getTitle() {
        return title;
    }

    public String getLayout() {
        return layout;
    }

    private String layout;

    public String getIcon() {
        return icon;
    }


    public int getLimit() {
        return limit;
    }

    public String getId() {
        return id;
    }

    public String getSee_all() {
        return see_all;
    }


    public String getPre_fix() {
        return pre_fix;
    }

    public String getCat_id() {
        if(cat_id != null){
            return cat_id;
        }else{
            return null;
        }
    }

    public int getTotal_items() {
        return total_items;
    }

    public int getTotal_page() {
        return total_page;
    }

    public int getCurrent_page() {
        return current_page;
    }

    public int getNext_page() {
        return next_page;
    }

    public int getPer_page_items() {
        return per_page_items;
    }

    public String getIs_subscribed() {
        return is_subscribed;
    }
}
