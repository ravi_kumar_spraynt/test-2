package com.imentors.wealthdragons.models;

import java.io.Serializable;

public class CountryCode implements Serializable {



        private String id;
        private String iso;
        private String name;
        private String nicename;
        private String iso3;
        private String numcode;
        private String phonecode;

    public String getId() {
        return id;
    }

    public String getIso() {
        return iso;
    }

    public String getName() {
        return name;
    }

    public String getNicename() {
        return nicename;
    }

    public String getIso3() {
        return iso3;
    }

    public String getNumcode() {
        return numcode;
    }

    public String getPhonecode() {
        return phonecode;
    }
}
