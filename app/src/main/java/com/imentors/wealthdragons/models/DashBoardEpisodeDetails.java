package com.imentors.wealthdragons.models;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.io.Serializable;
import java.util.List;

public class DashBoardEpisodeDetails implements Serializable{


   private boolean show_ratings;
   private boolean show_course_name;

   private boolean show_mentor_name;
   private boolean show_mentor_image;
   private boolean show_mentor_tagline;
   private boolean show_view_detail_btn;
   private boolean show_free_video_name;
   private boolean show_free_video_mentor_name;

   private boolean social_sharing;
   private boolean wishlist;
   private String is_subscribed;
   private DashBoardPlanSetting PlanSetting;
   private VideoDetail VideoDetail;
   private VideoDetail EpisodeDetail;

   private List<Video> RelatedVideos;
   private DashBoardEpisodeDetails.Given Given;
   private DashBoardEpisodeDetails.Taken Taken;
   private RelatedVideos Videos;
   private String setting;
   private List<VideoUrl> RelatedVideosUrls;

   public DashBoardEpisodeDetails.Given getGiven() {
      return Given;
   }

   public DashBoardEpisodeDetails.Taken getTaken() {
      return Taken;
   }

   public DashBoardEpisodeDetails.VideoDetail getVideoDetail() {
      return VideoDetail;
   }

   public List<Video> getRelatedVideos() {
      return RelatedVideos;
   }



   public DashBoardPlanSetting getPlanSetting() {
      return PlanSetting;
   }

   public boolean isShowRatings() {
      return show_ratings;
   }

   public boolean isShowCourseName() {
      return show_course_name;
   }

   public boolean isShowMentorName() {
      return show_mentor_name;
   }

   public boolean isShowMentorImage() {
      return show_mentor_image;
   }

   public boolean isShowMentorTagline() {
      return show_mentor_tagline;
   }

   public boolean isShowViewDetailBtn() {
      return show_view_detail_btn;
   }

   public boolean isMentorLayoutVisible(){

       return isShowMentorImage() || isShowMentorName() || isShowMentorTagline();
   }


   public boolean isSocial_sharing() {
      return social_sharing;
   }

   public boolean isWishlist() {
      return wishlist;
   }

   public String getIs_subscribed() {
      return is_subscribed;
   }

   public DashBoardEpisodeDetails.RelatedVideos getVideos() {
      return Videos;
   }

   public DashBoardEpisodeDetails.VideoDetail getEpisodeDetail() {
      return EpisodeDetail;
   }

   public String getSetting() {
      return setting;
   }

   public boolean isAutoPlay(){
       return getSetting().equals(WealthDragonsOnlineApplication.sharedInstance().getString(R.string.auto_play));
   }

   public List<VideoUrl> getRelatedVideosUrls() {
      return RelatedVideosUrls;
   }

   public static class VideoDetail{

      private String name;
      private String title;
      private String mentor_id;
      private String taste_preference_id;
      private String alis;
      private String video;
      private String play_url;
      private String mobile_big_banner;
      private String mobile_small_banner;
      private String taste_prefrence;
      private String mentor_name;
      private String first_mentor_name;
      private String other_mentor_name;
      private String product_id;
      private String subscription;
      private String mobile_small_banner_status;
      private String mobile_big_banner_status;
      private String duration;
      private String description;
      private String free_paid;
      private String id;
      private String tag_line;
      private String punch_line;
      private String slug;
      private String share_url;
      private String rating;
      private int rating_persons;
      private String video_ordering;
      private String video_id;
      private String seek_time;
      private String low_qty_banner;


      public String getName() {
         return name;
      }

      public String getTitle() {
         return title;
      }

      public String getMentor_id() {
         return mentor_id;
      }

      public String getTaste_preference_id() {
         return taste_preference_id;
      }

      public String getAlis() {
         return alis;
      }

      public String getVideo() {
         return video;
      }

      public String getPlay_url() {
         return play_url;
      }

      public String getMobile_big_banner() {
         return mobile_big_banner;
      }

      public String getMobile_small_banner() {
         return mobile_small_banner;
      }

      public String getTaste_prefrence() {
         return taste_prefrence;
      }

      public String getMentor_name() {
         return mentor_name;
      }

      public String getFirst_mentor_name() {
         return first_mentor_name;
      }

      public String getOther_mentor_name() {
         return other_mentor_name;
      }

      public String getProduct_id() {
         return product_id;
      }

      public String getSubscription() {
         return subscription;
      }

      public String getMobile_small_banner_status() {
         return mobile_small_banner_status;
      }

      public String getMobile_big_banner_status() {
         return mobile_big_banner_status;
      }

      public String getDuration() {
         return duration;
      }

      public String getDescription() {

         description = description.replace("\n", "<br>");
         description = description.replace("\t", "  ");
         return description;
      }

      public String getFree_paid() {
         return free_paid;
      }

      public String getId() {
         return id;
      }

      public String getTag_line() {
         return tag_line;
      }

      public String getPunch_line() {
         return punch_line;
      }

      public String getSlug() {
         return slug;
      }

      public String getShare_url() {
         return share_url;
      }

      public String getRating() {

         return rating;
      }

      public int getRating_persons() {
         return rating_persons;
      }

      public String getVideo_ordering() {
         return video_ordering;
      }

      public String getVideo_id() {
         return video_id;
      }

      public String getSeek_time() {
         return seek_time;
      }

      public void setSeek_time(String seek_time) {
         this.seek_time = seek_time;
      }

      public String getLow_qty_banner() {
         return low_qty_banner;
      }
   }

   public static class RelatedVideos{
      private int total_items;
      private int total_page;
      private int current_page;
      private int next_page;
      private int per_page_items;
      private List<Video> videos;

      public int getTotal_items() {
         return total_items;
      }

      public int getTotal_page() {
         return total_page;
      }

      public int getCurrent_page() {
         return current_page;
      }

      public int getNext_page() {
         return next_page;
      }

      public int getPer_page_items() {
         return per_page_items;
      }

      public List<Video> getVideos() {
         return videos;
      }

   }


   public static class VideoUrl{
      private String id;
      private String title;
      private String status;
      private String video;
      private String play_url;
      private String duration;

      public String getId() {
         return id;
      }

      public String getTitle() {
         return title;
      }

      public String getStatus() {
         return status;
      }

      public String getVideo() {
         return video;
      }

      public String getPlay_url() {
         return play_url;
      }

      public String getDuration() {
         return duration;
      }
   }



   public static class Given{
       private CourseDetail.RelatedCourses Course;
       private CourseDetail.RelatedProgramme Programme;

      public CourseDetail.RelatedCourses getCourse() {
         return Course;
      }

      public CourseDetail.RelatedProgramme getProgramme() {
         return Programme;
      }
   }

    public static class Taken{
        private CourseDetail.RelatedCourses Course;
        private CourseDetail.RelatedProgramme Programme;

       public CourseDetail.RelatedCourses getCourse() {
          return Course;
       }

       public CourseDetail.RelatedProgramme getProgramme() {
          return Programme;
       }
    }

}


