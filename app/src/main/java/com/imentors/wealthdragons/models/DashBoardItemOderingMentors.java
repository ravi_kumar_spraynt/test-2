package com.imentors.wealthdragons.models;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.util.List;

public class DashBoardItemOderingMentors extends DashBoardItemOdering{


    private String banner;
    private List<com.imentors.wealthdragons.models.SubCategory> SubCategory;
    private List<Mentors> Mentors;
    private List<Mentors> experts;


    // for dashboard
    public List<Mentors> getItems() {
        return items;
    }

    private List<Mentors> items;


    // dashboard mentor tab
    public List<com.imentors.wealthdragons.models.Mentors> getMentors() {
        return Mentors;
    }

    public String getBanner() {
        return banner;
    }


    public List<com.imentors.wealthdragons.models.SubCategory> getSubCategory() {
        return SubCategory;
    }

    public List<com.imentors.wealthdragons.models.Mentors> getExperts() {
        return experts;
    }
}
