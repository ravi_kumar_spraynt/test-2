package com.imentors.wealthdragons.models;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;

import java.io.Serializable;
import java.util.List;

public class WelcomeModel implements Serializable{

    private List<Slider> Slider;
    private UserGroup UserGroup;

    public List<com.imentors.wealthdragons.models.Slider> getSlider() {
        return Slider;
    }

    public WelcomeModel.UserGroup getUserGroup() {
        return UserGroup;
    }

    public int getNumberOfFragmentsInSlider(){
        return getSlider().size();
    }


    public int getTotalNumberOfFragments(){

        // 1 is automatically added because forgot password fragment cant be removed in any condition
        int sizeOfFragment = getNumberOfFragmentsInSlider() + 1;


        // check for sign In Fragment Condition
        if(getUserGroup().isSignInVisible()){
            sizeOfFragment += 1;
        }

        //check for sign up fragment condition
        if(getUserGroup().isSignUpButtonVisible()){
            sizeOfFragment += 1;
        }


        return sizeOfFragment;
    }


    public Slider getSliderDataPerPosition(int position){
        return getSlider().get(position);
    }



    public class UserGroup implements Serializable{
        private String sign_up_btn_text;
        private String fb_btn_text;
        private int login_text;

        public String getFbBtnText() {
            return fb_btn_text;
        }

        public String getSignUpBtnText() {
            return sign_up_btn_text;
        }

        public boolean isFbButtonVisible(){
            return !TextUtils.isEmpty(getFbBtnText());

        }

        public boolean isSignUpButtonVisible(){
           return !TextUtils.isEmpty(getSignUpBtnText());
        }

        public boolean isSignInVisible(){
            return login_text != 0;

        }
    }

}
