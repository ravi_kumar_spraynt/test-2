package com.imentors.wealthdragons.models;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.io.Serializable;
import java.util.List;

public class DashBoardMentors implements Serializable{


   private List<DashBoardItemOderingMentors> All_Mentors;

   private boolean show_mentor_mentor_name;
   private boolean show_mentor_mentor_tagline;
   private boolean wishlist;





   public boolean isShowMentorMentorTagline() {
      return show_mentor_mentor_tagline;
   }

   public boolean isShowMentorMentorName() {
      return show_mentor_mentor_name;
   }




   // layout type mentor
   public boolean isMentorTypeLayoutVisible(){
       return isShowMentorMentorName() || isShowMentorMentorTagline();
   }


   public List<DashBoardItemOderingMentors> getAll_Mentors() {
      return All_Mentors;
   }

   public boolean isWishlist() {
      return wishlist;
   }
}


