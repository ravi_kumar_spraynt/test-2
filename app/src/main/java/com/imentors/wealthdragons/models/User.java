package com.imentors.wealthdragons.models;

import android.text.TextUtils;

import java.io.Serializable;

public class User implements Serializable {

    private String username;
    private String email;
    private String password;
    private String user_id;
    private String servicecode;
    private String service_code;
    private String verification;
    private String user_group_id;
    private String reg_date;
    private boolean datshboard;
    private String success;
    private String userimg;
    private String image;
    private String subscription_status;
    private String user_token;
    private String country;




    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getUserId() {
        return user_id;
    }

    public String getServiceCode() {
        return servicecode;
    }

    public String getVerification() {
        return verification;
    }

    public String getUserGroupId() {
        return user_group_id;
    }

    public String getRegDate() {
        return reg_date;
    }

    public boolean isDashboard() {
        return datshboard;
    }

    public String getSuccess() {
        return success;
    }

    public String getUsername() {
        return username;
    }

    public String getUserimg() {
        return userimg;
    }

    public String getUserToken() {
        return user_token;
    }

    public String getCountry() {
        return country;
    }

    public String getService_code() {
        if(TextUtils.isEmpty(servicecode)){
            return service_code;
        }else{
            return servicecode;
        }

    }

    public void setDatshboard(boolean datshboard) {
        this.datshboard = datshboard;
    }
}
