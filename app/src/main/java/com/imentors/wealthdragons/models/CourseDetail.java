package com.imentors.wealthdragons.models;

import android.text.TextUtils;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.io.Serializable;
import java.util.List;

public class CourseDetail implements Serializable {

    private String id;
    private String title;
    private String mentor_id;
    private String punch_line;
    private String taste_preference_id;
    private String push_live;
    private String free_paid;
    private String alis;
    private String banner;
    private String taste_preference;
    private String video;
    private String play_url;
    private String banner_status;
    private String cost;
    private String discount_cost;
    private String short_description;
    private String long_description;
    private String pre_booking_date;
    private String pre_booking_cost;
    private String final_cost;
    private String share_url;
    private String percentage;
    private String rating;
    private String rating_persons;
    private String purchase;
    private String wishlist;
    private String subscription;
    private String product_id;
    private String reference_name;
    private String subscribe_msg;
    private String upcoming;
    private String big_banner;
    private String seek_time;
    private String no_of_enrollment;
    private String currency;
    private String dollar_cost;
    private String dollar_discount_cost;
    private String pre_booking_dollar_cost;
    private String item_orignal_cost;
    private String item_discounted_cost;
    private String item_pre_booking_date;
    private String item_pre_booking_status;
    private String course_intro_id;
    private String dc_button_show;
    private String dc_button_title;
    private String digi_coaching_screen;

    private List<Chapters> Chapters;
    private List<MentorDetail> Mentors;
    private RatingDetail RatingDetail;
    private RelatedCourses Related_Courses;
    private RelatedProgramme Related_Programme;
    private RelatedCourses Courses;
    private RelatedCourses FeaturedCourse;
    private DashBoardWishList.RelatedProgramme FeaturedProgramme;

    private List<Review> ReviewList;
    private boolean subscription_btn;
    private String low_qty_banner;
    private String low_qty_big_banner;
    private String category_label;
    private List<CourseItemListModel> CourseLearning;
    private List<CourseItemListModel> CourseRequirement;
    private List<CourseItemListModel> CourseTargetAudience;
    private List<CourseItemListModel> CourseReagon;
    private List<CourseItemListModel> QuestionAnswers;
    private DashBoardItemOderingVideo Video;
    private DashBoardItemOderingMentors Expert;
    private String downLoadedVideoAddress;


    public String getDc_button_show() {
        return dc_button_show;
    }

    public String getDc_button_title() {
        return dc_button_title;
    }


    public String getCourse_intro_id() {
        return course_intro_id;
    }

    public String getDollar_cost() {
        return dollar_cost;
    }

    public String getPre_booking_dollar_cost() {
        return pre_booking_dollar_cost;
    }

    public String getDollar_discount_cost() {
        return dollar_discount_cost;
    }

    public String getItem_orignal_cost() {
        return item_orignal_cost;
    }

    public String getItem_discounted_cost() {
        return item_discounted_cost;
    }

    public String getItem_pre_booking_date() {
        return item_pre_booking_date;
    }

    public String getItem_pre_booking_status() {
        return item_pre_booking_status;
    }

    public String getCurrency() {
        return currency;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getMentor_id() {
        return mentor_id;
    }

    public String getPunch_line() {
        return punch_line;
    }

    public String getTaste_preference_id() {
        return taste_preference_id;
    }

    public String getPush_live() {
        return push_live;
    }

    public String getFree_paid() {
        return free_paid;
    }

    public String getAlis() {
        return alis;
    }

    public String getBanner() {
        return banner;
    }

    public String getTaste_preference() {
        return taste_preference;
    }

    public String getVideo() {
        return video;
    }

    public String getPlay_url() {
        return play_url;
    }

    public String getBanner_status() {
        return banner_status;
    }

    public String getCost() {
        return cost;
    }

    public String getDiscount_cost() {
        return discount_cost;
    }

    public String getShort_description() {
        short_description = short_description.replace("\n", "<br>");
        short_description = short_description.replace("\t", "  ");

        return short_description;
    }

    public String getLong_description() {


        long_description = long_description.replace("\n", "<br>");
        long_description = long_description.replace("\t", "  ");

        return long_description;
    }

    public String getPre_booking_date() {
        return pre_booking_date;
    }

    public String getPre_booking_cost() {
        return pre_booking_cost;
    }

    public String getFinal_cost() {
        return final_cost;
    }

    public String getShare_url() {
        return share_url;
    }

    public String getPercentage() {
        return percentage;
    }

    public String getRating() {
        return rating;
    }

    public String getRating_persons() {
        return rating_persons;
    }

    public String getPurchase() {
        return purchase;
    }

    public String getWishlist() {
        return wishlist;
    }

    public String getSubscription() {
        return subscription;
    }


    public String getProduct_id() {
        return product_id;
    }

    public String getReference_name() {
        return reference_name;
    }

    public String getSubscribe_msg() {
        return subscribe_msg;
    }


    public String getUpcoming() {
        return upcoming;
    }

    public List<com.imentors.wealthdragons.models.Chapters> getChapters() {
        return Chapters;
    }

    public List<MentorDetail> getMentors() {
        return Mentors;
    }

    public com.imentors.wealthdragons.models.RatingDetail getRatingDetail() {
        return RatingDetail;
    }


    public RelatedProgramme getRelated_Programme() {
        return Related_Programme;
    }

    public List<Review> getReviewList() {
        return ReviewList;
    }


    public boolean isSubscription_btn() {
        return subscription_btn;
    }

    public RelatedCourses getRelated_Courses() {
        return Related_Courses;
    }

    public RelatedCourses getCourses() {
        return Courses;
    }

    public RelatedCourses getFeaturedCourse() {
        return FeaturedCourse;
    }

    public DashBoardWishList.RelatedProgramme getFeaturedProgramme() {
        return FeaturedProgramme;
    }

    public String getBig_banner() {
        return big_banner;
    }

    public String getSeek_time() {
        return seek_time;
    }

    public void setSeek_time(String seek_time) {
        this.seek_time = seek_time;
    }

    public String getLow_qty_banner() {
        return low_qty_banner;
    }

    public String getLow_qty_big_banner() {
        return low_qty_big_banner;
    }

    public String getCategory_label() {
        return category_label;
    }



    public String getNo_of_enrollment() {
        return no_of_enrollment;
    }

    public List<CourseItemListModel> getCourseLearning() {
        return CourseLearning;
    }

    public List<CourseItemListModel> getCourseRequirement() {
        return CourseRequirement;
    }

    public List<CourseItemListModel> getCourseTargetAudience() {
        return CourseTargetAudience;
    }

    public List<CourseItemListModel> getCourseReagon() {
        return CourseReagon;
    }

    public List<CourseItemListModel> getQuestionAnswers() {
        return QuestionAnswers;
    }

    public String getDigi_coaching_screen() {
        return digi_coaching_screen;
    }

    public String getDownLoadedVideoAddress() {
        return downLoadedVideoAddress;
    }

    public void setDownLoadedVideoAddress(String downLoadedVideoAddress) {
        this.downLoadedVideoAddress = downLoadedVideoAddress;
    }

    public class RelatedCourses implements Serializable{
        private int total_items;
        private int total_page;
        private int current_page;
        private int next_page;
        private int per_page_items;
        private List<Article> courses;
        private List<Article> items;


        public int getTotal_items() {
            return total_items;
        }

        public int getTotal_page() {
            return total_page;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public int getNext_page() {
            return next_page;
        }

        public int getPer_page_items() {
            return per_page_items;
        }

        public List<Article> getCourses() {
            return courses;
        }

        public String getTitle(){


            return  WealthDragonsOnlineApplication.sharedInstance().getString(R.string.featured_courses);
        }

        public List<Article> getItems() {
            return items;
        }
    }

    public class RelatedProgramme implements Serializable{
        private int total_items;
        private int total_page;
        private int current_page;
        private int next_page;
        private int per_page_items;
        private List<Article> programmes;

        public int getTotal_items() {
            return total_items;
        }

        public int getTotal_page() {
            return total_page;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public int getNext_page() {
            return next_page;
        }

        public int getPer_page_items() {
            return per_page_items;
        }

        public List<Article> getProgrammes() {
            return programmes;
        }

        public String getTitle(){
            return  WealthDragonsOnlineApplication.sharedInstance().getString(R.string.buy_complete_programme);
        }

    }


    public DashBoardItemOderingVideo getWishlistVideos(){
        return Video;
    }

    public DashBoardItemOderingMentors getExpert() {
        return Expert;
    }


    public boolean isDiscountAvailable(){

        return !TextUtils.isEmpty(getItem_orignal_cost()) && !TextUtils.isEmpty(getFinal_cost()) && !TextUtils.equals(getFinal_cost(), getItem_orignal_cost());
    }
}
