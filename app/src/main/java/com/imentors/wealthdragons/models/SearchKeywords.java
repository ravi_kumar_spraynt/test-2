package com.imentors.wealthdragons.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

public class SearchKeywords implements Serializable {

    private List<String> key_words;

    public List<String> getKey_words() {
        return key_words;
    }

    private int total_items;

    private int total_page;


    private int current_page;


    private int next_page;

    private int per_page_items;

    public int getTotal_items() {
        return total_items;
    }

    public int getTotal_page() {
        return total_page;
    }

    public int getCurrent_page() {
        return current_page;
    }

    public int getNext_page() {
        return next_page;
    }

    public int getPer_page_items() {
        return per_page_items;
    }
}
