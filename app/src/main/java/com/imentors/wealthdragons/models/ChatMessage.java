package com.imentors.wealthdragons.models;

import android.widget.ImageView;

import com.google.firebase.Timestamp;

import java.io.Serializable;

public class ChatMessage implements Serializable{


   public ChatMessage(String message, String senderImage, String chatTime, String chat_token, String senderName, String userId, Timestamp time, String userType){
     this.chat_timing = chatTime;
     this.message = message;
     this.chat_token = chat_token;
     this.image = senderImage;
     this.fullname = senderName;
     this.user_id = userId;
     this.time = time;
     this.user_type = userType;


   }

   public ChatMessage(){

   }

   private String message;




   private String fullname;
    private String chat_timing;
    private String chat_token;
    private String image;
    private Timestamp time;
    private String user_id;

    private String user_type;




   public String getMessage() {
      return message;
   }


    public String getUser_type() {
        return user_type;
    }

    public String getUser_id() {
        return user_id;
    }

    public Timestamp getTime() {
        return time;
    }

    public String getImage() {
        return image;
    }

    public String getChat_token() {
        return chat_token;
    }

    public String getChat_timing() {
        return chat_timing;
    }

    public String getFullname() {
        return fullname;
    }
}


