package com.imentors.wealthdragons.models;

import java.io.Serializable;

public class UserCredentials implements Serializable{


   private String email;

   private String password;

   private boolean isCredentailsSaved = false;


   public String getEmail() {
      return email;
   }

   public String getPassword() {
      return password;
   }


   public boolean isCredentailsSaved() {
      return isCredentailsSaved;
   }


   public void setCredentailsSavedStatus(boolean isCredentailsSaved){
      this.isCredentailsSaved = isCredentailsSaved;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public void setPassword(String password) {
      this.password = password;
   }
}


