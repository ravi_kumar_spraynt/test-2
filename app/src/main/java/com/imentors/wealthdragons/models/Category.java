package com.imentors.wealthdragons.models;

import java.io.Serializable;
import java.util.List;

public class Category implements Serializable {

    private String id;
    private String title;
    private String banner;
    private String taste;
    private List<SubCategory> Sub;

    // for taste prefernces expand
    private boolean isExpanded = false;

    public Category(String id , String title , String banner , List<SubCategory> sub){
        this.id = id;
        this.title = title;
        this.banner = banner;
        this.Sub = sub;
    }

    public String getTitle() {
        return title;
    }

    public String getBanner() {
        return banner;
    }


    public String getId() {
        return id;
    }

    public List<SubCategory> getSub() {
        return Sub;
    }

    public String getTaste() {
        return taste;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public class SubCategory implements Serializable{

        private String id;
        private String title;
        private String taste;

        public String getTitle() {
            return title;
        }

        public String getId() {
            return id;
        }

        public String getTaste() {
            return taste;
        }

        public void setTaste(String taste){
            this.taste = taste;
        }
    }

    public void setTaste(String taste){
        this.taste = taste;
        for(SubCategory subCategory: getSub()){
            subCategory.setTaste(taste);
        }
    }


}
