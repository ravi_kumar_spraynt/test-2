package com.imentors.wealthdragons.models;

import java.util.List;

public class SubCategory {


    public SubCategory(String id,String title){
        this.id = id;
        this.title =  title;
    }

    private String id;
    private String title;

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }
}
