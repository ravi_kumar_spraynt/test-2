package com.imentors.wealthdragons.interfaces;

/**
 * Created on 7/17/17.
 */

public interface EventClickListener {
    void onEventClick(String detailType, String detailId);
}
