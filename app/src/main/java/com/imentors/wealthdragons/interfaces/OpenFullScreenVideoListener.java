package com.imentors.wealthdragons.interfaces;

import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.DashBoardVideoDetails;

import java.util.List;

/**
 * Created on 7/17/17.
 */

public interface OpenFullScreenVideoListener {
    void onFullScreenVideoButtonClick(int  resumeWindow, long resumePosition , List<String> url , DashBoardCourseDetails dashBoardCourseDetails, DashBoardVideoDetails dashBoardVideoDetails, String title, boolean isCourseDetail, String previewImage, String posterImage);
}
