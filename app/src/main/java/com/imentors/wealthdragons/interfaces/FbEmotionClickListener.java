package com.imentors.wealthdragons.interfaces;

import com.facebook.keyframes.KeyframesDrawable;
import com.facebook.keyframes.model.KFAnimationGroup;
import com.facebook.keyframes.model.KFImage;
import com.imentors.wealthdragons.fbEmojiAnimation.Emotion;

/**
 * Created on 7/17/17.
 */

public interface FbEmotionClickListener {
    void onEMotionRelease(String kfImage);
}
