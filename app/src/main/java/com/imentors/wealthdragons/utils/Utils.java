package com.imentors.wealthdragons.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.provider.Settings;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.facebook.login.LoginManager;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.CategoryChooseActivity;
import com.imentors.wealthdragons.activity.CustomerSupportActivity;
import com.imentors.wealthdragons.activity.LoginActivity;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.fragments.ContactSupportFragment;
import com.imentors.wealthdragons.fragments.MyGroupDownloadFragment;
import com.imentors.wealthdragons.fragments.WelcomePagerFragment;
import com.imentors.wealthdragons.models.Categories;
import com.imentors.wealthdragons.models.Category;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.DeepLink;

import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2core.Downloader;
import com.tonyodev.fetch2core.Func;
import com.tonyodev.fetch2okhttp.OkHttpDownloader;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;
import static com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication.sharedInstance;


public class Utils {

    private static Fetch fetch;

    private Utils() {
        /* cannot be instantiated */
    }


    private static boolean isDialogVisible = false;
    public static boolean isKeyboardVisibleBoolean;
    private static SimpleExoPlayer mPlayerFullScreen , mPlayerHeader ,mAuidoPlayer;
    private static MediaSource mVideoSourceFullScreen , mVideoSourceHeader ,mAudioSource;
    public static float pxWidth , pxHeight;




    public interface DialogInteraction {
        void onPositiveButtonClick(AlertDialog dialog,boolean isNotification);

        void onNegativeButtonClick(AlertDialog dialog, boolean isNotification);
    }


    public interface DialogSingleButtonInteraction {
        void onPositiveButtonClick(AlertDialog dialog);
    }

    /**
     * Show the network error based on the type of error logic
     *
     * @param context context
     * @param error   volley error
     */

    public static void showNetworkError(Context context, VolleyError error) {


        if (error instanceof NoConnectionError) {
            Utils.showSplashInternetError(context, true);

        } else if (error instanceof TimeoutError) {

            Utils.showAlertDialog(context, context.getString(R.string.error), context.getString(R.string.error_api_try_again), null, false,true);

        } else if (error.networkResponse != null) {
            NetworkResponse response = error.networkResponse;

//            if (response.statusCode == 401 && AppPreferences.getSession().isLoggedIn()) {
//                sharedInstance().logoutUser(context); // Logout User
//
//            } else
            if (response.statusCode == 500) {
                Utils.showAlertDialog(context, context.getString(R.string.error), context.getString(R.string.error_api_try_again), null, false,true);
            } else if (response.data != null) {
                String responseJSON = new String(response.data);
                String errorMessage = trimErrorMessage(responseJSON);

                if (errorMessage != null) {

                    Utils.showAlertDialog(context, null, errorMessage, null, false,true);

                } else {
                    Utils.showAlertDialog(context, context.getString(R.string.error), context.getString(R.string.error_api_try_again), null, false,true);
                }

            } else {
                Utils.showAlertDialog(context, context.getString(R.string.error), context.getString(R.string.error_api_try_again), null, false,true);
            }

        } else {
            Utils.showAlertDialog(context, context.getString(R.string.error), context.getString(R.string.error_api_try_again), null, false,true);
        }
    }

    public static void setDialogValue(){
        isDialogVisible = false;
    }


    public static void showCustomerSupportNetworkError(Context context, VolleyError error) {


        if (error instanceof NoConnectionError) {
            Utils.showInternetError(context, true);

        } else if (error instanceof TimeoutError) {

            Utils.showAlertDialog(context, context.getString(R.string.error), context.getString(R.string.error_api_try_again), null, false,true);

        } else if (error.networkResponse != null) {
            NetworkResponse response = error.networkResponse;

//            if (response.statusCode == 401 && AppPreferences.getSession().isLoggedIn()) {
//                sharedInstance().logoutUser(context); // Logout User
//
//            } else
            if (response.statusCode == 500) {
                Utils.showAlertDialog(context, context.getString(R.string.error), context.getString(R.string.error_api_try_again), null, false,true);
            } else if (response.data != null) {
                String responseJSON = new String(response.data);
                String errorMessage = trimErrorMessage(responseJSON);

                if (errorMessage != null) {

                    Utils.showAlertDialog(context, null, errorMessage, null, false,true);

                } else {
                    Utils.showAlertDialog(context, context.getString(R.string.error), context.getString(R.string.error_api_try_again), null, false,true);
                }

            } else {
                Utils.showAlertDialog(context, context.getString(R.string.error), context.getString(R.string.error_api_try_again), null, false,true);
            }

        } else {
            Utils.showAlertDialog(context, context.getString(R.string.error), context.getString(R.string.error_api_try_again), null, false,true);
        }
    }

    /**
     * Parse the error message
     *
     * @param json json string from the response
     * @return return string message
     */
    private static String trimErrorMessage(String json) {

        String trimmedMessage = null;

        try {
            JSONObject responseObject = new JSONObject(json);
            trimmedMessage = responseObject.getString("error");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return trimmedMessage;
    }

    /**
     * show internet error based on the network available status
     *
     * @param context context of the activity
     */
    public static void showInternetError(Context context, boolean isCallBackRequired) {

        Utils.showAlertDialog(context, context.getString(R.string.error_api_no_internet_connection), context.getString(R.string.error_api_no_internet_connection_message), context.getString(R.string.retry), isCallBackRequired,true);

//      open if toast and snackbar is needed
//        if (Utils.isNetworkAvailable(context)) {
//            showUnknownError(context, view);
//        } else {
//            showToast(context, view, context.getString(R.string
//                    .error_api_no_internet_connection));
//        }
    }

    public static void showSplashInternetError(Context context, boolean isCallBackRequired) {

        Utils.showNetworkAlertDialog(context, context.getString(R.string.error_api_no_internet_connection), context.getString(R.string.error_api_no_internet_connection_message), context.getString(R.string.retry), isCallBackRequired,true);

//      open if toast and snackbar is needed
//        if (Utils.isNetworkAvailable(context)) {
//            showUnknownError(context, view);
//        } else {
//            showToast(context, view, context.getString(R.string
//                    .error_api_no_internet_connection));
//        }
    }

    /**
     * Show unKnown error message
     *
     * @param context context of the activity
     */
    public static void showUnknownError(Context context, View view) {
        showToast(context, view, context.getString(R.string
                .error_api_try_again));
    }


    /**
     * Show the message using toast or snack bar based on view
     *
     * @param context context
     * @param view    current display view
     * @param message message to show
     */
    public static void showToast(Context context, View view, String message) {
        //if view is null then use toast otherwise snak bar
//        if (view == null) {
//            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
//        } else {
//            Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
//        }
    }

    /**
     * Show confirm alert dialog, it will be dismiss by user action
     *
     * @param context            context
     * @param title              title to be shown
     * @param message            message to be shown
     * @param buttonTitle        text to be shown on button
     * @param isCallBackRequired do we need to register broadcast or not
     */
    public static void showAlertDialog(final Context context, final String title, final String message, String buttonTitle, final boolean isCallBackRequired, final boolean showWarningSign) {

        if (!isDialogVisible) {

            if(context!=null) {


                Activity activity = (Activity) context;


                if (!activity.isDestroyed()) {

                    isDialogVisible = !isDialogVisible;


                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View dialogView = inflater.inflate(R.layout.dialog_error, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    final TextView tvTitle = dialogView.findViewById(R.id.tv_title);
                    TextView tvMessage = dialogView.findViewById(R.id.tv_message);
                    ImageView ivWarningSign = dialogView.findViewById(R.id.iv_warning_sign);
                    TextView tvPositiveButton = dialogView.findViewById(R.id.tv_positive_button);
                    FrameLayout fmPositiveButtonContainer = dialogView.findViewById(R.id.fl_btn_orange);
                    TextView tvDownloadsButton= dialogView.findViewById(R.id.tv_download_button);
                    FrameLayout fmDownloadButtonContainer = dialogView.findViewById(R.id.fl_btn_downloads);

                    tvDownloadsButton.setVisibility(View.GONE);
                    fmDownloadButtonContainer.setVisibility(View.GONE);

                    if(showWarningSign){
                        ivWarningSign.setVisibility(View.VISIBLE);
                    }else{
                        ivWarningSign.setVisibility(View.GONE);
                    }

                    if (!TextUtils.isEmpty(title)) {
                        tvTitle.setText(title);
                        Utils.callEventLogApi("Dialog <b>"+ title+"</b> opened");

                    } else {
                        tvTitle.setText(context.getString(R.string.error));
                        Utils.callEventLogApi("Dialog <b>Alert</b> opened");

                    }

                    if (!TextUtils.isEmpty(message)) {
                        tvMessage.setText(message);
                    }

                    if (!TextUtils.isEmpty(buttonTitle)) {
                        tvPositiveButton.setText(buttonTitle);
                    } else {
                        tvPositiveButton.setText(R.string.okay_text);
                    }

                    final AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();
                    alertDialog.getWindow().setLayout((int) context.getResources().getDimension(R.dimen.error_dialog_width), LinearLayout.LayoutParams.WRAP_CONTENT);

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            isDialogVisible = false;

                        }
                    });


                    fmPositiveButtonContainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.callEventLogApi("dialog <b>button</b> clicked");
                            alertDialog.dismiss();


                            if (isCallBackRequired) {


                                // check fragment to handle case of opening of customer support fragment
                                // local broadcast reciever is called many times . So , handle it here.
                                if (context instanceof LoginActivity) {
                                    FragmentManager fm = ((LoginActivity) context).getSupportFragmentManager();
                                    Fragment fr = fm.findFragmentById(R.id.fm_main);

                                    if (fr instanceof WelcomePagerFragment) {

                                        moveToContactSupportFragment(fm);

                                    }
                                }


                                LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcast(new Intent(Constants.BROADCAST_DISMISS_ALERT_DIALOG));
                            } else {
                                // in every case of inactive throw user to  splash screen


                                if (tvTitle.getText().toString().equals(context.getString(R.string.account_inactive)) || tvTitle.getText().toString().equals(context.getString(R.string.inactive))) {
                                    if (context instanceof LoginActivity) {
                                        ((LoginActivity) context).getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    } else {

                                        // move to customer support in  case of inactive account
                                        AppPreferences.setOpenContactSupport(true);
                                        WealthDragonsOnlineApplication.sharedInstance().redirectToSplashActivity(context);
                                    }
                                }else{
                                    if (context instanceof MainActivity || context instanceof CustomerSupportActivity || context instanceof CategoryChooseActivity) {
                                        WealthDragonsOnlineApplication.sharedInstance().redirectToSplashActivity(context);
                                    }
                                }

                            }

                        }
                    });

                }
            }
        }

    }

    public static void showNetworkAlertDialog(final Context context, final String title, final String message, String buttonTitle, final boolean isCallBackRequired, final boolean showWarningSign) {

        if (!isDialogVisible) {

            if(context!=null) {


                Activity activity = (Activity) context;


                if (!activity.isDestroyed()) {

                    isDialogVisible = !isDialogVisible;


                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View dialogView = inflater.inflate(R.layout.dialog_error, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    final TextView tvTitle = dialogView.findViewById(R.id.tv_title);
                    TextView tvMessage = dialogView.findViewById(R.id.tv_message);
                    ImageView ivWarningSign = dialogView.findViewById(R.id.iv_warning_sign);
                    TextView tvPositiveButton = dialogView.findViewById(R.id.tv_positive_button);
                    FrameLayout fmPositiveButtonContainer = dialogView.findViewById(R.id.fl_btn_orange);
                    TextView tvDownloadsButton= dialogView.findViewById(R.id.tv_download_button);
                    FrameLayout fmDownloadButtonContainer = dialogView.findViewById(R.id.fl_btn_downloads);

                    tvDownloadsButton.setVisibility(View.VISIBLE);
                    fmDownloadButtonContainer.setVisibility(View.VISIBLE);


                    if(showWarningSign){
                        ivWarningSign.setVisibility(View.VISIBLE);
                    }else{
                        ivWarningSign.setVisibility(View.GONE);
                    }

                    if (!TextUtils.isEmpty(title)) {
                        tvTitle.setText(title);
                        Utils.callEventLogApi("Dialog <b>"+ title+"</b> opened");

                    } else {
                        tvTitle.setText(context.getString(R.string.error));
                        Utils.callEventLogApi("Dialog <b>Alert</b> opened");

                    }

                    if (!TextUtils.isEmpty(message)) {
                        tvMessage.setText(message);
                    }

                    if (!TextUtils.isEmpty(buttonTitle)) {
                        tvPositiveButton.setText(buttonTitle);
                    } else {
                        tvPositiveButton.setText(R.string.okay_text);
                    }

                    final AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();
                    alertDialog.getWindow().setLayout((int) context.getResources().getDimension(R.dimen.error_dialog_width), LinearLayout.LayoutParams.WRAP_CONTENT);

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            isDialogVisible = false;

                        }
                    });


                    fmDownloadButtonContainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            addFragment(MyGroupDownloadFragment.newInstance(),context);

                        }
                    });

                    fmPositiveButtonContainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.callEventLogApi("dialog <b>button</b> clicked");
                            alertDialog.dismiss();


                            if (isCallBackRequired) {


                                // check fragment to handle case of opening of customer support fragment
                                // local broadcast reciever is called many times . So , handle it here.
                                if (context instanceof LoginActivity) {
                                    FragmentManager fm = ((LoginActivity) context).getSupportFragmentManager();
                                    Fragment fr = fm.findFragmentById(R.id.fm_main);

                                    if (fr instanceof WelcomePagerFragment) {

                                        moveToContactSupportFragment(fm);

                                    }
                                }


//                                LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcast(new Intent(Constants.BROADCAST_DISMISS_ALERT_DIALOG));

                                Intent intent = new Intent(Constants.BROADCAST_DISMISS_ALERT_DIALOG);

                                intent.putExtra("isClearResponse",true);

                                LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);
                            } else {
                                // in every case of inactive throw user to  splash screen


                                if (tvTitle.getText().toString().equals(context.getString(R.string.account_inactive)) || tvTitle.getText().toString().equals(context.getString(R.string.inactive))) {
                                    if (context instanceof LoginActivity) {
                                        ((LoginActivity) context).getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    } else {

                                        // move to customer support in  case of inactive account
                                        AppPreferences.setOpenContactSupport(true);
                                        WealthDragonsOnlineApplication.sharedInstance().redirectToSplashActivity(context);
                                    }
                                }else{
                                    if (context instanceof MainActivity || context instanceof CustomerSupportActivity || context instanceof CategoryChooseActivity) {
                                        WealthDragonsOnlineApplication.sharedInstance().redirectToSplashActivity(context);
                                    }
                                }

                            }

                        }
                    });

                }
            }
        }

    }


    public static void addFragment(Fragment fragment,Context context) {

        FragmentManager fm = ((MainActivity) context).getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        if (transaction.isAddToBackStackAllowed()) {
            transaction.addToBackStack(null);
            transaction.add(R.id.fm_main, fragment).commitAllowingStateLoss();
        }
    }

    private static void moveToContactSupportFragment(FragmentManager fm) {
        ContactSupportFragment termFragment = ContactSupportFragment.newInstance();

        FragmentTransaction ft = fm.beginTransaction();

        ft.setCustomAnimations(
                R.anim.slide_in_from_right, R.anim.slide_out_to_right);

        ft.add(R.id.fm_main, termFragment);

        ft.addToBackStack(null);

        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

        ft.commit();
    }


    /**
     * Show simple confirm alert dialog, it will be dismiss by user action
     *
     * @param context            context
     * @param title              title to be shown
     * @param message            message to be shown
     * @param buttonTitle        text to be shown on button
     */
    public static void showSimpleAlertDialog(final Context context, final String title, final String message, final String buttonTitle, final boolean showWarningSign) {

        if (!isDialogVisible) {


            if (context != null) {
                Activity activity = (Activity) context;


                if (!activity.isDestroyed()) {
                    isDialogVisible = !isDialogVisible;

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View dialogView = inflater.inflate(R.layout.dialog_error, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView tvTitle = dialogView.findViewById(R.id.tv_title);
                    TextView tvMessage = dialogView.findViewById(R.id.tv_message);
                    TextView tvPositiveButton = dialogView.findViewById(R.id.tv_positive_button);
                    FrameLayout fmPositiveButtonContainer = dialogView.findViewById(R.id.fl_btn_orange);
                    ImageView ivWarningSign = dialogView.findViewById(R.id.iv_warning_sign);


                    if (!TextUtils.isEmpty(title)) {
                        tvTitle.setText(title);
                    } else {
                        tvTitle.setText(context.getString(R.string.error));
                    }

                    if (!TextUtils.isEmpty(message)) {
                        tvMessage.setText(message);
                    }

                    if(showWarningSign){
                        ivWarningSign.setVisibility(View.VISIBLE);
                    }else{
                        ivWarningSign.setVisibility(View.GONE);

                    }

                    Utils.callEventLogApi("open Dialog with message <b>" +message+"</b>");

                    if (!TextUtils.isEmpty(buttonTitle)) {
                        tvPositiveButton.setText(buttonTitle);
                    } else {
                        tvPositiveButton.setText(R.string.okay_text);
                    }

                    final AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();
                    alertDialog.getWindow().setLayout((int) context.getResources().getDimension(R.dimen.error_dialog_width), LinearLayout.LayoutParams.WRAP_CONTENT);
                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            isDialogVisible = false;

                        }
                    });

                    fmPositiveButtonContainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            if (!TextUtils.isEmpty(buttonTitle)) {
                                Utils.callEventLogApi("closed Dialog with message <b>" +message+"</b> on pressing <b>"+buttonTitle+"</b> button");
                            } else {
                                Utils.callEventLogApi("closed Dialog with message <b>" +message+"</b> on pressing <b>okay</b> button");

                            }

                        }

                    });

                }
            }
        }

    }



    /**
     * Show simple confirm alert dialog, it will be dismiss by user action
     *
     * @param context            context
     * @param title              title to be shown
     * @param message            message to be shown
     * @param buttonTitle        text to be shown on button
     */
    public static void showDownloadAlertDialog(final Context context, final String title, final String message, final String buttonTitle, final boolean showWarningSign, final com.tonyodev.fetch2.Request request) {

        if (!isDialogVisible) {


            if (context != null) {
                Activity activity = (Activity) context;


                if (!activity.isDestroyed()) {
                    isDialogVisible = !isDialogVisible;

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View dialogView = inflater.inflate(R.layout.dialog_error, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView tvTitle = dialogView.findViewById(R.id.tv_title);
                    TextView tvMessage = dialogView.findViewById(R.id.tv_message);
                    TextView tvPositiveButton = dialogView.findViewById(R.id.tv_positive_button);
                    FrameLayout fmPositiveButtonContainer = dialogView.findViewById(R.id.fl_btn_orange);
                    ImageView ivWarningSign = dialogView.findViewById(R.id.iv_warning_sign);


                    if (!TextUtils.isEmpty(title)) {
                        tvTitle.setText(title);
                    } else {
                        tvTitle.setText(context.getString(R.string.error));
                    }

                    if (!TextUtils.isEmpty(message)) {
                        tvMessage.setText(message);
                    }

                    if(showWarningSign){
                        ivWarningSign.setVisibility(View.VISIBLE);
                    }else{
                        ivWarningSign.setVisibility(View.GONE);

                    }

                    Utils.callEventLogApi("open Dialog with message <b>" +message+"</b>");

                    if (!TextUtils.isEmpty(buttonTitle)) {
                        tvPositiveButton.setText(buttonTitle);
                    } else {
                        tvPositiveButton.setText(R.string.okay_text);
                    }

                    final AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();
                    alertDialog.getWindow().setLayout((int) context.getResources().getDimension(R.dimen.error_dialog_width), LinearLayout.LayoutParams.WRAP_CONTENT);
                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            isDialogVisible = false;

                        }
                    });

                    fmPositiveButtonContainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            getFetchInstance().remove(request.getId());
                            if (!TextUtils.isEmpty(buttonTitle)) {
                                Utils.callEventLogApi("closed Dialog with message <b>" +message+"</b> on pressing <b>"+buttonTitle+"</b> button");
                            } else {
                                Utils.callEventLogApi("closed Dialog with message <b>" +message+"</b> on pressing <b>okay</b> button");

                            }

                        }

                    });

                }
            }
        }

    }



    /**
     * Show confirm alert dialog, it will be dismiss by user action
     *
     * @param context context
     * @param title   title to be shown
     * @param message message to be shown
     */
    public static void showTwoButtonAlertDialog(final DialogInteraction dialogInteraction, final Context context, String title, String message, final String postiveButtonTitle , String negativeButtonTitle) {

        if (context != null) {

            Activity activity = (Activity) context;


            if (!activity.isDestroyed()) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View dialogView = inflater.inflate(R.layout.dialog_error_two_button, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView tvTitle = dialogView.findViewById(R.id.tv_title);
                TextView tvMessage = dialogView.findViewById(R.id.tv_message);
                FrameLayout fmPositiveButtonContainer = dialogView.findViewById(R.id.fl_btn_orange_positive);
                FrameLayout fmNegativeButtonContainer = dialogView.findViewById(R.id.fl_btn_orange_negative);

                TextView tvPositiveButton = dialogView.findViewById(R.id.tv_positive_button);
                TextView tvNegativeButton = dialogView.findViewById(R.id.tv_negative_button);

                if (!TextUtils.isEmpty(title)) {
                    tvTitle.setText(title);
                } else {
                    tvTitle.setText(context.getString(R.string.error));
                }

                if (!TextUtils.isEmpty(message)) {
                    tvMessage.setText(message);
                }


                if(TextUtils.isEmpty(postiveButtonTitle)){
                    tvPositiveButton.setText(R.string.yes);
                }else{
                    tvPositiveButton.setText(postiveButtonTitle);
                }

                if(TextUtils.isEmpty(negativeButtonTitle)){
                    tvNegativeButton.setText(R.string.no);
                }else{
                    tvNegativeButton.setText(negativeButtonTitle);
                }


                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                alertDialog.getWindow().setLayout((int) context.getResources().getDimension(R.dimen.error_dialog_width), LinearLayout.LayoutParams.WRAP_CONTENT);


                fmPositiveButtonContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(dialogInteraction != null) {
                            if (TextUtils.isEmpty(postiveButtonTitle)) {
                                dialogInteraction.onPositiveButtonClick(alertDialog, false);
                            } else {
                                dialogInteraction.onPositiveButtonClick(alertDialog, postiveButtonTitle.equals(context.getString(R.string.view)));
                            }
                        }


                    }
                });

                fmNegativeButtonContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(dialogInteraction != null) {

                            if (TextUtils.isEmpty(postiveButtonTitle)) {
                                dialogInteraction.onNegativeButtonClick(alertDialog, false);
                            } else {
                                dialogInteraction.onNegativeButtonClick(alertDialog, postiveButtonTitle.equals(context.getString(R.string.view)));
                            }
                        }

                    }
                });
            }
        }
    }


    /**
     * Show confirm alert dialog, it will be dismiss by user action
     *
     * @param context               context
     * @param title                 title to be shown
     * @param message               message to be shown
     * @param postiveTextId         id to the string for positive button
     * @param negativeTextId        id of the string for negative button
     * @param positiveClickListener active to be performed when positive button click
     * @param negativeClickListener active to be performed when negative button click
     */
    public static void showErrorDialog(Context context, String title, String message, @SuppressWarnings("SameParameterValue") int postiveTextId, @SuppressWarnings("SameParameterValue") int
            negativeTextId, DialogInterface.OnClickListener positiveClickListener, DialogInterface.OnClickListener negativeClickListener) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        if (!TextUtils.isEmpty(title)) {
            alertDialog.setTitle(title);
        }
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        if(postiveTextId != -1) {
            alertDialog.setPositiveButton(postiveTextId, positiveClickListener);
        }
        if(negativeTextId != -1) {
            alertDialog.setNegativeButton(negativeTextId, negativeClickListener);
        }
        alertDialog.show();
    }

    /**
     * Return true if network connection available
     *
     * @param context context
     * @return true if network connection available
     */
    public static boolean isNetworkAvailable(@NonNull Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context
                .CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        return false;
    }

    /**
     * Hide the keyboard
     *
     * @param context context from which keyboard needs to be hide
     */
    public static void hideKeyboard(Context context) {
        AppCompatActivity activity = (AppCompatActivity) context;
        if (activity != null) {
            View focus = activity.getCurrentFocus();
            if (focus != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.hideSoftInputFromWindow(focus.getWindowToken(), 0);
                    isKeyboardVisibleBoolean = false;

            }
        }
    }

    /**
     * Open the keyboard on the give editText
     *
     * @param context  context
     * @param editText editText
     */
    public static void showKeyboard(Context context, EditText editText) {
        AppCompatActivity activity = (AppCompatActivity) context;
        if (activity != null) {
            View focus = activity.getCurrentFocus();
            if (focus != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                    isKeyboardVisibleBoolean = true;

            }
        }
    }

    /**
     * keyboard status
     *

     */
    public static boolean isKeyboardVisible() {
        return isKeyboardVisibleBoolean;
    }



    /**
     * Return the readableFileSize format
     *
     * @param size of the file
     * @return readable string format
     */
    public static String readableFileSize(long size) {
        if (size <= 0) return "0";
        final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    /**
     * Vibrate the device
     *
     * @param context context from which this method called
     */
    public static void vibrate(Context context) {
        try {
            Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            if (v != null)
                v.vibrate(200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Return the int value after parsing the string
     *
     * @param text         str to be parsed
     * @param defaultValue value to be used if parse unsuccessful
     * @return parse result
     */
    public static int parseInt(String text, int defaultValue) {
        try {
            return Integer.parseInt(text);
        } catch (Exception ex) {
            return defaultValue;
        }
    }

    /**
     * Return the long value after parsing the string
     *
     * @param text         str to be parsed
     * @param defaultValue value to be used if parse unsuccessfull
     * @return parse result
     */
    public static long parseLong(String text, long defaultValue) {
        try {
            return Long.parseLong(text);
        } catch (Exception ex) {
            return defaultValue;
        }
    }

    /**
     * Return the double value after parsing the string
     *
     * @param text         str to be parsed
     * @param defaultValue value to be used if parse unsuccessful
     * @return parse result
     */
    public static double parseDouble(String text, double defaultValue) {
        try {
            return Double.parseDouble(text);
        } catch (Exception ex) {
            return defaultValue;
        }
    }

    /**
     * Return the float value after parsing the string
     *
     * @param text         str to be parsed
     * @param defaultValue value to be used if parse unsuccessful
     * @return parse result
     */
    public static float parseFloat(String text, float defaultValue) {
        try {
            return Float.parseFloat(text);
        } catch (Exception ex) {
            return defaultValue;
        }
    }

    /**
     * Open the dial screen name with number specified
     *
     * @param context activity context
     * @param number  phone number
     * @return whether intent exists for handling phone no
     */
    public static boolean dialNumber(Context context, String number) {

        Intent intent = new Intent(Intent.ACTION_DIAL);
        if (TextUtils.isEmpty(number)) {
            return false;
        } else {
            intent.setData(Uri.parse("tel:" + number));
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(intent);
                return true;
            } else {
                return false;
            }
        }
    }

    /***
     * Convert the json string from file to JsonObject
     * @param fileName file name
     * @return json object
     */
    public static JSONObject getJSONObjectFromAssets(String fileName) {
        try {
            InputStream is = sharedInstance().getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            //noinspection ResultOfMethodCallIgnored
            is.read(buffer);
            is.close();
            String json = new String(buffer, StandardCharsets.UTF_8);
            return new JSONObject(json);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Return true if emailId is valid
     *
     * @param emailId the email id to be examined
     * @return true if emailId is valid
     */
    public static boolean isValidEmail(CharSequence emailId) {
        return !TextUtils.isEmpty(emailId) && android.util.Patterns.EMAIL_ADDRESS.matcher(emailId).matches();
    }





    public static boolean isValidName(String mFirstName)
    {
//        String USER_PATTERN = "[A-z-Z]+";
        String USER_PATTERN = "^[\\p{L} .'-]+$";
        Pattern pattern=Pattern.compile(USER_PATTERN);
        Matcher matcher=pattern.matcher(mFirstName);
        return matcher.matches();
    }


    public static boolean isValidPassword(String password)
    {
        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+-=])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();
    }

    public static int[] regExIndex(String pattern, String text, Integer fromIndex){
        Matcher matcher = Pattern.compile(pattern).matcher(text);
        if ( ( fromIndex != null && matcher.find(fromIndex) ) || matcher.find()) {
            return new int[]{matcher.start(), matcher.end()};
        }
        return new int[]{-1, -1};
    }




    /**
     * Return the dp value
     *
     * @param px value in pixel
     * @return change pixel vale to dp value
     */
    public static int pxToDp(int px) {
        return (int) (px / sharedInstance().getResources()
                .getDisplayMetrics().density);
    }

    /**
     * Return the pixel value
     *
     * @param dp value to convert
     * @return pixel value
     */
    public static int dpToPx(int dp) {
        return (int) (dp * sharedInstance().getResources()
                .getDisplayMetrics().density);
    }


    /**
     * Get device id
     *
     * @return device id
     */
    public static String getDeviceId() {
        @SuppressLint("HardwareIds") String id = Settings.Secure.getString(sharedInstance().getContentResolver(), Settings.Secure.ANDROID_ID);
        if (TextUtils.isEmpty(id)) {
            return java.util.UUID.randomUUID().toString();
        } else {
            return id;
        }
    }

    /**
     * device name
     *
     * @return device name
     */
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model;
        } else {
            return manufacturer + " " + model;
        }
    }

    public static float getScreenWidth(Activity activity) {
        if(pxWidth == 0) {
            Display display = activity.getWindowManager().getDefaultDisplay();
            DisplayMetrics outMetrics = new DisplayMetrics();
            display.getMetrics(outMetrics);
            pxWidth = outMetrics.widthPixels;
        }
        return pxWidth;
    }

    public static float getScreenHeight(Activity activity) {
        if(pxHeight==0) {
            Display display = activity.getWindowManager().getDefaultDisplay();
            DisplayMetrics outMetrics = new DisplayMetrics();
            display.getMetrics(outMetrics);
            pxHeight = outMetrics.heightPixels;
        }
        return pxHeight;
    }


    public static double getDetailScreenImageHeight(Activity activity) {
        return ((getScreenWidth(activity) - 12 ) / 308) * 183 ;
    }


    public static double getCourseImageWidth(Activity activity) {

        boolean isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);
        float density = activity.getResources().getDisplayMetrics().density;


        if (isTablet) {
            return getScreenWidth(activity) / 2.7;
        } else {
            return getScreenWidth(activity) / 1.6;
        }

    }


    public static boolean isTablet(){
        return WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);
    }


    public static double getCourseImageHeight(Activity activity) {

        // ratio for course is 85:148  H:W
        double ratio = 1.74;


        return getCourseImageWidth(activity) / ratio;

    }



    public static double getDownloadImageWidth(Activity activity) {

        boolean isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);
        float density = activity.getResources().getDisplayMetrics().density;



        return getScreenWidth(activity) / 2.7;

    }





    public static double getDownloadImageHeight(Activity activity) {

        // ratio for course is 85:148  H:W
        double ratio = 1.74;


//        if(isTablet()){
//            return getDownloadImageWidth(activity) / (ratio);
//
//        }
        return getDownloadImageWidth(activity) / (ratio);

    }



    public static double getFeedsImageHeight(Activity activity) {

        // ratio for course is 85:148  H:W
        double ratio = 1.74;


        if(isTablet()){
            return getScreenWidth(activity) / (ratio*3);

        }
        return getScreenWidth(activity) / (ratio*2);

    }

    public static double getMentorImageWidth(Activity activity) {

        boolean isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);
        float density = activity.getResources().getDisplayMetrics().density;

        if (isTablet) {
            return getScreenWidth(activity) / 4.5;
        } else {
            return getScreenWidth(activity) / 2.5;
        }
    }


    public static double getMentorImageHeight(Activity activity) {

        // ratio for course is 1:1
        double ratio = 1;


        return getMentorImageWidth(activity) / ratio;

    }


    public static double getVideoImageWidth(Activity activity) {

        boolean isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);

        if (isTablet) {
            return getScreenWidth(activity) / 2.7;
        } else {
            return getScreenWidth(activity) / 1.6;
        }
    }

    public static double getDigitalDetailImageHeight(double width) {

        // ratio for course is 75:31
        double ratio = 2;


        return width / ratio;

    }

    public static double getDigitalDetailImageWidth(Activity activity) {

        boolean isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);

        if (isTablet) {
            return getScreenWidth(activity) / 2.7;
        } else {
            return getScreenWidth(activity) / 1.6;
        }
    }


    public static double getDigitalImageWidth(Activity activity) {

        boolean isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);

        if (isTablet) {
            return getScreenWidth(activity) / 2.7;
        } else {
            return getScreenWidth(activity) / 1.6;
        }
    }


    public static double getDigitalImageHeight(Activity activity) {

        // ratio for course is 75:31
        double ratio = 2;


        return getVideoImageWidth(activity) / ratio;

    }


    public static double getVideoImageHeight(Activity activity) {

        // ratio for course is 75:31
        double ratio = 2.4;


        return getVideoImageWidth(activity) / ratio;

    }


    public static double getSliderImageWidth(Activity activity) {

        boolean isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);

        if (isTablet) {
            return getScreenWidth(activity) / 2;
        } else {
            return getScreenWidth(activity);
        }
    }


    public static double getSliderImageHeight(Activity activity) {

        // divide width by width/320 and then multiply by 144 in ios
//        double ratio = getSliderImageWidth(activity)/320;
//
//
//        return ratio*144;


        return getSliderImageWidth(activity) / 2;


    }


    public static String getImageUrl(String imageUrl, double width, double height) {

        if (!TextUtils.isEmpty(imageUrl)) {

            int lastIndex, secondLastIndex, thirdLastIndex;
            lastIndex = imageUrl.lastIndexOf('/');
            secondLastIndex = imageUrl.lastIndexOf('/', lastIndex - 1);
            thirdLastIndex = imageUrl.lastIndexOf("/", secondLastIndex - 2);

            StringBuilder builderUrl = new StringBuilder(imageUrl);

            // add height
            builderUrl.replace(secondLastIndex + 1, lastIndex, Double.toString(height));

            // add width
            builderUrl.replace(thirdLastIndex + 1, secondLastIndex, Double.toString(width));


            return builderUrl.toString();

        } else {
            return "";
        }
    }


    public static String getImageName(String url) {

        // save whole url
        if (!TextUtils.isEmpty(url)) {

//            int lastIndex;
//            lastIndex = url.lastIndexOf('/');
//            String imageName = url.substring(lastIndex + 1, url.length());
//
//            return imageName;
            return url;
        } else {
            return "";
        }
    }


    // remove \r from string

    public static String removeExtraCharacterFromString(String string) {

        string.replaceAll("(\\r)", "");

        return string;

    }


    // get params
    public static Map<String, String>  getParams(boolean isTokenRequired) {
        Map<String, String> params = new HashMap<String, String>();



        if (WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet)) {
            params.put(Keys.DEVICE, Keys.TABLET);

        } else {
            params.put(Keys.DEVICE, Keys.ANDROID);

        }

        params.put(Keys.PAYMENT_METHOD, Keys.CC);

        if (isTokenRequired) {

            params.put(Keys.TOKEN, FirebaseInstanceId.getInstance().getToken());
            params.put(Keys.DEVICE_ID, getDeviceId());
            params.put(Keys.TYPE,Keys.ANDROID);
            String manufractureName = android.os.Build.MANUFACTURER;
            manufractureName = manufractureName.substring(0, 1).toUpperCase() + manufractureName.substring(1);
            params.put(Keys.DEVICE_NAME, manufractureName + " " + android.os.Build.MODEL);


        } else {
            if (!TextUtils.isEmpty(AppPreferences.getUserToken())) {
                params.put(Keys.USER_TOKEN, AppPreferences.getUserToken());
            }
        }

        return params;
    }


    // spannable String
    public static void makeLinks(TextView textView, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(textView.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = textView.getText().toString().indexOf(link);
            if (startIndexOfLink >= 0) {
                spannableString.setSpan(clickableSpan, startIndexOfLink,
                        startIndexOfLink + link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        textView.setHighlightColor(
                Color.TRANSPARENT); // prevent TextView change background when highlight
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }


    public static void logoutFbSession() {
        LoginManager.getInstance().logOut();
        // remove all downloaded videos on logout
//        getFetchInstance().deleteAll();
    }


    public static List<Category> addStaticCategories(Categories categories) {
        boolean isDownloads=true;
        List<Category> staticCategoryList = new ArrayList<>();
        if (categories.getCategory() != null) {
            if (categories.isMy_wishlist()) {
                staticCategoryList.add(new Category(null, Integer.toString(R.string.my_wishlist), Integer.toString(R.drawable.ic_heart), null));
            }
            if (categories.isMessages()) {
                staticCategoryList.add(new Category(null, Integer.toString(R.string.messages), Integer.toString(R.drawable.ic_alarm), null));
            }
            if (categories.isTast_preference()) {
                staticCategoryList.add(new Category(null, Integer.toString(R.string.taste_perferance), Integer.toString(R.drawable.ic_taste_perferences), null));
            }
            if (categories.isAbout_us()) {
                staticCategoryList.add(new Category(null, Integer.toString(R.string.about), Integer.toString(R.drawable.ic_about), null));
            }
            if (categories.isSettings()) {
                staticCategoryList.add(new Category(null, Integer.toString(R.string.settings), Integer.toString(R.drawable.ic_setting), null));
            }
            if (categories.isPayment_history()) {
                staticCategoryList.add(new Category(null, Integer.toString(R.string.payment_history), Integer.toString(R.drawable.icon_payment_history), null));
            }
            if (categories.isMy_badges()) {
                staticCategoryList.add(new Category(null, Integer.toString(R.string.my_badges), Integer.toString(R.drawable.icon_badge), null));
            }
            if (isDownloads) {
                staticCategoryList.add(new Category(null, Integer.toString(R.string.my_downloads), Integer.toString(R.drawable.ic_grey_download), null));
            }
            if (categories.isSubscription_tab()) {
                staticCategoryList.add(new Category(null, Integer.toString(R.string.subscription), Integer.toString(R.drawable.ic_subscribe), null));
            }

            if (categories.isPurchased()) {
                staticCategoryList.add(new Category(null, Integer.toString(R.string.purchase), Integer.toString(R.drawable.ic_videos_purchased_black), null));
            }

            if (categories.isCustomer_support()) {
                staticCategoryList.add(new Category(null, Integer.toString(R.string.customer_support), Integer.toString(R.drawable.ic_support), null));
            }


        } else {
            staticCategoryList.add(new Category(null, Integer.toString(R.string.my_wishlist), Integer.toString(R.drawable.ic_heart), null));
            staticCategoryList.add(new Category(null, Integer.toString(R.string.messages), Integer.toString(R.drawable.ic_alarm), null));
            staticCategoryList.add(new Category(null, Integer.toString(R.string.taste_perferance), Integer.toString(R.drawable.ic_taste_perferences), null));
            staticCategoryList.add(new Category(null, Integer.toString(R.string.about), Integer.toString(R.drawable.ic_about), null));
            staticCategoryList.add(new Category(null, Integer.toString(R.string.settings), Integer.toString(R.drawable.ic_setting), null));
            staticCategoryList.add(new Category(null, Integer.toString(R.string.payment_history), Integer.toString(R.drawable.icon_payment_history), null));
            staticCategoryList.add(new Category(null, Integer.toString(R.string.my_badges), Integer.toString(R.drawable.icon_badge), null));
            staticCategoryList.add(new Category(null, Integer.toString(R.string.my_downloads), Integer.toString(R.drawable.ic_grey_download), null));
            staticCategoryList.add(new Category(null, Integer.toString(R.string.subscription), Integer.toString(R.drawable.ic_subscribe), null));
            staticCategoryList.add(new Category(null, Integer.toString(R.string.purchase), Integer.toString(R.drawable.ic_videos_purchased_black), null));
            staticCategoryList.add(new Category(null, Integer.toString(R.string.customer_support), Integer.toString(R.drawable.ic_support), null));

        }



        return staticCategoryList;

    }



    public static String getEventDate(String eventDate) {
        DateTimeFormatter EVENT_DATE_FORMAT = DateTimeFormat.forPattern("dd MMM yyyy " + "h:mm a").withZone
                (DateTimeZone.getDefault());

        DateTimeFormatter DEFAULT_DATE_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZone(DateTimeZone.getDefault());


        try {
            DateTime defaultTimeFormat = DEFAULT_DATE_FORMAT.parseDateTime(eventDate);

            return EVENT_DATE_FORMAT.print(defaultTimeFormat);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }

    }

    public static String getEventDetailDate(String eventDate) {
        DateTimeFormatter EVENT_DATE_FORMAT = DateTimeFormat.forPattern("MMMMM, dd yyyy").withZone
                (DateTimeZone.getDefault());

        DateTimeFormatter DEFAULT_DATE_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZone(DateTimeZone.getDefault());


        try {
            DateTime defaultTimeFormat = DEFAULT_DATE_FORMAT.parseDateTime(eventDate);

            return EVENT_DATE_FORMAT.print(defaultTimeFormat);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }

    }

    public static String getPaymentHistoryDate(String eventDate) {
        DateTimeFormatter EVENT_DATE_FORMAT = DateTimeFormat.forPattern("MMMMM dd yyyy").withZone
                (DateTimeZone.getDefault());

        DateTimeFormatter DEFAULT_DATE_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZone(DateTimeZone.getDefault());


        try {
            DateTime defaultTimeFormat = DEFAULT_DATE_FORMAT.parseDateTime(eventDate);

            return EVENT_DATE_FORMAT.print(defaultTimeFormat);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }

    }
    public static String getEventDetailTime(String eventDate) {
        DateTimeFormatter TIME_FORMAT = DateTimeFormat.forPattern("hh:mm a" ).withZone
                (DateTimeZone.getDefault());

        DateTimeFormatter DEFAULT_DATE_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZone(DateTimeZone.getDefault());


        try {
            DateTime defaultTimeFormat = DEFAULT_DATE_FORMAT.parseDateTime(eventDate).withZoneRetainFields(DateTimeZone.UTC)
                    .withZone(DateTimeZone.getDefault());


            String time  = defaultTimeFormat.toString(TIME_FORMAT);


            return time.replace("AM", "am").replace("PM","pm");
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }

    }


    public static String getMessageDate(String eventDate) {
        DateTimeFormatter MESSAGE_FORMAT = DateTimeFormat.forPattern("MMMMM dd, yyyy  "+ "h:mm a" ).withZone
                (DateTimeZone.getDefault());


        DateTimeFormatter DEFAULT_DATE_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZone(DateTimeZone.getDefault());


        try {
            DateTime defaultTimeFormat = DEFAULT_DATE_FORMAT.parseDateTime(eventDate).withZoneRetainFields(DateTimeZone.UTC)
                    .withZone(DateTimeZone.getDefault());

            return MESSAGE_FORMAT.print(defaultTimeFormat);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }

    }


    public static String getMessageTime(String eventDate) {
        DateTimeFormatter TIME_FORMAT = DateTimeFormat.forPattern("h:mm a" ).withZone
                (DateTimeZone.getDefault());

        DateTimeFormatter DEFAULT_DATE_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZone(DateTimeZone.getDefault());


        try {
            DateTime defaultTimeFormat = DEFAULT_DATE_FORMAT.parseDateTime(eventDate).withZoneRetainFields(DateTimeZone.UTC)
                    .withZone(DateTimeZone.getDefault());


            String time  = defaultTimeFormat.toString(TIME_FORMAT);


            return time.replace("AM", "am").replace("PM","pm");
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }

    }



    public static void setImagesInMemCache(String url) {
                Glide.with(WealthDragonsOnlineApplication.sharedInstance())
                        .load(url)
                        .downloadOnly(Target.SIZE_ORIGINAL,Target.SIZE_ORIGINAL);
    }

    public static String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;

    }


    public static int getDrawerWidth(Activity activity) {
        int oneSixthPart = (int) getScreenWidth(activity) / 6;
        int drawerWidth  = (int) getScreenWidth(activity) - oneSixthPart;

        if (!isTablet()) {
            return drawerWidth;
        } else {
            // for small tablet like samsung
            if (getScreenWidth(activity) < 1080) {
                return drawerWidth;
            }
            // for big tablet
            return 720;
        }
    }


    public static boolean setErrorDialog(String response, Context context) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);


        if (!TextUtils.isEmpty(jsonObject.optString(Keys.ERROR))) {

            String message = jsonObject.getString(Keys.ERROR);


            Utils.showAlertDialog(context, context.getString(R.string.error), message, context.getString(R.string.okay_text), false,true);

            return true;


        } else if (!TextUtils.isEmpty(jsonObject.optString(Keys.INACTIVE))) {

            String message = jsonObject.getString(Keys.INACTIVE);

            Utils.showAlertDialog(context, context.getString(R.string.inactive), message, context.getString(R.string.okay_text), false,true);

            return true;


        } else if (!TextUtils.isEmpty(jsonObject.optString(Keys._INACTIVE))) {

            String message = jsonObject.getString(Keys._INACTIVE);


            Utils.showAlertDialog(context, context.getString(R.string.inactive), message, context.getString(R.string.okay_text), false,true);

            return true;

        } else if (!TextUtils.isEmpty(jsonObject.optString(Keys._ERROR))) {

            String message = jsonObject.getString(Keys._ERROR);


            Utils.showAlertDialog(context, context.getString(R.string.error), message, context.getString(R.string.okay_text), false,true);
            return true;

        } else {
            return false;
        }
    }



    public static void releaseFullScreenPlayer(){
        if(mPlayerFullScreen !=null) {
            mPlayerFullScreen.release();
            mPlayerFullScreen = null;
        }

        if(mVideoSourceHeader !=null) {
            mVideoSourceHeader = null;
        }
    }


    public static SimpleExoPlayer getDetailScreePlayer(){

        return mPlayerHeader;
    }






    public static void releaseDetailScreenPlayer(){
        if(mPlayerHeader !=null) {
            mPlayerHeader.release();
            mPlayerHeader = null;
        }
        if(mVideoSourceHeader !=null) {
            mVideoSourceHeader = null;
        }


    }






    private static MediaSource buildMediaSource(Uri uri, DefaultDataSourceFactory dataSourceFactory) {
        return new ExtractorMediaSource.Factory(dataSourceFactory).
                createMediaSource(uri);
    }



    // video list

    private static void initilizeConsolidatedScreenMediaSource(List<String> eClassesList, Context mContext ){
        mVideoSourceHeader = null;

        List<MediaSource> mediaSourceList = new ArrayList<>();

        for(String playUrl: eClassesList){
            if(!TextUtils.isEmpty(playUrl)){
                String userAgent = Util.getUserAgent(mContext, mContext.getApplicationContext().getApplicationInfo().packageName);

                // Produces DataSource instances through which media data is loaded.
                DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(mContext, userAgent, new DefaultBandwidthMeter());

                Uri daUri = Uri.parse(playUrl);
                MediaSource mediaSource = buildMediaSource(daUri, dataSourceFactory);
                if (mediaSource != null) {
                    mediaSourceList.add(mediaSource);
                }
            }
        }


        ConcatenatingMediaSource concatenatedSource =  new ConcatenatingMediaSource();
        if(mediaSourceList.size()>0){

            concatenatedSource.addMediaSources(mediaSourceList);

            mVideoSourceHeader = concatenatedSource;
        }




    }


    public static SimpleExoPlayer getDetailScreenConcatinatedPlayer(){
        return mPlayerHeader;
    }


    public static void initilizeDetailScreenConcatinatedPlayer(Context mContext  , List<String> mVideoUrls , List<Chapters.EClasses> mEclassesList,boolean isAutoPlay,String type, String videoName,boolean isIntroVideo){
        mPlayerHeader = null;

        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        LoadControl loadControl = new DefaultLoadControl();
        mPlayerHeader = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(mContext), trackSelector, loadControl);

        //mPlayerHeader.addListener(new ExoPlayerListener(mPlayerHeader,mEclassesList,isAutoPlay,type,videoName,isIntroVideo));
        initilizeConsolidatedScreenMediaSource(mVideoUrls,mContext);

        if(mVideoSourceHeader !=null) {
            mPlayerHeader.prepare(mVideoSourceHeader);
        }

    }



    //audio list

    public static SimpleExoPlayer getAudioPlayer(){
        return mAuidoPlayer;
    }


    public static void initilizeAudioPlayer(Context mContext,String url){
        mAuidoPlayer = null;

        initilizeAudioMediaSource(url,mContext);

        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        LoadControl loadControl = new DefaultLoadControl();
        mAuidoPlayer = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(mContext), trackSelector, loadControl);

        if(mAuidoPlayer !=null){
            mAuidoPlayer.prepare(mAudioSource);
        }

    }



    public static void releaseAudioPlayer(){
        if(mAuidoPlayer !=null) {
            mAuidoPlayer.release();
            mAuidoPlayer = null;
        }
        if(mAudioSource !=null) {
            mAudioSource = null;
        }
    }



    private static void initilizeAudioMediaSource(String url, Context mContext){
        mAudioSource = null;
        String userAgent = Util.getUserAgent(mContext, mContext.getApplicationContext().getApplicationInfo().packageName);
        DefaultHttpDataSourceFactory httpDataSourceFactory = new DefaultHttpDataSourceFactory(userAgent, null, DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS, DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS, true);
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(mContext, null, httpDataSourceFactory);

        Uri daUri = Uri.parse(url);
        mAudioSource = buildMediaSource(daUri, dataSourceFactory);

    }


    /**
     * Open link the intent active view
     *
     * @param context The host activity.
     * @param uri      url to open
     */
    public static void openWebLink(Context context, final Uri uri) {
        Utils.callEventLogApi("opened  <b>"+uri+" in browser</b>");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }


    /**
     * sharing articles
     *
     * @param mShareUrl  shareUrl
     */
    public static void shareArticleUrl(String mShareUrl , Context context , String title) {

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_TEXT, mShareUrl);

        context.startActivity(Intent.createChooser(share, title));
    }



    public static void openDocument(String pdf_url, Context context) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pdf_url));
        context.startActivity(browserIntent);
    }


    public static void openEmail(String mailToUrl, Context context) {

        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        /* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, mailToUrl);

        /* Send it off to the Activity-Chooser */
        context.startActivity(Intent.createChooser(emailIntent, "Send mail"));

    }


    public static boolean IsWifiConnected(){
        ConnectivityManager connectionManager = (ConnectivityManager) WealthDragonsOnlineApplication.sharedInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectionManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
    }

    public static long getTimeInMilliSeconds(String duration){
        long timeInMilliSeconds =0;

        if(!TextUtils.isEmpty(duration)){

                timeInMilliSeconds = Long.parseLong(duration)*1000  ;

        }

        return timeInMilliSeconds;
    }



    //TODO implemented maintenance mode
    public static  void callEventLogApi(final String logInfo){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.ADD_EVENT_LOG, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(WealthDragonsOnlineApplication.sharedInstance());
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("res",response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("res",error.toString());

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.MESSAGE_LOG,logInfo);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    //TODO implemented maintenance mode
    public static void getAppVersionCodeFromServer(final Activity actvity) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.CHECK_APP_VERSION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(WealthDragonsOnlineApplication.sharedInstance());
                        return;
                    }
                    onAppVersionServerResponse(response,actvity);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    private static void onAppVersionServerResponse(String response, final Activity activity) throws Exception {

        JSONObject jsonResultObject = new JSONObject(response);
        float serverVersionCode = Float.parseFloat(jsonResultObject.getString(Keys.VERSION));

        PackageInfo packageInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
        float appVersionCode = Float.parseFloat(packageInfo.versionName);

        if (appVersionCode < serverVersionCode ) {
            Utils.showErrorDialog(activity, activity.getString(R.string.update_title), activity.getString(R.string.update_body) + " " + serverVersionCode, R.string.update, -1, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Uri uri = Uri.parse(Constants.APP_MARKET_URL);
                    Utils.openWebLink(activity,uri);
                }
            }, null);
        }
    }


    public static boolean isAppRunning(final Context context) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null)
        {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(context.getPackageName())) {
                    return true;
                }
            }
        }
        return false;
    }


    //TODO implemented maintenance mode
    public static void callDeepLinkApi(final Uri url) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.DEEP_LINK_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(WealthDragonsOnlineApplication.sharedInstance());
                        return;
                    }
                    if(response!=null) {
                        DeepLink deepLinkData = new Gson().fromJson(response, DeepLink.class);
                        AppPreferences.setDeepLinkData(deepLinkData);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                // currently using constant url
//                params.put(Keys.URL,url.toString());
                params.put(Keys.URL,url.toString());

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    public static  boolean isFingerPrintAvailable(Activity activity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            KeyguardManager keyguardManager = (KeyguardManager) activity.getSystemService(KEYGUARD_SERVICE);
            FingerprintManager fingerprintManager = (FingerprintManager) activity.getSystemService(FINGERPRINT_SERVICE);

            if(fingerprintManager !=null || keyguardManager !=null) {

            return  false;

            }


                if (!fingerprintManager.isHardwareDetected()) {
                    return false;

                } else if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                    return false;

                } else if (!fingerprintManager.hasEnrolledFingerprints()) {
                    return false;

                } else return keyguardManager.isKeyguardSecure();


        }else{
            return false;

        }
    }


    public static String getMinHourSecFromSeconds(long biggy){

        String minHourSecText = null;

            int hours = (int) biggy / 3600;
            int remainder = (int) biggy - hours * 3600;
            int mins = remainder / 60;
            remainder = remainder - mins * 60;
            int secs = remainder;

            if(hours>0){
                minHourSecText = new DecimalFormat("00").format(hours)+":"+new DecimalFormat("00").format(mins)+":"+new DecimalFormat("00").format(secs);
            }else{
                minHourSecText = new DecimalFormat("00").format(mins)+":"+new DecimalFormat("00").format(secs);
            }


            return minHourSecText;
    }


    public static boolean checkForMaintainceMode(String response) throws JSONException {

        Object json = new JSONTokener(response).nextValue();
        if(json instanceof JsonObject) {

            JSONObject jsonObject = new JSONObject(response);
            if (!TextUtils.isEmpty(jsonObject.optString(Keys._MAINTAINANCE))) {

                String message = jsonObject.getString(Keys._MAINTAINANCE);
                if (message.equals(Keys.ON)) {
                    return true;
                } else {
                    AppPreferences.setMaintainceMode(false);
                    return false;
                }

            }
        }
        return false;
    }


    public static void showToast(String value, LayoutInflater context){
// Get the custom layout view.
//        View toastView = context.inflate(R.layout.activity_toast_custom_view, null);
//
//        // Initiate the Toast instance.
//        Toast toast = new Toast(WealthDragonsOnlineApplication.sharedInstance().getApplicationContext());
//        // Set custom view in toast.
//        toast.setView(toastView);
//        toast.setDuration(Toast.LENGTH_SHORT);
//        toast.setGravity(Gravity.CENTER, 0,0);
//        toast.show();
    }

    public static boolean hasNavBar (Resources resources)
    {
        int id = resources.getIdentifier("config_showNavigationBar", "bool", "android");
        return id > 0 && resources.getBoolean(id);
    }


    public static int getNavigationBarHeight(Resources resources){
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }


    public static Fetch getFetchInstance(){
        if(fetch==null || fetch.isClosed()) {
            FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(WealthDragonsOnlineApplication.sharedInstance())
                    .setDownloadConcurrentLimit(4)
                    .setAutoRetryMaxAttempts(5)
                    .setHttpDownloader(new OkHttpDownloader(Downloader.FileDownloaderType.PARALLEL))
                    .build();
            fetch = Fetch.Impl.getInstance(fetchConfiguration);
        }

        return fetch;
    }


    public static void addItemToFetchList(com.tonyodev.fetch2.Request request){
        if(fetch==null || fetch.isClosed()) {
            getFetchInstance();
        }
            fetch.enqueue(request, new Func<com.tonyodev.fetch2.Request>() {
                @Override
                public void call(com.tonyodev.fetch2.Request result) {
                }
            }, new Func<Error>() {
                @Override
                public void call(@NotNull Error result) {

                }
            });

    }

    public static void closeFetch(){
        fetch.close();
    }




}