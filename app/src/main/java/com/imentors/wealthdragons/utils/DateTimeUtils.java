package com.imentors.wealthdragons.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created on 8/09/17.
 */
public class DateTimeUtils {

    public static final DateTimeFormatter PUBLISH_DATE_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy").withZone
            (DateTimeZone.getDefault());

    public static final DateTimeFormatter EVENT_FORMAT = DateTimeFormat.forPattern("dd MMM yyyy").withZone
            (DateTimeZone.getDefault());

    public static final DateTimeFormatter EVENT_SHORT_FORMAT = DateTimeFormat.forPattern("dd MMM").withZone
            (DateTimeZone.getDefault());

    public static final DateTimeFormatter WEBINAR_FORMAT = DateTimeFormat.forPattern("dd MMM yyyy 'at' " +
            "HH:mm").withZone(DateTimeZone.getDefault());

    public static final DateTimeFormatter QUALIFICATION_MONTH_YEAR_FORMAT = DateTimeFormat.forPattern("MMMM yyyy")
            .withZone(DateTimeZone.getDefault());
    private static final DateTimeFormatter QUALIFICATION_DAY_FORMAT = DateTimeFormat.forPattern("d").withZone
            (DateTimeZone.getDefault());
    @SuppressWarnings("CanBeFinal")
    static SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

    private static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private static final SimpleDateFormat DEFAULT_TIME_FORMAT = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());

    private static final SimpleDateFormat HOUR_TIME_FORMAT = new SimpleDateFormat("hh:mm", Locale.getDefault());


    private static final SimpleDateFormat ORIGINAL_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static final DateTimeFormatter DEFAULT_JODA_DATE_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd").withZone
            (DateTimeZone.getDefault());

    private static final DateTimeFormatter ORIGINAL_JODA_DATE_FORMAT =  DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZone(DateTimeZone.getDefault());

    private DateTimeUtils() {
        /* cannot be instantiated */
    }

    public static DateTime getDateTime(String dateString) {
        try {
            return DateTime.parse(dateString).withZone(DateTimeZone.getDefault());
        } catch (Exception ex) {
            return new DateTime(new Date());
        }
    }

    /**
     * get date to display on screen
     *
     * @param dateString        Date in String Format
     * @param dateTimeFormatter Format in which date is required
     */

    public static String getDisplayDate(String dateString, DateTimeFormatter dateTimeFormatter) {
        try {
            DateTime dateTime = DateTime.parse(dateString).withZone(DateTimeZone.getDefault());
            return dateTimeFormatter.print(dateTime);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }


    public static String getTimeInHours(long timestamp) {

        try {
            Date date = new Date(timestamp);
            return HOUR_TIME_FORMAT.format(date);
        } catch (Exception ex) {
            return "";
        }
    }

    /**
     * get qualification date to display
     *
     * @param dateString date in string
     * @return date
     */
    public static String getQualificationDisplayDate(String dateString) {
        String monthYearString = getDisplayDate(dateString, QUALIFICATION_MONTH_YEAR_FORMAT);
        String dayString = getDisplayDate(dateString, QUALIFICATION_DAY_FORMAT);
        return dayString + getDayNumberSuffix(Utils.parseInt(dayString, 1)) + " " + monthYearString;
    }

    /**
     * get day Number suffix to display on alert
     *
     * @param day day count
     * @return suffix string
     */
    private static String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    /**
     * Date comparison
     *
     * @param dateString        first date
     * @param anotherDateString second date
     * @return resultant date
     */
    public static int compareDate(String dateString, String anotherDateString) {
        try {
            Date date = SIMPLE_DATE_FORMAT.parse(dateString);
            Date anotherDate = SIMPLE_DATE_FORMAT.parse(anotherDateString);
            return date.compareTo(anotherDate);

        } catch (ParseException e1) {
            return 0;
        }
    }


    /**
     * Date comparison
     *
     * @param dateString        first date
     * @return resultant date
     */
    public static int compareWithTodayDate(String dateString) {
        try {
            Date date = DEFAULT_DATE_FORMAT.parse(dateString);

            //compare only date
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            return date.compareTo(calendar.getTime());

        } catch (ParseException e1) {
            return 0;
        }
    }

    // get current date

    public static String getTodaysDate(){
        Date c = Calendar.getInstance().getTime();
        return DEFAULT_DATE_FORMAT.format(c);
    }


    /**
     * get time  in MS
     *
     * @param dateString date string
     * @return long
     */
    public static long getTimeInMS(String dateString) {

        String strDate = getDisplayDate(dateString, PUBLISH_DATE_FORMAT);

        long timeInMilliseconds = 0;

        try {
            Date mDate = SIMPLE_DATE_FORMAT.parse(strDate);
            timeInMilliseconds = mDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timeInMilliseconds;
    }

    /**
     * get difference between today and provided date
     *
     * @param strDate date to be compared
     * @return no.of days
     */
    public static int getDaysDifference(String strDate) {

        Calendar today = Calendar.getInstance();
        String strToday = DateTimeUtils.DEFAULT_DATE_FORMAT.format(today.getTime());

        try {
            Date dateToday = DEFAULT_DATE_FORMAT.parse(strToday);
            Date date = DEFAULT_DATE_FORMAT.parse(strDate);
            long diff = dateToday.getTime() - date.getTime();
            long daysDiff = diff / (24 * 60 * 60 * 1000);
            return (int) daysDiff;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return -1000;
    }


    public static String  getTimeDifference(String strDate){



//        DateTime defaultTimeFormat = ORIGINAL_JODA_DATE_FORMAT.parseDateTime(strDate);
//
//        DateTime dtd = new DateTime().withZone(DateTimeZone.getDefault());
//
//
//        int check = dtd.getHourOfDay();
//        int checke = defaultTimeFormat.getHourOfDay();
//
//
//        int diff = dtd.getHourOfDay() - defaultTimeFormat.getHourOfDay();
//        if(diff>0){
//            return Integer.toString((int)diff) + "h";
//        }else{
//           int minuteDiff =  dtd.getMinuteOfHour() - defaultTimeFormat.getMinuteOfHour();
//           if(minuteDiff>0){
//               return Integer.toString((int)diff) + "m";
//           }else{
//               return "";
//           }
//
//        }




        Calendar today = Calendar.getInstance();
        ORIGINAL_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));

        String strToday = DateTimeUtils.ORIGINAL_DATE_FORMAT.format(today.getTime());

        try {
            Date dateToday = ORIGINAL_DATE_FORMAT.parse(strToday);
            Date date = ORIGINAL_DATE_FORMAT.parse(strDate);
            long diff = dateToday.getTime() - date.getTime();


            if(diff > 60*60*1000){
                long timeDiff = diff / ( 60 * 60 * 1000);
                return (int) timeDiff + "h";

            }else{
                long timeDiff = diff / ( 60 * 1000);
                if(timeDiff > 5) {
                    return (int) timeDiff + "m";
                }else{
                    return " ";
                }

            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return " ";
    }

}
