package com.imentors.wealthdragons.utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.imentors.wealthdragons.models.DashBoardItemOdering;
import com.imentors.wealthdragons.models.DashBoardItemOderingArticle;
import com.imentors.wealthdragons.models.DashBoardItemOderingEvent;
import com.imentors.wealthdragons.models.DashBoardItemOderingMentors;
import com.imentors.wealthdragons.models.DashBoardItemOderingVideo;

import java.lang.reflect.Type;

public class ItemTypeDeserilization implements JsonDeserializer<DashBoardItemOdering> {


    @Override
    public DashBoardItemOdering deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();
        JsonElement type = jsonObject.get("layout");
        if (type != null) {
            switch (type.getAsString()) {
                case Constants.TYPE_COURSE:
                    return context.deserialize(jsonObject,
                            DashBoardItemOderingArticle.class);
                case Constants.TYPE_VIDEO:
                    return context.deserialize(jsonObject,
                            DashBoardItemOderingVideo.class);
                case Constants.TYPE_MENTOR:
                    return context.deserialize(jsonObject,
                            DashBoardItemOderingMentors.class);
                case Constants.TYPE_PROGRAMMES:
                    return context.deserialize(jsonObject,
                            DashBoardItemOderingArticle.class);
                default: return context.deserialize(jsonObject,
                        DashBoardItemOderingEvent.class);
            }
        }

        return null;
    }
}
