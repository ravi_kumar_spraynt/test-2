package com.imentors.wealthdragons.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.facebook.FacebookSdk.getApplicationContext;

public class CacheImages extends AsyncTask<Bitmap,String,String> {


    private static  final String DIRECTORY_NAME = "WEALTH_DRAGON_IMAGES";


    private String mKey;

    public CacheImages(String key){
        mKey = key;
    }

    @Override
    protected String doInBackground(Bitmap... bitmaps) {
        return saveImagesToInternalStorage(bitmaps[0]);
    }

    private String saveImagesToInternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir(DIRECTORY_NAME, Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,mKey);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(fos!=null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }


    @Override
    protected void onPostExecute(String path) {
        super.onPostExecute(path);
        AppPreferences.setImagesPath(mKey,path);

    }
}
