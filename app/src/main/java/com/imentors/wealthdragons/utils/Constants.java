package com.imentors.wealthdragons.utils;

public class Constants {

    // broadcast message for dismissing alert dialog on login page
    public static final String BROADCAST_DISMISS_ALERT_DIALOG = "dismiss_alert_dialog";
    public static final String BROADCAST_TABLAYOUT_POSITION = "tablayout_position";
    public static final String BROADCAST_DASHBOARD_TAB_LIST_UPDATED = "list_updated";
    public static final String BROADCAST_OTHER_TAB_LIST_UPDATED = "other_tab_list_updated";
    public static final String BROADCAST_SEE_ALL_LIST_UPDATED = "see_all_list_updated";
    public static final String BROADCAST_SAVE_VIDEO_POSITION = "save_video_position";
    public static final String BROADCAST_MARK_RUNNING_TRACK = "mark_running_track";
    public static final String BROADCAST_CHECK_FREE_PAID_TRACK = "check_free_paid";
    public static final String BROADCAST_CLEAR_REPONSE = "clear_response";
    public static final String BROADCAST_IS_AUDIO_ACTIVE = "is_audio_active";
    public static final String BROADCAST_REFRESH_LIST = "refresh_list";
    public static final String WELCOME_MODEL_DATA = "WELCOME_MODEL_DATA";
    public static final String ITEM_POSITION = "ITEM_POSITION";
    public static final String SIGNING_MODEL_DATA = "SIGNING_MODEL_DATA";
    public static final String PAGE_POSITION = "PAGE_POSITION";
    public static final String CLICK_LISTENER = "CLICK_LISTENER";
    public static final String IS_NEW_PAGER = "IS_NEW_PAGER";
    public static final String TABLAYOUT_POSITION = "TABLAYOUT_POSITION";
    public static final String QUERY = "QUERY";
    public static final String IS_SPLASH = "IS_SPLASH";
    public static final String IS_CATEGORY_CHOOSER = "IS_CATEGORY_CHOOSER";
    public static final String TOTAL_ITEM = "TOTAL_ITEM";
    public static final String NEXT_PAGE = "NEXT_PAGE";
    public static final String DIALOG = "dialog";
    public static final String LOG = "log";
    public static final String STATUS = "Status";

    // gson helping types
    public static final String TYPE = "type";
    public static final String VIDEO_ERROR = "video_error";
    public static final String BROADCAST_VIDEO_BUFFERING = "video_buffering";
    public static final String BROADCAST_VIDEO_BUFFERING_STOP = "video_buffering_stop";
    public static final String BROADCAST_VIDEO_POSITION_CHANGED = "BROADCAST_VIDEO_POSITION_CHANGED";
    public static final String BROADCAST_PAY_FINSIH = "BROADCAST_PAY_FINSIH";
    public static final String BROADCAST_LIVE_CHAT_FINSIH = "BROADCAST_LIVE_CHAT_FINSIH";
    public static final String BROADCAST_WISHLIST_RELOAD = "BROADCAST_WISHLIST_RELOAD";
    public static final String BROADCAST_VIDEO_ENDED = "video_ended";
    public static final String BROADCAST_DETAIL_SET = "BROADCAST_DETAIL_SET";
    public static final String SUBSCRIPTION = "SUBSCRIPTION";
    public static final String COURSE_DETAIL_RELOAD = "COURSE_DETAIL_RELOAD";
    public static final String TYPE_COURSE = "Course";
    public static final String TYPE_VIDEO = "Video";
    public static final String TYPE_EVENT = "Event";
    public static final String TYPE_MENTOR = "Mentor";
    public static final String TYPE_PROGRAMMES = "Programme";
    public static final String TYPE_EPISODE = "Episode";
    public static final String TYPE_EXPERT = "Expert";
    public static final String TYPE_TAG = "Tag";
    public static final String TYPE_CHAPTER = "Chapter";
    public static final String TYPE_COURSES = "All Courses";
    public static final String TYPE_OLD = "TYPE_OLD";
    public static final String TYPE_MENTORS = "Mentors";
    public static final String TYPE_ALL_MENTORS = "All Mentors";
    public static final String TYPE_SEE_ALL_COURSES = "See_All_Courses";
    public static final String TYPE_SEE_ALL_EVENTS = "See_All_Events";
    public static final String TYPE_REVIEW = "review";
    public static final String TYPE_SEARCH = "search";
    public static final String TYPE_COURSE_DASHBOARD_DETAIL_RELATED_COURSE = "course_detail_related_course";
    public static final String TYPE_COURSE_DASHBOARD_DETAIL_PROGRAMME_COURSE = "course_detail_related_programme";
    public static final String TYPE_MENTOR_DASHBOARD_DETAIL_PROGRAMME = "mentor_detail_programme";
    public static final String TYPE_MENTOR_DASHBOARD_DETAIL_COURSE = "mentor_detail_course";
    public static final String TYPE_MENTOR_DASHBOARD_DETAIL_VIDEO = "mentor_detail_video";
    public static final String TYPE_MENTOR_DASHBOARD_DETAIL_EVENT = "mentor_detail_event";
    public static final String TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING = "mentor_detail_digital_coaching";
    public static final String TYPE_WISHLLIST_FEATURED_COURSES = "wishlist_featured_courses";
    public static final String TYPE_WISHLLIST_FEATURED_PROGRAMMES = "wishlist_featured_programmes";
    public static final String TYPE_WISHLLIST_FEATURED_EVENTS = "wishlist_featured_events";
    public static final String TYPE_PURCHASE_FEATURED_COURSES = "purchase_featured_courses";
    public static final String TYPE_PURCHASE_FEATURED_PROGRAMMES = "purchase_featured_programmes";
    public static final String TYPE_WISHLLIST_FEATURED_VIDEOS = "wishlist_featured_videos";
    public static final String TYPE_WISHLLIST_FEATURED_MENTORS = "wishlist_featured_mentors";
    public static final String TAB_POSITION = "TAB_POSITION";
    public static final int COURSE_TAB = 3;
    public static final String DOWNLOAD = "download";
    public static final String STARTING_BOLD = "<b>";
    public static final String ENDING_BOLD = "</b>";


    // Dashboard Tab
    public static final int VIDEO_TAB = 2;
    public static final int MENTOR_TAB = 1;
    public static final int DASHBOARD_TAB = 0;
    public static final int SEE_ALL_COURSES_TAB = 4;
    public static final int SEE_ALL_EVENT_TAB = 6;
    public static final int DASHBOARD_DETAIL_TAB = 5;
    public static final int SEARCH_TAB = 7;
    public static final int PURCHASE_TAB = 8;
    public static final String SEE_ALL_API = "SEE_ALL_API";
    public static final String CAT_ID = "CAT_ID";
    public static final String COURSE_TITLE = "COURSE_TITLE";
    public static final String IS_LOGIN_ACTIVITY = "IS_LOGIN_ACTIVITY";
    public static final String IS_PAYMENT_CARD_LIST_DIALOG = "IS_PAYMENT_CARD_LIST_DIALOG";
    public static final String SEE_ALL_KEY = "SEE_ALL_KEY";
    public static final String CATEGORY_NAME = "CATEGORY_NAME";
    public static final String MESSAGE_ID = "MESSAGE_ID";
    public static final String CHAPTER = "Chapter";
    public static final String SELECTED_TAB = "SELECTED_TAB";
    public static final String DETAIL_KEY = "DETAIL_KEY";
    public static final String EPISODE_KEY = "EPISODE_KEY";
    public static final String IS_DOWNLOADED = "IS_DOWNLOADED";
    public static final String SAVED_RESPONSE = "SAVED_RESPONSE";
    // See All Keys
    public static final String COURSES_KEY = "all_course";
    public static final String MENTORS_KEY = "all_mentors";
    public static final String VIDEOS_KEY = "all_videos";
    public static final String ALL_PROGRAMMES_KEY = "all_programmes";
    public static final String ALL_EVENTS_KEY = "all_events";
    public static final String ON_SALE_KEY = "all_onsale";
    public static final String ALL_UPCOMING_COURSE = "all_upcoming_course";
    public static final String COURSES_YOU_MAY_LIKE = "all_course_you_may_like";
    //Search Screen
    public static final String SEARCH_ITEM = "SEARCH_ITEM";
    public static final String SEARCH_TYPE = "SEARCH_TYPE";
    public static final String SEARCH_ID = "SEARCH_ID";
    public static final String SEARCH_IS_AUTOSEARCH = "SEARCH_IS_AUTOSEARCH";
    //detail Screen
    public static final String STATE_RESUME_WINDOW = "resumeWindow";
    public static final String STATE_RESUME_POSITION = "resumePosition";
    public static final String VIDEO_URL_LIST = "video_url_list";
    public static final String DASHBOARD_COURSE_DATA = "DASHBOARD_COURSE_DATA";
    public static final String DASHBOARD_VIDEO_DATA = "DASHBOARD_VIDEO_DATA";
    public static final String VIDEO_TITLE = "VIDEO_TITLE";
    public static final String STATE_VIDEO_DATA = "STATE_VIDEO_DATA";
    public static final String IS_COURSEDETAIL = "VIDEO_TITLE";
    public static final String THUMB_URL = "THUMB_URL";
    public static final String POSTER_IMAGE_URL = "POSTER_IMAGE_URL";
    public static final String TYPE_DETAIL_VIDEO = "Video";
    public static final String TYPE_DETAIL_AUDIO = "Audio";
    public static final String TYPE_DETAIL_QUIZ = "Quiz";
    public static final String TYPE_DETAIL_PDF = "Pdf";
    public static final String TYPE_DETAIL_EXCEL = "Excel";
    public static final String IS_TOP_FRAGMENT = "IS_TOP_FRAGMENT";
    public static final String IS_ECLASSES_VIDEO = "IS_ECLASSES_VIDEO";
    public static final String ECLASSES_VIDEO_ID = "ECLASSES_VIDEO_ID";
    public static final String IS_VIDEO_PLAYED = "IS_VIDEO_PLAYED";
    public static final String IS_FULL_SCREEN = "IS_FULL_SCREEN";
    public static final String IS_AUTOPLAY = "IS_AUTOPLAY";
    public static final String QUIZ_ID = "QUIZ_ID";
    public static final String TRUE_FALSE = "TrueAndFalse";


    //Quiz
    public static final String FILL_IN_BLANKS = "FillInTheBlank";
    public static final String MULTIPLE_CHOICE_RADIO = "MultipleChoiceRadio";
    public static final String MULTIPLE_CHOICE_CHECKBOX = "MultipleChoiceCheckbox";
    public static final String SUPPORT = "SUPPORT";
    public static final int COUNTRY_CODE_DIALOG = 1;
    public static final String COUNTRY_CODE = "COUNTRY_CODE";
    public static final String SERVICE_TYPE = "SERVICE_TYPE";
    public static final String SERVICE_TYPE_ID = "SERVICE_TYPE_ID";
    public static final int SERVICE_TYPE_DIALOG = 2;
    // settings
    public static final String LOW = "Low";
    public static final String MEDIUM = "Medium";
    public static final String HIGH = "High";
    public static final String MANUAL = "Manual";
    public static final String AUTO_PLAY = "Auto Play";
    public static final String ORDER_DATA = "ORDER_DATA";
    public static final String BADGE_DATA = "BADGE_DATA";
    public static final String APP_MARKET_URL = "market://details?id=com.imentors.wealthdragons";
    public static final String MAIN_ACTIVITY_STATE = "MAIN_ACTIVITY_STATE";
    public static final String NOTIFICATION_CLICKED = "NOTIFICATION_CLICKED";
    public static final String SHOW_NOTIFICATION_DIALOG = "SHOW_NOTIFICATION_DIALOG";
    public static final String CUSTOMER_SUPPORT_NOTIFICATION = "CustomerSupport";
    public static final String LIVE_NOTIFICATION_DIALOG = "Live Session";
    public static final String CURRENCY = "currency";
    public static final String ITEM_ID = "item_id";
    public static final String ITEM_TYPE = "item_type";
    public static final String RESULT = "result";
    public static final String PENDING = "pending";
    public static final String FAILED = "failed";
    public static final String FINISH = "FINISH";
    public static final String STARTED = "STARTED";
    public static final String TITLE = "TITLE";
    public static final String ID = "ID";
    public static final String BANNER_PATH = "BANNER_PATH";
    public static final String PUNCH_LINE = "PUNCH_LINE";
    public static final String MENTOR_NAME = "MENTOR_NAME";
    public static final String VIDEO_URL = "VIDEO_URL";
    public static final String IMAGE_URL = "IMAGE_URL";
    public static final String VIDEO_PATH = "VIDEO_PATH";
    public static final String VIDEO_DURATION = "VIDEO_DURATION";
    public static final String GROUP_IMAGE_PATH = "GROUP_IMAGE_PATH";
    public static final String GROUP_TITLE = "GROUP_TITLE";
    public static final String GROUP_ID = "GROUP_ID";
    public static final String GROUP_TYPE = "GROUP_TYPE";
    public static final String DATA = "DATA";
    // for subscription dialog
    public static final String EVERY_TIME = "Every Time";
    public static final String EVERY_DAY = "Every Day";
    public static final String ALTERNATE_DAY = "Every Alternate Day";
    public static final String EVERY_WEEK = "Every Week";
    // Course detail new items
    public static final String COURSE_LEARNING = "COURSE_LEARNING";
    public static final String COURSE_REQUIREMENTS = "COURSE_REQUIREMENTS";
    public static final String COURSE_TARGET_AUDINECE = "COURSE_TARGET_AUDINECE";
    public static final String COURSE_FAQS = "COURSE_FAQS";
    public static final String COURSE_REGION = "COURSE_REGION";
    // Mentor detail new items
    public static final String MENTOR_EXPERIENCE = "MENTOR_EXPERIENCE";
    public static final String MENTOR_HONORS = "MENTOR_HONORS";
    public static final String MENTOR_SKILLS = "MENTOR_SKILLS";
    public static final String MENTOR_ANNOUNCEMRNTS = "MENTOR_ANNOUNCEMRNTS";
    public static final String SAD = "sademoji";
    public static final String LIKE = "likeemoji";


    //Emojis Name
    public static final String ANGRY = "angryemoji";
    public static final String LOVE = "loveemoji";
    public static final String HAHA = "hahaemoji";
    public static final String WOW = "wowemoji";
    public static final int MAX_ALPHA = 255;
    public static final String MENTOR_ABOUT = "6";
    public static final String MENTOR_DIGITAL_COACHING_LIST = "5";
    public static final String MENTOR_DIGITAL_COACHING_INFO = "13";
    public static final String MENTOR_DIGITAL_COACHING_BUTTON = "11";
    public static final String MENTOR_COURSES_LIST = "2";


    // Detail page Mentor
    public static final String MENTOR_EVENTS_LIST = "4";
    public static final String MENTOR_PROGRAMMES_LIST = "3";
    public static final String MENTOR_EDUCATION = "10";
    public static final String MENTOR_EXPERINECE = "8";
    public static final String MENTOR_OTHERS = "17";
    public static final String MENTOR_INTRODUCTION_VIDEO = "1";
    public static final String MENTOR_DIGITAL_COACHING_INTRODUCTION_VIDEO = "12";
    public static final String MENTOR_ABOUT_SKILLS = "14";
    public static final String MENTOR_ABOUT_HONORS = "15";
    public static final String TOP_COLLECTIONS = "16";
    public static String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";
    // fb emoji animation
    public static int HORIZONTAL_SPACING = Utils.dpToPx(4);
    public static int VERTICAL_SPACING = Utils.dpToPx(6);
    public static int HEIGHT_VIEW_REACTION = Utils.dpToPx(390);

    private Constants() {
        /* cannot be instantiated */
    }


}
