package com.imentors.wealthdragons.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Button;

import com.imentors.wealthdragons.models.DownloadFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;


public class DownloadTask {

    private static final String TAG = "Download Task";
    private Context context;
    private String downloadUrl = "", downloadFileName = "";
    private DownloadFile mDownLoadFile;

    public DownloadTask(Context context, DownloadFile downloadFile) {
        this.context = context;
        this.downloadUrl = downloadFile.getVideoUrl();
        mDownLoadFile = downloadFile;
        AppPreferences.addDownloadListItem(mDownLoadFile);

        downloadFileName = downloadFile.getTitle();//Create file name by picking download file name from URL
        Log.e(TAG, downloadFileName);

        //Start Downloading Task
         AsyncTask task = new DownloadingTask().execute();
    }

    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }


        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (outputFile != null) {
                    //If Download completed then change button text
                    AppPreferences.removeDownloadListItem(mDownLoadFile);
//                    mDownLoadFile.setDownloaded(true);
                    AppPreferences.addDownloadListItem(mDownLoadFile);
                } else {
                    //If download failed change button text
                    Log.e("Downlaod", "Download Failed");
                    AppPreferences.removeDownloadListItem(mDownLoadFile);


                }
            } catch (Exception e) {
                e.printStackTrace();
                AppPreferences.removeDownloadListItem(mDownLoadFile);
                Log.e("Downlaod", "Download Error Exception " + e.getMessage());


                //Change button text if exception occurs

            }


            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(downloadUrl);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection

                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e("Downlaod", "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());

                }




                outputFile = new File(context.getFilesDir(), downloadFileName);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e("Downlaod", "File Created");
                }

                AppPreferences.removeDownloadListItem(mDownLoadFile);

                mDownLoadFile.setVideoAddress(outputFile.getPath());
                AppPreferences.addDownloadListItem(mDownLoadFile);


                FileOutputStream fos = context.openFileOutput(downloadFileName, Context.MODE_PRIVATE);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {

                    if(isCancelled()){
                        Log.e("Downlaod", "Download Error Exception ");

                    }

                    fos.write(buffer, 0, len1);//Write new file
                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e("Downlaod", "Download Error Exception " + e.getMessage());
                AppPreferences.removeDownloadListItem(mDownLoadFile);

            }

            return null;
        }
    }
}
