package com.imentors.wealthdragons.eventDetailView;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.DashBoardEventDetails;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.WealthDragonTextView;

public class EventDetailDescriptionBelow extends LinearLayout {


    private Context mContext;
    private WealthDragonTextView mTvEndDate, mTvStartDate , mTvStartTime ,mTvEndTime , mTvCost , mTvAddress , mTvZone , mTvCountry , mTvCity;




    public EventDetailDescriptionBelow(Context context) {
        super(context);
        mContext = context;
    }

    public EventDetailDescriptionBelow(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public EventDetailDescriptionBelow(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public EventDetailDescriptionBelow(Context context, DashBoardEventDetails dashBoardDetails) {
        super(context);
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set data in view
    private void bindView(final DashBoardEventDetails dashBoardDetails) {

        mTvAddress.setText(dashBoardDetails.getEvent().getAddress());
        if(dashBoardDetails.getEvent().getFree_paid().equals(mContext.getString(R.string.free))){
            mTvCost.setText(mContext.getString(R.string.free_registration));
        }else{
            mTvCost.setText("£" + dashBoardDetails.getEvent().getCost());
        }
        mTvEndTime.setText(Utils.getEventDetailTime(dashBoardDetails.getEvent().getEnd_date()));
        mTvStartTime.setText(Utils.getEventDetailTime(dashBoardDetails.getEvent().getStart_date()));
        mTvStartDate.setText(Utils.getEventDetailDate(dashBoardDetails.getEvent().getStart_date()));
        mTvEndDate.setText(Utils.getEventDetailDate(dashBoardDetails.getEvent().getEnd_date()));
        mTvZone.setText(dashBoardDetails.getEvent().getZone());
        mTvCountry.setText(dashBoardDetails.getEvent().getCountry());
        mTvCity.setText(dashBoardDetails.getEvent().getCity());

    }

    //  initialize event detail description below view
    private void initViews(Context context ) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.event_detail, this);
        mContext = context;

        mTvStartDate = view.findViewById(R.id.tv_start_date);
        mTvEndDate = view.findViewById(R.id.tv_end_date);
        mTvAddress = view.findViewById(R.id.tv_address);
        mTvCost = view.findViewById(R.id.tv_cost);
        mTvStartTime = view.findViewById(R.id.tv_start_time);
        mTvEndTime = view.findViewById(R.id.tv_end_time);
        mTvCity = view.findViewById(R.id.tv_city);
        mTvCountry = view.findViewById(R.id.tv_country);
        mTvZone = view.findViewById(R.id.tv_zone);

    }



}
