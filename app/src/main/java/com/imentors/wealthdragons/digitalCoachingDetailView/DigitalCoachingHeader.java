package com.imentors.wealthdragons.digitalCoachingDetailView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;

import android.content.pm.ActivityInfo;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.BaseActivity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.fragments.DashBoardDetailFragment;
import com.imentors.wealthdragons.interfaces.OpenFullScreenVideoListener;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.DigitalCoachingDetails;
import com.imentors.wealthdragons.models.VideoData;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DigitalCoachingHeader extends LinearLayout {


    private Activity mActivity;
    private Context mContext;
    private ImageView mIvVideoPlay;
    private ImageView mIvCourseImage;
    private PlayerView mExoPlayerView;
    private FrameLayout mVideoFrameLayout;
    private MediaSource mVideoSource;
    private FrameLayout mFullScreenButton,mDialogFullScreenButton;
    private ImageView mFullScreenIcon, mPlayIcon;
    private SimpleExoPlayer mPlayer;
    private DigitalCoachingDetails mDashBoardDetails;

    private long mResumePosition;
    private String mVideoUrl;
    private OpenFullScreenVideoListener mOpenFullScreenVideoListener;
    private List<String> mVideoUrls = new ArrayList<>();
    private boolean mExoPlayerFullscreen, mIsVideoError = false;
    private Dialog mFullScreenDialog;
    private PlayerView mDialogplayerView;
    private boolean isPotrait;
    private boolean isLandScape;
    private WealthDragonTextView mVideoError;
    private ImageButton mPlayImageButton;


    private String mVideoNameForListenerLog;
    private LinearLayout nextVideoFrame;
    private TextView mNextVideo;
    private RelativeLayout mLlHeadingContainer;
    private TextView mTvEClassNameSmallVideo;
    private ImageView dialogPosterImage;
    private ProgressBar mProgressBar;
    private RelativeLayout rlFrameError;

    public DigitalCoachingHeader(Context context) {
        super(context);
        mContext = context;
    }

    public DigitalCoachingHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public DigitalCoachingHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    public DigitalCoachingHeader(Context context, DigitalCoachingDetails dashBoardDetails, Activity activity) {
        super(context);
        mActivity = activity;
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set data in view
    private void bindView(DigitalCoachingDetails dashBoardDetails) {

        mDashBoardDetails = dashBoardDetails;


        if (!TextUtils.isEmpty(mDashBoardDetails.getDigi_intro_video()) && !dashBoardDetails.getDigi_intro_video().equals("NoVideoExist")) {
            // add only intro video in list
            mVideoUrl = mDashBoardDetails.getDigi_intro_video();
            mVideoUrls.add(mVideoUrl);
        }

            Glide.with(mContext).load(dashBoardDetails.getIntro_video_banner()).dontAnimate().listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    rlFrameError.setVisibility(VISIBLE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    return false;
                }
            }).into(mIvCourseImage);




        mResumePosition = AppPreferences.getVideoResumePosition(mDashBoardDetails.getMentor_id());

        mVideoNameForListenerLog = "VIP CLUB";
        mTvEClassNameSmallVideo.setText(mVideoNameForListenerLog);







        if (!TextUtils.isEmpty(dashBoardDetails.getDigi_intro_video())  && !dashBoardDetails.getDigi_intro_video().equals("NoVideoExist")) {
            mIvVideoPlay.setVisibility(View.GONE);
            mExoPlayerView.setVisibility(View.VISIBLE);
            mVideoError.setVisibility(INVISIBLE);
            rlFrameError.setVisibility(INVISIBLE);

            callVideoUrl(dashBoardDetails.getMentor_id());
            initFullscreenButton();
            initFullscreenDialog();

        } else {
            mIvCourseImage.setVisibility(INVISIBLE);

            mIvVideoPlay.setVisibility(View.INVISIBLE);
            rlFrameError.setVisibility(VISIBLE);

        }



        mIvVideoPlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvCourseImage.setVisibility(INVISIBLE);

                mIvVideoPlay.setVisibility(View.INVISIBLE);
                mExoPlayerView.setVisibility(VISIBLE);
                callVideoUrl(mDashBoardDetails.getMentor_id());
            }
        });


    }

    public void pauseVideo() {
        if (mPlayer != null) {
            mPlayer.setPlayWhenReady(false);
        }

    }


    //  initialize video detail header view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (context instanceof OpenFullScreenVideoListener) {
            mOpenFullScreenVideoListener = (OpenFullScreenVideoListener) context;
        }



        View view = inflater.inflate(R.layout.digital_detail_header, this);
        mContext = context;

        // heading
        mVideoError = view.findViewById(R.id.video_error);
        mProgressBar = view.findViewById(R.id.progress_bar);
        rlFrameError = view.findViewById(R.id.frame_error);

        // video Frame
        mVideoFrameLayout = view.findViewById(R.id.frame_video);
        mVideoFrameLayout.getLayoutParams().height = (int) Utils.getDetailScreenImageHeight((Activity) mContext);


        mIvVideoPlay = view.findViewById(R.id.iv_video_play);
        mIvVideoPlay.setVisibility(INVISIBLE);

        mIvCourseImage = view.findViewById(R.id.iv_video_image);
        mIvCourseImage.setVisibility(INVISIBLE);
        mExoPlayerView = findViewById(R.id.exoplayer);
        mTvEClassNameSmallVideo = mExoPlayerView.findViewById(R.id.tv_eclasses_title);

    }



    private void bufferingListener(){
        if(mExoPlayerView.getVisibility() == View.INVISIBLE) {
            mProgressBar.setVisibility(VISIBLE);
            mIvCourseImage.setVisibility(VISIBLE);
        }
    }


    private void bufferingStopListener(){
        if (mIsVideoError) {
            mPlayImageButton.setActivated(false);
            mVideoError.setVisibility(VISIBLE);
            rlFrameError.setVisibility(VISIBLE);
            mExoPlayerView.setVisibility(View.INVISIBLE);
            mProgressBar.setVisibility(INVISIBLE);
        } else {
            if(mExoPlayerView.getVisibility() == View.INVISIBLE) {

                mProgressBar.setVisibility(INVISIBLE);
                mExoPlayerView.setVisibility(VISIBLE);
                mExoPlayerView.setControllerAutoShow(true);
                mExoPlayerView.setUseController(true);
                mIvCourseImage.setVisibility(INVISIBLE);
            }
        }
    }



    private void videoErrorListener(){
        mIsVideoError = true;
        if(Utils.isNetworkAvailable(mContext)) {

            if (mPlayer != null) {
                if (mPlayer.getPlayWhenReady()) {
                    try {
                        if (isLandScape) {
                            closeFullscreenDialog();
                        }

                        mPlayImageButton.setActivated(false);
                        mIvCourseImage.setVisibility(View.INVISIBLE);
                        mVideoError.setVisibility(VISIBLE);
                        rlFrameError.setVisibility(VISIBLE);

                        mExoPlayerView.setVisibility(View.INVISIBLE);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }else{
            if (isLandScape) {
                closeFullscreenDialog();
            }
            Utils.showNetworkError(mContext, new NoConnectionError());

        }
    }




    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (mExoPlayerView != null && mPlayer != null && mExoPlayerView.getPlayer() != null) {
            mPlayer.release();
            mPlayer = null;

            // releasing full screen player too
            Utils.releaseFullScreenPlayer();
            Utils.releaseDetailScreenPlayer();
        }

    }


    private void initFullscreenButton() {

        mFullScreenIcon = mExoPlayerView.findViewById(R.id.exo_fullscreen_icon);
        mPlayImageButton = mExoPlayerView.findViewById(R.id.exo_play);

        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_expand));
        mFullScreenButton = mExoPlayerView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                openFullScreenVideo(Surface.ROTATION_0);
            }

        });
    }


    //  opened full screen video mode
    public void openFullScreenVideo(int rotationAngle) {
         if (!mExoPlayerFullscreen) {
            if (!isLandScape) {
                openFullscreenDialog(rotationAngle);
            }
        } else {
            if (!isPotrait) {
                closeFullscreenDialog();
            }
        }
    }







    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }


    private void openFullscreenDialog(int rotationAngle) {
        Utils.callEventLogApi("clicked <b>" + "Full Screen Video" + " Mode</b> in Player from " + "Digital Coaching");

        isPotrait = false;
        isLandScape = true;
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.activity_video_fullscreen, null);
        mDialogplayerView = dialogView.findViewById(R.id.exoplayer_fullScreen);
        mNextVideo = dialogView.findViewById(R.id.tv_nxtvideo);
        mNextVideo.setVisibility(GONE);

        dialogPosterImage = dialogView.findViewById(R.id.iv_poster_image);

        mLlHeadingContainer = dialogView.findViewById(R.id.header_holder);

        mNextVideo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mPlayer.getNextWindowIndex() != -1) {
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());

                }

            }
        });
        nextVideoFrame = dialogView.findViewById(R.id.next_video_image_holder);
        nextVideoFrame.setVisibility(GONE);
        TextView tvEClassName = dialogView.findViewById(R.id.tv_eclasses_title);

        tvEClassName.setText("VIP CLUB");
        ImageView dialogCourseImage = dialogView.findViewById(R.id.iv_video_image);


        // on Next Click
        nextVideoFrame.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer.getNextWindowIndex() != -1) {
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());

                }
            }
        });

        initDialogFullScreenButton();
        PlayerView.switchTargetView(mExoPlayerView.getPlayer(), mExoPlayerView, mDialogplayerView);
        mFullScreenDialog.addContentView(dialogView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
        Log.d("orientation", "lanscape");

    }

    //  closed full screen dialog
    public void closeFullscreenDialog() {
        Utils.callEventLogApi("clicked <b>" + "Compact Screen Video" + " Mode</b> in Player from " + "Digital Coaching");

        isPotrait = true;
        isLandScape = false;
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        PlayerView.switchTargetView(mDialogplayerView.getPlayer(), mDialogplayerView, mExoPlayerView);
        mFullScreenDialog.dismiss();
        mExoPlayerFullscreen = false;
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_expand));
        Log.d("orientation", "potrait");

        if (mPlayer.getPlaybackState() == Player.STATE_ENDED) {

            mExoPlayerView.setVisibility(GONE);
            mVideoError.setVisibility(View.INVISIBLE);
            rlFrameError.setVisibility(INVISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
            mIvCourseImage.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(mDashBoardDetails.getIntro_video_banner()).dontAnimate().error(R.drawable.no_img_mob).into(mIvCourseImage);
            mIvVideoPlay.setVisibility(VISIBLE);
        }

    }


    private void initDialogFullScreenButton() {

        mDialogplayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if (visibility == View.VISIBLE) {
                    mLlHeadingContainer.setVisibility(VISIBLE);
//                    if (mPlayer.getNextWindowIndex() != -1 && mPlayer.getRepeatMode() == Player.REPEAT_MODE_ALL) {
//                        mNextVideo.setVisibility(VISIBLE);
//                    } else {
//                        mNextVideo.setVisibility(GONE);
//                    }
                    mNextVideo.setVisibility(GONE);
                } else {
                    mLlHeadingContainer.setVisibility(GONE);

                }
            }
        });
        mFullScreenIcon = mDialogplayerView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_skrink));
        mDialogFullScreenButton = mDialogplayerView.findViewById(R.id.exo_fullscreen_button);
        mDialogFullScreenButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFullscreenDialog();
            }

        });

    }

    public void pressFullScreenDialogButton(){

        mDialogFullScreenButton.performClick();

    }





    // for internal use
    private void initExoPlayer() {

        // get position from preferences

        Utils.releaseDetailScreenPlayer();
        mPlayer = null;
        ArrayList<String> mVideoUrlList = new ArrayList<>();
        mVideoUrlList.add(mVideoUrl);

        Utils.initilizeDetailScreenConcatinatedPlayer(mContext, mVideoUrlList, null, false, Constants.TYPE_EXPERT, "Digital Coaching", false);


        mPlayer = Utils.getDetailScreenConcatinatedPlayer();


        mPlayer.clearVideoSurface();

        Utils.callEventLogApi("started video of <b>" +mVideoNameForListenerLog+"</b>" );


        mExoPlayerView.setPlayer(mPlayer);






        mPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {


            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {





                if (playWhenReady) {
                } else {
                    Utils.callEventLogApi("paused <b>" + mVideoNameForListenerLog + "</b> mentor profile video ");
                }


                if (playbackState == Player.STATE_BUFFERING) {
                    bufferingListener();

                } else {

                    bufferingStopListener();
                }

                if(!((BaseActivity)mActivity).isFragmentInBackStack(new DashBoardDetailFragment())){
                    Utils.releaseDetailScreenPlayer();
                }

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                try {

                    Utils.callEventLogApi(error.getSourceException().getMessage() + " in " + mVideoNameForListenerLog + "</b>");

                }catch (Exception e){
                    e.printStackTrace();

                }
                videoErrorListener();


            }

            @Override
            public void onPositionDiscontinuity(int reason) {

                // for handling log
                switch (reason) {
                    case Player.DISCONTINUITY_REASON_SEEK:
                        Utils.callEventLogApi("scrolled to  " + "<b>" + Utils.getMinHourSecFromSeconds(mPlayer.getContentPosition() / 1000) + "</b>" + " from" + "<b> " + mVideoNameForListenerLog + "</b>");

                        break;
                }


            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {
                if (mPlayer.getPlaybackState() == Player.STATE_BUFFERING) {
                    bufferingListener();
                } else {
                    bufferingStopListener();
                }
            }
        });




        mPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);

        mPlayer.seekTo(0, mResumePosition);


        mPlayer.setPlayWhenReady(true);
    }


    private void callVideoUrl(final String mItemId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.NEW_VIDEO_URL, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    mVideoUrl = jsonObject.getString("video_url");
                    initExoPlayer();
                } catch (Exception e) {
                    e.printStackTrace();
                    mVideoUrl = mVideoUrl;
                    initExoPlayer();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mVideoUrl = mVideoUrl;

                initExoPlayer();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.ITEM_ID, mItemId);
                params.put(Keys.ITEM_TYPE, "DigitalVideoIntro");
                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    public VideoData getCurrentVideoData(){
        VideoData videoDataToBeSent = new VideoData();

        try {

            if (mPlayer != null) {
                String seekTime;
                if (mPlayer.getContentPosition() > mPlayer.getDuration() - 5 * 1000) {
                    mResumePosition = 0;
                    videoDataToBeSent.setTime("0");
                    seekTime = "0";
                } else {
                    videoDataToBeSent.setTime(Long.toString(mPlayer.getContentPosition() / 1000));
                    seekTime = Long.toString(mPlayer.getContentPosition() / 1000);
                    mResumePosition = Math.max(0, mPlayer.getContentPosition());
                }

                videoDataToBeSent.setTime(seekTime);
                videoDataToBeSent.setType("DigiIntro");

                videoDataToBeSent.setVideoId(mDashBoardDetails.getMentor_id());
                AppPreferences.setVideoResumePosition(mDashBoardDetails.getMentor_id(),mResumePosition);

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return videoDataToBeSent;

    }



}
