package com.imentors.wealthdragons.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.LruCache;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.Article;
import com.imentors.wealthdragons.models.DashBoard;
import com.imentors.wealthdragons.models.DashBoardCourses;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.DashBoardEpisodeDetails;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.models.DashBoardVideoDetails;
import com.imentors.wealthdragons.models.DashBoardWishList;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONObject;

import java.io.File;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ArticleView extends LinearLayout {


    private Context mContext;
    private CircleImageView mMentorImage;
    private ImageView mArticleImage , mArticleWishlist ;
    private WealthDragonTextView mMentorName, mArticleTitle, mMentorDetials, mTvCoursePrice, mRatingPeople, mStripFreePaid , mTvOriginalPrice , mTvFinalPrice;
    private RatingBar mRatingBar;
    private View mAboveViewDetailBtn, mBelowViewDetailBtn, mBelowRatingStar;
    private LinearLayout mRatingLayout, mMentorlayout, mRatingTitleBtnLayout, mBuyPriceLayout;
    private FrameLayout mViewButtonLayout;
    private boolean isWishlistSelected = false,mIsProgramme;
    private String mItemId;
    private boolean mIsDownloaded,mIsMentorLayoutVisible, mIsShowMentorImage, mIsShowMentorName, mIsShowMentorTagline, mIsShowViewDetailBtn, mIsShowCourseName, mIsShowRatings ,mIsWishlist;

    public ArticleView(Context context) {
        super(context);
        mContext = context;
    }

    public ArticleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public ArticleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mMentorImage = findViewById(R.id.circle_iv_speaker);
        mMentorName = findViewById(R.id.tv_mentor_name);
        mMentorDetials = findViewById(R.id.tv_mentor_details);
        mArticleTitle = findViewById(R.id.tv_article_title);
        mArticleImage = findViewById(R.id.iv_articleImage);
        mRatingBar = findViewById(R.id.ratingBar);
        mRatingPeople = findViewById(R.id.tv_bracket_rating);
        mStripFreePaid = findViewById(R.id.tv_strip_detail);
        mTvCoursePrice = findViewById(R.id.tv_positive_button);
        mBuyPriceLayout = findViewById(R.id.ll_price_view);
        mAboveViewDetailBtn = findViewById(R.id.view_above_view_detail_btn);
        mBelowViewDetailBtn = findViewById(R.id.view_below_view_detail_btn);
        mBelowRatingStar = findViewById(R.id.view_below_rating_bar);
        mViewButtonLayout = findViewById(R.id.fl_btn_orange);
        mRatingLayout = findViewById(R.id.rating_layout);
        mMentorlayout = findViewById(R.id.speaker_info);
        mRatingTitleBtnLayout = findViewById(R.id.title_rating_btn_container);
        mTvOriginalPrice = findViewById(R.id.tv_article_original_price);
        mTvFinalPrice = findViewById(R.id.tv_article_final_price);
        mArticleWishlist = findViewById(R.id.iv_add_wishlist);
        mArticleWishlist.setVisibility(VISIBLE);
    }

    // Set values to the list items
    public void bind(final Article article, LruCache memCache, final int tabPosition, boolean isProgrammeType, boolean isDownloaded) {

        String mentorName = article.getMentor_name();
        String mentorDetails = article.getPunch_line();
        String articleTitle = article.getTitle();

        if(isDownloaded){
            if (!TextUtils.isEmpty(mentorName)) {
                mMentorName.setText(mentorName);
            }

            if (!TextUtils.isEmpty(mentorDetails)) {
                mMentorDetials.setText(mentorDetails);
            }

            if (!TextUtils.isEmpty(articleTitle)) {
                mArticleTitle.setText(articleTitle);
            }


            mAboveViewDetailBtn.setVisibility(GONE);
            mMentorName.setVisibility(GONE);
            mMentorDetials.setVisibility(GONE);

            mMentorImage.setVisibility(GONE);
            mStripFreePaid.setVisibility(View.GONE);
            mArticleWishlist.setVisibility(GONE);

            mBuyPriceLayout.setVisibility(GONE);

            mRatingLayout.setVisibility(GONE);

            mBelowViewDetailBtn.setVisibility(GONE);
            mBelowRatingStar.setVisibility(GONE);
            mViewButtonLayout.setVisibility(GONE);

            mArticleImage.getLayoutParams().width = (int) Utils.getCourseImageWidth((Activity) mContext);
            mArticleImage.getLayoutParams().height = (int) Utils.getCourseImageHeight((Activity) mContext);
            Glide.with(mContext).load(Uri.fromFile(new File(article.getBanner()))).error(R.drawable.no_img_course).dontAnimate().into(mArticleImage);


        }else{

            mItemId=article.getId();
            mIsProgramme = isProgrammeType;

            if(!TextUtils.isEmpty(article.getWishlist()) && article.getWishlist().equals("Yes")){
                mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);
                isWishlistSelected = true;

            }else{
                mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart);
                isWishlistSelected = false;
            }


            mArticleWishlist.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isWishlistSelected ) {
                        mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart);
                        callWishListApi(mItemId,article);

                    }else{
                        mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);
                        callWishListApi(mItemId,article);

                    }
                    isWishlistSelected = !isWishlistSelected;
                }
            });




            if (!TextUtils.isEmpty(mentorName)) {
                mMentorName.setText(mentorName);
            }

            if (!TextUtils.isEmpty(mentorDetails)) {
                mMentorDetials.setText(mentorDetails);
            }

            if (!TextUtils.isEmpty(articleTitle)) {
                mArticleTitle.setText(articleTitle);
            }


            if (article.isDiscountAvailable() && article.getPurchase().equals("No")) {
                float finalCost = Float.parseFloat(article.getFinal_cost());
                float actualCost = Float.parseFloat(article.getCost());
                int discountPercentage = 0;

                if(finalCost!= actualCost){
                    discountPercentage = (int)((finalCost/actualCost)*100);
                    mStripFreePaid.setVisibility(View.VISIBLE);

                    mStripFreePaid.setText(100-discountPercentage + "% OFF");
                }else{
                    mStripFreePaid.setVisibility(View.INVISIBLE);
                }

            } else {
                mStripFreePaid.setVisibility(View.INVISIBLE);
            }

            mViewButtonLayout.setVisibility(View.GONE);

            mTvOriginalPrice.setPaintFlags(mTvOriginalPrice.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            mTvFinalPrice.setText("");
            mTvOriginalPrice.setText("");

            if (!TextUtils.isEmpty(article.getFree_paid())) {

                if (article.getFree_paid().equals("Paid") && article.getPurchase().equals("No")) {

                    if (article.getPush_live().equals("2")) {
                        if (article.getPre_booking_status().equals("Yes")) {
                            mTvFinalPrice.setVisibility(VISIBLE);
                            mTvOriginalPrice.setVisibility(INVISIBLE);

                            mTvFinalPrice.setText(article.getCurrency()+ article.getFinal_cost());
                            if(!TextUtils.equals(article.getCost(),article.getFinal_cost())){
                                mTvOriginalPrice.setVisibility(VISIBLE);

                                mTvOriginalPrice.setText(article.getCurrency()+ article.getCost());
                                mTvOriginalPrice.setPaintFlags(mTvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                            }


                        }
                        else
                        {
                            mTvFinalPrice.setVisibility(INVISIBLE);
                            mTvOriginalPrice.setVisibility(VISIBLE);

                            if(mIsProgramme){
                                mTvOriginalPrice.setText("Free Programme");

                            }else{
                                mTvOriginalPrice.setText("Free Course");

                            }

                            mTvOriginalPrice.setTextColor(getResources().getColor(R.color.textColorblue));

                        }
                    }else{
                        mTvFinalPrice.setVisibility(VISIBLE);
                        mTvOriginalPrice.setVisibility(INVISIBLE);

                        mTvFinalPrice.setText(article.getCurrency()+ article.getFinal_cost());
                        if(!TextUtils.equals(article.getCost(),article.getFinal_cost())){
                            mTvOriginalPrice.setVisibility(VISIBLE);

                            mTvOriginalPrice.setText(article.getCurrency()+ article.getCost());
                            mTvOriginalPrice.setPaintFlags(mTvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                        }

                    }



                } else {
                    mTvFinalPrice.setVisibility(INVISIBLE);
                    mTvOriginalPrice.setVisibility(VISIBLE);
                    switch(article.getFree_paid()){

                        case "Enrollment":
                            mTvOriginalPrice.setText("Free Enrollment Course");
                            break;

                        case "Paid":
                            mTvOriginalPrice.setText("Purchased");
                            break;

                        default:
                            if(mIsProgramme){
                                mTvOriginalPrice.setText("Free Programme");

                            }else{
                                mTvOriginalPrice.setText("Free Course");

                            }
                            break;

                    }
                    mTvOriginalPrice.setTextColor(getResources().getColor(R.color.textColorblue));

                }


            } else {
                mTvFinalPrice.setVisibility(INVISIBLE);
                mTvOriginalPrice.setVisibility(VISIBLE);

                if(mIsProgramme){
                    mTvOriginalPrice.setText("Free Programme");

                }else{
                    mTvOriginalPrice.setText("Free Course");

                }


                mTvOriginalPrice.setTextColor(getResources().getColor(R.color.textColorblue));
            }


            mRatingBar.setRating(article.getRating());
            mRatingPeople.setText("(" + article.getRating_persons() + ")");

            mArticleImage.getLayoutParams().width = (int) Utils.getCourseImageWidth((Activity) mContext);
            mArticleImage.getLayoutParams().height = (int) Utils.getCourseImageHeight((Activity) mContext);
            Glide.with(mContext).load(article.getMentor_banner()).error(R.drawable.no_img_mentor).dontAnimate().into(mMentorImage);


            double height = mArticleImage.getLayoutParams().height;
            double width = mArticleImage.getLayoutParams().width;

            String newImageUrl = Utils.getImageUrl(article.getBanner(), width, height);

            String thumbUrl = Utils.getImageUrl(article.getLow_qty_banner(), width, height);


            Glide.with(mContext).load(article.getBanner()).thumbnail(Glide.with(mContext).load(thumbUrl).dontAnimate()).error(R.drawable.no_img_course).dontAnimate().into(mArticleImage);

            // For Course Tab
            if (tabPosition == Constants.COURSE_TAB) {

                DashBoardCourses dashBoard = AppPreferences.getCoursesDashBoard();

                mIsMentorLayoutVisible = dashBoard.isMentorLayoutVisible();
                mIsShowCourseName = dashBoard.isShowCourseName();
                mIsShowMentorImage = dashBoard.isShowMentorImage();
                mIsShowMentorTagline = dashBoard.isShowMentorTagline();
                mIsShowMentorName = dashBoard.isShowMentorName();
                mIsShowViewDetailBtn = dashBoard.isShowViewDetailBtn();
                mIsShowRatings = dashBoard.isShowRatings();
                mIsWishlist=dashBoard.isWishlist();

            } // For Purchase Tab
            else if (tabPosition == Constants.PURCHASE_TAB) {

                DashBoard dashBoard = AppPreferences.getDashBoard();

                mIsMentorLayoutVisible = dashBoard.isMentorLayoutVisible();
                mIsShowCourseName = dashBoard.isShowCourseName();
                mIsShowMentorImage = dashBoard.isShowMentorImage();
                mIsShowMentorTagline = dashBoard.isShowMentorTagline();
                mIsShowMentorName = dashBoard.isShowMentorName();
                mIsShowViewDetailBtn = dashBoard.isShowViewDetailBtn();
                mIsShowRatings = dashBoard.isShowRatings();
                // no wishlist icon in purchase tab
                mIsWishlist=false;

            }else if (tabPosition == Constants.DASHBOARD_DETAIL_TAB) {
                // For DashBoardDetail tab
                DashBoardCourseDetails dashBoardCourseDetails = AppPreferences.getCourseDashBoardDetailData();
                if (dashBoardCourseDetails != null) {
                    mIsMentorLayoutVisible = dashBoardCourseDetails.isMentorLayoutVisible();
                    mIsShowCourseName = dashBoardCourseDetails.isShowCourseName();
                    mIsShowMentorImage = dashBoardCourseDetails.isShowMentorImage();
                    mIsShowMentorTagline = dashBoardCourseDetails.isShowMentorTagline();
                    mIsShowMentorName = dashBoardCourseDetails.isShowMentorName();
                    mIsShowViewDetailBtn = dashBoardCourseDetails.isShowViewDetailBtn();
                    mIsShowRatings = dashBoardCourseDetails.isShowRatings();
                    mIsWishlist=dashBoardCourseDetails.isWishlist();

                }
                DashBoardMentorDetails dashBoardMentorDetails = AppPreferences.getMentorDashBoardDetailData();
                if (dashBoardMentorDetails != null) {
                    mIsMentorLayoutVisible = dashBoardMentorDetails.isMentorLayoutVisible();
                    mIsShowCourseName = dashBoardMentorDetails.isShowCourseName();
                    mIsShowMentorImage = dashBoardMentorDetails.isShowMentorImage();
                    mIsShowMentorTagline = dashBoardMentorDetails.isShowMentorTagline();
                    mIsShowMentorName = dashBoardMentorDetails.isShowMentorName();
                    mIsShowViewDetailBtn = dashBoardMentorDetails.isShowViewDetailBtn();
                    mIsShowRatings = dashBoardMentorDetails.isShowRatings();
                    mIsWishlist=dashBoardMentorDetails.isWishlist();

                }
                DashBoardVideoDetails dashBoardVideoDetails = AppPreferences.getVideoDashBoardDetailData();
                if (dashBoardVideoDetails != null) {
                    mIsMentorLayoutVisible = dashBoardVideoDetails.isMentorLayoutVisible();
                    mIsShowCourseName = dashBoardVideoDetails.isShowCourseName();
                    mIsShowMentorImage = dashBoardVideoDetails.isShowMentorImage();
                    mIsShowMentorTagline = dashBoardVideoDetails.isShowMentorTagline();
                    mIsShowMentorName = dashBoardVideoDetails.isShowMentorName();
                    mIsShowViewDetailBtn = dashBoardVideoDetails.isShowViewDetailBtn();
                    mIsShowRatings = dashBoardVideoDetails.isShowRatings();
                    mIsWishlist=dashBoardVideoDetails.isWishlist();

                }

                DashBoardEpisodeDetails dashBoardEpisodeDetails = AppPreferences.getEpisodeDashBoardDetailData();
                if (dashBoardEpisodeDetails != null) {
                    mIsMentorLayoutVisible = dashBoardEpisodeDetails.isMentorLayoutVisible();
                    mIsShowCourseName = dashBoardEpisodeDetails.isShowCourseName();
                    mIsShowMentorImage = dashBoardEpisodeDetails.isShowMentorImage();
                    mIsShowMentorTagline = dashBoardEpisodeDetails.isShowMentorTagline();
                    mIsShowMentorName = dashBoardEpisodeDetails.isShowMentorName();
                    mIsShowViewDetailBtn = dashBoardEpisodeDetails.isShowViewDetailBtn();
                    mIsShowRatings = dashBoardEpisodeDetails.isShowRatings();
                }

                DashBoardWishList dashBoardWishList = AppPreferences.getDashBoardWishList();
                if (dashBoardWishList != null) {
                    mIsMentorLayoutVisible = dashBoardWishList.isMentorLayoutVisible();
                    mIsShowCourseName = dashBoardWishList.isShowCourseName();
                    mIsShowMentorImage = dashBoardWishList.isShowMentorImage();
                    mIsShowMentorTagline = dashBoardWishList.isShowMentorTagline();
                    mIsShowMentorName = dashBoardWishList.isShowMentorName();
                    mIsShowViewDetailBtn = dashBoardWishList.isShowViewDetailBtn();
                    mIsShowRatings = dashBoardWishList.isShowRatings();
                    mIsWishlist=dashBoardWishList.isWishlist();

                }


            } else {


                // For DashBoard Tab


                DashBoard dashBoard = AppPreferences.getDashBoard();


                mIsMentorLayoutVisible = dashBoard.isMentorLayoutVisible();
                mIsShowCourseName = dashBoard.isShowCourseName();
                mIsShowMentorImage = dashBoard.isShowMentorImage();
                mIsShowMentorTagline = dashBoard.isShowMentorTagline();
                mIsShowMentorName = dashBoard.isShowMentorName();
                mIsShowViewDetailBtn = dashBoard.isShowViewDetailBtn();
                mIsShowRatings = dashBoard.isShowRatings();
                mIsWishlist=dashBoard.isWishlist();

            }

            showDataOnView();

        }

    }
    //  show data on view
    private void showDataOnView() {

        if (mIsWishlist)
        {
            mArticleWishlist.setVisibility(VISIBLE);
        }
        else
        {
            mArticleWishlist.setVisibility(GONE);
        }

        if (!mIsMentorLayoutVisible) {
            mMentorlayout.setVisibility(View.GONE);
            mBelowRatingStar.setVisibility(View.GONE);
        } else {
            mMentorlayout.setVisibility(View.VISIBLE);
            mBelowRatingStar.setVisibility(View.VISIBLE);
        }


        if (!mIsShowMentorImage) {

            mMentorImage.setVisibility(View.GONE);

        } else {

            mMentorImage.setVisibility(View.VISIBLE);

        }

        if (!mIsShowMentorName) {

            mMentorName.setVisibility(View.GONE);

        } else {

            mMentorName.setVisibility(View.VISIBLE);

        }

        if (!mIsShowMentorTagline) {

            mMentorDetials.setVisibility(View.GONE);

        } else {
            mMentorDetials.setVisibility(View.VISIBLE);


        }


        // layout above mentor

        if (!mIsShowViewDetailBtn && !mIsShowCourseName && !mIsShowRatings) {
            mRatingTitleBtnLayout.setVisibility(GONE);


        } else {

            mRatingTitleBtnLayout.setVisibility(VISIBLE);
        }


        if (!mIsShowCourseName) {

            mArticleTitle.setVisibility(View.GONE);
            mAboveViewDetailBtn.setVisibility(View.GONE);


        } else {

            mArticleTitle.setVisibility(View.VISIBLE);
            mAboveViewDetailBtn.setVisibility(View.VISIBLE);


        }


        if (!mIsShowRatings) {

            mRatingLayout.setVisibility(View.GONE);
            mBelowRatingStar.setVisibility(View.GONE);

        } else {

            mRatingLayout.setVisibility(View.VISIBLE);
            mBelowRatingStar.setVisibility(View.VISIBLE);

        }

        if (!mIsShowViewDetailBtn) {

            mViewButtonLayout.setVisibility(View.GONE);
            mAboveViewDetailBtn.setVisibility(View.GONE);
            mBuyPriceLayout.setVisibility(GONE);


        } else {

            mViewButtonLayout.setVisibility(GONE);
            mBuyPriceLayout.setVisibility(VISIBLE);
            mAboveViewDetailBtn.setVisibility(View.VISIBLE);
        }


        // if mentor item visible


        // if rating visible
        if ((!mIsShowCourseName && !mIsShowViewDetailBtn && mIsShowRatings) && (mIsMentorLayoutVisible)) {

            mBelowRatingStar.setVisibility(View.VISIBLE);
            mAboveViewDetailBtn.setVisibility(View.GONE);
            mBelowViewDetailBtn.setVisibility(View.GONE);

        }

        if ((!mIsShowCourseName && !mIsShowViewDetailBtn && mIsShowRatings) && (!mIsMentorLayoutVisible)) {

            mBelowRatingStar.setVisibility(View.GONE);
            mAboveViewDetailBtn.setVisibility(View.GONE);
            mBelowViewDetailBtn.setVisibility(View.GONE);

        }

        // if course title visible
        if ((mIsShowCourseName && !mIsShowViewDetailBtn && !mIsShowRatings) && (mIsMentorLayoutVisible)) {

            mBelowRatingStar.setVisibility(View.VISIBLE);
            mAboveViewDetailBtn.setVisibility(View.GONE);
            mBelowViewDetailBtn.setVisibility(View.GONE);

        }

        if ((mIsShowCourseName && !mIsShowViewDetailBtn && !mIsShowRatings) && (!mIsMentorLayoutVisible)) {

            mBelowRatingStar.setVisibility(View.GONE);
            mAboveViewDetailBtn.setVisibility(View.GONE);
            mBelowViewDetailBtn.setVisibility(View.GONE);

        }


        // if view Detail Btn Visible
        if ((!mIsShowCourseName && mIsShowViewDetailBtn && !mIsShowRatings) && (mIsMentorLayoutVisible)) {

            mBelowRatingStar.setVisibility(View.GONE);
            mAboveViewDetailBtn.setVisibility(View.GONE);
            mBelowViewDetailBtn.setVisibility(View.VISIBLE);

        }


        if ((!mIsShowCourseName && mIsShowViewDetailBtn && !mIsShowRatings) && (!mIsMentorLayoutVisible)) {

            mBelowRatingStar.setVisibility(View.GONE);
            mAboveViewDetailBtn.setVisibility(View.GONE);
            mBelowViewDetailBtn.setVisibility(View.GONE);

        }


        // if view Detail Btn and rating Visible
        if ((!mIsShowCourseName && mIsShowViewDetailBtn && mIsShowRatings) && (mIsMentorLayoutVisible)) {

            mBelowRatingStar.setVisibility(View.VISIBLE);
            mAboveViewDetailBtn.setVisibility(View.GONE);
            mBelowViewDetailBtn.setVisibility(View.VISIBLE);

        }


        if ((!mIsShowCourseName && mIsShowViewDetailBtn && mIsShowRatings) && (!mIsMentorLayoutVisible)) {

            mBelowRatingStar.setVisibility(View.GONE);
            mAboveViewDetailBtn.setVisibility(View.GONE);
            mBelowViewDetailBtn.setVisibility(View.VISIBLE);

        }

        // if course name and view detail btn visible
        if ((mIsShowCourseName && mIsShowViewDetailBtn && !mIsShowRatings) && (mIsMentorLayoutVisible)) {

            mBelowRatingStar.setVisibility(View.GONE);
            mAboveViewDetailBtn.setVisibility(View.VISIBLE);
            mBelowViewDetailBtn.setVisibility(View.VISIBLE);

        }

        if ((mIsShowCourseName && mIsShowViewDetailBtn && !mIsShowRatings) && (!mIsMentorLayoutVisible)) {

            mBelowRatingStar.setVisibility(View.GONE);
            mAboveViewDetailBtn.setVisibility(View.VISIBLE);
            mBelowViewDetailBtn.setVisibility(View.GONE);

        }


        // if course name and rating visible
        if ((mIsShowCourseName && !mIsShowViewDetailBtn && mIsShowRatings) && (mIsMentorLayoutVisible)) {

            mBelowRatingStar.setVisibility(View.VISIBLE);
            mAboveViewDetailBtn.setVisibility(View.GONE);
            mBelowViewDetailBtn.setVisibility(View.VISIBLE);

        }

        if ((mIsShowCourseName && !mIsShowViewDetailBtn && mIsShowRatings) && (!mIsMentorLayoutVisible)) {

            mBelowRatingStar.setVisibility(View.GONE);
            mAboveViewDetailBtn.setVisibility(View.GONE);
            mBelowViewDetailBtn.setVisibility(View.VISIBLE);

        }
    }

    public void callWishListApi(final String mItemId,final Article mArticle) {
        String api = null;

        if (isWishlistSelected) {
            api = Api.REMOVE_WISHLIST_API;

        } else {
            api = Api.ADD_WISHLIST_API;
        }


        //TODO implemented maintenance mode
        // calling add wishlist and remove wishlist api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(mContext);
                        return;
                    }

                    JSONObject jsonObject = new JSONObject(response);

                    if (!TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS)))
                    {

                        LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_WISHLIST_RELOAD));

                        if (isWishlistSelected) {

                            Toast.makeText(mContext, "Added to Wishlist", Toast.LENGTH_SHORT).show();
                            mArticle.setWishlist("Yes");

                        }
                        else
                        {
                            mArticle.setWishlist("No");
                        }

                        try {
                            Intent intent = new Intent(Constants.BROADCAST_DETAIL_SET);
                            intent.putExtra(Keys.ARTICLE_ID, mItemId);
                            intent.putExtra(Keys.WISHLIST_VALUE, isWishlistSelected ? "Yes" : "No");

                            LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }




                } catch (Exception e) {

                    if (isWishlistSelected) {
                        Utils.callEventLogApi("remove <b>Wishlist </b> error in api ");
                        mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart);


                    } else {

                        Utils.callEventLogApi("add <b>Wishlist </b> error in api");
                        mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);


                    }
                    isWishlistSelected=!isWishlistSelected;
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (isWishlistSelected) {
                    Utils.callEventLogApi("remove <b>Wishlist </b> error in api");
                    mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart);

                } else {
                    Utils.callEventLogApi("add <b>Wishlist </b> error in api");
                    mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);


                }
                isWishlistSelected=!isWishlistSelected;
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);


                params.put(Keys.ITEM_ID,mItemId);
                if (mIsProgramme) {
                    params.put(Keys.TYPE, Constants.TYPE_PROGRAMMES);

                } else {
                    params.put(Keys.TYPE, Constants.TYPE_COURSE);

                }
                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


}
