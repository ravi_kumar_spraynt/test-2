package com.imentors.wealthdragons.views;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.util.AttributeSet;
import android.widget.EditText;

import com.braintreepayments.cardform.view.CvvEditText;

/**
 * An {@link EditText} for entering dates, used for card expiration dates.
 * Will automatically format input as it is entered.
 */
public class CardFormCvvEditText extends CvvEditText implements ViewUtils.ViewExtension {


    public CardFormCvvEditText(Context context) {
        super(context);
        init(context, null);    }

    public CardFormCvvEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public CardFormCvvEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context,attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        ViewUtils.init(this, context, attrs);

    }


    @Override
    public void onInit(Typeface typeface) {
        if (typeface != null) {
            this.setTypeface(typeface);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
        return;
    }
}
