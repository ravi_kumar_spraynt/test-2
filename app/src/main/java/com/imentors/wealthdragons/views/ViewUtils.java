package com.imentors.wealthdragons.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;

import android.text.TextUtils;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.FontCache;


/**
 * Created on 7/12/17.
 */
class ViewUtils {

    public interface ViewExtension {
        void onInit(Typeface typeface);
    }

    private ViewUtils() {
        /* cannot be instantiated */
    }

    /**
     * init the view attributes
     *
     * @param viewExtension view implementing view extension interface
     * @param context       context
     * @param attrs         view attrs
     */
    public static void init(ViewExtension viewExtension, Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.WeathDragonTextView);

        Typeface typeface = null;
        String fontName = attributeArray.getString(R.styleable.WeathDragonTextView_textFont);
        if (!TextUtils.isEmpty(fontName)) {
            int textStyle = attrs.getAttributeIntValue(Constants.ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
            typeface = selectTypeface(context, fontName, textStyle);
        }


        viewExtension.onInit(typeface);

        attributeArray.recycle();
    }

    /**
     * select the typeface
     *
     * @param context   context
     * @param fontName  name of the custom font
     * @param textStyle text style
     * @return typeface
     */
    @Nullable
    private static Typeface selectTypeface(@NonNull Context context, String fontName, int textStyle) {

        switch (textStyle) {
            case Typeface.BOLD: // bold DIAVLO_BOLD_II
                return FontCache.getTypeface(fontName + "_BOLD_II.OTF", context);

            default:
                //DIAVLO_BOOK_II
                return FontCache.getTypeface(fontName + "_BOOK_II.OTF", context);
        }
    }
}
