package com.imentors.wealthdragons.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.imentors.wealthdragons.utils.Utils;

/**
 * Created on 7/12/17.
 */
public class WealthDragonMentorTextView extends TextView implements ViewUtils.ViewExtension {


    private String shortDescription;

    public WealthDragonMentorTextView(@NonNull Context context) {
        super(context);
        if (!isInEditMode()){
            init(context, null);
        }
    }

    public WealthDragonMentorTextView(@NonNull Context context, @NonNull AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            init(context, attrs);
        }
    }

    public WealthDragonMentorTextView(@NonNull Context context, @NonNull AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode()) {
            init(context, attrs);
        }
    }

    /**
     * init the view
     *
     * @param context context
     * @param attrs   attrs
     */
    private void init(Context context, AttributeSet attrs) {
        ViewUtils.init(this, context, attrs);
        setSpannableFactory(Factory.getInstance());
    }



    @Override
    public void onInit(Typeface typeface) {
        if (typeface != null) {
            this.setTypeface(typeface);
        }
    }


    private static class Factory extends Spannable.Factory {
        private final static Factory sInstance = new Factory();

        public static Factory getInstance() {
            return sInstance;
        }

        @Override
        public Spannable newSpannable(CharSequence source) {
            return new SpannableNoUnderline(source);
        }
    }

    private static class SpannableNoUnderline extends SpannableString {
        public SpannableNoUnderline(CharSequence source) {
            super(source);
        }

        @Override
        public void setSpan(Object what, int start, int end, int flags) {
            if (what instanceof URLSpan) {
                what = new UrlSpanNoUnderline((URLSpan) what);
            }
            super.setSpan(what, start, end, flags);
        }
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        shortDescription = text.toString();
        super.setText(text, type);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAntiAlias(true);
        paint.setColor(Color.TRANSPARENT);
        canvas.drawPath(getPath(),paint);



        TextPaint mTextPaint=new TextPaint();
        mTextPaint.setTextSize(20);
        StaticLayout mTextLayout = new StaticLayout(shortDescription, mTextPaint, canvas.getWidth()- Utils.dpToPx(98), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

        canvas.save();

        canvas.translate(Utils.dpToPx(98), 0);
        mTextLayout.draw(canvas);
        canvas.restore();






//        paint.setColor(Color.BLACK);
//        paint.setTextSize(20);
//        canvas.drawText(shortDescription, Utils.dpToPx(98), 60, paint);

    }


    private Path getPath() {
        Path path = new Path();
        path.moveTo(Utils.dpToPx(98), 0);
        path.lineTo(Utils.dpToPx(98), Utils.dpToPx(98));
        path.lineTo(0, Utils.dpToPx(98));
        path.lineTo(0,this.getMeasuredHeight());
        path.lineTo(this.getMeasuredWidth(),this.getMeasuredHeight());
        path.lineTo(this.getMeasuredWidth(),0);
        path.close();

        return path;
    }

//    private void drawText(Canvas canvas) {
//        Rect rect = new Rect();
//        // For simplicity I am using a hardcoded string
//        mTextPaint.getTextBounds(TEST_STRING, 0, 1, rect);
//        int w = getWidth(), h = getHeight();
//        float x = (w - rect.width()) / 2, y = ((h - rect.height()) / 2) + rect.height();
//        canvas.drawText(TEST_STRING, 0, 1, x, y, mTextPaint);
//
//
//    }

}