package com.imentors.wealthdragons.views;

import android.content.Context;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;


import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.SeeAllListAdapter;
import com.imentors.wealthdragons.adapters.SubCategoryListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.interfaces.ArticleClickListener;
import com.imentors.wealthdragons.interfaces.MentorClickListener;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.interfaces.VideoClickListener;
import com.imentors.wealthdragons.models.Article;
import com.imentors.wealthdragons.models.DashBoardItemOdering;
import com.imentors.wealthdragons.models.DashBoardItemOderingArticle;
import com.imentors.wealthdragons.models.DashBoardItemOderingEvent;
import com.imentors.wealthdragons.models.DashBoardItemOderingMentors;
import com.imentors.wealthdragons.models.DashBoardItemOderingVideo;
import com.imentors.wealthdragons.models.Event;
import com.imentors.wealthdragons.models.Mentors;
import com.imentors.wealthdragons.models.PagentaionItem;
import com.imentors.wealthdragons.models.SubCategory;
import com.imentors.wealthdragons.models.Video;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.ItemTypeDeserilization;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SeeAllTabView extends LinearLayout implements SeeAllListAdapter.OnItemClickListener,PagingListener{

    private RecyclerView mRecyclerView ,mSubCategoryRecyclerView;
    private WealthDragonTextView mRecylerViewHeading , mTxtViewAll  , mSubCategoryName;
    private SeeAllListAdapter seeAllListAdapter;
    private ImageView mHeadingImage, mSubCategoryUpIcon;
    private LinearLayout mHeadingContainer,mSubCategoryContainer;
    private LinearLayoutManager mLinearLayoutManager;
    private List<SubCategory> mSubCategoryList = new ArrayList<>();
    private String mTasteId;
    private boolean isSubCategoryListVisible = false;
    private boolean isSubCategoryFilter = false,isTablet;
    private SubCategoryListAdapter mSubCategoryListAdapter;
    private Context mContext;
    private SubCategoryListAdapter.onSubCategoryClick mSubCategoryListener;
    private ArticleClickListener mArticleClickListeners;
    private MentorClickListener mMentorClickListener;
    private VideoClickListener mVideoClickListener;
    private boolean mIsProgramme;
    private String mPaginationId;


    private static final int  MAX_ITEM_COUNT = 1;


    public SeeAllTabView(Context context) {
        super(context);
    }

    public SeeAllTabView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SeeAllTabView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SeeAllTabView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public SeeAllTabView(Context context , DashBoardItemOderingMentors mentorList , String title , String tasteId,List<SubCategory> subCategoryList,String subCategoryName,String paginationId){
        super(context);
        mPaginationId = paginationId;

        if(subCategoryList!=null) {
            if(subCategoryList.size()>0) {

                mSubCategoryList.add(new SubCategory(null, "Show All"));
                mSubCategoryList.addAll(subCategoryList);
            }
        }
        initViews(context);


        if(TextUtils.isEmpty(subCategoryName)){
            if(subCategoryList!=null){
                if(subCategoryList.size()>0) {
                    mSubCategoryName.setText(mSubCategoryList.get(0).getTitle());
                }

            }
        }else{
            mSubCategoryName.setText(subCategoryName);
        }

        setMentorList(context,mentorList,title,tasteId);

    }

    //  set mentor list data in view
    private void setMentorList(Context context, DashBoardItemOderingMentors mentorList ,String title,String tasteId) {

        mRecylerViewHeading.setVisibility(View.VISIBLE);

        if(TextUtils.isEmpty(title)) {
            mRecylerViewHeading.setText(mentorList.getTitle());
        }else{
            mRecylerViewHeading.setText(title);

        }

        if(TextUtils.isEmpty(mentorList.getBanner())){
            mHeadingImage.setVisibility(View.GONE);
        }else{
            mHeadingImage.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(mentorList.getBanner()).into(mHeadingImage);
        }



        seeAllListAdapter = new SeeAllListAdapter(this,this,context);

        mSubCategoryListAdapter = new SubCategoryListAdapter(mSubCategoryListener,this,context,true);



        if(TextUtils.isEmpty(title)){
            seeAllListAdapter.setItems(null,mentorList.getItems(),null,null,mentorList.getTitle(),mentorList.getIcon(),mentorList.getNext_page(),mentorList.getTotal_items(),tasteId,null,mIsProgramme);
        }else{
            seeAllListAdapter.setItems(null,mentorList.getItems(),null,null,title,mentorList.getIcon(),mentorList.getNext_page(),mentorList.getTotal_items(),tasteId,null,mIsProgramme);

        }

        mTasteId = tasteId;


        mSubCategoryListAdapter.setItems(mSubCategoryList,null,Constants.TYPE_MENTOR,mTasteId);



        mRecyclerView.setAdapter(seeAllListAdapter);

        mSubCategoryRecyclerView.setAdapter(mSubCategoryListAdapter);


    }


    public SeeAllTabView(Context context , DashBoardItemOderingVideo videoList , String title, String tasteId ,List<SubCategory> subCategoryList,String subCategoryName,String paginationId){
        super(context);
        mPaginationId = paginationId;

        if(subCategoryList!=null) {
            if(subCategoryList.size()>0) {

                mSubCategoryList.add(new SubCategory(null, "Show All"));
                mSubCategoryList.addAll(subCategoryList);
            }
        }
        initViews(context);

        if(TextUtils.isEmpty(subCategoryName)){
            if(subCategoryList!=null){
                if(subCategoryList.size()>0) {

                    mSubCategoryName.setText(mSubCategoryList.get(0).getTitle());
                }

            }
        }else{
            mSubCategoryName.setText(subCategoryName);
        }

        setVideoList(context, videoList,title,tasteId);
    }

    //  set video list data in view
    private void setVideoList(Context context, DashBoardItemOderingVideo videoList, String title, String tasteId) {


        mRecylerViewHeading.setVisibility(View.VISIBLE);

        if(TextUtils.isEmpty(title)) {
            mRecylerViewHeading.setText(videoList.getTitle());
        }else{
            mRecylerViewHeading.setText(title);

        }

        if(TextUtils.isEmpty(videoList.getBanner())){
            mHeadingImage.setVisibility(View.GONE);
        }else{
            mHeadingImage.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(videoList.getBanner()).into(mHeadingImage);
        }


        seeAllListAdapter = new SeeAllListAdapter(this,this,context);

        mSubCategoryListAdapter = new SubCategoryListAdapter(mSubCategoryListener,this,context,true);


        if(TextUtils.isEmpty(title)) {

            seeAllListAdapter.setItems(null, null, videoList.getItems(), null, videoList.getTitle(), videoList.getIcon(),videoList.getNext_page(), videoList.getTotal_items(),tasteId,null,mIsProgramme);
        }else{
            seeAllListAdapter.setItems(null, null, videoList.getItems(), null, title, videoList.getIcon(),videoList.getNext_page(), videoList.getTotal_items(),tasteId,null,mIsProgramme);

        }

        mTasteId = tasteId;

        mSubCategoryListAdapter.setItems(mSubCategoryList,null,Constants.TYPE_MENTOR,mTasteId);


        mRecyclerView.setAdapter(seeAllListAdapter);

        mSubCategoryRecyclerView.setAdapter(mSubCategoryListAdapter);



    }


    public SeeAllTabView(Context context , DashBoardItemOderingArticle articleList, String title, String tasteId,String seeAllApiKey,List<SubCategory> subCategoryList,String subCategoryName,boolean isProgramme,String paginationId){
        super(context);
        mIsProgramme = isProgramme;
        mPaginationId = paginationId;
        if(subCategoryList!=null) {
            if(subCategoryList.size()>0) {

                mSubCategoryList.add(new SubCategory(null, "Show All"));
                mSubCategoryList.addAll(subCategoryList);
            }
        }

        initViews(context);
        if(TextUtils.isEmpty(subCategoryName)){
            if(subCategoryList!=null){
                if(subCategoryList.size()>0) {

                    mSubCategoryName.setText(mSubCategoryList.get(0).getTitle());
                }

            }
        }else{
            mSubCategoryName.setText(subCategoryName);
        }

        setArticleList(context,articleList,title,tasteId,seeAllApiKey);
    }

    //  set article list data in view
    private void setArticleList(Context context, DashBoardItemOderingArticle articleList, String title, String tasteId,String seeAllApiKey) {

        mRecylerViewHeading.setVisibility(View.VISIBLE);

        if(TextUtils.isEmpty(title)) {
            mRecylerViewHeading.setText(articleList.getTitle());
        }else{
            mRecylerViewHeading.setText(title);

        }

        if(TextUtils.isEmpty(articleList.getBanner())){
            mHeadingImage.setVisibility(View.GONE);
        }else{
            mHeadingImage.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(articleList.getBanner()).into(mHeadingImage);
        }


        seeAllListAdapter = new SeeAllListAdapter(this,this,context);

        mSubCategoryListAdapter = new SubCategoryListAdapter(mSubCategoryListener,this,context,true);


        if(TextUtils.isEmpty(title)) {
            seeAllListAdapter.setItems(articleList.getItems(), null, null, null, articleList.getTitle(), articleList.getIcon(),articleList.getNext_page(), articleList.getTotal_items(),tasteId,seeAllApiKey,mIsProgramme);
        }else{
            seeAllListAdapter.setItems(articleList.getItems(), null, null, null, title, articleList.getIcon(),articleList.getNext_page(), articleList.getTotal_items(),tasteId,seeAllApiKey,mIsProgramme);

        }

        mRecyclerView.setAdapter(seeAllListAdapter);
        mTasteId = tasteId;

        mSubCategoryListAdapter.setItems(mSubCategoryList,null,Constants.TYPE_COURSE,mTasteId);


        mSubCategoryRecyclerView.setAdapter(mSubCategoryListAdapter);



    }

    public SeeAllTabView(Context context , DashBoardItemOderingEvent eventList , String title, String tasteId,List<SubCategory> subCategoryList,String subCategoryName,String paginationId){
        super(context);
        mPaginationId = paginationId;

        if(subCategoryList!=null) {
            if(subCategoryList.size()>0) {

                mSubCategoryList.add(new SubCategory(null, "Show All"));
                mSubCategoryList.addAll(subCategoryList);
            }
        }

        initViews(context);

        if(TextUtils.isEmpty(subCategoryName)){
            if(subCategoryList!=null){
                if(subCategoryList.size()>0) {

                    mSubCategoryName.setText(mSubCategoryList.get(0).getTitle());
                }

            }
        }else{
            mSubCategoryName.setText(subCategoryName);
        }

        setEventList(context,eventList,title,tasteId);
    }

    //  set event list data in view
    private void setEventList(Context context, DashBoardItemOderingEvent eventList, String title, String tasteId) {

        mRecylerViewHeading.setVisibility(View.VISIBLE);

        if(TextUtils.isEmpty(title)) {
            mRecylerViewHeading.setText(eventList.getTitle());
        }else{
            mRecylerViewHeading.setText(title);

        }

        if(TextUtils.isEmpty(eventList.getBanner())){
            mHeadingImage.setVisibility(View.GONE);
        }else{
            mHeadingImage.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(eventList.getBanner()).into(mHeadingImage);
        }

        seeAllListAdapter = new SeeAllListAdapter(this,this,context);

        mSubCategoryListAdapter = new SubCategoryListAdapter(mSubCategoryListener,this,context,true);


        if(TextUtils.isEmpty(title)) {
            seeAllListAdapter.setItems(null, null, null, eventList.getItems(), eventList.getTitle(), eventList.getIcon(),eventList.getNext_page(), eventList.getTotal_items(),tasteId,null,mIsProgramme);
        }else{
            seeAllListAdapter.setItems(null, null, null, eventList.getItems(), title, eventList.getIcon(),eventList.getNext_page(), eventList.getTotal_items(),tasteId,null,mIsProgramme);

        }

        mRecyclerView.setAdapter(seeAllListAdapter);
        mTasteId = tasteId;

        mSubCategoryListAdapter.setItems(mSubCategoryList,null,Constants.TYPE_EVENT,mTasteId);


        mSubCategoryRecyclerView.setAdapter(mSubCategoryListAdapter);


    }



    private void initViews(final Context context ){

        mContext= context;


        if(context instanceof SubCategoryListAdapter.onSubCategoryClick){
            mSubCategoryListener= (SubCategoryListAdapter.onSubCategoryClick) context;
        }

        if(context instanceof ArticleClickListener){
            mArticleClickListeners = (ArticleClickListener) context;
        }

        if(context instanceof MentorClickListener){
            mMentorClickListener = (MentorClickListener) context;
        }

        if(context instanceof VideoClickListener){
            mVideoClickListener = (VideoClickListener) context;
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.dashboard_item_view, this);

        mLinearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        mLinearLayoutManager.setInitialPrefetchItemCount(8);
        mLinearLayoutManager.setItemPrefetchEnabled(true);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mRecylerViewHeading = view.findViewById(R.id.txt_header_name);
        mHeadingImage = view.findViewById(R.id.image_view_header);
        mHeadingContainer = view.findViewById(R.id.ll_header_view);
        mTxtViewAll = view.findViewById(R.id.txt_view_all);

        mSubCategoryContainer = view.findViewById(R.id.subcategory_container);
        mSubCategoryName= view.findViewById(R.id.tv_subcategory);
        mSubCategoryRecyclerView = view.findViewById(R.id.recycler_view_subcategory);
        mSubCategoryUpIcon = view.findViewById(R.id.iv_subcategory_up_icon);
        isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);


        // show subcategory in case of more than single subcategory
        if(mSubCategoryList.size()>2){

            mSubCategoryContainer.setVisibility(View.VISIBLE);
            mSubCategoryName.setText(mSubCategoryList.get(0).getTitle());
        }

        mSubCategoryContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isSubCategoryListVisible){
                    hideSubCategory();
                }else{
                    mSubCategoryRecyclerView.setVisibility(View.VISIBLE);
                    mSubCategoryUpIcon.setVisibility(View.VISIBLE);

                    if(isTablet) {
                        mSubCategoryRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
                    }else{
                        mSubCategoryRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1));
                    }
                    mSubCategoryRecyclerView.requestLayout();
                    isSubCategoryListVisible = !isSubCategoryListVisible;

                }

            }
        });

        mTxtViewAll.setVisibility(View.GONE);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


    }

    @Override
    public void onArticleClick(Article item, int position) {
        if(item.getLayout().equals(Constants.TYPE_PROGRAMMES)){
            mArticleClickListeners.onArticleClick(Constants.TYPE_PROGRAMMES,item.getId());

        }else{

            mArticleClickListeners.onArticleClick(Constants.TYPE_COURSE,item.getId());

        }
    }

    @Override
    public void onMentorClick(Mentors item, int position) {

        mMentorClickListener.onMentorClick(Constants.TYPE_MENTOR,item.getId());

    }

    @Override
    public void onVideoClick(Video item, int position) {
        if(item.isEpisode()){
            mVideoClickListener.onVideoClick(Constants.TYPE_EPISODE,item.getVideo_id(),item.getId());
        }else{
            mVideoClickListener.onVideoClick(Constants.TYPE_VIDEO,item.getId(),null);
        }
    }

    @Override
    public void onEventClick(Event item, int position) {

    }


    @Override
    public void setHeader(String title, String imageHeader, int listSize ) {

    }




    //TODO implemented maintenance mode
    public void callSeeAllPaginationApi(final int pageNumber , final String tasteId , String api , final String viewType) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(mContext);
                        return;
                    }
                    addDataInAdapter(response,viewType);

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        if(!Utils.setErrorDialog(response,getContext())){
                            seeAllListAdapter.setServerError();
                        }
                    } catch (Exception e1) {
                        Utils.callEventLogApi("getting <b> Pagination</b> error");

                        e1.printStackTrace();
                        seeAllListAdapter.setServerError();                    }

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b> Pagination</b> error");

                error.printStackTrace();
                if (error instanceof NoConnectionError) {

                }else{
                    seeAllListAdapter.setServerError();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                if(!TextUtils.isEmpty(mPaginationId)){
                    params.put(Keys.TASTE_ID,mPaginationId);
                }

                if(pageNumber !=0){
                    params.put(Keys.PAGE_NO,String.valueOf(pageNumber));
                }

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    @Override
    public void onGetItemFromApi(int pageNumber, String tasteId ,String viewType,String seeAllType) {

        switch (viewType){
            case Constants.TYPE_COURSE:
                if(!TextUtils.isEmpty(seeAllType)) {
                    switch (seeAllType) {
                        // for different types of courses
                        case Constants.ALL_PROGRAMMES_KEY:
                            callSeeAllPaginationApi(pageNumber, tasteId, Api.PROGRAMME_PAGENTATION, viewType);
                            break;
                        case Constants.ALL_UPCOMING_COURSE:
                            callSeeAllPaginationApi(pageNumber, tasteId, Api.UPCOMING_COURSE_PAGENTATION, viewType);
                            break;
                        case Constants.ON_SALE_KEY:
                            // missing api
                            callSeeAllPaginationApi(pageNumber, tasteId, Api.ON_SALE_PAGENTATION, viewType);
                            break;
                        default:
                            callSeeAllPaginationApi(pageNumber, tasteId, Api.COURSE_PAGENATION, viewType);
                            break;
                    }
                }else{
                    callSeeAllPaginationApi(pageNumber, tasteId, Api.COURSE_PAGENATION, viewType);
                }

                break;
            case Constants.TYPE_MENTOR:
                callSeeAllPaginationApi(pageNumber,tasteId, Api.MENTOR_PAGENATION,viewType);
                break;
            case Constants.TYPE_VIDEO:
                callSeeAllPaginationApi(pageNumber,tasteId, Api.VIDEO_PAGENTATION,viewType);
                break;

            default:
                break;
        }
    }



    private void addDataInAdapter(String response ,String viewType){

        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
        final Gson gson = gsonBuilder.create();

        // Parse JSON to Java
        final PagentaionItem dashBoardPagenationData = gson.fromJson(response, PagentaionItem.class);

        //  get heading ordering from dashboard pagination data
        DashBoardItemOdering dashBoard = dashBoardPagenationData.getHeadingOrdering();

        switch (viewType){
            case Constants.TYPE_COURSE:
                DashBoardItemOderingArticle dashBoardItemOderingArticle = (DashBoardItemOderingArticle) dashBoard;
                if(dashBoardItemOderingArticle.getItems().size()>0) {

                    seeAllListAdapter.addItems(dashBoardItemOderingArticle.getItems(),null,null,null,dashBoardItemOderingArticle.getNext_page(),dashBoardItemOderingArticle.getTotal_items(),mTasteId);

                }
                break;
            case Constants.TYPE_MENTOR:
                DashBoardItemOderingMentors dashBoardItemOderingMentor = (DashBoardItemOderingMentors) dashBoard;
                if(dashBoardItemOderingMentor.getItems().size()>0) {

                    seeAllListAdapter.addItems(null,dashBoardItemOderingMentor.getItems(),null,null,dashBoardItemOderingMentor.getNext_page(),dashBoardItemOderingMentor.getTotal_items(),mTasteId);

                }
                break;
            case Constants.TYPE_VIDEO:
                DashBoardItemOderingVideo dashBoardItemOderingVideo = (DashBoardItemOderingVideo) dashBoard;
                if(dashBoardItemOderingVideo.getItems().size()>0) {

                    seeAllListAdapter.addItems(null,null,dashBoardItemOderingVideo.getItems(),null,dashBoardItemOderingVideo.getNext_page(),dashBoardItemOderingVideo.getTotal_items(),mTasteId);

                }
                break;
            default:
                break;
        }
    }



    //  hide sub category
    private void hideSubCategory() {
        mSubCategoryRecyclerView.setVisibility(View.GONE);
        mSubCategoryUpIcon.setVisibility(View.GONE);
        isSubCategoryListVisible = !isSubCategoryListVisible;
    }



    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mArticleClickListeners = null;
        mMentorClickListener=null;
        mVideoClickListener=null;
        mSubCategoryListener = null;
    }
}
