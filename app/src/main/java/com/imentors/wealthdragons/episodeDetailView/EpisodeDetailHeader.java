package com.imentors.wealthdragons.episodeDetailView;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Handler;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.interfaces.OpenFullScreenVideoListener;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.DashBoardEpisodeDetails;
import com.imentors.wealthdragons.models.VideoData;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class EpisodeDetailHeader extends LinearLayout {


    private Activity mActivity;
    private Context mContext;
    private WealthDragonTextView mTvCourseTitle, mTvCoursePunchLine, mTvCoursePreferance;
    private ImageView mIvCourseImage, mIvVideoPlay;
    private PlayerView mExoPlayerView;
    private FrameLayout mVideoFrameLayout;
    private ImageView  mPlayIcon;
    private SimpleExoPlayer mPlayer;
    private DashBoardEpisodeDetails mDashBoardDetails;
    private boolean isFromActivityResult = false, isPaused = false;
    private List<String> mVideoUrls = new ArrayList<>();
    private List<Chapters.EClasses> eClassesArrayList = new ArrayList<>();
    private List<DashBoardEpisodeDetails.VideoUrl> mEpisodeUrl = new ArrayList<>();

    private int mResumeWindow;
    private long mResumePosition;
    private String mVideoUrl;
    private OpenFullScreenVideoListener mOpenFullScreenVideoListener;
    private Utils.DialogInteraction mDialogListener;
    private boolean mExoPlayerFullscreen,mIsVideoError = false;
    private Dialog mFullScreenDialog;
    private boolean isLandScape, isPotrait;
    private PlayerView mDialogplayerView;
    private ImageButton mPlayImageButton;
    private WealthDragonTextView mVideoError;
    private Timer mTimerNextVideo = new Timer();
    private TextView  mTvNextVideoLabelDialog;
    final Handler mNextVideoHandler = new Handler();
    private String imageUrl;
    private FrameLayout nextVideoFrame;
    private TextView mNextVideo;
    private TextView mTvEClassName;
    private LinearLayout mLlHeadingContainer;
    private PlayerControlView controlView;


    public EpisodeDetailHeader(Context context) {
        super(context);
        mContext = context;
    }

    public EpisodeDetailHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public EpisodeDetailHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public EpisodeDetailHeader(Context context, DashBoardEpisodeDetails dashBoardDetails,Activity activity) {
        super(context);
        mActivity = activity;
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set data in view
    private void bindView(DashBoardEpisodeDetails dashBoardDetails) {

        mDashBoardDetails = dashBoardDetails;


        // add first intro video in urls
        mVideoUrls.add(mDashBoardDetails.getVideoDetail().getVideo());

        if (mDashBoardDetails.getRelatedVideosUrls() != null) {
            if (mDashBoardDetails.getRelatedVideosUrls().size() > 0) {

                for (DashBoardEpisodeDetails.VideoUrl videoUrl : mDashBoardDetails.getRelatedVideosUrls()) {
                    if (!TextUtils.isEmpty(videoUrl.getVideo())) {
                        mVideoUrls.add(videoUrl.getVideo());
                        mEpisodeUrl.add(videoUrl);
                    }
                }
            }
        }


        imageUrl = Utils.getImageUrl(dashBoardDetails.getVideoDetail().getMobile_big_banner(), Utils.getScreenWidth((Activity) mContext), Utils.getDetailScreenImageHeight((Activity) mContext));
        String thumbImageUrl = Utils.getImageUrl(dashBoardDetails.getVideoDetail().getLow_qty_banner(), Utils.getScreenWidth((Activity) mContext), Utils.getDetailScreenImageHeight((Activity) mContext));

        Glide.with(mContext).load(imageUrl).thumbnail(Glide.with(mContext).load(thumbImageUrl).dontAnimate()).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).into(mIvCourseImage);

        if (!TextUtils.isEmpty(dashBoardDetails.getVideoDetail().getTitle())) {
            mTvCourseTitle.setVisibility(View.VISIBLE);
            mTvCourseTitle.setText(dashBoardDetails.getVideoDetail().getTitle());
        } else {
            mTvCourseTitle.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(dashBoardDetails.getVideoDetail().getPunch_line())) {
            mTvCoursePunchLine.setVisibility(View.VISIBLE);
            mTvCoursePunchLine.setText(dashBoardDetails.getVideoDetail().getPunch_line());
        } else {
            mTvCoursePunchLine.setVisibility(View.GONE);
        }

        mTvCoursePunchLine.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));


        if (!TextUtils.isEmpty(dashBoardDetails.getVideoDetail().getTaste_prefrence())) {
            mTvCoursePreferance.setVisibility(View.VISIBLE);
            mTvCoursePreferance.setText(dashBoardDetails.getVideoDetail().getTaste_prefrence());
        } else {
            mTvCoursePreferance.setVisibility(View.GONE);
        }

        mTvCoursePreferance.setTextColor(ContextCompat.getColor(mContext, R.color.textColorblack));


        if (!TextUtils.isEmpty(dashBoardDetails.getVideoDetail().getVideo())) {
            mIvVideoPlay.setVisibility(View.VISIBLE);
            mExoPlayerView.setVisibility(View.VISIBLE);
            mVideoError.setVisibility(INVISIBLE);


            initFullscreenButton();
//            Utils.initilizeDetailScreenConcatinatedPlayer(mContext, mVideoUrls, eClassesArrayList, true,Constants.TYPE_EPISODE);


            initExoPlayer();


        } else {
            mIvVideoPlay.setVisibility(View.INVISIBLE);
            mExoPlayerView.setVisibility(View.INVISIBLE);
            mVideoError.setVisibility(INVISIBLE);

        }


    }

    //  initialize episode detail header view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (context instanceof OpenFullScreenVideoListener) {
            mOpenFullScreenVideoListener = (OpenFullScreenVideoListener) context;
        }

        if (context instanceof Utils.DialogInteraction) {
            mDialogListener = (Utils.DialogInteraction) context;
        }

        View view = inflater.inflate(R.layout.course_detail_header, this);
        mContext = context;

        // heading
        mTvCourseTitle = view.findViewById(R.id.tv_course_detail_title);
        mTvCoursePunchLine = view.findViewById(R.id.tv_course_detail_punch_line);
        mTvCoursePreferance = view.findViewById(R.id.txt_course_detail_taste_preferance);
        mVideoError = view.findViewById(R.id.video_error);


        // video Frame
        mVideoFrameLayout = view.findViewById(R.id.frame_video);
        mVideoFrameLayout.getLayoutParams().height = (int) Utils.getDetailScreenImageHeight((Activity) mContext);


        mIvVideoPlay = view.findViewById(R.id.iv_video_play);
        mIvCourseImage = view.findViewById(R.id.iv_video_image);
        mExoPlayerView = findViewById(R.id.exoplayer);
        controlView = mExoPlayerView.findViewById(R.id.exo_controller);
//        controlView.findViewById(R.id.exo_player_audio).setVisibility(GONE);
//        controlView.findViewById(R.id.exo_player_video).setVisibility(VISIBLE);


        mIvVideoPlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                initFullscreenDialog();

                // true means we have to play videos only on wifi connection on else play video
                if (AppPreferences.getWifiState() && !Utils.IsWifiConnected()) {

                    Utils.showTwoButtonAlertDialog(mDialogListener, mContext, mContext.getString(R.string.notification), mContext.getString(R.string.check_wifi_status), mContext.getString(R.string.okay_text), mContext.getString(R.string.go_to_setting));

                } else {

                    if(mIsVideoError){
                        mPlayImageButton.setActivated(false);
                        mIvCourseImage.setVisibility(View.INVISIBLE);
                        mIvVideoPlay.setVisibility(View.INVISIBLE);
                        mVideoError.setVisibility(VISIBLE);
                        mExoPlayerView.setVisibility(View.INVISIBLE);
                    }else{
                        mExoPlayerView.setVisibility(VISIBLE);
                        mIvCourseImage.setVisibility(INVISIBLE);
                        mIvVideoPlay.setVisibility(INVISIBLE);
                        mVideoError.setVisibility(INVISIBLE);
                    }

                    mPlayer.setPlayWhenReady(true);
                    startNextVideoTimer();

                }
            }
        });

        LocalBroadcastManager.getInstance(context).registerReceiver(mSaveVideoPosition, new IntentFilter(Constants.BROADCAST_SAVE_VIDEO_POSITION));
        LocalBroadcastManager.getInstance(context).registerReceiver(mVideoErrorListener, new IntentFilter(Constants.VIDEO_ERROR));

    }


    private BroadcastReceiver mSaveVideoPosition = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            boolean isDetailOnTop = intent.getBooleanExtra(Constants.IS_TOP_FRAGMENT, false);
            cancelNextVideoTimer();

            if (mExoPlayerView != null && mPlayer != null) {
                mResumeWindow = mPlayer.getCurrentWindowIndex();
                AppPreferences.setVideoResumeWindow(mVideoUrl, mResumeWindow);
                mResumePosition = Math.max(0, mExoPlayerView.getPlayer().getContentPosition());
                AppPreferences.setVideoResumePosition(mVideoUrl, mResumePosition);
            }

            if (!isDetailOnTop) {
                Utils.releaseDetailScreenPlayer();
                Utils.releaseFullScreenPlayer();
            }

            if (!isFromActivityResult && !isPaused) {
                mExoPlayerView.setVisibility(INVISIBLE);
                mIvCourseImage.setVisibility(INVISIBLE);
                mIvVideoPlay.setVisibility(INVISIBLE);
            }

            isFromActivityResult = false;
            isPaused = false;

        }
    };

    private BroadcastReceiver mVideoErrorListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mIsVideoError = true;
            cancelNextVideoTimer();

            if(mPlayer!=null) {

                if (mPlayer.getPlayWhenReady()) {
                    try {
                        mPlayImageButton.setActivated(false);
                        mIvCourseImage.setVisibility(View.INVISIBLE);
                        mIvVideoPlay.setVisibility(View.INVISIBLE);
                        mVideoError.setVisibility(VISIBLE);
                        mExoPlayerView.setVisibility(View.INVISIBLE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        cancelNextVideoTimer();

        if (mExoPlayerView != null && mPlayer != null) {
            mResumeWindow = mPlayer.getCurrentWindowIndex();
            AppPreferences.setVideoResumeWindow(mVideoUrl, mResumeWindow);
            mResumePosition = Math.max(0, mExoPlayerView.getPlayer().getContentPosition());
            AppPreferences.setVideoResumePosition(mVideoUrl, mResumePosition);
            mPlayer.release();
            mPlayer = null;

            // releasing full screen player too
            Utils.releaseFullScreenPlayer();
            Utils.releaseDetailScreenPlayer();
        }

        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(mSaveVideoPosition);
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(mVideoErrorListener);

    }


    private void initFullscreenButton() {


        mPlayImageButton  = controlView.findViewById(R.id.exo_play);

    }

    public void openFullScreenVideo(int rotationAngle){
        if (!mExoPlayerFullscreen) {
            if (!isLandScape) {
                openFullscreenDialog(rotationAngle);
            }
        } else {
            if (!isPotrait) {
                closeFullscreenDialog();
            }
        }
    }

    public boolean getPlayBackState(){
        return mPlayer.getPlayWhenReady();
    }


    // for internal use
    private void initExoPlayer() {

        // get position from preferences

        if(!TextUtils.isEmpty(mDashBoardDetails.getVideoDetail().getSeek_time())){
            mResumePosition =  Utils.getTimeInMilliSeconds(mDashBoardDetails.getVideoDetail().getSeek_time());
        }else{
            mResumePosition=0;
        }
        mResumeWindow = AppPreferences.getVideoResumeWindow(mVideoUrl);

        mPlayer = Utils.getDetailScreenConcatinatedPlayer();

        mExoPlayerView.setPlayer(mPlayer);

        mPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);

        boolean haveResumePosition = mResumeWindow != C.INDEX_UNSET;

        if (haveResumePosition) {
            mPlayer.seekTo(0, mResumePosition);
        }

        mPlayer.setPlayWhenReady(false);
    }


    @Override
    protected void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        if (mVideoUrls.size() > 0) {

            // for full screen initilizing player and media source

            if (Utils.getDetailScreePlayer() == null) {
//                Utils.initilizeDetailScreenConcatinatedPlayer(mContext, mVideoUrls, eClassesArrayList, true,Constants.TYPE_EPISODE);

            }



        }
    }


    public void setExoPlayer(int resumeWindow, long resumePosition) {
        if (mExoPlayerView == null) {

            mExoPlayerView = findViewById(R.id.exoplayer);
            initFullscreenButton();

//            Utils.initilizeDetailScreenConcatinatedPlayer(mContext, mVideoUrls, eClassesArrayList, true,Constants.TYPE_EPISODE);
        }

        initExoPlayer(resumeWindow, resumePosition);
    }

    //for activity result use
    private void initExoPlayer(int resumeWindow, long resumePosition) {


        mResumePosition = resumePosition;
        mResumeWindow = resumeWindow;


        if (mPlayer == null) {
//            Utils.initilizeDetailScreenConcatinatedPlayer(mContext, mVideoUrls, eClassesArrayList, true,Constants.TYPE_EPISODE);

            mPlayer = Utils.getDetailScreePlayer();

            mExoPlayerView.setPlayer(mPlayer);
            mPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);

            boolean haveResumePosition = resumeWindow != C.INDEX_UNSET;

            if (haveResumePosition) {
                mPlayer.seekTo(resumeWindow, resumePosition);
            }
            mPlayer.setPlayWhenReady(true);
        } else {
            boolean haveResumePosition = resumeWindow != C.INDEX_UNSET;

            if (haveResumePosition) {
                mPlayer.seekTo(resumeWindow, resumePosition);
            }
            mPlayer.setPlayWhenReady(true);
        }


    }

    public void pauseVideo() {
        if (mPlayer != null) {
            mPlayer.setPlayWhenReady(false);
        }

        isPaused = true;
    }

    public VideoData getCurrentVideoData(){
        VideoData videoDataToBeSent = new VideoData();

        try {

            if (mPlayer != null) {
                videoDataToBeSent.setTime(Long.toString(mPlayer.getContentPosition()/1000));

                videoDataToBeSent.setType(Constants.TYPE_EPISODE);

                if(mPlayer.getCurrentWindowIndex()==0){
                    mDashBoardDetails.getVideoDetail().getId();
                    mDashBoardDetails.getVideoDetail().setSeek_time(Long.toString(mPlayer.getContentPosition()/1000));
                }else {
                    videoDataToBeSent.setVideoId(mEpisodeUrl.get(mPlayer.getCurrentWindowIndex()-1).getId());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return videoDataToBeSent;

    }


    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }


    private void openFullscreenDialog(int rotationAngle) {
        Utils.callEventLogApi("clicked <b>]"+ "Full Screen Video" +" Mode</b> in Player from "+ mDashBoardDetails.getEpisodeDetail().getTitle());

        isPotrait = false;
        isLandScape = true;
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.activity_video_fullscreen, null);
        mDialogplayerView = dialogView.findViewById(R.id.exoplayer);
        mNextVideo=dialogView.findViewById(R.id.tv_nxtvideo);
        mLlHeadingContainer = dialogView.findViewById(R.id.header_holder);

        mNextVideo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.callEventLogApi("Clicked <b> Next E-Class</b> Video");


                if(mPlayer.getNextWindowIndex() != -1) {
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());

                }

            }
        });
        nextVideoFrame = dialogView.findViewById(R.id.frame_video);
        mTvEClassName = dialogView.findViewById(R.id.tv_eclasses_title);
        String  videoUrl = mVideoUrls.get(mExoPlayerView.getPlayer().getCurrentWindowIndex());
        String mEClassesTitle = null;
        for(Chapters.EClasses eClass:eClassesArrayList){
            if(videoUrl.equals(eClass.getPlay_url())){
                mEClassesTitle = eClass.getTitle();
            }
        }

        mTvEClassName.setText(mEClassesTitle);
        ImageView dialogCourseImage = dialogView.findViewById(R.id.iv_video_image);

        Glide.with(mContext).load(imageUrl).into(dialogCourseImage);

        // on Next Click
        nextVideoFrame.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("Clicked <b> Next Video</b> ");

                if(mPlayer.getNextWindowIndex() != -1) {
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());

                }
            }
        });

        initDialogFullScreenButton();
        PlayerView.switchTargetView(mExoPlayerView.getPlayer(), mExoPlayerView, mDialogplayerView);
        mFullScreenDialog.addContentView(dialogView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
        Log.d("orientation", "lanscape");

    }

    //  closed full screen dialog
    public void closeFullscreenDialog() {
        Utils.callEventLogApi("clicked <b>]"+ "Compact Screen Video" +" Mode</b> in Player from "+ mDashBoardDetails.getEpisodeDetail().getTitle());

        isPotrait = true;
        isLandScape = false;
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        PlayerView.switchTargetView(mDialogplayerView.getPlayer(), mDialogplayerView, mExoPlayerView);
        mFullScreenDialog.dismiss();
        mExoPlayerFullscreen = false;
        Log.d("orientation", "potrait");

    }


    private void initDialogFullScreenButton() {

        PlayerControlView playerControlViewFullScreen = mDialogplayerView.findViewById(R.id.exo_controller);
        playerControlViewFullScreen.setVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if(visibility==View.VISIBLE){
                    mLlHeadingContainer.setVisibility(VISIBLE);
                    if(mPlayer.getNextWindowIndex() != -1 && mPlayer.getRepeatMode() == Player.REPEAT_MODE_ALL) {
                        mNextVideo.setVisibility(VISIBLE);
                    }else{
                        mNextVideo.setVisibility(GONE);
                    }
                }else{
                    mLlHeadingContainer.setVisibility(GONE);

                }
            }
        });

    }



    private void startNextVideoTimer(){
                mTimerNextVideo.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        mNextVideoHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (mPlayer != null) {
                                    // next video option will be shown before 45 seconds from end
                                    if (mPlayer.getContentPosition() > mPlayer.getDuration() - 10 * 1000 && mPlayer.getNextWindowIndex() != -1 && mPlayer.getRepeatMode() == Player.REPEAT_MODE_ALL) {
                                        showNextVideoLabel();
                                    } else {
                                        disableNextVideoLabel();
                                    }
                                }

                            }
                        });
                    }
                }, 0, 500);
    }


    private void cancelNextVideoTimer(){
        mTimerNextVideo.cancel();
        disableNextVideoLabel();
    }

    private void showNextVideoLabel(){
        // if full screen
        if(mExoPlayerFullscreen) {

            if (nextVideoFrame.getVisibility() == GONE) {
                nextVideoFrame.setVisibility(VISIBLE);
            }
        }

    }

    private void disableNextVideoLabel(){
        // if full screen
        if(mExoPlayerFullscreen) {

            if (nextVideoFrame.getVisibility() == VISIBLE) {
                nextVideoFrame.setVisibility(GONE);
            }
        }
    }



}
