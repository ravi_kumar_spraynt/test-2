package com.imentors.wealthdragons.api;

/**
 * Created on 7/4/17.
 */
public class Api {

    private Api() {
        /* cannot be instantiated */
    }
//  private static final String TEST = "https://www.wdotestserver.com/";
//    private static final String TEST = "https://www.abusinesspage.com/";

    private static final String TEST = "https://services.abusinesspage.com/";

    //http://services.abusinesspage.com/
//    private static final String TEST = "http://www.skincarebusiness.net/";
//    private static final String DEVELOPMENT = "https://www.wealthdragonsonline.com/";
//    private static final String DEVELOPMENT = "https://services.wealthdragons.com/";
        private static final String DEVELOPMENT = "https://webservices.wealthdragons.com/";


    private static final String BASE = DEVELOPMENT ;
    private static final String VERSION = "v2/";
    private static final String API_V1 = BASE + "app-" + VERSION;
    public static final String HOMEPAGE_API = API_V1 + "homepage_" + VERSION + "homepage";
    public static final String SUBSCRIPTION_API = API_V1 +"subscription_" + VERSION + "subscription_content";
    public static final String SIGN_IN_API = API_V1 + "login_" + VERSION + "normal_login";
    public static final String REGISTER_API = API_V1 + "register_" + VERSION + "register";
    public static final String FORGOT_PASSWORD_API = API_V1 + "forgot_password_" + VERSION + "forgot_password";
    public static final String FB_LOG_IN_API = API_V1 + "login_" + VERSION + "fb_login";
    public static final String SIGN_OUT_API = API_V1 + "login_" + VERSION + "logout";
    public static final String PRIVACY_POLICY_API = API_V1 + "subscription_" + VERSION + "privacy_policy";
    public static final String TERMS_AND_CONDITION_API = API_V1 + "subscription_" + VERSION + "terms_and_conditions";
    public static final String CONTACT_SUPPORT_API = API_V1 + "contact_" + VERSION + "contact_support";
    public static final String LOG_EVENT_API = API_V1 + "log_" + VERSION + "add_event_log";
    public static final String CATEGORIES_API = API_V1 + "category_" + VERSION + "categories";
    public static final String DASHBOARD_OTHER_API = API_V1 + "dashboard_" + VERSION + "dashboard_other";
    public static final String DASHBOARD_BLANK_API = API_V1 + "dashboard_" + VERSION + "dashboard_blank";
    public static final String DASHBOARD_API = API_V1 + "dashboard_" + VERSION + "dashboard_android";
    public static final String DASHBOARD_IMAGES_API = API_V1 + "dashboard_" + VERSION + "dashboard_images";

    public static final String MENTORS_API = API_V1 + "mentor_list_" + VERSION + "all_mentors_android";
    public static final String VIDEOS_API = API_V1 + "video_list_" + VERSION + "all_videos_android";
    public static final String COURSES_API = API_V1 + "course_list_" + VERSION + "all_course_android";


    public static final String SEE_ALL_API = API_V1 + "category_" + VERSION + "taste_android_new" ;
    public static final String SEE_ALL_API_FILTER = API_V1 + "category_" + VERSION + "filter_subcategory" ;

    public static final String ALL_PROGRAMMES_API = API_V1 + "programme_list_" + VERSION + "all_programmes_android" ;
    public static final String ALL_EVENTS_API = API_V1 + "event_list_" + VERSION + "all_events_android" ;
    public static final String ON_SALE_API = API_V1 + "course_list_" + VERSION + "all_onsale_android" ;
    public static final String UPCOMING_COURSES_API = API_V1 + "course_list_" + VERSION + "all_upcoming_course_android" ;
    public static final String COURSE_PAGENATION = API_V1 + "category_" + VERSION + "course_pagination" ;
    public static final String MENTOR_PAGENATION = API_V1 + "category_" + VERSION + "mentor_pagination" ;
    public static final String VIDEO_PAGENTATION = API_V1 + "category_" + VERSION + "video_pagination" ;
    public static final String UPCOMING_COURSE_PAGENTATION = API_V1 + "category_" + VERSION + "upcoming_course_pagination" ;
    public static final String PROGRAMME_PAGENTATION = API_V1 + "category_" + VERSION + "programme_pagination" ;
    public static final String ON_SALE_PAGENTATION = API_V1 + "category_" + VERSION + "onsale_pagination" ;
    public static final String EVENT_PAGENTATION = API_V1 + "category_" + VERSION + "event_pagination" ;

    //dashboard pagenation api
    public static final String DASHBOARD_COURSE_PAGENATION = API_V1 + "dashboard_" + VERSION + "featured_category_course_pagination" ;
    public static final String DASHBOARD_MENTOR_PAGENATION = API_V1 + "dashboard_" + VERSION + "featured_mentor_pagination" ;
    public static final String DASHBOARD_VIDEO_PAGENATION = API_V1 + "dashboard_" + VERSION + "featured_video_pagination" ;
    public static final String DASHBOARD_PROGRAMME_PAGENATION = API_V1 + "dashboard_" + VERSION + "featured_programme_pagination" ;
    public static final String DASHBOARD_UPCOMING_COURSE_PAGENATION = API_V1 + "dashboard_" + VERSION + "featured_upcomingcourse_pagination" ;
    public static final String DASHBOARD_FEATURED_COURSE_PAGENATION = API_V1 + "dashboard_" + VERSION + "featured_course_pagination" ;
    public static final String DASHBOARD_NEW_MENTOR_PAGENATION = API_V1 + "dashboard_" + VERSION + "featured_category_mentor_pagination" ;


    //https://www.abusinesspage.com/app-v1/dashboard_v1/featured_category_course_pagination
    //https://www.abusinesspage.com/app-v1/dashboard_v1/featured_video_pagination
    //https://www.abusinesspage.com/app-v1/dashboard_v1/featured_programme_pagination
    //https://www.abusinesspage.com/app-v1/dashboard_v1/featured_upcomingcourse_pagination
    //https://www.abusinesspage.com/app-v1/dashboard_v1/featured_mentor_pagination
    //https://www.wdotestserver.com/app-v1/dashboard_v1/featured_course_pagination
    //https://services.abusinesspage.com/app-v1/dashboard_v1/featured_category_mentor_pagination


    public static final String ABOUT_US_API = API_V1 + "general_" + VERSION + "about_us_android" ;
    public static final String SEARCH_API = API_V1 + "search_" + VERSION + "search_new";
    public static final String SEARCH_AUTO_COMPLETE_API = BASE + "search_" + VERSION + "search_autocomplete";
    public static final String SEARCH_VIDEO_API = API_V1 + "search_" + VERSION + "video_pagination";
    public static final String SEARCH_MENTOR_API = API_V1 + "search_" + VERSION + "mentor_pagination";
    public static final String SEARCH_COURSE_API = API_V1 + "search_" + VERSION + "course_pagination";
    public static final String SEARCH_PROGRAMME_API = API_V1 + "search_" + VERSION + "programme_pagination";
    public static final String SEARCH_UPCOMING_COURSE_API = API_V1 + "search_" + VERSION + "upcoming_course_pagination";
    public static final String SEARCH_TOP_KEYWORDS_API = API_V1 + "search_" + VERSION + "key_words";
    public static final String SEARCH_TOP_KEYWORDS_SEARCH_API = API_V1 + "search_" + VERSION + "key_words_android";
    public static final String SEARCH_TOP_KEYWORDS_SEARCH_PAGINATION_API = API_V1 + "search_" + VERSION + "key_words_pagination";



    public static final String SEARCH_MENTOR_COURSE_PAGINATION_API = API_V1 + "search_" + VERSION + "course_mentor_pagination";
    public static final String SEARCH_MENTOR_UPCOMING_COURSE_PAGINATION_API = API_V1 + "search_" + VERSION + "upcoming_course_mentor_pagination";
    public static final String SEARCH_MENTOR_VIDEO_PAGINATION_API = API_V1 + "search_" + VERSION + "video_mentor_pagination ";
    public static final String SEARCH_MENTOR_PROGRAMME_PAGINATION_API = API_V1 + "search_" + VERSION + "programme_mentor_pagination";

    public static final String SEARCH_TAG_COURSE_PAGINATION_API = API_V1 + "search_" + VERSION + "course_tag_pagination";
    public static final String SEARCH_TAG_UPCOMING_COURSE_PAGINATION_API = API_V1 + "search_" + VERSION + "upcoming_course_tag_pagination";
    public static final String SEARCH_TAG_VIDEO_PAGINATION_API = API_V1 + "search_" + VERSION + "video_tag_pagination";
    public static final String SEARCH_TAG_PROGRAMME_PAGINATION_API = API_V1 + "search_" + VERSION + "programme_tag_pagination";
    public static final String SEARCH_TAG_MENTOR_PAGINATION_API = API_V1 + "search_" + VERSION + "mentor_tag_pagination";

    public static final String SEARCH_CHAPTER_COURSE_PAGINATION_API = API_V1 + "search_" + VERSION + "chapter_pagination";


    public static final String COURSE_DETAIL_API = API_V1 + "course_" + VERSION + "course_detail_android_new";
    public static final String VIDEO_DETAIL_API = API_V1 + "video_" + VERSION + "video_detail_android";
    public static final String MENTOR_DETAIL_API = API_V1 + "mentor_" + VERSION + "profile_android";
    public static final String EVENT_DETAIL_API = API_V1 + "event_" + VERSION + "event_detail";

    public static final String RATING_PAGINATION = API_V1 + "course_" + VERSION + "rating_pagination";
    public static final String COURSE_DETAIL_RELATED_COURSES_PAGINATION = API_V1 + "course_" + VERSION + "related_course_pagination";
    public static final String COURSE_DETAIL_RELATED_PROGRAMME_PAGINATION = API_V1 + "course_" + VERSION + "related_programme_pagination";
    public static final String COURSES_YOU_MAY_LIKE = API_V1 + "course_list_" + VERSION + "all_course_you_may_like";

    public static final String MENTOR_DETAIL_COURSE_PAGINATION = API_V1 + "mentor_" + VERSION + "course_pagination";
    public static final String MENTOR_DETAIL_VIDEO_PAGINATION = API_V1 + "mentor_" + VERSION + "video_pagination";
    public static final String MENTOR_DETAIL_PROGRAMME_PAGINATION = API_V1 + "mentor_" + VERSION + "programme_pagination";
    public static final String MENTOR_DETAIL_EVENT_PAGINATION = API_V1 + "mentor_" + VERSION + "event_pagination";
    public static final String MENTOR_DETAIL_DIGITAL_PAGINATION = API_V1 + "mentor_" + VERSION + "digital_coaching_pagination";
    public static final String MENTOR_DETAIL_DIGITAL_COACHING_DETAIL_PAGE = API_V1 + "mentor_" + VERSION + "digital_coaching_subscription";
    public static final String MENTOR_DETAIL_DIGITAL_COACHING_PAGE = API_V1 + "mentor_" + VERSION + "mentor_digital_coachings";

    public static final String QUIZ_API = API_V1 + "quiz_" + VERSION + "quiz";
    public static final String SAVE_CURRENT_QUIZ_API = API_V1 + "quiz_" + VERSION + "save_current_question";
    public static final String SAVE_QUESTION_FOR_USER_QUIZ_API = API_V1 + "quiz_" + VERSION + "save_question_for_user";
    public static final String RETAKE_QUIZ_API = API_V1 + "quiz_" + VERSION + "retake_quiz";
    public static final String QUIZ_RESULT_API = API_V1 + "quiz_" + VERSION + "quiz_result";


    public static final String WISHLIST_API = API_V1 + "wishlist_" + VERSION + "wishlist_android";
    public static final String ADD_WISHLIST_API = API_V1 + "wishlist_" + VERSION + "add_to_wishlist";
    public static final String REMOVE_WISHLIST_API = API_V1 + "wishlist_" + VERSION + "remove_wishlist";
    public static final String WISHLIST_COURSE_PAGINATION = API_V1 + "wishlist_" + VERSION + "course_pagination";
    public static final String WISHLIST_PROGRAMME_PAGINATION = API_V1 + "wishlist_" + VERSION + "programme_pagination";
    public static final String WISHLIST_EVENT_PAGENATION = API_V1 + "wishlist_" + VERSION + "event_pagination" ;
    public static final String WISHLIST_MENTOR_PAGENATION = API_V1 + "wishlist_" + VERSION + "expert_pagination" ;
    public static final String WISHLIST_VIDEO_PAGENATION = API_V1 + "wishlist_" + VERSION + "video_pagination" ;

    public static final String MESSAGE_API = API_V1 + "general_" + VERSION + "notifications";
    public static final String MESSAGE_DETAIL_API = API_V1 + "general_" + VERSION + "message_detail";
    public static final String MESSAGE_SEEN_API = API_V1 + "general_" + VERSION + "seen_notifications";


    public static final String EXISTING_SUPPORT_REQUESTS_API = API_V1 + "customer_support_" + VERSION + "existing_support_requests";
    public static final String ADD_SUPPORT_REQUESTS_API = API_V1 + "customer_support_" + VERSION + "add_online_customer_contact";
    public static final String REPLY_SUPPORT_REQUEST_API = API_V1 + "customer_support_" + VERSION + "reply_contact_request";
    public static final String REPLY_SUPPORT_LIST_DETAIL_API = API_V1 + "customer_support_" + VERSION + "reply_list";
    public static final String SUPPORT_TYPES_API = API_V1 + "customer_support_" + VERSION + "support_type";
    public static final String COUNTRY_CODES_API = API_V1 + "general_" + VERSION + "country_codes";
    //https://www.wdotestserver.com/app-v1/general_v1/country_codes

    public static final String CUSTOMER_DATA_API = API_V1 + "customer_" + VERSION + "user_detail";


    public static final String SET_PREFERENCES_API = API_V1 + "general_" + VERSION + "set_preference";
    public static final String GET_PREFERENCES_API = API_V1 + "general_" + VERSION + "taste_preference";
    public static final String SAVE_VIDEO_TIME_API = API_V1 + "general_" + VERSION + "save_seek_timing";


    public static final String GET_SETTINGS_API = API_V1 + "general_" + VERSION + "playback_setting";
    public static final String SET_SETTINGS_API = API_V1 + "general_" + VERSION + "set_playback_setting";
    public static final String SET_NOTIFICATION_SETTINGS_API = API_V1 + "general_" + VERSION + "notification_setting";

    public static final String PAYMENT_HISTORY_API = API_V1 + "general_" + VERSION + "payment_history";
    public static final String BADGES_API = API_V1 + "quiz_" + VERSION + "all_badges";

    public static final String PROGRAMMES_DETAIL_API = API_V1 + "programme_" + VERSION + "programme_detail_android";
    public static final String EPISODES_DETAIL_API = API_V1 + "episode_" + VERSION + "episode";
    public static final String ADD_EVENT_LOG = API_V1 + "log_" + VERSION + "add_event_log";
    public static final String UPDATE_USER_TOKEN = API_V1 + "general_" + VERSION + "save_token";
    public static final String CHECK_APP_VERSION = API_V1 + "general_" + VERSION + "android_app_version";
    public static final String LOGIN_HISTORY = API_V1 + "general_" + VERSION + "save_login_history";
    public static final String DEEP_LINK_API = BASE + "appdomains/item_type";
    public static final String PAYMENT_THORUGH_CARD_LIST_API = API_V1+"payments/cutomer_take_item_forever_direct";
    public static final String PAYMENT_CARD_LIST =BASE+"payments/customer_credit_card_details";
    public static final String DELETE_CARD =BASE+"payments/delete_credit_card_details";
    public static final String ITEM_PURCHASE_API = BASE+"payments/cutomer_take_item_forever";
    public static final String ECLASSES_ICON_API = BASE + "general_" + VERSION +"eclass_icons";


    public static final String MAINTENANCE_PAGE = API_V1 + "general_" + VERSION +"maintenance";
    public static final String MAINTENANCE_NOTIFICATION = API_V1 + "general_" + VERSION +"maintenance_notification";

    public static final String ENROLLMENT_API = API_V1 + "general_" + VERSION +"save_enrollment";
    public static final String ENROLLMENT_PROGRAMME_API = API_V1 + "general_" + VERSION +"save_programme_enrollment";

    public static final String INVOICE_API = API_V1 + "general_" + VERSION +"invoice";
    public static final String RESEND_INVOICE_API = API_V1 + "account_" + VERSION +"resend_invoice";
    public static final String MY_PURCHASE_API = API_V1 + "account_" + VERSION +"my_purchase";
    public static final String MY_PURCHASE_PROGRAMME_PAGINATION_API = API_V1 + "account_" + VERSION +"programme_purchase_pagination";

    public static final String MY_PURCHASE_PAGINATION_API = API_V1 + "account_" + VERSION +"my_purchase_pagination";
    public static final String JOIN_DIGITAL_PAYMENT_API = BASE + "payment_" + VERSION +"digital_coaching_subscription";
    public static final String JOIN_DIGITAL_PAYMENT_SAVE_CARD_API = BASE + "payment_" + VERSION +"digital_coaching_subscription_by_saved_card";
    public static final String NORMAL_SUBSCRIBE = BASE + "mentor_" + VERSION +"normal_subscribe";
    public static final String FEEDS_API = BASE + "mentor_" + VERSION +"feeds";
    public static final String FEEDS_API_PAGINATION = BASE + "mentor_" + VERSION +"feed_pagination";

    public static final String NEW_VIDEO_URL = BASE + "mentor_" + VERSION +"get_video_url";
    public static final String SUBSCRIPTION_PAYMENT_API = BASE + "payment_" + VERSION +"stripe_subscription";

    public static final String USER_CATEGORIES = BASE + "category_" + VERSION +"user_categories";
    public static final String SAVE_USER_CATEGORIES = BASE + "category_" + VERSION +"save_user_category";
    public static final String CATEGORY_SKIP = BASE + "category_" + VERSION +"skip_user_category";
    public static final String SAVE_FB_IMAGE = API_V1 + "general_" + VERSION +"save_fb_img";
    public static final String ON_SALE_PAGINATION = API_V1 + "dashboard_" + VERSION +"onsale_pagination";

    public static final String WATCH_LIVE = API_V1 + "go_live_" + VERSION +"watch_live_session";

    public static final String LIVE_USER_COUNT = API_V1 + "go_live_" + VERSION +"live_audience";

    public static final String LIVE_USER_CLOSE_SESSION = API_V1 + "go_live_" + VERSION +"close_session";









//https://services.wealthdragons.com/app-v1/go_live_v1/watch_live_session
    //https://www.abusinesspage.com/app-v1/payment_v1/stripe_subscription

    //http://www.skincarebusiness.net/mentor_v1/get_video_url (device,user_token,item_id,item_type)

//    'Video' => 'Events'
//            'Course' => 'Course Intro video'
//            'Chapter' => 'E Classes'
//            'DigitalVideoIntro' => Digital coaching Intro Video
//'DigitalCoaching' => Digital Coachings
//'Announcements' => 'Experts Feeds'
//        'Expert' => 'Expert Intro Video'


    //http://www.skincarebusiness.net/mentor_v1/feed_pagination
    //https://www.wealthdragonsonline.com/appdomains/item_type
    //https://www.abusinesspage.com/payments/delete_credit_card_details
    //https://www.abusinesspage.com/app-v1/payment_v1/purchase_item

    //add_event_log
    //https://www.wdotestserver.com/app-v1/general_v1/save_login_history
    // https://www.wdotestserver.com/app-v1/episode_v1/episode
    //https://www.wdotestserver.com/app-v1/mentor_list_v1/all_mentors
    //https://www.wdotestserver.com/app-v1/video_list_v1/all_videos
    //https://www.wdotestserver.com/app-v1/course_list_v1/all_course
    //https://www.wdotestserver.com/app-v1/category_v1/taste_android
    //https://www.wdotestserver.com/app-v1/programme_list_v1/all_programmes
    //https://www.wdotestserver.com/app-v1/course_list_v1/all_onsale
    //https://www.wdotestserver.com/app-v1/course_list_v1/all_upcoming_course
    //https://www.wdotestserver.com/app-v1/category_v1/taste_android_new
    //https://www.wdotestserver.com/app-v1/category_v1/mentor_pagination
    //https://www.wdotestserver.com/app-v1/category_v1/course_pagination
    //https://www.wdotestserver.com/app-v1/category_v1/video_pagination
    //https://www.wdotestserver.com/app-v1/category_v1/programme_pagination
    //https://www.wdotestserver.com/app-v1/category_v1/upcoming_course_pagination
    //https://www.wdotestserver.com/app-v1/course_list_v1/all_upcoming_course_android
    //https://www.wdotestserver.com/app-v1/mentor_list_v1/all_mentors_android
    //https://www.wdotestserver.com/app-v1/course_list_v1/all_course_android
    //https://www.wdotestserver.com/app-v1/video_list_v1/all_videos_android
    //https://www.wdotestserver.com/app-v1/course_list_v1/all_onsale_android
    //https://www.wdotestserver.com/app-v1/category_v1/onsale_pagination
    //https://www.wdotestserver.com/app-v1/general_v1/about_us
    //https://www.wdotestserver.com/app-v1/search_v1/search
    //https://www.wdotestserver.com/app-v1/search_v1/video_pagination
    //https://www.wdotestserver.com/app-v1/search_v1/mentor_pagination
    //https://www.wdotestserver.com/app-v1/search_v1/course_pagination
    //https://www.wdotestserver.com/app-v1/search_v1/programme_pagination
    //https://www.wdotestserver.com/app-v1/search_v1/upcoming_course_pagination
    //https://www.wdotestserver.com/app-v1/category_v1/filter_subcategory
    //https://www.wdotestserver.com/app-v1/search_v1/key_words
    //https://www.wdotestserver.com/app-v1/course_v1/course_detail_android
    //https://www.wdotestserver.com/app-v1/video_v1/video_detail
    //https://www.wdotestserver.com/app-v1/mentor_v1/profile
    //https://www.wdotestserver.com/app-v1/course_v1/rating_pagination
    //https://www.wdotestserver.com/app-v1/course_v1/related_course_pagination
    //https://www.wdotestserver.com/app-v1/course_v1/related_programme_pagination
    //https://www.wdotestserver.com/app-v1/course_list_v1/all_course_you_may_like
    //https://www.wdotestserver.com/app-v1/mentor_v1/course_pagination
    //https://www.wdotestserver.com/app-v1/mentor_v1/video_pagination
    //https://www.wdotestserver.com/app-v1/mentor_v1/programme_pagination
    //https://www.wdotestserver.com/app-v1/quiz_v1/quiz
    //https://www.wdotestserver.com/app-v1/quiz_v1/save_current_question
    //https://www.wdotestserver.com/app-v1/quiz_v1/save_question_for_user
    //https://www.wdotestserver.com/app-v1/quiz_v1/retake_quiz
    //https://www.wdotestserver.com/app-v1/quiz_v1/quiz_result
    //https://www.wdotestserver.com/app-v1/wishlist_v1/add_to_wishlist
    //https://www.wdotestserver.com/app-v1/wishlist_v1/remove_wishlist
    //https://www.wdotestserver.com/app-v1/wishlist_v1/wishlist
    //device, user_token,type,item_id
    //https://www.wdotestserver.com/app-v1/wishlist_v1/course_pagination
    //https://www.wdotestserver.com/app-v1/wishlist_v1/programme_pagination
    //https://www.wdotestserver.com/app-v1/general_v1/notifications
    //https://www.wdotestserver.com/app-v1/general_v1/message_detail
    //https://www.wdotestserver.com/app-v1/general_v1/seen_notifications

    //https://www.wdotestserver.com/app-v1/customer_support_v1/existing_support_requests
    //https://www.wdotestserver.com/app-v1/customer_support_v1/add_online_customer_contact
    //https://www.wdotestserver.com/app-v1/customer_support_v1/reply_contact_request
    //https://www.wdotestserver.com/app-v1/customer_support_v1/reply_list
    //https://www.wdotestserver.com/app-v1/customer_support_v1/support_type
    //https://www.wdotestserver.com/app-v1/general_v1/country_codes
    //https://www.abusinesspage.com/app-v1/payment_v1/stripe_subscription

    //https://www.wdotestserver.com/app-v1/customer_v1/user_detail

    //https://www.wdotestserver.com/app-v1/event_list_v1/all_events_android

    //https://www.wdotestserver.com/app-v1/event_v1/event_detail

    //https://www.wdotestserver.com/app-v1/wishlist_v1/event_pagination

    //https://www.wdotestserver.com/app-v1/category_v1/event_pagination


    //https://www.wdotestserver.com/app-v1/general_v1/set_preference

    //https://www.wdotestserver.com/app-v1/general_v1/about_us_android


    //https://www.wdotestserver.com/app-v1/general_v1/playback_setting
    //https://www.wdotestserver.com/app-v1/general_v1/set_playback_setting
    //https://www.wdotestserver.com/app-v1/general_v1/notification_setting  enable 0 or 1
    //https://www.wdotestserver.com/app-v1/general_v1/payment_history
    //https://www.wdotestserver.com/app-v1/quiz_v1/all_badges

    //https://www.wdotestserver.com/app-v1/programme_v1/programme_detail
    //https://www.wdotestserver.com/app-v1/general_v1/save_seek_timing
    //Service URL: https://www.wdotestserver.com/app-v1/general_v1/save_token
    //Params: type(IOS/Android), user_token, token(for notification)
    //https://www.wdotestserver.com/app-v1/general_v1/app_version

}
