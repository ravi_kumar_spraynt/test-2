package com.imentors.wealthdragons.videoDetailView;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.BaseActivity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.fragments.DashBoardDetailFragment;
import com.imentors.wealthdragons.interfaces.OpenFullScreenVideoListener;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.DashBoardVideoDetails;
import com.imentors.wealthdragons.models.VideoData;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VideoDetailHeader extends LinearLayout {


    private Activity mActivity;
    private Context mContext;
    private WealthDragonTextView mTvCourseTitle, mTvCoursePunchLine, mTvCoursePreferance;
    private ImageView mIvVideoPlay;
    private ImageView mIvCourseImage;
    private PlayerView mExoPlayerView;
    private FrameLayout mVideoFrameLayout;
    private MediaSource mVideoSource;
    private FrameLayout mFullScreenButton,mDialogFullScreenButton;
    private ImageView mFullScreenIcon, mPlayIcon;
    private SimpleExoPlayer mPlayer;
    private DashBoardVideoDetails mDashBoardDetails;
    private boolean isFromActivityResult = false, isPaused = false,isTablet;

    private int mResumeWindow;
    private long mResumePosition;
    private String mVideoUrl;
    private OpenFullScreenVideoListener mOpenFullScreenVideoListener;
    private List<String> mVideoUrls = new ArrayList<>();
    private List<Chapters.EClasses> eClassesArrayList = new ArrayList<>();
    private Utils.DialogInteraction mDialogListener;
    private boolean mExoPlayerFullscreen, mIsVideoError = false;
    private Dialog mFullScreenDialog;
    private PlayerView mDialogplayerView;
    private boolean isPotrait, isBufferering = true;
    private boolean isLandScape;
    private FrameLayout mShareBtn;
    private WealthDragonTextView mVideoError;
    private ImageButton mPlayImageButton;


    private String imageUrl, thumbUrl;
    private String mVideoNameForListenerLog;
    private LinearLayout nextVideoFrame;
    private TextView mNextVideo;
    private RelativeLayout mLlHeadingContainer;
    private TextView mTvEClassNameSmallVideo;
    private ImageView dialogPosterImage;
    private ProgressBar mProgressBar;
    private ImageView mArticleWishlist;
    private boolean mIsItemAdded;
    private TextView mDialogVideoError;

    public VideoDetailHeader(Context context) {
        super(context);
        mContext = context;
    }

    public VideoDetailHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public VideoDetailHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    public VideoDetailHeader(Context context, DashBoardVideoDetails dashBoardDetails, Activity activity) {
        super(context);
        mActivity = activity;
        isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set data in view
    private void bindView(DashBoardVideoDetails dashBoardDetails) {

        mDashBoardDetails = dashBoardDetails;

        if (dashBoardDetails.isSocial_sharing()) {
            mShareBtn.setVisibility(View.VISIBLE);
        } else {
            mShareBtn.setVisibility(View.GONE);
        }

        if (dashBoardDetails.isWishlist()) {
            mArticleWishlist.setVisibility(View.VISIBLE);

        } else {
            mArticleWishlist.setVisibility(View.GONE);

        }

        if (dashBoardDetails.getVideoDetail().getWishlist().equals(mContext.getString(R.string.no))) {

            mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart);

            mIsItemAdded = false;

        } else {

            mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);

            mIsItemAdded = true;

        }

        mArticleWishlist.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mIsItemAdded ) {
                    mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart);
                    callWishListApi(mDashBoardDetails.getVideoDetail().getId());

                }else{
                    mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);
                    callWishListApi(mDashBoardDetails.getVideoDetail().getId());

                }
                mIsItemAdded = !mIsItemAdded;
            }
        });




        if (!TextUtils.isEmpty(mDashBoardDetails.getVideoDetail().getPlay_url()) && !TextUtils.isEmpty(dashBoardDetails.getVideoDetail().getPlay_url()) && !dashBoardDetails.getVideoDetail().getPlay_url().equals("NoVideoExist")) {
            // add only intro video in list
            mVideoUrl = mDashBoardDetails.getVideoDetail().getPlay_url();
        }

        if(isTablet){
            imageUrl = Utils.getImageUrl(dashBoardDetails.getVideoDetail().getMobile_big_banner(),924,549);
            thumbUrl = Utils.getImageUrl(dashBoardDetails.getVideoDetail().getLow_qty_mobile_big_banner(), 924, 549);
            Glide.with(mContext).load(imageUrl).thumbnail(Glide.with(mContext).load(thumbUrl).dontAnimate()).dontAnimate().error(R.drawable.no_img_tab).into(mIvCourseImage);


        }else{
            imageUrl = Utils.getImageUrl(dashBoardDetails.getVideoDetail().getMobile_small_banner(), Utils.getScreenWidth((Activity) mContext), Utils.getDetailScreenImageHeight((Activity) mContext));
            thumbUrl = Utils.getImageUrl(dashBoardDetails.getVideoDetail().getLow_qty_mobile_small_banner(), Utils.getScreenWidth((Activity) mContext), Utils.getDetailScreenImageHeight((Activity) mContext));
            Glide.with(mContext).load(imageUrl).thumbnail(Glide.with(mContext).load(thumbUrl).dontAnimate()).dontAnimate().error(R.drawable.no_img_mob).into(mIvCourseImage);

        }

//        mIvCourseImage.setImageURI(imageUrl);



        if (!TextUtils.isEmpty(dashBoardDetails.getVideoDetail().getTitle())) {
            mTvCourseTitle.setVisibility(View.VISIBLE);
            mTvCourseTitle.setText(dashBoardDetails.getVideoDetail().getTitle());
            mVideoNameForListenerLog = mDashBoardDetails.getVideoDetail().getTitle();
            mTvEClassNameSmallVideo.setText(mVideoNameForListenerLog);
        } else {
            mTvCourseTitle.setVisibility(View.GONE);
        }




        if (!TextUtils.isEmpty(dashBoardDetails.getVideoDetail().getPunch_line())) {
            mTvCoursePunchLine.setVisibility(View.VISIBLE);
            mTvCoursePunchLine.setText(dashBoardDetails.getVideoDetail().getPunch_line());
        } else {
            mTvCoursePunchLine.setVisibility(View.GONE);
        }

        mTvCoursePunchLine.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));


        if (!TextUtils.isEmpty(dashBoardDetails.getVideoDetail().getTaste_prefrence())) {
            mTvCoursePreferance.setVisibility(View.VISIBLE);
            mTvCoursePreferance.setText(dashBoardDetails.getVideoDetail().getCategory_label() + ": " + dashBoardDetails.getVideoDetail().getTaste_prefrence());
        } else {
            mTvCoursePreferance.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(dashBoardDetails.getVideoDetail().getPlay_url()) && !TextUtils.isEmpty(dashBoardDetails.getVideoDetail().getPlay_url()) && !dashBoardDetails.getVideoDetail().getPlay_url().equals("NoVideoExist")) {
            mIvVideoPlay.setVisibility(View.VISIBLE);
            mExoPlayerView.setVisibility(View.INVISIBLE);
            mVideoError.setVisibility(INVISIBLE);

            initFullscreenButton();
        } else {
            mIvVideoPlay.setVisibility(View.INVISIBLE);

        }

        mShareBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>Share</b> from " + mDashBoardDetails.getVideoDetail().getTitle() + "<b> </b>" + Constants.TYPE_VIDEO + " type");

                Utils.shareArticleUrl(mDashBoardDetails.getVideoDetail().getShare_url(), mContext, mContext.getString(R.string.share_event));
            }
        });

        mExoPlayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if(visibility == View.VISIBLE){
                    if(mDashBoardDetails.isWishlist()) {
                        mArticleWishlist.setVisibility(VISIBLE);
                    }else{
                        mArticleWishlist.setVisibility(GONE);
                    }

                }else{
                    mArticleWishlist.setVisibility(GONE);

                }
            }
        });



        mIvVideoPlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(mContext)) {
                    initFullscreenDialog();
                    if (AppPreferences.getWifiState() && !Utils.IsWifiConnected()) {

                        Utils.showTwoButtonAlertDialog(mDialogListener, mContext, mContext.getString(R.string.notification), mContext.getString(R.string.check_wifi_status), mContext.getString(R.string.okay_text), mContext.getString(R.string.go_to_setting));

                    } else {

                        mIvVideoPlay.setVisibility(GONE);
                        callVideoUrl(mDashBoardDetails.getVideoDetail().getId());

                        Utils.callEventLogApi("clicked play button of <b>" + mVideoNameForListenerLog+"</b> in video detail");

                    }
                } else {
                    Utils.showNetworkError(mContext, new NoConnectionError());
                }
            }
        });


    }

    //  initialize video detail header view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (context instanceof OpenFullScreenVideoListener) {
            mOpenFullScreenVideoListener = (OpenFullScreenVideoListener) context;
        }

        if (context instanceof Utils.DialogInteraction) {
            mDialogListener = (Utils.DialogInteraction) context;
        }

        View view = inflater.inflate(R.layout.course_detail_header, this);
        mContext = context;

        // heading
        mTvCourseTitle = view.findViewById(R.id.tv_course_detail_title);
        mTvCoursePunchLine = view.findViewById(R.id.tv_course_detail_punch_line);
        mTvCoursePreferance = view.findViewById(R.id.txt_course_detail_taste_preferance);
        mShareBtn = view.findViewById(R.id.fl_btn_full_details);
        mVideoError = view.findViewById(R.id.video_error);
        mProgressBar = view.findViewById(R.id.progress_bar);
        mArticleWishlist = view.findViewById(R.id.iv_add_wishlist);


        // video Frame
        mVideoFrameLayout = view.findViewById(R.id.frame_video);
        mVideoFrameLayout.getLayoutParams().height = (int) Utils.getDetailScreenImageHeight((Activity) mContext);


        mIvVideoPlay = view.findViewById(R.id.iv_video_play);
        mIvCourseImage = view.findViewById(R.id.iv_video_image);
        mExoPlayerView = findViewById(R.id.exoplayer);
        mTvEClassNameSmallVideo = mExoPlayerView.findViewById(R.id.tv_eclasses_title);

        mExoPlayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if(visibility == View.VISIBLE){
                    mArticleWishlist.setVisibility(VISIBLE);
                }else{
                    mArticleWishlist.setVisibility(GONE);

                }
            }
        });


        LocalBroadcastManager.getInstance(mContext).registerReceiver(mSaveVideoPosition, new IntentFilter(Constants.BROADCAST_SAVE_VIDEO_POSITION));


    }



    private void bufferingListener(){
        if(mExoPlayerView.getVisibility() == View.INVISIBLE) {
            mProgressBar.setVisibility(VISIBLE);
            mIvCourseImage.setVisibility(VISIBLE);
        }
    }


    private void bufferingStopListener() {
        if (mIsVideoError) {
            mPlayImageButton.setActivated(false);
            mIvCourseImage.setVisibility(View.GONE);
            mIvVideoPlay.setVisibility(View.GONE);
            mVideoError.setVisibility(VISIBLE);
            mExoPlayerView.setVisibility(View.GONE);
            mProgressBar.setVisibility(GONE);
        } else {
            isBufferering = false;
            if (mExoPlayerView.getVisibility() == View.INVISIBLE) {

                mProgressBar.setVisibility(GONE);
                mExoPlayerView.setVisibility(VISIBLE);
                mExoPlayerView.setControllerAutoShow(true);
                mExoPlayerView.setUseController(true);
                mIvCourseImage.setVisibility(INVISIBLE);
            }
        }
    }





    private BroadcastReceiver mSaveVideoPosition = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            boolean isDetailOnTop = intent.getBooleanExtra(Constants.IS_TOP_FRAGMENT, false);

            if (mExoPlayerView != null && mPlayer != null) {
                mResumeWindow = mPlayer.getCurrentWindowIndex();
                AppPreferences.setVideoResumeWindow(mVideoUrl, mResumeWindow);
                mResumePosition = Math.max(0, mExoPlayerView.getPlayer().getContentPosition());
                AppPreferences.setVideoResumePosition(mVideoUrl, mResumePosition);
            }

            if (!isDetailOnTop) {
                Utils.releaseDetailScreenPlayer();
                Utils.releaseFullScreenPlayer();
            }

            if (!isFromActivityResult && !isPaused) {
                mExoPlayerView.setVisibility(INVISIBLE);
                mIvCourseImage.setVisibility(INVISIBLE);
                mIvVideoPlay.setVisibility(INVISIBLE);
                mVideoError.setVisibility(INVISIBLE);

            }

            isFromActivityResult = false;
            isPaused = false;

        }
    };


    private void videoErrorListener(){
        mIsVideoError = true;
        if(Utils.isNetworkAvailable(mContext)) {

            if (mPlayer != null) {
                if (mPlayer.getPlayWhenReady()) {
                    try {
                        if (isLandScape) {
                            closeFullscreenDialog();
                        }

                        mPlayImageButton.setActivated(false);
                        mIvCourseImage.setVisibility(View.INVISIBLE);
                        mIvVideoPlay.setVisibility(View.INVISIBLE);
                        mVideoError.setVisibility(VISIBLE);
                        mExoPlayerView.setVisibility(View.INVISIBLE);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }else{
            if (isLandScape) {
                closeFullscreenDialog();
            }
            Utils.showNetworkError(mContext, new NoConnectionError());

        }
    }




    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (mExoPlayerView != null && mPlayer != null && mExoPlayerView.getPlayer() != null) {
            mResumeWindow = mPlayer.getCurrentWindowIndex();
            AppPreferences.setVideoResumeWindow(mVideoUrl, mResumeWindow);
            mResumePosition = Math.max(0, mExoPlayerView.getPlayer().getContentPosition());
            AppPreferences.setVideoResumePosition(mVideoUrl, mResumePosition);
            mPlayer.release();
            mPlayer = null;

            // releasing full screen player too
            Utils.releaseFullScreenPlayer();
            Utils.releaseDetailScreenPlayer();
        }

        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mSaveVideoPosition);

    }


    private void initFullscreenButton() {

        mFullScreenIcon = mExoPlayerView.findViewById(R.id.exo_fullscreen_icon);
        mPlayImageButton = mExoPlayerView.findViewById(R.id.exo_play);

        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_expand));
        mFullScreenButton = mExoPlayerView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                openFullScreenVideo(Surface.ROTATION_0);
            }

        });
    }


    //  opened full screen video mode
    public void openFullScreenVideo(int rotationAngle) {
         if (!mExoPlayerFullscreen) {
            if (!isLandScape) {
                openFullscreenDialog(rotationAngle);
            }
        } else {
            if (!isPotrait) {
                closeFullscreenDialog();
            }
        }
    }


    public boolean getPlayBackState() {
        return mPlayer.getPlayWhenReady();
    }






    public void pauseVideo() {
        if (mPlayer != null) {
            mPlayer.setPlayWhenReady(false);
        }

        isPaused = true;
    }

    public VideoData getCurrentVideoData() {
        VideoData videoDataToBeSent = new VideoData();

        try {

            if (mPlayer != null) {
                videoDataToBeSent.setTime(Long.toString(mPlayer.getContentPosition() / 1000));

                videoDataToBeSent.setType(Constants.TYPE_VIDEO);

                videoDataToBeSent.setVideoId(mDashBoardDetails.getVideoDetail().getId());


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return videoDataToBeSent;

    }

    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }


    private void openFullscreenDialog(int rotationAngle) {
        Utils.callEventLogApi("clicked <b>" + "Full Screen Video" + " Mode</b> in Player from " + mDashBoardDetails.getVideoDetail().getTitle());

        isPotrait = false;
        isLandScape = true;
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.activity_video_fullscreen, null);
        mDialogplayerView = dialogView.findViewById(R.id.exoplayer_fullScreen);
        mNextVideo = dialogView.findViewById(R.id.tv_nxtvideo);
        mNextVideo.setVisibility(GONE);
        mDialogVideoError = dialogView.findViewById(R.id.video_error);

        dialogPosterImage = dialogView.findViewById(R.id.iv_poster_image);
        Glide.with(mContext).load(imageUrl).thumbnail(Glide.with(mContext).load(thumbUrl).dontAnimate()).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.no_img_video).into(mIvCourseImage);

        mLlHeadingContainer = dialogView.findViewById(R.id.header_holder);

        mNextVideo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mPlayer.getNextWindowIndex() != -1) {
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());

                }

            }
        });
        nextVideoFrame = dialogView.findViewById(R.id.next_video_image_holder);
        nextVideoFrame.setVisibility(GONE);
        TextView tvEClassName = dialogView.findViewById(R.id.tv_eclasses_title);

        tvEClassName.setText(mDashBoardDetails.getVideoDetail().getTitle());
        ImageView dialogCourseImage = dialogView.findViewById(R.id.iv_video_image);

        Glide.with(mContext).load(imageUrl).into(dialogCourseImage);

        // on Next Click
        nextVideoFrame.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer.getNextWindowIndex() != -1) {
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());

                }
            }
        });

        initDialogFullScreenButton();
        PlayerView.switchTargetView(mExoPlayerView.getPlayer(), mExoPlayerView, mDialogplayerView);
        mFullScreenDialog.addContentView(dialogView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
        Log.d("orientation", "lanscape");

    }

    //  closed full screen dialog
    public void closeFullscreenDialog() {
        Utils.callEventLogApi("clicked <b>" + "Compact Screen Video" + " Mode</b> in Player from " + mDashBoardDetails.getVideoDetail().getTitle());

        isPotrait = true;
        isLandScape = false;
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        PlayerView.switchTargetView(mDialogplayerView.getPlayer(), mDialogplayerView, mExoPlayerView);
        mFullScreenDialog.dismiss();
        mExoPlayerFullscreen = false;
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_expand));
        Log.d("orientation", "potrait");

    }


    private void initDialogFullScreenButton() {

        mDialogplayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if (visibility == View.VISIBLE) {
                    mLlHeadingContainer.setVisibility(VISIBLE);
//                    if (mPlayer.getNextWindowIndex() != -1 && mPlayer.getRepeatMode() == Player.REPEAT_MODE_ALL) {
//                        mNextVideo.setVisibility(VISIBLE);
//                    } else {
//                        mNextVideo.setVisibility(GONE);
//                    }
                    mNextVideo.setVisibility(GONE);
                } else {
                    mLlHeadingContainer.setVisibility(GONE);

                }
            }
        });
        mFullScreenIcon = mDialogplayerView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_skrink));
        mDialogFullScreenButton = mDialogplayerView.findViewById(R.id.exo_fullscreen_button);
        mDialogFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFullscreenDialog();
            }

        });

    }

    public void pressFullScreenDialogButton(){

        mDialogFullScreenButton.performClick();

    }


    public void callWishListApi(final String mItemId) {
        String api = null;

        if (mIsItemAdded) {
            api = Api.REMOVE_WISHLIST_API;

        } else {
            api = Api.ADD_WISHLIST_API;
        }


        //TODO implemented maintenance mode
        // calling add wishlist and remove wishlist api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(mContext);
                        return;
                    }

                    JSONObject jsonObject = new JSONObject(response);

                    if (!TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS)))
                    {


                        if (mIsItemAdded) {

                            Toast.makeText(mContext, "Added to Wishlist", Toast.LENGTH_SHORT).show();

                        }

                    }




                } catch (Exception e) {

                    if (mIsItemAdded) {
                        Utils.callEventLogApi("remove <b>Wishlist </b> error in api ");
                        mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart);


                    } else {

                        Utils.callEventLogApi("add <b>Wishlist </b> error in api");
                        mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);


                    }
                    mIsItemAdded=!mIsItemAdded;
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mIsItemAdded) {
                    Utils.callEventLogApi("remove <b>Wishlist </b> error in api");
                    mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart);

                } else {
                    Utils.callEventLogApi("add <b>Wishlist </b> error in api");
                    mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);


                }
                mIsItemAdded=!mIsItemAdded;
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);


                params.put(Keys.ITEM_ID, mItemId);
                params.put(Keys.TYPE, Constants.TYPE_EVENT);




                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    // for internal use
    private void initExoPlayer() {

        // get position from preferences

        Utils.releaseDetailScreenPlayer();
        mPlayer = null;
        ArrayList<String> mVideoUrlList = new ArrayList<>();
        mVideoUrlList.add(mVideoUrl);

        Utils.initilizeDetailScreenConcatinatedPlayer(mContext, mVideoUrlList, null, false, Constants.TYPE_EXPERT, "Digital Coaching", false);


        mPlayer = Utils.getDetailScreenConcatinatedPlayer();


        mPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {


            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {


                if (playbackState == Player.STATE_ENDED) {

                    Utils.callEventLogApi("watched video of<b>" + mVideoNameForListenerLog+" </b>" + "from Video Detail" );


                    if(mExoPlayerFullscreen){
                        dialogPosterImage.setVisibility(INVISIBLE);
                        mDialogplayerView.setVisibility(View.VISIBLE);
                        mDialogVideoError.setVisibility(INVISIBLE);
                    }


                    mExoPlayerView.setVisibility(GONE);
                    mVideoError.setVisibility(View.INVISIBLE);
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mIvCourseImage.setVisibility(View.VISIBLE);
                    mIvVideoPlay.setVisibility(VISIBLE);
                }


                if (playWhenReady) {
                } else {
                    Utils.callEventLogApi("paused <b>" + mVideoNameForListenerLog + "</b> mentor profile ");
                }


                if (playbackState == Player.STATE_BUFFERING) {
                    bufferingListener();

                } else {

                    bufferingStopListener();
                }

                if(!((BaseActivity)mActivity).isFragmentInBackStack(new DashBoardDetailFragment())){
                    Utils.releaseDetailScreenPlayer();
                }

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                try {

                    Utils.callEventLogApi(error.getSourceException().getMessage() + " in " + mVideoNameForListenerLog + "</b>");


                }
                catch (Exception e)
                {
                    e.printStackTrace();

                }
               videoErrorListener();


            }

            @Override
            public void onPositionDiscontinuity(int reason) {

                // for handling log
                switch (reason) {
                    case Player.DISCONTINUITY_REASON_SEEK:
                        Utils.callEventLogApi("scrolled to  " + "<b>" + Utils.getMinHourSecFromSeconds(mPlayer.getContentPosition() / 1000) + "</b>" + " from" + "<b> " + mVideoNameForListenerLog + "</b>");

                        break;
                }


            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {
                if (mPlayer.getPlaybackState() == Player.STATE_BUFFERING) {
                    bufferingListener();
                } else {
                  bufferingStopListener();
                }
            }
        });


        mPlayer.clearVideoSurface();
        Utils.callEventLogApi("started new video  <b>" + mVideoNameForListenerLog+" </b>" + "in Video Detail" );
        mExoPlayerView.setVisibility(View.VISIBLE);
        mExoPlayerView.setPlayer(mPlayer);


        mResumePosition = Utils.getTimeInMilliSeconds(mDashBoardDetails.getVideoDetail().getSeek_time());

        mPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);

        mPlayer.seekTo(0, mResumePosition);

        mVideoError.setVisibility(View.INVISIBLE);
        mIvCourseImage.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);


        mPlayer.setPlayWhenReady(true);
    }


    private void callVideoUrl(final String mItemId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.NEW_VIDEO_URL, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    mVideoUrl = jsonObject.getString("video_url");
                    initExoPlayer();
                } catch (Exception e) {
                    e.printStackTrace();
                    initExoPlayer();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                initExoPlayer();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.ITEM_ID, mItemId);
                params.put(Keys.ITEM_TYPE, Constants.TYPE_DETAIL_VIDEO);
                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


}
