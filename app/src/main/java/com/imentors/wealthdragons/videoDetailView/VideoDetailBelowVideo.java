package com.imentors.wealthdragons.videoDetailView;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.imentors.wealthdragons.R;

import com.imentors.wealthdragons.models.DashBoardVideoDetails;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.WealthDragonTextView;


public class VideoDetailBelowVideo extends RelativeLayout {


    private Context mContext;
    private FrameLayout mShareBtn , mBookBtn;
    private WealthDragonTextView mMentorDescription, mMentorName , mRatingCount , mBookBtnText, mMentorHeading;
    private RatingBar mRatingBar;
    private LinearLayout mRatingLayout;
    private boolean mIsItemAdded = false,mIsVideo = true;
    private String mItemId;




    public VideoDetailBelowVideo(Context context) {
        super(context);
        mContext = context;
    }

    public VideoDetailBelowVideo(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public VideoDetailBelowVideo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    public VideoDetailBelowVideo(Context context, DashBoardVideoDetails dashBoardDetails) {
        super(context);
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set the data in view
    private void bindView(final DashBoardVideoDetails dashBoardDetails) {


        mItemId=dashBoardDetails.getVideoDetail().getId();







        if(!TextUtils.isEmpty(dashBoardDetails.getVideoDetail().getButton_link()) && !TextUtils.isEmpty(dashBoardDetails.getVideoDetail().getButton_title())){
            mBookBtn.setVisibility(VISIBLE);
            mBookBtnText.setText(dashBoardDetails.getVideoDetail().getButton_title());
        }else{
            mBookBtn.setVisibility(GONE);
        }

        mBookBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>Book Button</b> from <b>"+dashBoardDetails.getVideoDetail().getTitle()+"</b> Detail Screen ");

                Utils.openWebLink(mContext, Uri.parse(dashBoardDetails.getVideoDetail().getButton_link()));
            }
        });





        mRatingBar.setRating(Float.parseFloat(dashBoardDetails.getVideoDetail().getRating()));

        mMentorHeading.setText(dashBoardDetails.getVideoDetail().getExpert_label()+": ");

        mMentorName.setText(dashBoardDetails.getVideoDetail().getMentor_name());

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            mMentorDescription.setText(Html.fromHtml(dashBoardDetails.getVideoDetail().getDescription(),Html.FROM_HTML_MODE_LEGACY));
        } else {
            mMentorDescription.setText(Html.fromHtml(dashBoardDetails.getVideoDetail().getDescription()));
        }


        mRatingCount.setText((int)Float.parseFloat(dashBoardDetails.getVideoDetail().getRating()) + " Ratings");

        mMentorDescription.setMovementMethod(LinkMovementMethod.getInstance());


        if(dashBoardDetails.isShowRatings()){
            mRatingLayout.setVisibility(VISIBLE);
        }else{
            mRatingLayout.setVisibility(GONE);

        }




    }

    //  initialize video detail below video view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.video_detail_below_video, this);
        mContext = context;



        mMentorDescription = view.findViewById(R.id.tv_mentor_description);
        mMentorName = view.findViewById(R.id.tv_mentor_name);
        mRatingCount = view.findViewById(R.id.tv_set_rating_count);
        mRatingBar = findViewById(R.id.ratingBar);
        mBookBtn = view.findViewById(R.id.fl_btn_book_ticket);
        mBookBtnText = view.findViewById(R.id.tv_book_ticket);
        mRatingLayout = view.findViewById(R.id.rating_container);
        mMentorHeading = view.findViewById(R.id.tv_expert_heading);


        if (Utils.isTablet())
        {

            double reuiredButtonWidth = Utils.getScreenWidth((Activity)mContext)/1.5;

            int marginFromStart = (int)(Utils.getScreenWidth((Activity)mContext) - reuiredButtonWidth)/2;

            LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams( ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params4.setMarginStart(marginFromStart-30);

            LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams((int) (reuiredButtonWidth), Utils.dpToPx(60));
            Params.gravity = Gravity.CENTER_HORIZONTAL;
            mBookBtn.setLayoutParams(Params);

        }
        else {
            LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Utils.dpToPx(50));
            mBookBtn.setLayoutParams(Params);
            Params.setMargins(0,16,0,0);
        }


    }







}
