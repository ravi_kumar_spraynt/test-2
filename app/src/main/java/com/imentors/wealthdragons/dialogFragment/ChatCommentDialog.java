package com.imentors.wealthdragons.dialogFragment;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.facebook.keyframes.KeyframesDrawable;
import com.facebook.keyframes.KeyframesDrawableBuilder;
import com.facebook.keyframes.deserializers.KFImageDeserializer;
import com.facebook.keyframes.model.KFImage;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.ChatMessageListAdapter;
import com.imentors.wealthdragons.fbEmojiAnimation.ReactionView;
import com.imentors.wealthdragons.interfaces.FbEmotionClickListener;
import com.imentors.wealthdragons.models.ChatMessage;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.DateTimeUtils;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;


public class ChatCommentDialog extends DialogFragment implements FbEmotionClickListener {

    private final static String IS_CANCEL_TITLE = "IS_CANCEL_TITLE";
    ImageView mIvSend;
    EditText mEtChatComment;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    CollectionReference docRef = db.collection("chat");
    private RecyclerView mRecycleview;
    private LinearLayout mLLBottomContainer;
    private PopupWindow popupWindow, popupWindowComment;
    private ImageView mLike, mHeart, mAngry, mSurprised, mSad, mHappy;
    private int mKeyboardHeight, mEditextHeight, mOriginalKeyboardHeight;
    private FrameLayout mFrameLayout, popupView;
    private onKeyBoardOpened mOnKeyBoardOpened;
    private ChatMessageListAdapter mChatMessagesListAdapter;
    private ArrayList<ChatMessage> data = new ArrayList<>();
    private String mChatToken, mUserId, mUserType, mLiveChatCancelTitle;
    private View mEmptyView;
    private ReactionView mReactionView;
    private LiveChatErrorDialog dialogLiveChatError;

    private Timestamp loginTimeStamp;

//    FirebaseDatabase database = FirebaseDatabase.getInstance("https://chatapp-263fd.firebaseio.com/");
//    DatabaseReference myRef = database.getReference("chatData");

    public static ChatCommentDialog newInstance() {

        Bundle args = new Bundle();
        ChatCommentDialog fragment = new ChatCommentDialog();
        fragment.setStyle(DialogFragment.STYLE_NO_FRAME,
                R.style.Chat_Comment_Dialog);
        fragment.setArguments(args);
        return fragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);


        // Add back button listener
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                // getAction to make sure this doesn't double fire
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    // Your code here
                    showDialog(true);

                    return true; // Capture onKey
                }
                return false; // Don't capture
            }
        });

        return dialog;

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        final View view = inflater.inflate(R.layout.dialog_chat_comment, container, false);


        Utils.callEventLogApi("opened <b> Live Chat Comment</b> dialog");

        mRecycleview = view.findViewById(R.id.lv_listview);


        for (int i = 0; i < 15; i++) {
            data.add(new ChatMessage());
        }



        mEtChatComment = view.findViewById(R.id.et_edittext);


//        mIvUserImage = view.findViewById(R.id.iv_user);
        mLLBottomContainer = view.findViewById(R.id.bottom_container);
        mIvSend = view.findViewById(R.id.iv_send);
        mFrameLayout = view.findViewById(R.id.ll_container);

        mEmptyView = view.findViewById(R.id.empty_view);

        popupView = view.findViewById(R.id.fl_pop_up);

        mReactionView = view.findViewById(R.id.emoji_container);

        mReactionView.setEmojiListener(this);


        final View rootview = getActivity().getWindow().getDecorView();
        rootview.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        Rect r = new Rect();
                        rootview.getWindowVisibleDisplayFrame(r);
                        int screenHeight = rootview.getRootView().getHeight();


                        if (Utils.hasNavBar(getResources())) {
                            mKeyboardHeight = screenHeight - (r.bottom - r.top) - Utils.dpToPx(40);

                            if (mKeyboardHeight > 80) {
                                mKeyboardHeight = mKeyboardHeight - Utils.dpToPx(20);
                            }

                        } else {
                            mKeyboardHeight = screenHeight - (r.bottom - r.top);

                            if (mKeyboardHeight > 10) {
                                mKeyboardHeight = mKeyboardHeight - Utils.dpToPx(20);
                            }
                        }


                        mOriginalKeyboardHeight = screenHeight - (r.bottom - r.top);


                        if (mKeyboardHeight > 80) {
                            mOnKeyBoardOpened.hideCross(true);
                        } else {
                            mOnKeyBoardOpened.hideCross(false);
                        }


                    }
                });


        mEmptyView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    float touchAreaX = Utils.getScreenWidth(getActivity()) - Utils.dpToPx(36);
                    float touchAreaY = Utils.dpToPx(36);


                    if (event.getX() > touchAreaX && event.getY() < touchAreaY) {
                        showDialog(true);
                    }

                    return true;

                }

                return false;
            }
        });


        final ImageView emojiButton = view.findViewById(R.id.emoji_btn);

        emojiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getContext())) {
                    if (popupView.getVisibility() == View.VISIBLE) {
                        popupView.setVisibility(View.GONE);
                    } else {
                        popupView.invalidate();
                        popupView.setVisibility(View.VISIBLE);
                    }
                } else {
                    showDialog(false);
                }

            }
        });


        mLLBottomContainer.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {

                        mEditextHeight = mLLBottomContainer.getHeight();

                    }
                });


        mEtChatComment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    mIvSend.performClick();
                    return true;
                }
                return false;
            }
        });



        showPopup(view);

        showSupportRequestList(data);


        mIvSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getContext())) {
                    String message = mEtChatComment.getText().toString();
                    if (!TextUtils.isEmpty(message.trim())) {


                        docRef.add(new ChatMessage(message
                                , AppPreferences.getCustomerData().getCustomer().getChat_image()
                                , DateTimeUtils.getTimeInHours(System.currentTimeMillis())
                                , mChatToken
                                , AppPreferences.getCategories().getUserData().getUserName()
                                , mUserId
                                , new Timestamp(new Date())
                                , mUserType
                        ));
                        mEtChatComment.getText().clear();

                    }

                } else {
                    showDialog(false);
                }
            }
        });

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (popupWindow != null) {
            popupWindow.dismiss();
            mKeyboardHeight = Utils.dpToPx(20);

        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (getActivity() != null) {
            getActivity().onBackPressed();
            Utils.hideKeyboard(getContext());
        }
    }


    private void showPopup(View view) {


        mLike = view.findViewById(R.id.iv_like);
        mHeart = view.findViewById(R.id.iv_love);
        mHappy = view.findViewById(R.id.iv_haha);
        mSurprised = view.findViewById(R.id.iv_wow);
        mSad = view.findViewById(R.id.iv_sad);
        mAngry = view.findViewById(R.id.iv_angry);


    }


    private void scaleAnimationUp(final ImageView v,final String resourceId) {
        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(v, "scaleX", 1.0f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(v, "scaleY", 1.0f);
        scaleDownX.setDuration(200);
        scaleDownY.setDuration(200);
        AnimatorSet userImageScalingAnimator = new AnimatorSet();
        userImageScalingAnimator.playTogether(scaleDownX, scaleDownY);
        userImageScalingAnimator.start();
        userImageScalingAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });




    }


    private void bubbleEffectDown(final ImageView v, final String resourceId) {

        ScaleAnimation scaleAnimation =new ScaleAnimation(
                0.8f, 1f, // Start and end values for the X axis scaling
                0.8f, 1f, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 1f);

        scaleAnimation.setDuration(1000);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setInterpolator(new BounceInterpolator());
        v.startAnimation(scaleAnimation);

    }


    private void showSupportRequestList(ArrayList<ChatMessage> data) {
        mChatMessagesListAdapter = new ChatMessageListAdapter(data, getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        ((LinearLayoutManager) mLayoutManager).setStackFromEnd(true);
        mRecycleview.setLayoutManager(mLayoutManager);
        mRecycleview.setAdapter(mChatMessagesListAdapter);
        mRecycleview.scrollToPosition(mRecycleview.getAdapter().getItemCount() - 1);

    }


    public void startAnimatingEmojis(String resourceId) {

        if (getActivity() != null) {

            View smileyAnimatingLayout = LayoutInflater.from(getActivity()).inflate(R.layout.smiley_animation_layout, mFrameLayout, false);

            final DisplayMetrics metrics = WealthDragonsOnlineApplication.sharedInstance().getResources().getDisplayMetrics();


            mFrameLayout.addView(smileyAnimatingLayout);
            startAnimation(smileyAnimatingLayout, metrics, resourceId);
        }
    }

    private void startAnimation(final View view, final DisplayMetrics metrics, final String resourceID) {

        if (this == null) {
            return;
        }

        final ImageView animatingBlastYellow, animatingBlastBlue, animatingEmoji, animatingUser;

        animatingBlastBlue = view.findViewById(R.id.iv_blue);
//        animatingBlastYellow = view.findViewById(R.id.iv_yellow);
        animatingEmoji = view.findViewById(R.id.iv_smiley);
        animatingUser = view.findViewById(R.id.iv_user);


        int margin = Utils.dpToPx(40);
        int halfMargin = margin / 2;
        float x = Utils.getScreenWidth(getActivity()) - margin - margin;
        float variationX = margin;

        Random r = new Random();
        int lowX = (int) Utils.getScreenWidth(getActivity()) / 2;
        int highX = (int) Utils.getScreenWidth(getActivity()) - margin - margin;
        float randomX = r.nextInt(highX - lowX) + lowX;

        float height = metrics.heightPixels - metrics.heightPixels / 6;

        int lowY = metrics.heightPixels - metrics.heightPixels / 3;
        int highY = (int) height;
        float randomY = r.nextInt(highY - lowY) + highY-350;






        //smiley animation of moving out of the screen after pop up
        PropertyValuesHolder smileyX = PropertyValuesHolder.ofFloat("x", randomX, x + variationX);
        PropertyValuesHolder smileyY = PropertyValuesHolder.ofFloat("y", randomY, 0);
        ObjectAnimator smileyAnimator = ObjectAnimator.ofPropertyValuesHolder(view, smileyX, smileyY);
        smileyAnimator.setDuration(4000);
        smileyAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

                view.setVisibility(View.VISIBLE);
                AlphaAnimation alpha = new AlphaAnimation(1F, 0F);
                alpha.setDuration(4000);
                alpha.setFillAfter(true);
                view.startAnimation(alpha);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


        // resizing when moving up
        ObjectAnimator scaleDownSmileyX = ObjectAnimator.ofFloat(animatingEmoji, "scaleX", 0.3f);
        final ObjectAnimator scaleDownSmileyY = ObjectAnimator.ofFloat(animatingEmoji, "scaleY", 0.3f);
        scaleDownSmileyX.setDuration(4000);
        scaleDownSmileyY.setDuration(4000);
        AnimatorSet scaleSmileyImageAnimator = new AnimatorSet();
        scaleSmileyImageAnimator.playTogether(scaleDownSmileyX, scaleDownSmileyY);


        final AnimatorSet combineScalingFadingSmiley = new AnimatorSet();
        combineScalingFadingSmiley.playTogether(scaleSmileyImageAnimator, smileyAnimator);





        // get the initial radius for the clipping circle
        // popup animation
        int initialRadius = Utils.dpToPx(40);


        Animator anim =
                ViewAnimationUtils.createCircularReveal(animatingBlastBlue, initialRadius, initialRadius, 0, initialRadius);
        anim.setDuration(100);


        //combine both
        final AnimatorSet combineAnimatorSet = new AnimatorSet();
        combineAnimatorSet.playTogether(anim);


        combineAnimatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

                animatingBlastBlue.setVisibility(View.VISIBLE);
//                animatingBlastYellow.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {

                animatingBlastBlue.setVisibility(View.INVISIBLE);





//                animatingEmoji.setImageDrawable(getResources().getDrawable(resourceID));

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });




        // scaling animation after first animation


        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(animatingUser, "scaleX", 2.0f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(animatingUser, "scaleY", 2.0f);
        scaleDownX.setDuration(200);
        scaleDownY.setDuration(200);
        final AnimatorSet userImageScalingAnimator = new AnimatorSet();

        userImageScalingAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        try{
                            combineAnimatorSet.start();

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },50);


            }

            @Override
            public void onAnimationEnd(Animator animator) {
                Glide.with(getActivity()).load(AppPreferences.getCustomerData().getCustomer().getChat_image()).into(animatingUser);


//                animatingBlastYellow.setVisibility(View.GONE);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        try{
                            animatingUser.setVisibility(View.INVISIBLE);

                            KeyframesDrawable imageDrawable = new KeyframesDrawableBuilder().withImage(getKFImage(resourceID)).build();
                            imageDrawable.startAnimation();
                            animatingEmoji.setVisibility(View.VISIBLE);

                            animatingEmoji.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                            animatingEmoji.setImageDrawable(imageDrawable);
                            animatingEmoji.setImageAlpha(0);
                            bubbleEffectDown(animatingEmoji,resourceID);
//                            scaleAnimationUp(animatingEmoji,resourceID);

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },1000);




                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        try{
                            combineScalingFadingSmiley.start();

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },3000);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        userImageScalingAnimator.playTogether(scaleDownX, scaleDownY);






        // first animation of poping out of screen animation


        PropertyValuesHolder pvyX = PropertyValuesHolder.ofFloat("x", randomX, randomX);
        PropertyValuesHolder pvyY = PropertyValuesHolder.ofFloat("y", metrics.heightPixels, randomY);
        ObjectAnimator userImgAnimator = ObjectAnimator.ofPropertyValuesHolder(view, pvyX, pvyY);
        userImgAnimator.setDuration(500);
        userImgAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                animatingUser.setVisibility(View.VISIBLE);
                if (getActivity() != null) {

                    Glide.with(getActivity()).load(AppPreferences.getCustomerData().getCustomer().getChat_image()).into(animatingUser);
                }

            }

            @Override
            public void onAnimationEnd(Animator animation) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        try{
                            userImageScalingAnimator.start();

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },200);


            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
















        try{
            AnimatorSet animationCollection = new AnimatorSet();
            animationCollection.playSequentially(userImgAnimator);
            animationCollection.start();
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public void setData(String chatToken, String userId, String userType, String CancelTitle) {

        if(TextUtils.isEmpty(mChatToken)) {
            mChatToken = chatToken;
            mUserId = userId;
            mUserType = userType;
            mLiveChatCancelTitle = CancelTitle;


            loginTimeStamp = new Timestamp(new Date());


            docRef.whereEqualTo("chat_token", mChatToken).orderBy("time", Query.Direction.ASCENDING).addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                    if (e != null) {

                        return;
                    }


                    if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty() && queryDocumentSnapshots.size() > 0) {


                        if (queryDocumentSnapshots.getDocumentChanges().size() > 0) {

                            for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {

                                if (data.size() > 22) {
                                    for (int i = 14; i >= 0; i--) {
                                        ChatMessage chat = data.get(i);
                                        if (TextUtils.isEmpty(chat.getChat_token())) {
                                            data.remove(i);
                                            mChatMessagesListAdapter.notifyItemRemoved(i);
                                        }
                                    }
                                }

                                switch (doc.getType()) {
                                    case ADDED:

                                        ChatMessage chatMessage = doc.getDocument().toObject(ChatMessage.class);


                                        if (!TextUtils.isEmpty(chatMessage.getMessage())) {


                                            switch (chatMessage.getMessage()) {
                                                case Constants.ANGRY:

                                                    if (chatMessage.getTime().compareTo(loginTimeStamp) >= 0) {
                                                        startAnimatingEmojis("Anger.json");
                                                    }

                                                    break;

                                                case Constants.SAD:

                                                    if (chatMessage.getTime().compareTo(loginTimeStamp) >= 0) {
                                                        startAnimatingEmojis("Sorry.json");
                                                    }

                                                    break;

                                                case Constants.HAHA:

                                                    if (chatMessage.getTime().compareTo(loginTimeStamp) >= 0) {
                                                        startAnimatingEmojis("Haha.json");

                                                    }

                                                    break;

                                                case Constants.LOVE:

                                                    if (chatMessage.getTime().compareTo(loginTimeStamp) >= 0) {
                                                        startAnimatingEmojis("Love.json");

                                                    }

                                                    break;

                                                case Constants.LIKE:

                                                    if (chatMessage.getTime().compareTo(loginTimeStamp) >= 0) {
                                                        startAnimatingEmojis("Like.json");

                                                    }

                                                    break;

                                                case Constants.WOW:

                                                    if (chatMessage.getTime().compareTo(loginTimeStamp) >= 0) {
                                                        startAnimatingEmojis("Wow.json");

                                                    }

                                                    break;
                                                default:
                                                    data.add(doc.getDocument().toObject(ChatMessage.class));
                                                    if (data.size() > 0) {
                                                        mChatMessagesListAdapter.notifyItemInserted(data.size() - 1);
                                                        mRecycleview.scrollToPosition(mChatMessagesListAdapter.getItemCount() - 1);

                                                    }
                                                    break;

                                            }

                                        }


                                        break;
                                    case MODIFIED:
                                        break;
                                    case REMOVED:
                                        break;
                                }

                            }


                        }
                    }
                }
            });
        }


    }



    private void showDialog(boolean isClosedClicked) {
        dialogLiveChatError = LiveChatErrorDialog.newInstance(isClosedClicked, mLiveChatCancelTitle);
        dialogLiveChatError.show(getFragmentManager(), "dialog");
        dialogLiveChatError.setCancelable(false);

        dialogLiveChatError.setListener((LiveChatErrorDialog.onRetryButtonClicked) getActivity());
    }


    @Override
    public void onEMotionRelease(String kfImage) {
//        showFbEmojiAnimation(kfImage);

//        startAnimatingEmojis(kfImage);

        switch (kfImage) {
            case "Like.json":


                docRef.add(new ChatMessage(Constants.LIKE
                        , AppPreferences.getCategories().getUserData().getUserImage()
                        , DateTimeUtils.getTimeInHours(System.currentTimeMillis())
                        , mChatToken
                        , AppPreferences.getCategories().getUserData().getUserName()
                        , mUserId
                        , new Timestamp(new Date())
                        , mUserType
                ));


                break;

            case "Love.json":

                docRef.add(new ChatMessage(Constants.LOVE
                        , AppPreferences.getCategories().getUserData().getUserImage()
                        , DateTimeUtils.getTimeInHours(System.currentTimeMillis())
                        , mChatToken
                        , AppPreferences.getCategories().getUserData().getUserName()
                        , mUserId
                        , new Timestamp(new Date())
                        , mUserType
                ));

                break;

            case "Haha.json":

                docRef.add(new ChatMessage(Constants.HAHA
                        , AppPreferences.getCategories().getUserData().getUserImage()
                        , DateTimeUtils.getTimeInHours(System.currentTimeMillis())
                        , mChatToken
                        , AppPreferences.getCategories().getUserData().getUserName()
                        , mUserId
                        , new Timestamp(new Date())
                        , mUserType
                ));

                break;

            case "Wow.json":

                docRef.add(new ChatMessage(Constants.WOW
                        , AppPreferences.getCategories().getUserData().getUserImage()
                        , DateTimeUtils.getTimeInHours(System.currentTimeMillis())
                        , mChatToken
                        , AppPreferences.getCategories().getUserData().getUserName()
                        , mUserId
                        , new Timestamp(new Date())
                        , mUserType
                ));
                break;


            case "Sorry.json":

                docRef.add(new ChatMessage(Constants.SAD
                        , AppPreferences.getCategories().getUserData().getUserImage()
                        , DateTimeUtils.getTimeInHours(System.currentTimeMillis())
                        , mChatToken
                        , AppPreferences.getCategories().getUserData().getUserName()
                        , mUserId
                        , new Timestamp(new Date())
                        , mUserType
                ));

                break;

            case "Anger.json":

                docRef.add(new ChatMessage(Constants.ANGRY
                        , AppPreferences.getCategories().getUserData().getUserImage()
                        , DateTimeUtils.getTimeInHours(System.currentTimeMillis())
                        , mChatToken
                        , AppPreferences.getCategories().getUserData().getUserName()
                        , mUserId
                        , new Timestamp(new Date())
                        , mUserType
                ));

                break;

        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("notifications", "message");

                popupView.setVisibility(View.GONE);

            }
        }, 400);


    }



    private KFImage getKFImage(String fileName) {
        AssetManager assetManager = getContext().getAssets();

        InputStream stream;
        KFImage kfImage = null;

        try {
            stream = assetManager.open(fileName);
            kfImage = KFImageDeserializer.deserialize(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return kfImage;
    }

    public void setListener(ChatCommentDialog.onKeyBoardOpened onKeyBoardOpened) {
        mOnKeyBoardOpened = onKeyBoardOpened;
    }


    public interface onKeyBoardOpened {

        void hideCross(boolean showCrossIcon);
    }

}
