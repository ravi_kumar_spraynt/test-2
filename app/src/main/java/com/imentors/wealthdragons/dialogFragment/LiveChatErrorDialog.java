package com.imentors.wealthdragons.dialogFragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.Utils;

public class LiveChatErrorDialog extends DialogFragment {

    private TextView mErrorMessage;
    private Button mClose,mRetry;
    private onRetryButtonClicked mOnRetryClicked;
    private final static String IS_CLOSED = "IS_CLOSED";
    private final static String IS_CANCEL_TITLE = "IS_CANCEL_TITLE";
    private boolean isClosedClicked;
    private String mCancelTitle;



    public static LiveChatErrorDialog newInstance(boolean isClosed,String cancelTitle) {

        Bundle args = new Bundle();
        LiveChatErrorDialog fragment = new LiveChatErrorDialog();
        args.putBoolean(IS_CLOSED,isClosed);
        args.putSerializable(IS_CANCEL_TITLE,cancelTitle);
        fragment.setStyle(DialogFragment.STYLE_NO_FRAME,
                R.style.Chat_Comment_Dialog);
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        boolean isClosedClose = getArguments().containsKey(IS_CLOSED);

        if(isClosedClose){
            isClosedClicked = getArguments().getBoolean(IS_CLOSED);
        }

        mCancelTitle= (String) getArguments().getSerializable(IS_CANCEL_TITLE);
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        setStyle(DialogFragment.STYLE_NORMAL,
//                R.style.Chat_Comment_Dialog);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        View view=inflater.inflate(R.layout.dialog_live_chat_error,container,false);

        Utils.callEventLogApi("opened <b> Live Chat Error</b> dialog");

        mErrorMessage=view.findViewById(R.id.tv_error_message);

        if(isClosedClicked){
            if(TextUtils.isEmpty(mCancelTitle)){
                mErrorMessage.setText("Are you sure you want to exit the Live session?");
            }else{
                mErrorMessage.setText(mCancelTitle);

            }
        }

        mClose=view.findViewById(R.id.bt_close);
        mRetry=view.findViewById(R.id.bt_retry);

        changeButtonName();

        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
                getActivity().onBackPressed();

            }
        });


        mRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
                if(!isClosedClicked){
                    mOnRetryClicked.onRetryClicked();

                }

            }
        });

        return view;
    }

    private void changeButtonName() {
        if (Utils.isNetworkAvailable(getContext()) && isClosedClicked)
        {
            mRetry.setText(R.string.no);
            mClose.setText(R.string.yes);
        }
        else
        {
            mRetry.setText(R.string.retry);
            mClose.setText(R.string.close);

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        changeButtonName();
    }

    public interface onRetryButtonClicked{

        void onRetryClicked( );
    }


    public void setListener(onRetryButtonClicked onRetryButtonClicked){
        mOnRetryClicked = onRetryButtonClicked;
    }


}
