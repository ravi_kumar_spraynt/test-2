package com.imentors.wealthdragons.dialogFragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;


import org.json.JSONException;

import java.util.Map;

public class EnrollmentDialogFragment extends DialogFragment {

    private ProgressBar mProgressBar;
    private FrameLayout mYes ,mNo;
    private String mEclassesId,mCourseId,mCourseTitle;
    private TextView mEnrollMessage;
    private TextView mEnrollNote,mTvResponse;
    private boolean mIsProgrammeType;



    public static EnrollmentDialogFragment newInstance(String eclassesId,String courseId,String courseTitle, boolean isProgramme) {

        //  put data in bundle
        Bundle args = new Bundle();
        EnrollmentDialogFragment fragment = new EnrollmentDialogFragment();
        args.putString(Constants.ITEM_ID,eclassesId);
        args.putString(Constants.CAT_ID,courseId);
        args.putString(Constants.COURSE_TITLE,courseTitle);
        args.putBoolean(Constants.TYPE_PROGRAMMES, isProgramme);
        fragment.setArguments(args);
        return fragment;
    }




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEclassesId = getArguments().getString(Constants.ITEM_ID);
        mCourseId = getArguments().getString(Constants.CAT_ID);
        mCourseTitle = getArguments().getString(Constants.COURSE_TITLE);
        mIsProgrammeType = getArguments().getBoolean(Constants.TYPE_PROGRAMMES);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        }
        else
        {
            setStyle(DialogFragment.STYLE_NORMAL, R.style.MyPaymentFragmentStyle);

        }

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        //getDialog().setTitle(R.string.card_detail);
        View view = inflater.inflate(R.layout.dialog_enrollment, container, false);

//        getDialog().getWindow().setLayout((int) getContext().getResources().getDimension(R.dimen.error_dialog_width), LinearLayout.LayoutParams.WRAP_CONTENT);


        Utils.callEventLogApi("opened <b> Enrollment</b> dialog");

        mProgressBar = view.findViewById(R.id.prog_bar);
        mYes=view.findViewById(R.id.fl_btn_orange_positive);
        mTvResponse=view.findViewById(R.id.tv_response_error);
        mNo=view.findViewById(R.id.fl_btn_orange_negative);
        mProgressBar=view.findViewById(R.id.prog_bar);
        mEnrollMessage=view.findViewById(R.id.tv_enroll_message);
        mEnrollNote=view.findViewById(R.id.tv_enroll_Note);
        mEnrollMessage.setText(getText(R.string.dialog_enroll_message));
        mEnrollMessage.append(" ");
        mEnrollMessage.append(mCourseTitle);
        mEnrollMessage.append("?");

        mYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.callEventLogApi("clicked <b>Yes </b> button in Eclasses Enroll");
                EclassesEnrollApi(mEclassesId,mCourseId);

            }
        });
        mNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.callEventLogApi("clicked <b>No </b> button in Eclasses Enroll");
                getDialog().dismiss();
            }
        });

       return view;
    }

    public void EclassesEnrollApi(final String eclassId, final String courseId)
    {

        // calling eclasses enroll api and maintaining response
        mProgressBar.setVisibility(View.VISIBLE);
         disableView();

         String api = null;

        if(mIsProgrammeType) {
            api = Api.ENROLLMENT_PROGRAMME_API;
        }else{
            api = Api.ENROLLMENT_API;

        }


        //TODO implemented maintenance mode
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getContext());
                        return;
                    }
                    enableView();
                    mProgressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(),"Enrollment Sucessfull",Toast.LENGTH_SHORT).show();
                    Utils.callEventLogApi("Enrollment Sucessfull of <b>"+mCourseTitle +"</b> in Course");
                    getDialog().dismiss();
                    LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_CLEAR_REPONSE));
                    Intent intent = new Intent(Constants.BROADCAST_PAY_FINSIH);
                    intent.putExtra(Constants.ECLASSES_VIDEO_ID, mEclassesId);
                    LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);

                } catch (JSONException e) {
                    mProgressBar.setVisibility(View.GONE);
                    mTvResponse.setVisibility(View.VISIBLE);
                    enableView();
                    mTvResponse.setText(getString(R.string.error_api_try_again));
                    mTvResponse.setTextColor(ContextCompat.getColor(getContext(), R.color.pinkStripColor));
                    Utils.callEventLogApi("getting <b>Eclasses Enroll </b> error in api");
                    e.printStackTrace();

                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // no reaction
                Utils.callEventLogApi("getting <b>Eclasses Enroll </b> error in api");
                mProgressBar.setVisibility(View.GONE);
                mTvResponse.setVisibility(View.VISIBLE);
                enableView();
                mTvResponse.setText(getString(R.string.error_api_try_again));
                mTvResponse.setTextColor(ContextCompat.getColor(getContext(), R.color.pinkStripColor));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                if(mIsProgrammeType) {
                    // handle programme
                    params.put(Keys.PROGRAMME_ID,courseId);
                }else{
                    // handle course

                    params.put(Keys.ECLASS_ID, eclassId);
                    params.put(Keys.COURSE_ID,courseId);
                }




                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }
    private void disableView(){
        mNo.setEnabled(false);
        mYes.setEnabled(false);


    }

    private void enableView(){
        mNo.setEnabled(true);
        mYes.setEnabled(true);


    }
}



