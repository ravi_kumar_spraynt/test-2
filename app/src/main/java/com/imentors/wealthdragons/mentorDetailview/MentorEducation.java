package com.imentors.wealthdragons.mentorDetailview;

import android.content.Context;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.MentorEducationItemsAdapter;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.List;

public class MentorEducation extends LinearLayout {


    private Context mContext;
    private WealthDragonTextView mTvNewItemHeading;
    private RecyclerView mItemListRecyclerView;
    private MentorEducationItemsAdapter mCourseNewItemsAdapter = null;
    private View mLineBelowHeading;



    public MentorEducation(Context context) {
        super(context);
        mContext = context;
    }

    public MentorEducation(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public MentorEducation(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public MentorEducation(Context context, List<DashBoardMentorDetails.Education> newItemList, String listTitle) {
        super(context);
        initViews(context);
        bindView(newItemList,listTitle);
    }

    //  set data in view
    private void bindView(List<DashBoardMentorDetails.Education> newItemList, String listTitle) {

        if(TextUtils.isEmpty(listTitle)){
            mTvNewItemHeading.setVisibility(GONE);
            mLineBelowHeading.setVisibility(GONE);

        }else{
            mLineBelowHeading.setVisibility(VISIBLE);
            mTvNewItemHeading.setVisibility(VISIBLE);
            mTvNewItemHeading.setText(listTitle);
        }





        mCourseNewItemsAdapter = new MentorEducationItemsAdapter(newItemList, mContext);

        mItemListRecyclerView.setAdapter(mCourseNewItemsAdapter);


    }

    //  initialize course detail description view
    private void initViews(Context context ) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.education_list, this);
        mContext = context;

        mTvNewItemHeading = view.findViewById(R.id.tv_detail_title);
        mLineBelowHeading = view.findViewById(R.id.view_below_heading);

        mItemListRecyclerView = view.findViewById(R.id.listView);


        mItemListRecyclerView.setLayoutManager (new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        mItemListRecyclerView.setNestedScrollingEnabled(false);
    }



}
