package com.imentors.wealthdragons.mentorDetailview;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.BaseActivity;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.dialogFragment.PayDialogFragment;
import com.imentors.wealthdragons.dialogFragment.PaymentCardListDialog;
import com.imentors.wealthdragons.fragments.DashBoardDetailFragment;
import com.imentors.wealthdragons.interfaces.DigitalCoachingClickListener;
import com.imentors.wealthdragons.interfaces.OpenFullScreenVideoListener;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.models.DownloadFile;
import com.imentors.wealthdragons.models.VideoData;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;
import com.tonyodev.fetch2.AbstractFetchListener;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2.Status;
import com.tonyodev.fetch2core.Extras;
import com.tonyodev.fetch2core.Func;
import com.tonyodev.fetch2core.MutableExtras;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MentorDetailVideo extends LinearLayout {


    private Activity mActivity;
    private Context mContext;
    private WealthDragonTextView mTvCourseTitle, mTvCoursePunchLine, mTvCoursePreferance,mTvDigitalCoching,mTvFeeds,mTvSubscrib,mTvUnSubscribe;
    private ImageView mIvVideoPlay;
    private ImageView mIvCourseImage;
    private PlayerView mExoPlayerView;
    private FrameLayout mVideoFrameLayout;
    private MediaSource mVideoSource;
    private FrameLayout mFullScreenButton, mDialogFullScreenButton;
    private ImageView mFullScreenIcon, mPlayIcon;
    private SimpleExoPlayer mPlayer;
    private DashBoardMentorDetails mDashBoardDetails;
    private boolean isFromActivityResult = false, isPaused = false, isTablet;

    private int mResumeWindow;
    private long mResumePosition;
    private String mVideoUrl;
    private OpenFullScreenVideoListener mOpenFullScreenVideoListener;
    private List<String> mVideoUrls = new ArrayList<>();
    private List<Chapters.EClasses> eClassesArrayList = new ArrayList<>();
    private Utils.DialogInteraction mDialogListener;
    private boolean mExoPlayerFullscreen, mIsVideoError = false;
    private Dialog mFullScreenDialog;
    private PlayerView mDialogplayerView;
    private boolean isPotrait, isBufferering = true;
    private boolean isLandScape;
    private FrameLayout mShareBtn;
    private WealthDragonTextView mVideoError;
    private ImageButton mPlayImageButton;
    private boolean mIsItemAdded = false,isSubscribeStatus;

    private String mVideoNameForListenerLog, mImageUrl, mItemId;
    private LinearLayout nextVideoFrame;
    private TextView mNextVideo;
    private RelativeLayout mLlHeadingContainer;
    private TextView mTvEClassNameSmallVideo;
    private ImageView dialogPosterImage;
    private ProgressBar mProgressBar;
    private ImageView mArticleWishlist;
    private TextView mDialogVideoError;
    private OnFavClickListener mOnFavClickListener = null;
    private FrameLayout mBtFullDetails , mBtShare ,mBtDigitalCoching,mBtFeeds,mBtSubscribe;
    private DigitalCoachingClickListener mDigitalCoachingListener;
    private MentorDetailMentorDescription.OnItemClickListener mOnItemClickListener = null;
    private MentorDigitalVideo.OnVideoPlayListener onVideoPlayListener;
    private boolean mIsVideoVisible, mIsDownloaded;
    private ProgressBar mProgrssDownload,mProgrssBarDownload;
    private ImageView mDownloadGrey,mDownloadOrange;
    private com.tonyodev.fetch2.Request mRequset;
    private Fetch mFetch;
    private List<Download> mDownLoadItemList;
    private  String mID,mSavedGroupImage, mGroupImageUrl;
    private String mBannerImagePath;
    private TextView mVideoTitle;




    public MentorDetailVideo(Context context) {
        super(context);
        mContext = context;
    }

    public MentorDetailVideo(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public MentorDetailVideo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    public MentorDetailVideo(Context context, DashBoardMentorDetails dashBoardDetails, Activity activity,
                             OnFavClickListener onFavClickListener,
                             MentorDetailMentorDescription.OnItemClickListener onItemClickListener,
                             MentorDigitalVideo.OnVideoPlayListener videoPlayListener, boolean isVideoVisible, boolean isDownloaded) {
        super(context);
        mActivity = activity;
        this.mOnFavClickListener = onFavClickListener;
        onVideoPlayListener = videoPlayListener;
        this.mOnItemClickListener = onItemClickListener;
        mIsVideoVisible = isVideoVisible;
        mIsDownloaded = isDownloaded;
        mFetch = Utils.getFetchInstance();
        isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set data in view
    private void bindView(final DashBoardMentorDetails dashBoardDetails) {

        mDashBoardDetails = dashBoardDetails;

        mID=dashBoardDetails.getProfile().getId();

        mGroupImageUrl=dashBoardDetails.getProfile().getBanner();


        mImageUrl = dashBoardDetails.getProfile().getExpert_intro_video_banner();


        mRequset = new com.tonyodev.fetch2.Request(dashBoardDetails.getProfile().getExpert_intro_video(), mContext.getApplicationContext().getFilesDir() + "/video/" + dashBoardDetails.getProfile().getId());


        mFetch.addListener(fetchListener);

        showDonwloadIcon();


        mDownloadOrange.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(mContext)) {

                    mProgrssBarDownload.setProgress(0);
                    mProgrssBarDownload.setVisibility(GONE);
                    mDownloadOrange.setVisibility(GONE);
                    mProgrssDownload.setVisibility(VISIBLE);
                    mDownloadGrey.setVisibility(GONE);

                    Toast.makeText(getApplicationContext(), R.string.download_added, Toast.LENGTH_SHORT).show();
                    Utils.callEventLogApi("downloading started  <b>" + mDashBoardDetails.getProfile().getName() + " </b>" + "from mentor detail");

                    new startGroupImageDownload().execute();

                    new startImageDownload().execute();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.error_api_no_internet_connection, Toast.LENGTH_SHORT).show();

                }


            }
        });

        mDownloadGrey.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        Toast.makeText(getContext(), R.string.download_removed, Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi("downloading removed  <b>" + mDashBoardDetails.getProfile().getName() + " </b>" + "from mentor detail");


                        mFetch.delete(mRequset.getId());
                        mDownloadOrange.setVisibility(VISIBLE);
                        mProgrssDownload.setVisibility(GONE);
                        mProgrssBarDownload.setVisibility(GONE);
                        mDownloadGrey.setVisibility(GONE);
                        dialog.dismiss();
                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();
                    }
                }, getContext(), getContext().getString(R.string.remove), getContext().getString(R.string.remove_message), getContext().getString(R.string.yes), getContext().getString(R.string.no));


            }
        });

        mProgrssDownload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        Toast.makeText(getContext(), R.string.download_canceled, Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi("downloading cancelled  <b>" + mDashBoardDetails.getProfile().getName() + " </b>" + "from mentor detail");


                        mFetch.delete(mRequset.getId());
                        mDownloadOrange.setVisibility(VISIBLE);
                        mProgrssDownload.setVisibility(GONE);
                        mProgrssBarDownload.setVisibility(GONE);
                        mDownloadGrey.setVisibility(GONE);
                        dialog.dismiss();
                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();
                    }
                }, getContext(), getContext().getString(R.string.cancel), getContext().getString(R.string.cancel_message), getContext().getString(R.string.yes), getContext().getString(R.string.no));


            }
        });

        mProgrssBarDownload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        Toast.makeText(getContext(), R.string.download_canceled, Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi("downloading cancelled  <b>" + mDashBoardDetails.getProfile().getName() + " </b>" + "from mentor detail");


                        mFetch.delete(mRequset.getId());
                        mDownloadOrange.setVisibility(VISIBLE);
                        mProgrssBarDownload.setVisibility(GONE);
                        mProgrssDownload.setVisibility(GONE);
                        mDownloadGrey.setVisibility(GONE);
                        dialog.dismiss();
                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();
                    }
                }, getContext(), getContext().getString(R.string.cancel), getContext().getString(R.string.cancel_message), getContext().getString(R.string.yes), getContext().getString(R.string.no));

            }
        });






        if( dashBoardDetails.getProfile().getDigi_subscription().equals("No") && dashBoardDetails.getProfile().getJoin_digi_btn().equals("Yes")){
            if(!dashBoardDetails.getProfile().getDigital_coaching_btn_name().isEmpty()){
                mTvDigitalCoching.setText(dashBoardDetails.getProfile().getDigital_coaching_btn_name());
            }
            if(!mIsDownloaded){
                mBtDigitalCoching.setVisibility(VISIBLE);

            }else{
                mBtDigitalCoching.setVisibility(GONE);

            }
        }else{
            mBtDigitalCoching.setVisibility(GONE);
        }

        if(mContext instanceof DigitalCoachingClickListener){
            mDigitalCoachingListener = (DigitalCoachingClickListener) mContext;
        }


        mBtDigitalCoching.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                mDigitalCoachingListener.onDigitalCoachingClick(Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING , mItemId,dashBoardDetails.getProfile().getDigi_subscription(),dashBoardDetails.getProfile().getNext_video(),null,0,"Paid");


              if(mIsDownloaded){
                  if(Utils.isNetworkAvailable(mContext)){
                      onDigitalCoachingClick(dashBoardDetails);

                  }else{
                      Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();

                  }

              }else{
                  onDigitalCoachingClick(dashBoardDetails);

              }

            }
        });


        if (dashBoardDetails.getProfile().getFeeds_btn().equals("Yes"))
        {
            if(!mIsDownloaded){
                mBtFeeds.setVisibility(VISIBLE);
            }else{
                mBtFeeds.setVisibility(GONE);

            }

        }
        else
        {
            mBtFeeds.setVisibility(GONE);
        }

        mBtFeeds.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mIsDownloaded){
                    if(Utils.isNetworkAvailable(mContext)){
                        mOnItemClickListener.onFeedsButtonClick(mItemId);

                    }else{
                        Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();

                    }

                }else{
                    mOnItemClickListener.onFeedsButtonClick(mItemId);

                }

            }
        });


        if(dashBoardDetails.getProfile().getExpert_subscription_btn()){
            if(!mIsDownloaded){
                mBtSubscribe.setVisibility(View.VISIBLE);

            }else{
                mBtSubscribe.setVisibility(View.GONE);
            }
        }else{
            mBtSubscribe.setVisibility(View.GONE);

        }

        if (dashBoardDetails.getProfile().getNormal_subscription().equals(mContext.getString(R.string.no))) {

            mTvUnSubscribe.setVisibility(GONE);
            mTvSubscrib.setVisibility(VISIBLE);
            isSubscribeStatus = false;

        } else {

            mTvUnSubscribe.setVisibility(VISIBLE);
            mTvSubscrib.setVisibility(GONE);
            isSubscribeStatus = true;

        }



        mBtSubscribe.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mIsDownloaded){
                    if(Utils.isNetworkAvailable(mContext)){
                        onSubscriptionClick(dashBoardDetails);

                    }else{
                        Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();

                    }

                }else{
                    onSubscriptionClick(dashBoardDetails);

                }



            }
        });





        mItemId=dashBoardDetails.getProfile().getId();



        if(mIsVideoVisible) {


            if (dashBoardDetails.isWishlist()) {
                mArticleWishlist.setVisibility(View.VISIBLE);

            } else {
                mArticleWishlist.setVisibility(View.GONE);

            }

            if (!TextUtils.isEmpty(mDashBoardDetails.getProfile().getExpert_intro_video()) && !dashBoardDetails.getProfile().getExpert_intro_video().equals("NoVideoExist")) {
                // add only intro video in list
                mDownloadOrange.setVisibility(VISIBLE);
                mVideoTitle.setVisibility(VISIBLE);
                mVideoTitle.setText("Download " + dashBoardDetails.getProfile().getName() + " Intro Video");
                mVideoUrl = mDashBoardDetails.getProfile().getExpert_intro_video();
                mVideoUrls.add(mVideoUrl);
            }
            mDownloadOrange.setVisibility(GONE);
            mVideoTitle.setVisibility(GONE);
            mDownloadGrey.setVisibility(GONE);
            mProgrssDownload.setVisibility(GONE);
            mProgrssBarDownload.setVisibility(GONE);


            mImageUrl = dashBoardDetails.getProfile().getExpert_intro_video_banner();

            Glide.with(mContext).load(dashBoardDetails.getProfile().getExpert_intro_video_banner()).dontAnimate().error(R.drawable.no_img_mob).into(mIvCourseImage);


            if (!TextUtils.isEmpty(dashBoardDetails.getProfile().getName())) {

                mVideoNameForListenerLog = "Intro Video";
                mTvEClassNameSmallVideo.setText(mVideoNameForListenerLog);
            }


            if (dashBoardDetails.getProfile().getWishlist().equals(mContext.getString(R.string.no))) {

                mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart);

                mIsItemAdded = false;

            } else {

                mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);

                mIsItemAdded = true;

            }

            mArticleWishlist.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mIsItemAdded) {
                        mOnFavClickListener.onFavButtonClick(mContext.getString(R.string.no));
                        mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart);
                        callWishListApi(dashBoardDetails.getProfile().getId());

                    } else {
                        mOnFavClickListener.onFavButtonClick(mContext.getString(R.string.yes));
                        mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);
                        callWishListApi(dashBoardDetails.getProfile().getId());

                    }
                    mIsItemAdded = !mIsItemAdded;
                }
            });

            if (!TextUtils.isEmpty(dashBoardDetails.getProfile().getExpert_intro_video()) && !dashBoardDetails.getProfile().getExpert_intro_video().equals("NoVideoExist")) {
                mIvVideoPlay.setVisibility(View.VISIBLE);
//            mExoPlayerView.setVisibility(View.VISIBLE);
                mVideoError.setVisibility(INVISIBLE);

                initFullscreenButton();


            } else {
                mIvVideoPlay.setVisibility(View.INVISIBLE);

            }

            mExoPlayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
                @Override
                public void onVisibilityChange(int visibility) {
                    if (visibility == View.VISIBLE) {
                        if (mDashBoardDetails.isWishlist()) {
                            mArticleWishlist.setVisibility(VISIBLE);
                        } else {
                            mArticleWishlist.setVisibility(GONE);
                        }

                    } else {
                        mArticleWishlist.setVisibility(GONE);

                    }
                }
            });

            mIvVideoPlay.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isNetworkAvailable(mContext)) {
                        initFullscreenDialog();
                        if (AppPreferences.getWifiState() && !Utils.IsWifiConnected()) {

                            Utils.showTwoButtonAlertDialog(mDialogListener, mContext, mContext.getString(R.string.notification), mContext.getString(R.string.check_wifi_status), mContext.getString(R.string.okay_text), mContext.getString(R.string.go_to_setting));

                        } else {

                            mIvVideoPlay.setVisibility(GONE);
                            callVideoUrl(dashBoardDetails.getProfile().getId());

                            Utils.callEventLogApi("clicked <b> play button </b> of  " + mDashBoardDetails.getProfile().getName());

                        }
                    } else {
                        Utils.showNetworkError(mContext, new NoConnectionError());
                    }
                }
            });


        }
        mDownloadOrange.setVisibility(GONE);
        mVideoTitle.setVisibility(GONE);
        mDownloadGrey.setVisibility(GONE);
        mProgrssDownload.setVisibility(GONE);
        mProgrssBarDownload.setVisibility(GONE);


    }

    private void onSubscriptionClick(final DashBoardMentorDetails dashBoardDetails) {
        Utils.callEventLogApi("clicked Subscribe Button of <b>"+ dashBoardDetails.getProfile().getName()+"</b> in mentor detail");
        Utils.callEventLogApi("opened Subscription dialog <b>"+ dashBoardDetails.getProfile().getName()+"</b> in mentor detail");

        Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
            @Override
            public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {

                Utils.callEventLogApi("clicked Yes Button of<b> "+ dashBoardDetails.getProfile().getName()+"</b> in subscription Dialog");


                if (isSubscribeStatus)
                {
                    mTvUnSubscribe.setVisibility(GONE);
                    mTvSubscrib.setVisibility(VISIBLE);

                }
                else
                {
                    mTvUnSubscribe.setVisibility(VISIBLE);
                    mTvSubscrib.setVisibility(GONE);
                }
                callSubscribeApi();
                dialog.dismiss();

            }

            @Override
            public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {

                Utils.callEventLogApi("clicked No Button of<b> "+ dashBoardDetails.getProfile().getName()+"</b> in subscription Dialog");
                dialog.dismiss();

            }
        },getContext(),getContext().getString(R.string.subsribe_title),isSubscribeStatus?getContext().getString(R.string.unsubscribe_now_message)+" "+dashBoardDetails.getProfile().getName()+"?":getContext().getString(R.string.subscribe_now_message)+" "+dashBoardDetails.getProfile().getName()+"?",getContext().getString(R.string.yes),getContext().getString(R.string.no));
    }

    private void onDigitalCoachingClick(DashBoardMentorDetails dashBoardDetails) {
        boolean mShowPayDialog= false;
        if(!TextUtils.isEmpty(mDashBoardDetails.getProfile().getDigi_coaching_screen()) && TextUtils.equals(mDashBoardDetails.getProfile().getDigi_coaching_screen(),"No")){
            mShowPayDialog = true;

        }

        if(mShowPayDialog){
            if (AppPreferences.getCreditCardList() != null && AppPreferences.getCreditCardList().size() > 0) {
                openDialog(PaymentCardListDialog.newInstance(dashBoardDetails.getProfile().getId(), Constants.TYPE_EXPERT, null));
            } else {
                openDialog(PayDialogFragment.newInstance("Ruppee", false, false, dashBoardDetails.getProfile().getId(), Constants.TYPE_EXPERT, null));
            }

        }else{
            if(mContext!=null) {
                ((MainActivity) mContext).addFragment(DashBoardDetailFragment.newInstance(dashBoardDetails.getProfile().getId(), Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING, null,false,null));
            }

        }

        onVideoPlayListener.onVideoPlayed();
        Utils.callEventLogApi("clicked <b>" + "join vip club"+"</b> button from mentor detail ");
    }

    //  initialize video detail header view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (context instanceof OpenFullScreenVideoListener) {
            mOpenFullScreenVideoListener = (OpenFullScreenVideoListener) context;
        }

        if (context instanceof Utils.DialogInteraction) {
            mDialogListener = (Utils.DialogInteraction) context;
        }

        View view = inflater.inflate(R.layout.mentor_header, this);
        mContext = context;

        // heading
        mTvCourseTitle = view.findViewById(R.id.tv_course_detail_title);
        mTvCourseTitle.setVisibility(GONE);
        mTvCoursePunchLine = view.findViewById(R.id.tv_course_detail_punch_line);
        mTvCoursePunchLine.setVisibility(GONE);
        mTvCoursePreferance = view.findViewById(R.id.txt_course_detail_taste_preferance);
        mTvCoursePreferance.setVisibility(GONE);
        mShareBtn = view.findViewById(R.id.fl_btn_full_details);
        mShareBtn.setVisibility(GONE);
        mVideoError = view.findViewById(R.id.video_error);
        mProgressBar = view.findViewById(R.id.progress_bar);
        mArticleWishlist = view.findViewById(R.id.iv_add_wishlist);
        mBtDigitalCoching=view.findViewById(R.id.fl_digital_coaching);
        mTvDigitalCoching=view.findViewById(R.id.tv_digital_coaching);
        mBtFeeds=view.findViewById(R.id.fl_btn_feeds);
        mTvFeeds=view.findViewById(R.id.tv_feeds);
        mBtSubscribe=view.findViewById(R.id.fl_btn_subscribe);
        mTvSubscrib=view.findViewById(R.id.tv_subscribe);
        mTvUnSubscribe=view.findViewById(R.id.tv_unsubscribe);
        mProgrssDownload=view.findViewById(R.id.progressBar);
        mProgrssBarDownload=view.findViewById(R.id.download_bar);
        mDownloadGrey=view.findViewById(R.id.iv_download_grey);
        mDownloadOrange=view.findViewById(R.id.iv_download_orange);
        mVideoTitle=view.findViewById(R.id.tv_title);

        // video Frame
        mVideoFrameLayout = view.findViewById(R.id.frame_video);
        mVideoFrameLayout.getLayoutParams().height = (int) Utils.getDetailScreenImageHeight((Activity) mContext);


        mIvVideoPlay = view.findViewById(R.id.iv_video_play);
        mIvCourseImage = view.findViewById(R.id.iv_video_image);
        mExoPlayerView = findViewById(R.id.exoplayer);


        mTvEClassNameSmallVideo = mExoPlayerView.findViewById(R.id.tv_eclasses_title);

        LocalBroadcastManager.getInstance(mContext).registerReceiver(mSaveVideoPosition, new IntentFilter(Constants.BROADCAST_SAVE_VIDEO_POSITION));

        if(mIsVideoVisible){
            mVideoFrameLayout.setVisibility(VISIBLE);
            mIvVideoPlay.setVisibility(VISIBLE);
            mIvCourseImage.setVisibility(VISIBLE);
            mExoPlayerView.setVisibility(VISIBLE);
            mTvEClassNameSmallVideo.setVisibility(VISIBLE);
            mArticleWishlist.setVisibility(VISIBLE);
            mVideoError.setVisibility(VISIBLE);
            mProgressBar.setVisibility(GONE);

        }else{
            mVideoFrameLayout.setVisibility(GONE);
            mIvVideoPlay.setVisibility(GONE);
            mIvCourseImage.setVisibility(GONE);
            mExoPlayerView.setVisibility(GONE);
            mTvEClassNameSmallVideo.setVisibility(GONE);
            mArticleWishlist.setVisibility(GONE);
            mVideoError.setVisibility(GONE);
            mProgressBar.setVisibility(GONE);
        }


        if (Utils.isTablet())
        {

            double reuiredButtonWidth = Utils.getScreenWidth((Activity)mContext)/1.5;

            int marginFromStart = (int)(Utils.getScreenWidth((Activity)mContext) - reuiredButtonWidth)/2;

            LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams( ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params4.setMarginStart(marginFromStart-30);

            LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams((int) (reuiredButtonWidth), Utils.dpToPx(60));
            Params.gravity = Gravity.CENTER_HORIZONTAL;
            mBtDigitalCoching.setLayoutParams(Params);
            mBtSubscribe.setLayoutParams(Params);
            mBtFeeds.setLayoutParams(Params);


        }
        else {
            LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Utils.dpToPx(50));
            mBtDigitalCoching.setLayoutParams(Params);
            mBtFeeds.setLayoutParams(Params);
            mBtSubscribe.setLayoutParams(Params);
        }



    }


    private BroadcastReceiver mSaveVideoPosition = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            boolean isDetailOnTop = intent.getBooleanExtra(Constants.IS_TOP_FRAGMENT, false);

            if (mExoPlayerView != null && mPlayer != null) {
                mResumeWindow = mPlayer.getCurrentWindowIndex();
                AppPreferences.setVideoResumeWindow(mVideoUrl, mResumeWindow);
                mResumePosition = Math.max(0, mExoPlayerView.getPlayer().getContentPosition());
                AppPreferences.setVideoResumePosition(mVideoUrl, mResumePosition);
            }

            if (!isDetailOnTop) {
                Utils.releaseDetailScreenPlayer();
                Utils.releaseFullScreenPlayer();
            }

            if (!isFromActivityResult && !isPaused) {
                mExoPlayerView.setVisibility(INVISIBLE);
                mIvCourseImage.setVisibility(INVISIBLE);
                mIvVideoPlay.setVisibility(INVISIBLE);
                mVideoError.setVisibility(INVISIBLE);

            }

        }
    };


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (mExoPlayerView != null && mPlayer != null && mExoPlayerView.getPlayer() != null) {
            mResumeWindow = mPlayer.getCurrentWindowIndex();
            AppPreferences.setVideoResumeWindow(mVideoUrl, mResumeWindow);
            mResumePosition = Math.max(0, mExoPlayerView.getPlayer().getContentPosition());
            AppPreferences.setVideoResumePosition(mVideoUrl, mResumePosition);
            mPlayer.release();
            mPlayer = null;

            // releasing full screen player too
            Utils.releaseFullScreenPlayer();
            Utils.releaseDetailScreenPlayer();
        }
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mSaveVideoPosition);

    }


    private void initFullscreenButton() {

        mFullScreenIcon = mExoPlayerView.findViewById(R.id.exo_fullscreen_icon);
        mPlayImageButton = mExoPlayerView.findViewById(R.id.exo_play);

        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_expand));
        mFullScreenButton = mExoPlayerView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                openFullScreenVideo(Surface.ROTATION_0);
            }

        });
    }


    //  opened full screen video mode
    public void openFullScreenVideo(int rotationAngle) {
        if (!mExoPlayerFullscreen) {
            if (!isLandScape) {
                openFullscreenDialog(rotationAngle);
            }
        } else {
            if (!isPotrait) {
                closeFullscreenDialog();
            }
        }
    }

    public void pauseVideo() {
        if (mPlayer != null) {
            mPlayer.setPlayWhenReady(false);
        }

        isPaused = true;
    }

    public void stopVideo(){

        if(mVideoUrls.size()>0) {
            if (mPlayer != null) {
                mPlayer.setPlayWhenReady(false);
            }

            mProgressBar.setVisibility(INVISIBLE);
            mExoPlayerView.setVisibility(INVISIBLE);
            mIvCourseImage.setVisibility(VISIBLE);
            mArticleWishlist.setVisibility(VISIBLE);
            mIvVideoPlay.setVisibility(VISIBLE);
            Glide.with(mContext).load(mDashBoardDetails.getProfile().getExpert_intro_video_banner()).dontAnimate().error(R.drawable.no_img_mob).into(mIvCourseImage);

        }
    }


    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }


    private void openFullscreenDialog(int rotationAngle) {
        Utils.callEventLogApi("clicked <b>" + "Full Screen Video" + " Mode</b> in Player from " + mDashBoardDetails.getProfile().getName());

        isPotrait = false;
        isLandScape = true;
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.activity_video_fullscreen, null);
        mDialogplayerView = dialogView.findViewById(R.id.exoplayer_fullScreen);
        mNextVideo = dialogView.findViewById(R.id.tv_nxtvideo);
        mNextVideo.setVisibility(GONE);
        mDialogVideoError = dialogView.findViewById(R.id.video_error);

        dialogPosterImage = dialogView.findViewById(R.id.iv_poster_image);
        Glide.with(mContext).load(mImageUrl).dontAnimate().error(R.drawable.no_img_video).into(dialogPosterImage);

        mLlHeadingContainer = dialogView.findViewById(R.id.header_holder);

        mNextVideo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mPlayer.getNextWindowIndex() != -1) {
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());
                }

            }
        });
        nextVideoFrame = dialogView.findViewById(R.id.next_video_image_holder);
        nextVideoFrame.setVisibility(GONE);
        TextView tvEClassName = dialogView.findViewById(R.id.tv_eclasses_title);

        tvEClassName.setText("Intro Video");


        // on Next Click
        nextVideoFrame.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer.getNextWindowIndex() != -1) {
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());

                }
            }
        });

        initDialogFullScreenButton();
        PlayerView.switchTargetView(mExoPlayerView.getPlayer(), mExoPlayerView, mDialogplayerView);
        mFullScreenDialog.addContentView(dialogView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
        Log.d("orientation", "landscape");

    }

    //  closed full screen dialog
    public void closeFullscreenDialog() {
        Utils.callEventLogApi("clicked <b>" + "Compact Screen Video" + " Mode</b> in Player from " + mDashBoardDetails.getProfile().getName());

        isPotrait = true;
        isLandScape = false;
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        PlayerView.switchTargetView(mDialogplayerView.getPlayer(), mDialogplayerView, mExoPlayerView);
        mFullScreenDialog.dismiss();
        mExoPlayerFullscreen = false;
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_expand));
        Log.d("orientation", "potrait");

    }


    private void initDialogFullScreenButton() {

        mDialogplayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if (visibility == View.VISIBLE) {
                    mLlHeadingContainer.setVisibility(VISIBLE);

//                    if (mPlayer.getNextWindowIndex() != -1 && mPlayer.getRepeatMode() == Player.REPEAT_MODE_ALL) {
//                        mNextVideo.setVisibility(VISIBLE);
//                    } else {
//                        mNextVideo.setVisibility(GONE);
//                    }

                    mNextVideo.setVisibility(GONE);
                } else {
                    mLlHeadingContainer.setVisibility(GONE);


                }
            }
        });
        mFullScreenIcon = mDialogplayerView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_skrink));
        mDialogFullScreenButton = mDialogplayerView.findViewById(R.id.exo_fullscreen_button);
        mDialogFullScreenButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFullscreenDialog();
            }

        });

    }

    public void pressFullScreenDialogButton() {
        if (isLandScape && mPlayer != null) {
            mDialogFullScreenButton.performClick();
        }

    }


    public void callWishListApi(final String mItemId) {
        String api = null;

        if (mIsItemAdded) {
            api = Api.REMOVE_WISHLIST_API;

        } else {
            api = Api.ADD_WISHLIST_API;
        }


        //TODO implemented maintenance mode
        // calling add wishlist and remove wishlist api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(mContext);
                        return;
                    }

                    JSONObject jsonObject = new JSONObject(response);

                    if (!TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS))) {


                        if (mIsItemAdded) {

                            Toast.makeText(mContext, "Added to Wishlist", Toast.LENGTH_SHORT).show();

                        }

                    }


                } catch (Exception e) {

                    if (mIsItemAdded) {
                        Utils.callEventLogApi("remove <b>Wishlist </b> error in api ");
                        mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart);


                    } else {

                        Utils.callEventLogApi("add <b>Wishlist </b> error in api");
                        mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);


                    }
                    mIsItemAdded = !mIsItemAdded;
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mIsItemAdded) {
                    Utils.callEventLogApi("remove <b>Wishlist </b> error in api");
                    mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart);

                } else {
                    Utils.callEventLogApi("add <b>Wishlist </b> error in api");
                    mArticleWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);


                }
                mIsItemAdded = !mIsItemAdded;
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);


                params.put(Keys.ITEM_ID, mItemId);
                params.put(Keys.TYPE, Constants.TYPE_EXPERT);

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    // for internal use
    private void initExoPlayer() {

        // get position from preferences

        Utils.releaseDetailScreenPlayer();
        mPlayer = null;
        ArrayList<String> mVideoUrlList = new ArrayList<>();
        mVideoUrlList.add(mVideoUrl);

        Utils.initilizeDetailScreenConcatinatedPlayer(mContext, mVideoUrlList, null, false, Constants.TYPE_EXPERT, "Digital Coaching", false);

        mResumePosition = Utils.getTimeInMilliSeconds(mDashBoardDetails.getProfile().getExpert_intro_video_seektime());

        mPlayer = Utils.getDetailScreenConcatinatedPlayer();


        mPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {


            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {


                if (playbackState == Player.STATE_ENDED) {

                    Utils.callEventLogApi("watched video of" +"<b>"+ mVideoNameForListenerLog+"</b>"+" " + "from Mentor Detail" );


                    if(mExoPlayerFullscreen){
                        dialogPosterImage.setVisibility(INVISIBLE);
                        mDialogplayerView.setVisibility(View.VISIBLE);
                        mDialogVideoError.setVisibility(INVISIBLE);
                    }


                    mExoPlayerView.setVisibility(VISIBLE);
                    mVideoError.setVisibility(View.INVISIBLE);
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mIvCourseImage.setVisibility(View.INVISIBLE);
                    Glide.with(mContext).load(mDashBoardDetails.getProfile().getExpert_intro_video_banner()).dontAnimate().error(R.drawable.no_img_mob).into(mIvCourseImage);

                    mIvVideoPlay.setVisibility(INVISIBLE);
                }


                if (playWhenReady) {
                    mIvVideoPlay.setVisibility(INVISIBLE);
                    onVideoPlayListener.onVideoPlayed();
                } else {
                    mIvVideoPlay.setVisibility(INVISIBLE);
                    Utils.callEventLogApi("paused <b>" + mVideoNameForListenerLog + "</b> mentor profile ");
                }


                if (playbackState == Player.STATE_BUFFERING) {
                    mProgressBar.setVisibility(VISIBLE);

                } else {

                    mProgressBar.setVisibility(INVISIBLE);
                    mExoPlayerView.setVisibility(VISIBLE);
                    mExoPlayerView.setControllerAutoShow(true);
                    mExoPlayerView.setUseController(true);
                    mIvCourseImage.setVisibility(INVISIBLE);
                    mArticleWishlist.setVisibility(VISIBLE);
                }

                if(!((BaseActivity)mActivity).isFragmentInBackStack(new DashBoardDetailFragment())){
                    Utils.releaseDetailScreenPlayer();
                }

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {


                try {

                    Utils.callEventLogApi(error.getSourceException().getMessage() + " in " + mVideoNameForListenerLog + "</b>");


                }
                catch (Exception e)
                {
                    e.printStackTrace();

                }
                // show error screen
                if (Utils.isNetworkAvailable(mContext)) {

                    if (mPlayer != null) {
                        if (mPlayer.getPlayWhenReady()) {
                            try {
                                if (isLandScape) {
                                    closeFullscreenDialog();
                                }

                                mPlayImageButton.setActivated(false);
                                mIvCourseImage.setVisibility(View.INVISIBLE);
                                mIvVideoPlay.setVisibility(View.INVISIBLE);
                                mVideoError.setVisibility(VISIBLE);
                                mExoPlayerView.setVisibility(View.INVISIBLE);
                                mArticleWishlist.setVisibility(VISIBLE);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    if (isLandScape) {
                        closeFullscreenDialog();
                    }
                    Utils.showNetworkError(mContext, new NoConnectionError());

                }


            }

            @Override
            public void onPositionDiscontinuity(int reason) {

                // for handling log
                switch (reason) {
                    case Player.DISCONTINUITY_REASON_SEEK:
                        Utils.callEventLogApi("scrolled to  " + "<b>" + Utils.getMinHourSecFromSeconds(mPlayer.getContentPosition() / 1000) + "</b>" + " from" + "<b> " + mVideoNameForListenerLog + "</b>");

                        break;
                }


            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {
                if (mPlayer.getPlaybackState() == Player.STATE_BUFFERING) {
                    mProgressBar.setVisibility(VISIBLE);
                } else {
                    mProgressBar.setVisibility(INVISIBLE);
                    mExoPlayerView.setVisibility(VISIBLE);
                    mExoPlayerView.setControllerAutoShow(true);
                    mExoPlayerView.setUseController(true);
                    mIvCourseImage.setVisibility(INVISIBLE);
                    mArticleWishlist.setVisibility(VISIBLE);
                }
            }
        });


        mPlayer.clearVideoSurface();
        Utils.callEventLogApi("started new video of " +"<b>"+ mVideoNameForListenerLog+"</b>" + " " + "in Mentor Detail");

        mExoPlayerView.setVisibility(View.VISIBLE);
        mExoPlayerView.setPlayer(mPlayer);


        mPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);

        mPlayer.seekTo(0, mResumePosition);

        mVideoError.setVisibility(View.INVISIBLE);
        mIvCourseImage.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);


        mPlayer.setPlayWhenReady(true);
    }


    private void callVideoUrl(final String mItemId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.NEW_VIDEO_URL, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    mVideoUrl = jsonObject.getString("video_url");
                    initExoPlayer();
                } catch (Exception e) {
                    e.printStackTrace();
                    mVideoUrl = mVideoUrl;
                    initExoPlayer();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mVideoUrl = mVideoUrl;

                initExoPlayer();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.ITEM_ID, mItemId);
                params.put(Keys.TYPE, Constants.TYPE_EXPERT);
                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    public VideoData getCurrentVideoData(){
        VideoData videoDataToBeSent = new VideoData();

        try {

            if (mPlayer != null && mPlayer.getPlayWhenReady()) {
                String seekTime;
                if (mPlayer.getContentPosition() > mPlayer.getDuration() - 5 * 1000) {
                    videoDataToBeSent.setTime("0");
                    seekTime = "0";
                } else {
                    videoDataToBeSent.setTime(Long.toString(mPlayer.getContentPosition() / 1000));
                    seekTime = Long.toString(mPlayer.getContentPosition() / 1000);
                }

                videoDataToBeSent.setTime(seekTime);
                videoDataToBeSent.setType("ExpertIntro");


                videoDataToBeSent.setVideoId(mDashBoardDetails.getProfile().getId());

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return videoDataToBeSent;

    }


    /**
     * different click handled by onItemClick
     */
    public interface OnFavClickListener {
        void onFavButtonClick(String favClicked);

    }




    public void callSubscribeApi() {

        //TODO implemented maintenance mode
        // calling add wishlist and remove wishlist api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.NORMAL_SUBSCRIBE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(mContext);
                        return;
                    }

                    JSONObject jsonObject = new JSONObject(response);

                    if (!TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS)))
                    {
                        if(!isSubscribeStatus) {
                            Utils.callEventLogApi("Subscribed of <b>"+mDashBoardDetails.getProfile().getName() +"</b> in subscription dialog");
                            Toast.makeText(getContext(), "Subscribed Sucessfully", Toast.LENGTH_SHORT).show();
                        }

                        isSubscribeStatus = !isSubscribeStatus;


                    }




                } catch (Exception e) {

                    if (isSubscribeStatus) {

                        mTvUnSubscribe.setVisibility(GONE);
                        mTvSubscrib.setVisibility(VISIBLE);

                    }
                    else
                    {
                        mTvUnSubscribe.setVisibility(VISIBLE);
                        mTvSubscrib.setVisibility(GONE);
                    }
                    Utils.callEventLogApi("getting <b>Subscription </b> error in api");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (isSubscribeStatus) {

                    mTvUnSubscribe.setVisibility(GONE);
                    mTvSubscrib.setVisibility(VISIBLE);

                }
                else
                {
                    mTvUnSubscribe.setVisibility(VISIBLE);
                    mTvSubscrib.setVisibility(GONE);
                }
                Utils.callEventLogApi("getting <b>Subscription </b> error in api");
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);


                params.put(Keys.MENTOR_ID,mItemId);
                if (isSubscribeStatus) {
                    params.put(Keys.STATUS, "0");

                } else {
                    params.put(Keys.STATUS, "1");

                }


                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    private void openDialog(DialogFragment dialogFragment) {
        dialogFragment.setCancelable(false);
        dialogFragment.show(((MainActivity)mContext).getSupportFragmentManager(), "dialog");

    }


    private void showDonwloadIcon() {

        mFetch.getDownloads(new Func<List<Download>>() {
            @Override
            public void call(@NotNull List<Download> result) {
                mDownLoadItemList = result;
                checkForCourse();
            }
        });


    }

    private void checkForCourse() {

        if (!TextUtils.isEmpty(mDashBoardDetails.getProfile().getExpert_intro_video())
                && !mDashBoardDetails.getProfile().getExpert_intro_video().equals("NoVideoExist") && mIsVideoVisible) {
            if (mDownLoadItemList != null && mDownLoadItemList.size() > 0) {
                for (Download downloadTask : mDownLoadItemList) {

                    if (downloadTask.getExtras().getString(Constants.ID, "1").equals(mDashBoardDetails.getProfile().getId())) {
                        if (downloadTask.getStatus() == Status.COMPLETED) {

                            mDashBoardDetails.getProfile().setSavedVideoAddress(downloadTask.getFile());

                            mProgrssBarDownload.setVisibility(GONE);
                            mDownloadOrange.setVisibility(GONE);
                            mVideoTitle.setVisibility(VISIBLE);
                            mVideoTitle.setText("Download " + mDashBoardDetails.getProfile().getName() + " Intro Video");
                            mDownloadGrey.setVisibility(VISIBLE);
                        } else if (downloadTask.getStatus() == Status.FAILED
                                || downloadTask.getStatus() == Status.CANCELLED
                                || downloadTask.getStatus() == Status.DELETED
                                || downloadTask.getStatus() == Status.REMOVED
                        ) {
                            mDashBoardDetails.getProfile().setSavedVideoAddress(null);

                            mProgrssBarDownload.setVisibility(GONE);
                            mDownloadOrange.setVisibility(VISIBLE);
                            mDownloadGrey.setVisibility(GONE);
                            mVideoTitle.setVisibility(VISIBLE);
                            mVideoTitle.setText("Download " + mDashBoardDetails.getProfile().getName() + " Intro Video");


                        } else {
                            mDashBoardDetails.getProfile().setSavedVideoAddress(downloadTask.getFile());
                            mProgrssBarDownload.setVisibility(VISIBLE);
                            mProgrssBarDownload.setProgress(downloadTask.getProgress());
                            mDownloadOrange.setVisibility(GONE);
                            mDownloadGrey.setVisibility(GONE);
                            mVideoTitle.setVisibility(VISIBLE);
                            mVideoTitle.setText("Download " + mDashBoardDetails.getProfile().getName() + " Intro Video");


                        }
                        break;
                    } else {
                        mDashBoardDetails.getProfile().setSavedVideoAddress(null);

                        mProgrssBarDownload.setVisibility(GONE);
                        mDownloadOrange.setVisibility(VISIBLE);
                        mDownloadGrey.setVisibility(GONE);
                        mVideoTitle.setVisibility(VISIBLE);
                        mVideoTitle.setText("Download " + mDashBoardDetails.getProfile().getName() + " Intro Video");


                    }
                }
            } else {
                mDashBoardDetails.getProfile().setSavedVideoAddress(null);

                mProgrssBarDownload.setVisibility(GONE);
                mDownloadOrange.setVisibility(VISIBLE);
                mDownloadGrey.setVisibility(GONE);
                mVideoTitle.setVisibility(VISIBLE);
                mVideoTitle.setText("Download " + mDashBoardDetails.getProfile().getName() + " Intro Video");

            }
        } else {
            mDashBoardDetails.getProfile().setSavedVideoAddress(null);

            mProgrssBarDownload.setVisibility(GONE);
            mDownloadOrange.setVisibility(GONE);
            mDownloadGrey.setVisibility(GONE);
            mProgrssDownload.setVisibility(GONE);
            mVideoTitle.setVisibility(GONE);

        }


    }


    class startImageDownload extends AsyncTask<Void, Void, Void> {


        File outputFile;


        @Override
        protected void onPostExecute(Void aVoid) {


            DownloadFile mDownLoadTask = new DownloadFile(
                    mRequset.getFile(),
                    null,
                    mDashBoardDetails.getProfile().getPunch_line(),
                    mDashBoardDetails.getProfile().getName(),
                    0,
                    null,
                    mDashBoardDetails.getProfile().getExpert_intro_video(),
                    mDashBoardDetails.getProfile().getId(),
                    Constants.TYPE_MENTOR,
                    mDashBoardDetails.getProfile().getExpert_intro_video_banner(),
                    mBannerImagePath,
                    null,
                    mDashBoardDetails.getProfile().getName(),
                    mSavedGroupImage,
                    Constants.TYPE_MENTOR

            );


            mRequset.setGroupId(Integer.valueOf(mDashBoardDetails.getProfile().getId()));

            mRequset.setExtras(getExtrasForRequest(mDownLoadTask));
            Utils.addItemToFetchList(mRequset);

            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {

                URL url = new URL(mImageUrl);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection


                outputFile = new File(getApplicationContext().getFilesDir(), mDashBoardDetails.getProfile().getId()+ "Intro");//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e("Downlaod", "File Created");
                }


                mBannerImagePath = outputFile.getPath();


                FileOutputStream fos = getApplicationContext().openFileOutput(mDashBoardDetails.getProfile().getId()+ "Intro", Context.MODE_PRIVATE);//Get OutputStream for NewFile Location


                InputStream is = c.getInputStream();//Get InputStream for connection
                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {

                    fos.write(buffer, 0, len1);//Write new file

                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {


                e.printStackTrace();
                outputFile = null;
                Log.e("Downlaod", "Download Error Exception " + e.getMessage());


            }

            return null;
        }
    }

    private Extras getExtrasForRequest(DownloadFile request) {
        final MutableExtras extras = new MutableExtras();
        if (!TextUtils.isEmpty(request.getTitle())) {
            extras.putString(Constants.TITLE, request.getTitle());

        }
        if (!TextUtils.isEmpty(request.getId())) {
            extras.putString(Constants.ID, request.getId());

        }

        if (!TextUtils.isEmpty(request.getmBannerAddress())) {
            extras.putString(Constants.BANNER_PATH, request.getmBannerAddress());

        }

        if (!TextUtils.isEmpty(request.getDescription())) {
            extras.putString(Constants.PUNCH_LINE, request.getDescription());

        }

        if (!TextUtils.isEmpty(request.getMentorName())) {
            extras.putString(Constants.MENTOR_NAME, request.getMentorName());

        }

        if (!TextUtils.isEmpty(request.getVideoUrl())) {
            extras.putString(Constants.VIDEO_URL, request.getVideoUrl());

        }

        if (!TextUtils.isEmpty(request.getBanner())) {
            extras.putString(Constants.IMAGE_URL, request.getBanner());

        }

        if (!TextUtils.isEmpty(request.getVideo_address())) {
            extras.putString(Constants.VIDEO_PATH, request.getVideo_address());

        }

        if (!TextUtils.isEmpty(request.getmGroupBannerAddress())) {
            extras.putString(Constants.GROUP_IMAGE_PATH, request.getmGroupBannerAddress());

        }

        if (!TextUtils.isEmpty(request.getmGroupTitle())) {
            extras.putString(Constants.GROUP_TITLE, request.getmGroupTitle());

        }

        if (!TextUtils.isEmpty(request.getmGroupType())) {
            extras.putString(Constants.GROUP_TYPE, request.getmGroupType());

        }

        extras.putString(Constants.TYPE, request.getType());

        extras.putString(Constants.DATA, new Gson().toJson(mDashBoardDetails));

        if (!TextUtils.isEmpty(request.getmVideoDuration())) {
            extras.putString(Constants.VIDEO_DURATION, request.getmVideoDuration());
        }
        return extras;
    }



    private final FetchListener fetchListener = new AbstractFetchListener() {


        @Override
        public void onCompleted(@NotNull Download download) {

            if (download.getExtras().getString(Constants.ID, "0").equals(mID)) {

                mDashBoardDetails.getProfile().setSavedVideoAddress(download.getFile());
                Toast.makeText(getApplicationContext(), R.string.download_complete, Toast.LENGTH_SHORT).show();
                Utils.callEventLogApi("downloading completed  <b>" + mDashBoardDetails.getProfile().getName() + " </b>" + "from mentor detail");

                mProgrssBarDownload.setVisibility(GONE);
                mDownloadOrange.setVisibility(GONE);
                mProgrssDownload.setVisibility(GONE);
                mDownloadGrey.setVisibility(VISIBLE);


            }

        }


        @Override
        public void onError(@NotNull Download download, @NotNull Error error, @org.jetbrains.annotations.Nullable Throwable throwable) {
            if (download.getExtras().getString(Constants.ID, "0").equals(mID)) {

                mDashBoardDetails.getProfile().setSavedVideoAddress(download.getFile());

                mFetch.delete(download.getId());
                Toast.makeText(getApplicationContext(), R.string.download_failed, Toast.LENGTH_SHORT).show();
                Utils.callEventLogApi("downloading failed  <b>" + mDashBoardDetails.getProfile().getName() + " </b>" + "from mentor detail");
                mDownloadOrange.setVisibility(VISIBLE);
                mProgrssBarDownload.setVisibility(GONE);
                mProgrssDownload.setVisibility(GONE);
                mDownloadGrey.setVisibility(GONE);

            }

        }


        @Override
        public void onProgress(@NotNull Download download, long etaInMilliSeconds, long downloadedBytesPerSecond) {

            if (download.getExtras().getString(Constants.ID, "0").equals(mID)) {
                mDashBoardDetails.getProfile().setSavedVideoAddress(download.getFile());

                updateProgress(download);
            }

        }

        @Override
        public void onQueued(@NotNull Download download, boolean waitingOnNetwork) {

        }


        @Override
        public void onRemoved(@NotNull Download download) {

            if (download.getExtras().getString(Constants.ID, "0").equals(mID)) {
                mDashBoardDetails.getProfile().setSavedVideoAddress(null);

                mProgrssBarDownload.setVisibility(GONE);
                mDownloadOrange.setVisibility(VISIBLE);
                mDownloadGrey.setVisibility(GONE);
                mProgrssDownload.setVisibility(GONE);

            }
        }

        @Override
        public void onDeleted(@NotNull Download download) {

            if (download.getExtras().getString(Constants.ID, "0").equals(mID)) {
                mDashBoardDetails.getProfile().setSavedVideoAddress(null);

                mProgrssBarDownload.setVisibility(GONE);
                mDownloadOrange.setVisibility(VISIBLE);
                mDownloadGrey.setVisibility(GONE);
                mProgrssDownload.setVisibility(GONE);


            }
        }

    };

    private void updateProgress(Download progress) {

        mProgrssDownload.setVisibility(GONE);
        mProgrssBarDownload.setVisibility(VISIBLE);
        mDownloadOrange.setVisibility(GONE);
        mDownloadGrey.setVisibility(GONE);
        mProgrssBarDownload.setProgress(progress.getProgress());

    }

    private class startGroupImageDownload extends AsyncTask<Void, Void, Void> {

        private File outputFile = null;

        @Override
        protected Void doInBackground(Void... voids) {
            try {

                URL url = new URL(mGroupImageUrl);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection


                outputFile = new File(getApplicationContext().getFilesDir(), mID);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e("Downlaod", "File Created");
                }


                mSavedGroupImage = outputFile.getPath();


                FileOutputStream fos = getApplicationContext().openFileOutput(mID, Context.MODE_PRIVATE);//Get OutputStream for NewFile Location


                InputStream is = c.getInputStream();//Get InputStream for connection
                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {

                    fos.write(buffer, 0, len1);//Write new file

                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {


                e.printStackTrace();
                outputFile = null;
                Log.e("Downlaod", "Download Error Exception " + e.getMessage());


            }
            return null;
        }

    }




}
