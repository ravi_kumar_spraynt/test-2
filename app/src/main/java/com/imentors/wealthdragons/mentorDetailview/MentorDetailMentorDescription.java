package com.imentors.wealthdragons.mentorDetailview;


import android.content.Context;
import android.content.Intent;

import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;


import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.ChatActivity;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.interfaces.DigitalCoachingClickListener;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.WealthDragonTextView;


public class MentorDetailMentorDescription extends LinearLayout {


    private Context mContext;
    private FrameLayout mBtFullDetails , mBtShare ;
    private WealthDragonTextView mTvMentorShortDescription, mTvMentorFullDescription , mTvFullDetailBtn ,mTvRatingCount, mMentorProfileHeading, mMentorProfileHeadingOrange;
    private boolean isFullVisible = false;
    private ImageView mMentorImage;
    private RatingBar mRatingBar;
    private LinearLayout mLlReviewContainer;
    private String mItemId;
    private DigitalCoachingClickListener mDigitalCoachingListener;
    private MentorDetailMentorDescription.OnLiveClickListener mOnLiveClickListener = null;


    private DashBoardMentorDetails mDashboardDetails;
    private FrameLayout mBtMentorLive;


    public MentorDetailMentorDescription(Context context) {
        super(context);
        mContext = context;
    }

    public MentorDetailMentorDescription(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public MentorDetailMentorDescription(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public MentorDetailMentorDescription(Context context, DashBoardMentorDetails dashBoardDetails,MentorDetailMentorDescription.OnLiveClickListener onLiveClickListener) {
        super(context);
        mOnLiveClickListener = onLiveClickListener;
        mDashboardDetails=dashBoardDetails;
        initViews(context);
        bindView(mDashboardDetails);

    }

    /**
     * different click handled by onItemClick
     */
    public interface OnItemClickListener {
        void onFeedsButtonClick(String mentorId);

    }


    /**
     * different click handled by onItemClick
     */
    public interface OnLiveClickListener {
        void onLiveButtonClick();

    }

    //  set data in view
    private void bindView(final DashBoardMentorDetails dashBoardDetails) {


        final DashBoardMentorDetails.Profile mentorDetail = dashBoardDetails.getProfile();

        mItemId=mentorDetail.getId();


        mMentorProfileHeading.setText(mentorDetail.getAboutMentorBlack());
        mMentorProfileHeadingOrange.setText(mentorDetail.getAboutMentorOrange());



        if(!TextUtils.isEmpty(mentorDetail.getLive_session_btn()) && mentorDetail.getLive_session_btn().equals("Yes")){
            mBtMentorLive.setVisibility(VISIBLE);
        }else{
            mBtMentorLive.setVisibility(GONE);

        }


        mBtMentorLive.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("visited <b>Live Chat Screen</b>");
                mOnLiveClickListener.onLiveButtonClick();
                Intent intent = new Intent();
                intent.putExtra(Keys.CHAT_USER_ID, dashBoardDetails.getProfile().getLive_session_row_id());
                intent.putExtra(Keys.LIVE_DIGI_COACHING_ID,dashBoardDetails.getProfile().getLive_digi_coaching_id());
                intent.putExtra(Keys.RELOAD_PAGE, true);

                intent.setClass(mContext, ChatActivity.class);
                ((MainActivity)mContext).startActivityForResult(intent,1);
//                mContext.startActivity(intent);
            }
        });









        if(!TextUtils.isEmpty(mentorDetail.getBanner())) {
            Glide.with(mContext).load(mentorDetail.getBanner()).error(R.drawable.no_img_mentor).into(mMentorImage);
        }

        if(dashBoardDetails.isShowRatings()) {
            mLlReviewContainer.setVisibility(VISIBLE);

            mRatingBar.setRating(Float.parseFloat(mentorDetail.getRating()));

            mTvRatingCount.setText("(" + mentorDetail.getRating_persons() + ")");
        }else{
            mLlReviewContainer.setVisibility(GONE);
        }

        if(!TextUtils.isEmpty(mentorDetail.getLong_details())){
            mBtFullDetails.setVisibility(View.GONE);
        }else{
            mBtFullDetails.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(mentorDetail.getShort_details())){
            mTvMentorShortDescription.setVisibility(View.VISIBLE);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                mTvMentorShortDescription.setText(Html.fromHtml(mentorDetail.getShort_details(),Html.FROM_HTML_MODE_LEGACY));
            } else {
                mTvMentorShortDescription.setText(Html.fromHtml(mentorDetail.getShort_details()));
            }
        }else{
            mTvMentorShortDescription.setVisibility(View.GONE);
            mBtFullDetails.setVisibility(View.GONE);
        }

        mTvMentorShortDescription.setMovementMethod(LinkMovementMethod.getInstance());



        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            mTvMentorFullDescription.requestFocus();
            mTvMentorFullDescription.setText(Html.fromHtml(mentorDetail.getLong_details(),Html.FROM_HTML_MODE_LEGACY));
        } else {
            mTvMentorFullDescription.setText(Html.fromHtml(mentorDetail.getLong_details()));
        }

        mBtFullDetails.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isFullVisible){
                    Utils.callEventLogApi("clicked <b>Full detail button</b> from <b>"+dashBoardDetails.getProfile().getName()+" </b>" + Constants.TYPE_MENTOR + " type");

                    mTvMentorFullDescription.setVisibility(View.VISIBLE);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        mTvMentorFullDescription.requestFocus();
                        mTvMentorFullDescription.setText(Html.fromHtml(mentorDetail.getLong_details(),Html.FROM_HTML_MODE_LEGACY));
                    } else {
                        mTvMentorFullDescription.setText(Html.fromHtml(mentorDetail.getLong_details()));
                    }
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams( Utils.dpToPx(94), Utils.dpToPx(30));
                    params.setMargins(Utils.dpToPx(4),Utils.dpToPx(10),0,0);
                    mBtFullDetails.setLayoutParams(params);
                    mTvMentorFullDescription.setMovementMethod(LinkMovementMethod.getInstance());

                    mTvFullDetailBtn.setText(mContext.getString(R.string.show_less));
                }else{
                    Utils.callEventLogApi("clicked <b>Show Less button</b> from <b>"+dashBoardDetails.getProfile().getName()+" </b>" + Constants.TYPE_MENTOR + " type ");

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams( Utils.dpToPx(94), Utils.dpToPx(30));
                    params.setMargins(Utils.dpToPx(126),Utils.dpToPx(10),0,0);
                    mBtFullDetails.setLayoutParams(params);

                    mTvMentorFullDescription.setVisibility(View.GONE);
                    mTvFullDetailBtn.setText(mContext.getString(R.string.full_details));
                }

                isFullVisible = !isFullVisible;

            }
        });

        mTvMentorFullDescription.setMovementMethod(LinkMovementMethod.getInstance());






    }

    // initialize mentor detail header view
    private void initViews(Context context ) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.mentor_detail_description, this);

        mContext = context;

        mMentorProfileHeading = view.findViewById(R.id.mentor_profile_heading);
        mMentorProfileHeadingOrange = view.findViewById(R.id.mentor_profile_heading_orange);
        mBtFullDetails = view.findViewById(R.id.fl_btn_full_details);
        mBtMentorLive = view.findViewById(R.id.bt_live);

        mMentorImage = view.findViewById(R.id.iv_mentor_image);

        mTvMentorFullDescription = view.findViewById(R.id.tv_mentor_full_description);
        mTvMentorShortDescription = view.findViewById(R.id.tv_mentor_small_description);
        mTvRatingCount = view.findViewById(R.id.tv_set_rating_count);
        mLlReviewContainer = view.findViewById(R.id.ll_review_container);

        mTvFullDetailBtn = view.findViewById(R.id.tv_full_detail_btn);
        mRatingBar = findViewById(R.id.ratingBar);




    }

    private void openDialog(DialogFragment dialogFragment) {
        dialogFragment.setCancelable(false);
        dialogFragment.show(((MainActivity)mContext).getSupportFragmentManager(), "dialog");

    }






}
