package com.imentors.wealthdragons.mentorDetailview;


import android.content.Context;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import android.widget.LinearLayout;


import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.views.WealthDragonTextView;


public class MentorDigitalDescription extends LinearLayout {


    private Context mContext;
    private WealthDragonTextView mTvCourseShortDescription , mTvCourseDescriptionTitle;

    private DashBoardMentorDetails mDashBoardDetails;





    public MentorDigitalDescription(Context context) {
        super(context);
        mContext = context;
    }

    public MentorDigitalDescription(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public MentorDigitalDescription(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public MentorDigitalDescription(Context context, DashBoardMentorDetails dashBoardDetails) {
        super(context);

        initViews(context);
        bindView(dashBoardDetails);
    }


    //  set data in view
    private void bindView(final DashBoardMentorDetails dashBoardDetails) {

        mDashBoardDetails = dashBoardDetails;

        if(!TextUtils.isEmpty(mDashBoardDetails.getProfile().getDigital_coaching_heading())){
            mTvCourseDescriptionTitle.setText(dashBoardDetails.getProfile().getDigital_coaching_heading());

        }else{
            mTvCourseDescriptionTitle.setVisibility(GONE);

        }


        if(!TextUtils.isEmpty(dashBoardDetails.getProfile().getDigital_coaching_description())) {
            mTvCourseShortDescription.setText(dashBoardDetails.getProfile().getDigital_coaching_description());

        }else{
            mTvCourseShortDescription.setVisibility(GONE);
        }



    }

    //  initialize course detail description view
    private void initViews(Context context ) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.mentor_digital_description, this);
        mContext = context;

        mTvCourseShortDescription = view.findViewById(R.id.tv_digital_description);

        mTvCourseDescriptionTitle = view.findViewById(R.id.tv_digital_title);


    }







}
